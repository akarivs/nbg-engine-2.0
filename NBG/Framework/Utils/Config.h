#ifndef FRAMEWORK_UTILS_CONFIG
#define FRAMEWORK_UTILS_CONFIG

#include <stdarg.h>
#include <../xml/pugixml.hpp>

#include <map>

using namespace pugi;

namespace NBG
{

    /** @brief Класс, реализующий работу с xml файлом конфигурации
    *
    * @description Можно загружать как из xml файла, так и с отдельной ноды.
    * Пример XML документа
    * <elements>
	*   <element id="start_scene" type="string" value="scene_splash" />
	* </elements>
    * @author Vadim Simonov <akari.vs@gmail.com>
    * @copyright 2013 New Bridge Games
    *
    */
    class CConfig
    {
    public:
        /// @name Конструкторы
        CConfig();
        ~CConfig();

        /// @name Загрузка конфига
        /// Загрузка с XML файла
        bool LoadConfig(const std::string path);
        /// Загрузка с XML ноды
        bool LoadConfig(pugi::xml_node node_elements);

        /// Загрузка int значения
        void GetValue(const std::string val, int &ret);
        /// Загрузка float значения
        void GetValue(const std::string val, float &ret);
        /// Загрузка bool значения
        void GetValue(const std::string val, bool &ret);
        /// Загрузка string значения
        void GetValue(const std::string val, std::string &ret);

		bool HasIntValue(const std::string val){return m_IntValues.find(val) != m_IntValues.end();};
		bool HasFloatValue(const std::string val){return m_FloatValues.find(val) != m_FloatValues.end();};
		bool HasBoolValue(const std::string val){return m_BoolValues.find(val) != m_BoolValues.end();};
		bool HasStringValue(const std::string val){return m_StringValues.find(val) != m_StringValues.end();};
    private:
        ///Разные типы карт, для более быстрого поиска
        std::map<std::string, int>m_IntValues;
        std::map<std::string, float>m_FloatValues;
        std::map<std::string, bool>m_BoolValues;
        std::map<std::string, std::string>m_StringValues;

        std::string m_Path;///Путь к файлу
    };
}
#endif ///FRAMEWORK_UTILS_CONFIG
