#include "Config.h"

#include <Framework.h>
#include <cstdlib>

namespace NBG
{
    CConfig::CConfig()
    {

    }
    CConfig::~CConfig()
    {
        m_IntValues.clear();
        m_FloatValues.clear();
        m_BoolValues.clear();
        m_StringValues.clear();
    }

    bool CConfig::LoadConfig(std::string path)
    {
        m_Path = path;
        NBG::CXMLResource * res = (NBG::CXMLResource*)g_ResManager->GetResource(path);
        xml_document* doc_level = res->GetXML();

        pugi::xml_node node_elements = doc_level->first_child();
        LoadConfig(node_elements);
		g_ResManager->ReleaseResource(res);
		return true;
    }

    bool CConfig::LoadConfig(pugi::xml_node node_elements)
    {
		m_IntValues.clear();
		m_FloatValues.clear();
		m_BoolValues.clear();
		m_StringValues.clear();
        for (pugi::xml_node element = node_elements.child("element"); element; element= element.next_sibling("element"))
		{
            std::string id      = element.attribute("id").value();
		    std::string type    = element.attribute("type").value();
		    if (type == "int")
            {
                m_IntValues[id] = atoi(element.attribute("value").value());
            }
            else if (type == "float")
            {
                m_FloatValues[id] = atof(element.attribute("value").value());
            }
            else if (type == "bool")
            {
                std::string val = element.attribute("value").value();
                m_BoolValues[id] = val == "true";
            }
            else if (type == "string")
            {
                m_StringValues[id] = element.attribute("value").value();
            }
		}
		return true;
    }

    void CConfig::GetValue(std::string val, int& ret)
    {
		if (HasIntValue(val))
			ret = m_IntValues[val];
    }


    void CConfig::GetValue(std::string val, float& ret)
    {
		if (HasFloatValue(val))
			ret = m_FloatValues[val];
    }

    void CConfig::GetValue(std::string val, bool& ret)
    {
		if (HasBoolValue(val))
			ret = m_BoolValues[val];
    }

    void CConfig::GetValue(std::string val, std::string& ret)
    {
		if (HasStringValue(val))
			ret = m_StringValues[val];
    }
}

