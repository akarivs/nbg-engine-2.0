#include "Tweener.h"

#include <framework.h>


namespace NBG
{    
    CTweener::CTweener()
    {
		m_TimeScale = 1.0f;
    }

    CTweener::~CTweener()
    {

    }

    int CTweener::MoveTo(NBG::CBaseObject * object, float time, float x, float y, float delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {



        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
        params.startX   = object->GetPosition().x;
        params.startY   = object->GetPosition().y;
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
            params.deltaX   = x;
            params.deltaY   = y;
        }
        else
        {
            params.deltaX   = x-object->GetPosition().x;
            params.deltaY   = y-object->GetPosition().y;
        }
        params.tweenX   = true;
        params.tweenY   = true;
        m_Tweens.push_back(params);
        return params.id;
    }

	int CTweener::AlphaTo(NBG::CBaseObject * object, const float time, const float alpha, const float delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {
        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
        params.startAlpha = object->GetColor().a;
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
            params.deltaAlpha = alpha;
        }
        else
        {
            params.deltaAlpha = alpha-params.startAlpha;
        }				
        params.tweenAlpha = true;	
        m_Tweens.push_back(params);
        return params.id;
    }

    int CTweener::RotateTo(NBG::CBaseObject * object, const float &time, const float &angle, const float &delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {
        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
        params.startAngle = object->GetRotation();
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
            params.deltaAngle = angle;
        }
        else
        {
            params.deltaAngle = angle-params.startAngle;
        }
        params.tweenAngle = true;
        m_Tweens.push_back(params);
        return params.id;
    }

	int CTweener::RotateBy(NBG::CBaseObject * object, const float &time, const float &angle, const float &delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
	{ 
		STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
        params.startAngle = object->GetRotation();
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
            params.deltaAngle = angle;
        }
        else
        {
            params.deltaAngle = angle;
        }
		params.tweenAngleBy = true;
        params.tweenAngle = true;
        m_Tweens.push_back(params);
        return params.id;
	}

	int CTweener::ScaleTo(NBG::CBaseObject * object, const float &time, const float &x, const float &y, const float &delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {
        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
		params.startScaleX = object->GetScale().x;
        params.startScaleY = object->GetScale().y;
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
			params.deltaScaleX = x;
            params.deltaScaleY = y;
        }
        else
        {
            params.deltaScaleX   = x-object->GetScale().x;
            params.deltaScaleY   = y-object->GetScale().y;
        }
		params.tweenScaleX   = true;
        params.tweenScaleY   = true;
        m_Tweens.push_back(params);
        return params.id;
    }

	int CTweener::MoveHotSpotTo(NBG::CBaseObject * object, const float &time, const float &x, const float &y, const float &delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {
        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_AK_BASE_OBJECT;
        params.object   = object;
		params.startHotSpotX = object->GetHotSpotOffset(object->GetHotSpot()).x;
        params.startHotSpotY = object->GetHotSpotOffset(object->GetHotSpot()).y;
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
			params.deltaHotSpotX = x;
            params.deltaHotSpotY = y;
        }
        else
        {
            params.deltaHotSpotX   = x-object->GetHotSpotOffset(object->GetHotSpot()).x;
            params.deltaHotSpotY   = y-object->GetHotSpotOffset(object->GetHotSpot()).y;
        }
		params.tweenHotSpotX = true;
        params.tweenHotSpotY = true;
        m_Tweens.push_back(params);
        return params.id;
    }

	int CTweener::FloatValueTo(float * value, const float time, const float newValue, const float delay, TweenEventFunc finishEvent, STweenEasing ease, void * userData)
    {
        STweenParams params;
        params.id       = GetUniqueId();
        params.type     = TO_Float;
        params.object   = value;
		params.startValue = *value;        
        params.time     = 0.0f;
        params.allTime  = time;
        params.delay    = delay;
        params.ease     = GetTween(ease);
        params.finishEvent = finishEvent;
        params.userData = userData;

        if (delay > 0.0f)
        {
			params.deltaValue = newValue;            
        }
        else
        {
            params.deltaValue   = newValue-*value;            
        }
		params.tweenValue = true;        
        m_Tweens.push_back(params);
        return params.id;
    }

    void CTweener::StopTween(int &id)
    {
        m_TweensIter = m_Tweens.begin();
        while (m_TweensIter != m_Tweens.end())
        {
           if ((*m_TweensIter).id == id)
           {
                m_Tweens.erase(m_TweensIter);
                break;
           }
           ++m_TweensIter;
        }
        id = -1;
    }


    void CTweener::StopAll()
    {
        m_Tweens.clear();        
    }

    int CTweener::GetUniqueId()
    {		
		return g_Random->RandI(0,6000000);
    }

    void CTweener::SetValue(STweenParams * tween, float val, STweenParamsTypes type)
    {
        Color color;
        switch(tween->type)
        {
		case TO_Float:
			switch(type)
            {
				case TPT_VALUE:
					(*(float *)tween->object) = val;					
					break;
			}
			break;
        case TO_AK_BASE_OBJECT:
            NBG::CBaseObject * object;
            object = (NBG::CBaseObject*)tween->object;
            switch (type)
            {
            case TPT_X:
                object->SetPosition(val,object->GetPosition().y);
                break;
            case TPT_Y:
                object->SetPosition(object->GetPosition().x, val);
                break;
			case TPT_SCALE_X:
				object->SetScale(val,object->GetScale().y);
                break;
			case TPT_SCALE_Y:
				object->SetScale(object->GetScale().x,val);
                break;
			case TPT_HOT_SPOT_X:
				object->SetHotSpot(HS_CUSTOM,Vector(val,object->GetHotSpotOffset(object->GetHotSpot()).y));
                break;
			case TPT_HOT_SPOT_Y:
				object->SetHotSpot(HS_CUSTOM,Vector(object->GetHotSpotOffset(object->GetHotSpot()).x,val));
                break;
            case TPT_ANGLE:
                object->SetRotation(val);
                break;
			case TPT_ALPHA:
				color = object->GetColor();
				color.a = val;
				object->SetColor(color);
				break;
            }
            break;
        }

    }

    float CTweener::GetValue(STweenParams * tween, STweenParamsTypes type)
    {
        switch(tween->type)
        {
		case TO_Float:
			float * value;
			value = (float*)tween->object;
			switch(type)
            {
				case TPT_VALUE:
					return *value;
					break;
			}			
			break;
        case TO_AK_BASE_OBJECT:
            NBG::CBaseObject * object;
            object = (NBG::CBaseObject*)tween->object;
            switch(type)
            {
            case TPT_X:
                return object->GetPosition().x;
                break;
            case TPT_Y:
                return object->GetPosition().y;
                break;
            case TPT_SCALE_X:
                return object->GetScale().x;
                break;
            case TPT_SCALE_Y:
                return object->GetScale().y;
                break;
            case TPT_HOT_SPOT_X:
				return object->GetHotSpotOffset(object->GetHotSpot()).x;
                break;
            case TPT_HOT_SPOT_Y:
                return object->GetHotSpotOffset(object->GetHotSpot()).y;
                break;
            case TPT_ANGLE:
                return object->GetRotation();
                break;
			case TPT_ALPHA:
				return object->GetColor().a;
				break;
            }
            break;
        }
        return 0.0f;
    }

    void CTweener::InitDelayedParams(STweenParams * tween)
    {
        if (tween->tweenX)
        {
            tween->startX = GetValue(tween,TPT_X);
            tween->deltaX = tween->deltaX - tween->startX;
        }
        if (tween->tweenY)
        {
            tween->startY = GetValue(tween,TPT_Y);
            tween->deltaY = tween->deltaY - tween->startY;
        }
        if (tween->tweenScaleX)
        {
			tween->startScaleX = GetValue(tween,TPT_SCALE_X);
            tween->deltaScaleX = tween->deltaScaleX - tween->startScaleX;
        }
        if (tween->tweenScaleY)
        {
            tween->startScaleY = GetValue(tween,TPT_SCALE_Y);
            tween->deltaScaleY = tween->deltaScaleY - tween->startScaleY;
        }
		if (tween->tweenHotSpotX)
        {
			tween->startHotSpotX = GetValue(tween,TPT_HOT_SPOT_X);
            tween->deltaHotSpotX = tween->deltaHotSpotX - tween->startHotSpotX;
        }
        if (tween->tweenHotSpotY)
        {
			tween->startHotSpotY = GetValue(tween,TPT_HOT_SPOT_Y);
            tween->deltaHotSpotY = tween->deltaHotSpotY - tween->startHotSpotY;
        }
        if (tween->tweenAngle)
        {			
            tween->startAngle = GetValue(tween,TPT_ANGLE);
			if (!tween->tweenAngleBy)
			{
				tween->deltaAngle = tween->deltaAngle - tween->startAngle;
			}
        }
        if (tween->tweenAlpha)
        {
            tween->startAlpha = GetValue(tween,TPT_ALPHA);
            tween->deltaAlpha = tween->deltaAlpha - tween->startAlpha;
        }
        if (tween->tweenValue)
        {
            tween->startValue = GetValue(tween,TPT_VALUE);
            tween->deltaValue = tween->deltaValue - tween->startValue;
        }
    }

    void CTweener::Update()
    {
       	float percent = 0.0f;
        float t,b,c,d = 0.0f;

        m_CallUserFunctions.clear();

        m_TweensIter = m_Tweens.begin();
        int id=0;
        while (m_TweensIter != m_Tweens.end())
        {
            STweenParams * tween = &(*m_TweensIter);
            if (tween->delay > 0.0f)
            {
                tween->delay -= g_FrameTime * m_TimeScale;
                if (tween->delay <= 0.0f)
                {
                    InitDelayedParams(tween);
                }
                else
                {
                    m_TweensIter++;
                    continue;
                }
            }

            percent = tween->time / tween->allTime;
            if ( percent >= 1.0 )
            {
                percent=1.0;
            }

            tween->time += g_FrameTime * m_TimeScale;

            t = tween->time;
            d = tween->allTime;
            if (t>d)t=d;


            if (tween->tweenX)
            {
                c = tween->deltaX;
                b = tween->startX;
                SetValue(tween, tween->ease(t,b,c,d), TPT_X);
            }

            if (tween->tweenY)
            {
                c = tween->deltaY;
                b = tween->startY;
                SetValue(tween, tween->ease(t,b,c,d), TPT_Y);
            }

            if (tween->tweenScaleX)
            {
                c = tween->deltaScaleX;
                b = tween->startScaleX;
                SetValue(tween, tween->ease(t,b,c,d), TPT_SCALE_X);
            }

            if (tween->tweenScaleY)
            {
                c = tween->deltaScaleY;
                b = tween->startScaleY;
                SetValue(tween, tween->ease(t,b,c,d), TPT_SCALE_Y);
            }

			if (tween->tweenHotSpotX)
            {
				c = tween->deltaHotSpotX;
				b = tween->startHotSpotX;
                SetValue(tween, tween->ease(t,b,c,d), TPT_HOT_SPOT_X);
            }

            if (tween->tweenHotSpotY)
            {
                c = tween->deltaHotSpotY;
                b = tween->startHotSpotY;
                SetValue(tween, tween->ease(t,b,c,d), TPT_HOT_SPOT_Y);
            }

            if (tween->tweenAngle)
            {
                c = tween->deltaAngle;
                b = tween->startAngle;
                SetValue(tween, tween->ease(t,b,c,d), TPT_ANGLE);
            }

            if (tween->tweenAlpha)
            {
                c = tween->deltaAlpha;
                b = tween->startAlpha;
                SetValue(tween, tween->ease(t,b,c,d), TPT_ALPHA);
            }

            if (tween->tweenValue)
            {
                c = tween->deltaValue;
                b = tween->startValue;
                SetValue(tween, tween->ease(t,b,c,d), TPT_VALUE);
            }

            if (percent == 1.0f)
            {
                if (tween->finishEvent)
                {
                    m_CallUserFunctions.push_back(*tween);
                }
                m_TweensIter = m_Tweens.erase(m_TweensIter);
                continue;
            }
            ++m_TweensIter;
        }

        for (size_t i=0; i<m_CallUserFunctions.size(); i++)
        {
            STweenParams * tween = &m_CallUserFunctions[i];
            tween->finishEvent(tween->userData);
        }
        m_CallUserFunctions.clear();
    }
}
