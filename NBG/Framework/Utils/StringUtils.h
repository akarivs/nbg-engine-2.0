#ifndef NBG_UTILS_STRING
#define NBG_UTILS_STRING

#include <string>
#include <sstream>
#include <vector>

namespace NBG
{
	/** @brief Класс-помощник со статичными строковыми функциями
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class StringUtils
	{
	public:
		/// @name Конструкторы
		StringUtils();
		~StringUtils();

		///Замена подстроки в строке
		template <class Type>
		inline static Type StringReplace(Type str, Type what, Type to, bool replaceAll = false)
		{
			for(size_t index=0; index=str.find(what, index), index!=std::string::npos;)
			{
				str.replace(index, what.length(), to);
				if (!replaceAll)
				{
					return str;
				}
				index+=to.length();
			}
			return str;
		}

		///Конвертация элемента в string
		template <class Type>
		inline static std::string ToString(Type element)
		{
			std::string str;
			std::stringstream out;
			out << element;
			str = out.str();
			return str;
		}

		///Конвертация элемента в wstring
		template <class Type>
		inline static std::wstring ToWString(Type element)
		{
			std::wstring str;
			std::wstringstream out;
			out << element;
			str = out.str();
			return str;
		}


		///Конвертация элемента в int
		template <class Type>
		inline static int ToInt(Type element)
		{
			return std::atoi( element.c_str() );
		}

		static std::string PathToId(const std::string path);
		///разбивает строку на массив по разделителю
		static std::vector<std::string> ExplodeString(const std::string& str, const char& ch);
		///конвертирует строку в HotSpot
		static int StringToHotSpot(const std::string &hotSpot);
		///Убирает extension из названия файла
		static std::string RemoveExtension(const std::string str);
		///Получает extension из названия файла
		static std::string GetExtension(const std::string str);
	private:


	};

}




#endif // NBG_UTILS_MATH

