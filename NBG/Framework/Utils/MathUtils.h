#ifndef NBG_UTILS_MATH
#define NBG_UTILS_MATH

#include <Framework/Datatypes/Vector.h>
#include <vector>

namespace NBG
{
	/** @brief Класс-помощник со статичными математическими функциями
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class MathUtils
	{
	public:
		/// @name Конструкторы
		MathUtils();
		~MathUtils();

		static void Init();
		static void RelativeRotate(float x1, float y1, float &x2, float &y2, float angle);
		static float AngleBetween(Vector start, Vector end);
		static float ToRadian(float degree);
		static float ToDegree(float radian);

		static float FastSin(float radian);
		static float FastCos(float radian);

		static bool IsLineIntersects(Vector a, Vector b, Vector c, Vector d, Vector * returnVec = NULL);
		static bool IsPointInsidePoly(const Vector &point, const std::vector<Vector> &poly);
		static Vector GetQuadBezierPoint(const Vector &start, const Vector &middle, const Vector &end, const float t);


		static float PI;
	private:


	};

}




#endif // NBG_UTILS_MATH

