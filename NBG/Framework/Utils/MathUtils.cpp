#include "MathUtils.h"
#include <math.h>

#include <Framework.h>

namespace NBG
{
	float MathUtils::PI = 3.14159265358979323846f;
	float __g_CosF = 0.0f;
	float __g_SinF = 0.0f;

	enum
	{
		SINCOS_NUM_BITS     = 12,                       // ?????????? ??? ????????
		SINCOS_TABLE_SIZE   = 1 << SINCOS_NUM_BITS,     // ?????? ???????

		SINCOS_TABLE_SHIFT  = 16 - SINCOS_NUM_BITS,     // ?????
		SINCOS_TABLE_MASK   = SINCOS_TABLE_SIZE - 1,    // ?????

		SINCOS_PI           = 32768,                    // ??
		SINCOS_2PI          = 2 * SINCOS_PI,
		SINCOS_PI_DIV_2     = SINCOS_PI / 2,            
	};


	const float ANGLE2INT   = float (SINCOS_PI / MathUtils::PI);
	const float INT2ANGLE   = float (MathUtils::PI / SINCOS_PI);


	float sin_table [SINCOS_TABLE_SIZE];


	__forceinline int angle2int ( float angle )
	{
		return int ( angle * ANGLE2INT );
	};

	__forceinline float int2angle ( int val )
	{
		return val * INT2ANGLE;
	};

	__forceinline float sin32 ( int val )
	{
		return sin_table [ ( val >> SINCOS_TABLE_SHIFT ) & SINCOS_TABLE_MASK];
	};

	__forceinline float cos32 ( int val )
	{
		return sin32 ( val + SINCOS_PI_DIV_2 );
	};

	float MathUtils::FastSin(float radian)
	{
		return sin32(angle2int(radian));
	}
	float MathUtils::FastCos(float radian)
	{
		return cos32(angle2int(radian));
	}


	void MathUtils::Init()
	{
		for ( int i = 0; i < SINCOS_TABLE_SIZE; i++ )
		{
			const float angle = int2angle ( i << SINCOS_TABLE_SHIFT );
			sin_table [i] = sinf (angle);
		}
	}

	void MathUtils::RelativeRotate(float x1, float y1, float &x2, float &y2, float angle)
	{
		if(angle == 0) return;

		__g_CosF = cos32(angle2int(angle));
		__g_SinF = sin32(angle2int(angle));

		float x12 = x1 + ( x2 - x1) * __g_CosF - (y2 - y1) * __g_SinF;
		float y12 = y1 + (y2 - y1) * __g_CosF + (x2 - x1) * __g_SinF;

		x2 = x12;
		y2 = y12;
	}

	float MathUtils::AngleBetween(Vector start, Vector end)
	{
		Vector angle = end;
		angle -= start;
		angle.Normalize();

		Vector referenceAngle = Vector(-1.0f,0.0f);


		float degree = acosf(angle.DotProduct(referenceAngle));
		float retAngle = 0.0f;

		if (angle.y<=0.0f)
		{
			retAngle = (degree * 180.0f) / PI;
		}
		else
		{
			retAngle = ((ToRadian(360.0f) - degree) * 180.0f) / PI;
		}

		return retAngle;
	}

	float MathUtils::ToRadian(float degree)
	{
		return degree * PI / 180.0f;
	}

	float MathUtils::ToDegree(float radian)
	{
		return radian * 180.0f / PI;
	}

	bool MathUtils::IsLineIntersects(Vector a, Vector b, Vector c, Vector d, Vector * returnVec)
	{
		Vector T;
		T.x=-((a.x*b.y-b.x*a.y)*(d.x-c.x)-(c.x*d.y-d.x*c.y)*(b.x-a.x))/((a.y-b.y)*(d.x-c.x)-(c.y-d.y)*(b.x-a.x));
		T.y=((c.y-d.y)*(-T.x)-(c.x*d.y-d.x*c.y))/(d.x-c.x);
		if (returnVec)
		{
			returnVec->x = T.x;
			returnVec->y = T.y;
		}
		return false;
	}

	bool MathUtils::IsPointInsidePoly(const Vector &point, const std::vector<Vector> &poly)
	{
		static const int q_patt[2][2]= { {0,1}, {3,2} };

		if (poly.size()<3) return false;

		std::vector<Vector>::const_iterator end=poly.end();
		Vector pred_pt=poly.back();
		pred_pt.x-=point.x;
		pred_pt.y-=point.y;

		int pred_q=q_patt[pred_pt.y<0][pred_pt.x<0];

		int w=0;

		for(std::vector<Vector>::const_iterator iter=poly.begin();iter!=end;++iter)
		{
			Vector cur_pt = *iter;

			cur_pt.x-=point.x;
			cur_pt.y-=point.y;

			int q=q_patt[cur_pt.y<0][cur_pt.x<0];

			switch (q-pred_q)
			{
			case -3:++w;break;
			case 3:--w;break;
			case -2:if(pred_pt.x*cur_pt.y>=pred_pt.y*cur_pt.x) ++w;break;
			case 2:if(!(pred_pt.x*cur_pt.y>=pred_pt.y*cur_pt.x)) --w;break;
			}

			pred_pt = cur_pt;
			pred_q = q;

		}
		return w!=0;
	}

	//////////////////////////////////////////////////////////////////////////================
	Vector	MathUtils::GetQuadBezierPoint(const Vector &start, const Vector &middle, const Vector &end, const float t)
	{
		if(t == 0) return start;
		if(t == 1) return end;

		float quad = t * t, coeff = 1.0f - t;

		return Vector(
			coeff * coeff * start.x + 2 * t * coeff * middle.x + quad * end.x,
			coeff * coeff * start.y + 2 * t * coeff * middle.y + quad * end.y,
			0.0f
			);
	}

}
