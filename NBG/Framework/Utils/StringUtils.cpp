#include "StringUtils.h"

#include <Framework/Datatypes.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	std::string StringUtils::PathToId(const std::string path)
	{
		std::string temp = path;
		temp = StringUtils::StringReplace(temp,std::string("/"),std::string("_"));
		temp = StringUtils::StringReplace(temp,std::string("\\"),std::string("_"));

		int found = temp.rfind(".");
		if (found != std::string::npos)
		{
			temp = temp.substr(0,found);
		}
		return temp;
	}

	//////////////////////////////////////////////////////////////////////////
	std::vector<std::string> StringUtils::ExplodeString(const std::string& str, const char& ch)
	{
		std::string next;
		std::vector<std::string> result;

		
		for (std::string::const_iterator it = str.begin(); it != str.end(); it++)
		{			
			if (*it == ch) {
				if (!next.empty()) {					
					result.push_back(next);
					next.clear();
				}
			} else {				
				next += *it;
			}
		}
		if (!next.empty())
			result.push_back(next);
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	int StringUtils::StringToHotSpot(const std::string &hotSpot)
	{
		if (hotSpot=="" || hotSpot=="HS_MID")return HS_MID;
		if (hotSpot=="HS_UL")return HS_UL;        
		if (hotSpot=="HS_MU")return HS_MU;		
		if (hotSpot=="HS_UR")return HS_UR;
		if (hotSpot=="HS_ML")return HS_ML;
		if (hotSpot=="HS_MR")return HS_MR;
		if (hotSpot=="HS_DL")return HS_DL;
		if (hotSpot=="HS_MD")return HS_MD;
		if (hotSpot=="HS_DR")return HS_DR;
		return HS_MID;
	}

	//////////////////////////////////////////////////////////////////////////
	std::string StringUtils::RemoveExtension(const std::string str)
	{
		std::string ext = str;
		int pos = ext.rfind(".");
		if (pos != std::string::npos)
		{
			ext = ext.substr(0,pos);
		}
		return ext;
	}

	//////////////////////////////////////////////////////////////////////////
	std::string StringUtils::GetExtension(const std::string str)
	{
		std::string ext = str;
		int pos = ext.rfind(".");
		if (pos != std::string::npos)
		{
			ext = ext.substr(pos+1);
		}
		return ext;
	}
}
