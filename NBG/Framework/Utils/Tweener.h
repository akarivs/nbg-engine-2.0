#ifndef _AK_FRAMEWORK_TWEENER
#define _AK_FRAMEWORK_TWEENER		

#include <Framework/GUI/BaseObject.h>
#include <math.h>
/*
    Класс для анимации
*/



namespace NBG
{
    #define A_PI 3.14159265359
    typedef float (*EaseFunc)(float,float,float,float);
    typedef void (*TweenEventFunc)(void*);

    enum STweenObjectTypes
    {
        TO_Transform		= 0,
        TO_Vector			= 1,		
        TO_AK_BASE_OBJECT   = 2,
		TO_Float			= 3,
    };
    enum STweenParamsTypes
    {
        TPT_X = 0,
        TPT_Y = 1,
        TPT_SCALE_X = 2,
        TPT_SCALE_Y = 3,                
        TPT_ANGLE = 4,
        TPT_ALPHA = 5,
		TPT_HOT_SPOT_X = 6,
		TPT_HOT_SPOT_Y = 7,
		TPT_VALUE = 8,
    };
    enum STweenEasing
    {
        EASE_LINEAR = 0,
		EASE_IN_QUAD = 1,
        EASE_OUT_QUAD = 2,
        EASE_IN_OUT_QUAD = 3,
        EASE_OUT_IN_QUAD = 4,
        EASE_IN_CUBIC = 5,
        EASE_OUT_CUBIC = 6,
        EASE_IN_OUT_CUBIC = 7,
        EASE_OUT_IN_CUBIC = 8,
        EASE_IN_ELASTIC = 9,
        EASE_OUT_ELASTIC = 10,
        EASE_IN_OUT_ELASTIC = 11,
        EASE_OUT_IN_ELASTIC = 12,
        EASE_IN_CIRCULAR = 13,
        EASE_OUT_CIRCULAR= 14,
        EASE_IN_OUT_CIRCULAR= 15,
        EASE_OUT_IN_CIRCULAR = 16,
        EASE_IN_SINE = 17,
        EASE_OUT_SINE= 18,
        EASE_IN_OUT_SINE= 19,
        EASE_OUT_IN_SINE = 20,
    };
    struct STweenParams
    {
        STweenObjectTypes type;
        EaseFunc ease;
        TweenEventFunc finishEvent;
        void * object; //объект, который нужно анимировать
        int id;
        float time;
        float delay;
        float allTime;
        float startX;
        float deltaX;
        float startY;
        float deltaY;		
		float startHotSpotX;
        float deltaHotSpotX;
		float startHotSpotY;
        float deltaHotSpotY;
        float startAngle;
        float deltaAngle;
        float startAlpha;
        float deltaAlpha;
        float startScaleX;
        float deltaScaleX;
        float startScaleY;
        float deltaScaleY;
		float startValue;
        float deltaValue;
        /*bezier modifiers*/
        //JT_VECTOR2_LIST bezierList;

        /*Флаги, указывающие, что нужно анимировать*/
        bool tweenX;
        bool tweenY;
        bool tweenAngle;
        bool tweenAlpha;
        bool tweenBezier;
        bool tweenScaleX;
        bool tweenScaleY;
		bool tweenHotSpotX;
        bool tweenHotSpotY;
		bool tweenValue;
		bool tweenAngleBy;

        void * userData;

        STweenParams()
        {
            tweenX = false;
            tweenY = false;
            tweenAngle = false;
            tweenAlpha = false;
            tweenBezier = false;
            tweenScaleX = false;
            tweenScaleY = false;
			tweenHotSpotX = false;
            tweenHotSpotY = false;
			tweenValue = false;
			tweenAngleBy = false;

            finishEvent = NULL;
            userData = NULL;
        }


    };
    class CTweener
    {
        public:            
            CTweener();
			~CTweener();


			

            void StopTween(int &id);
			void StopAll();

            int MoveTo(NBG::CBaseObject * object, float time, float x, float y, float delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
			int AlphaTo(NBG::CBaseObject * object, const float time, const float alpha, const float delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
            int RotateTo(NBG::CBaseObject * object, const float &time, const float &angle, const float &delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
			int RotateBy(NBG::CBaseObject * object, const float &time, const float &angle, const float &delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
			int ScaleTo(NBG::CBaseObject * object, const float &time, const float &x, const float &y, const float &delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
			int MoveHotSpotTo(NBG::CBaseObject * object, const float &time, const float &x, const float &y, const float &delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
			int FloatValueTo(float * value, float time, float newValue, float delay = 0.0f, TweenEventFunc finishEvent = NULL, STweenEasing ease = EASE_LINEAR, void * userData = NULL);
            void Update();

			

			void SetTimeScale(const float timeScale)
			{
				m_TimeScale = timeScale;
			}

			static EaseFunc GetTween(STweenEasing type)
            {
                switch ((STweenEasing)type)
                {
                    case EASE_LINEAR: return &easeNone; break;
                    case EASE_IN_QUAD: return &easeInQuad; break;
                    case EASE_OUT_QUAD: return &easeOutQuad; break;
                    case EASE_IN_OUT_QUAD: return &easeInOutQuad; break;
                    case EASE_OUT_IN_QUAD: return &easeOutInQuad; break;
                    case EASE_IN_CUBIC: return &easeInCubic; break;
                    case EASE_OUT_CUBIC: return &easeOutCubic; break;
                    case EASE_IN_OUT_CUBIC: return &easeInOutCubic; break;
                    case EASE_OUT_IN_CUBIC: return &easeOutInCubic; break;
                    case EASE_IN_ELASTIC: return &easeInElastic; break;
                    case EASE_OUT_ELASTIC: return &easeOutElastic; break;
                    case EASE_IN_OUT_ELASTIC: return &easeInOutElastic; break;
                    case EASE_IN_CIRCULAR: return &easeInCircular; break;
                    case EASE_OUT_CIRCULAR: return &easeOutCircular; break;
                    case EASE_IN_OUT_CIRCULAR: return &easeInOutCircular; break;
                    case EASE_OUT_IN_CIRCULAR: return &easeOutInCircular; break;
                    case EASE_IN_SINE: return &easeInSine; break;
                    case EASE_OUT_SINE: return &easeOutSine; break;
                    case EASE_IN_OUT_SINE: return &easeInOutSine; break;
                    case EASE_OUT_IN_SINE: return &easeOutInSine; break;
                }
                return easeNone;
            }
        private:
            std::vector<STweenParams>m_Tweens;
            std::vector<STweenParams>::iterator m_TweensIter;

			float m_TimeScale;


            std::vector<STweenParams>m_CallUserFunctions;

            void SetValue(STweenParams * tween, float val, STweenParamsTypes type);
            float GetValue(STweenParams * tween, STweenParamsTypes type);
            void InitDelayedParams(STweenParams * tween);

            int GetUniqueId();

            //STATIC FUNCTIONS OF TWEENING

            static float easeNone (float t,float b , float c, float d)
            {
                return c*t/d+b;
            }
            /**
            * Easing equation function for a quadratic (t^2) easing in: accelerating from zero velocity.
            */
            static float easeInQuad (float t,float b , float c, float d)
            {
                return c*(t/=d)*t + b;
            }
            /**
            * Easing equation function for a quadratic (t^2) easing out: decelerating to zero velocity.
            */
            static float easeOutQuad (float t,float b , float c, float d)
            {
                return -c *(t/=d)*(t-2) + b;
            }
            /**
            * Easing equation function for a quadratic (t^2) easing in/out: acceleration until halfway, then deceleration.
            */
            static float easeInOutQuad (float t,float b , float c, float d)
            {
                if ((t/=d/2) < 1) return c/2*t*t + b;
                return -c/2 * ((--t)*(t-2) - 1) + b;
            }
            /**
            * Easing equation function for a quadratic (t^2) easing out/in: deceleration until halfway, then acceleration.
            */
            static float easeOutInQuad (float t,float b , float c, float d)
            {
                if (t < d/2) return easeOutQuad (t*2, b, c/2, d);
                return easeInQuad((t*2)-d, b+c/2, c/2, d);
            }
            /**
            * Easing equation function for a cubic (t^3) easing in: accelerating from zero velocity.
            */
            static float easeInCubic(float t,float b , float c, float d)
            {
                return c*(t/=d)*t*t + b;
            }
            /**
            * Easing equation function for a cubic (t^3) easing out: decelerating to zero velocity.
            */
            static float easeOutCubic(float t,float b , float c, float d)
            {
                return c*((t=t/d-1)*t*t + 1) + b;
            }
            /**
            * Easing equation function for a cubic (t^3) easing in/out: acceleration until halfway, then deceleration.
            */
            static float easeInOutCubic(float t,float b , float c, float d)
            {
                if ((t/=d/2) < 1) return c/2*t*t*t + b;
                return c/2*((t-=2)*t*t + 2) + b;
            }
            /**
            * Easing equation function for a cubic (t^3) easing out/in: deceleration until halfway, then acceleration.
            */
            static float easeOutInCubic(float t,float b , float c, float d)
            {
                if (t < d/2) return easeOutCubic (t*2, b, c/2, d);
                return easeInCubic((t*2)-d, b+c/2, c/2, d);
            }

            /* PLACE FOR QUAD */

            /* PLACE FOR QUINT */

            /* PLACE FOR CIRCLE */
            /**
            * Easing equation function for a circular (sqrt(1-t^2)) easing in: accelerating from zero velocity.
            */
            static float easeInCircular(float t,float b , float c, float d)
            {
                return -c * (sqrt(1 - (t/=d)*t) - 1) + b;
            }
            /**
            * Easing equation function for a circular (sqrt(1-t^2)) easing out: decelerating from zero velocity.
            */
            static float easeOutCircular(float t,float b , float c, float d)
            {
                return c * sqrt(1 - (t=t/d-1)*t) + b;
            }
            /**
            * Easing equation function for a circular (sqrt(1-t^2)) easing in/out: acceleration until halfway, then deceleration.
            */
            static float easeInOutCircular(float t,float b , float c, float d)
            {
                if ((t/=d/2) < 1) return -c/2 * (sqrt(1 - t*t) - 1) + b;
                return c/2 * (sqrt(1 - (t-=2)*t) + 1) + b;
            }
            /**
            * Easing equation function for a circular (sqrt(1-t^2)) easing out/in: deceleration until halfway, then acceleration.
            */
            static float easeOutInCircular(float t,float b , float c, float d)
            {
                if (t < d/2) return easeOutCircular (t*2, b, c/2, d);
                return easeInCircular((t*2)-d, b+c/2, c/2, d);
            }


            /* PLACE FOR SIN */

            /**
            * Easing equation function for a sinusoidal (sin(t)) easing in: accelerating from zero velocity.
            */
            static float easeInSine(float t,float b , float c, float d)
            {
                return -c * cos(t/d * ((float)A_PI/2)) + c + b;
            }

            /**
            * Easing equation function for a sinusoidal (sin(t)) easing out: decelerating from zero velocity.
            */
            static float easeOutSine(float t,float b , float c, float d)
            {
                return c * sin(t/d * ((float)A_PI/2)) + b;
            }

            /**
            * Easing equation function for a sinusoidal (sin(t)) easing out/in: deceleration until halfway, then acceleration.
            */
            static float easeInOutSine(float t,float b , float c, float d)
            {
                if (t < d/2) return easeOutSine (t*2, b, c/2, d);
                return easeInSine((t*2)-d, b+c/2, c/2, d);
            }

            /**
            * Easing equation function for a sinusoidal (sin(t)) easing in/out: acceleration until halfway, then deceleration.
            */
            static float easeOutInSine(float t,float b , float c, float d)
            {
                return -c/2 * (cos((float)A_PI*t/d) - 1) + b;
            }

            /* PLACE FOR EXPOT */


            /**
            * Easing equation function for an elastic (exponentially decaying sine wave) easing in: accelerating from zero velocity.
            */
            static float easeInElastic(float t,float b , float c, float d)
            {
                if (t==0) return b;
                if ((t/=d)==1) return b+c;
                float p = d*0.3f;
                float s = 0;
                float a = 0;
                if (a < fabs(c)) {
                    a = c;
                    s = p/4;
                } else {
                    s = p/(2*(float)A_PI) * asin(c/a);
                }
                return -(a*pow(2,10*(t-=1)) * sin( (t*d-s)*(2*(float)A_PI)/p )) + b;
            }

            /**
            * Easing equation function for an elastic (exponentially decaying sine wave) easing out: decelerating from zero velocity.
            */
            static float easeOutElastic(float t,float b , float c, float d)
            {

                if (t==0)return b;
                if ((t/=d)==1) return b+c;
                float p = d*0.3f;
                float s = 0.0f;
                float a = 0.0f;
                if (a < fabs(c)) { a=c; s=p/4.0f;}
                else s = p/(2.0f*(float)A_PI)*asin(c/a);
                return (a*(float)pow(2,-10.0f*t) * sin((t*d-s)*(2.0f*(float)A_PI)/p) +c + b);
            }

            /**
            * Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: acceleration until halfway, then deceleration.
            */
            static float easeInOutElastic(float t,float b , float c, float d)
            {
                if (t==0) return b;
                if ((t/=d/2)==2) return b+c;
                float p = 0.7f;
                float s = 0.8f;
                float  a = 1.5f;
                if (a < fabs(c)) {
                    a = c;
                    s = p/4;
                } else {
                    s = p/(2*(float)A_PI) * asin (c/a);
                }
                if (t < 1) return -0.5f*(a*pow(2,10*(t-=1)) * sin( (t*d-s)*(2*(float)A_PI)/p )) + b;
                return a*pow(2,-10*(t-=1)) * sin( (t*d-s)*(2.0f*(float)A_PI)/p )*0.5f + c + b;
            }


            
    };    
}

#endif
