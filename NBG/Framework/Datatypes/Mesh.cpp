#include "Mesh.h"

#include <math.h>
#include <Framework.h>
#include <Framework/Utils/StringUtils.h>
#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Vector.h>

//#include <polypartition/polypartition.h>

namespace NBG
{

Matrix matrix;
Vertex __g_Vertexes[4];
Vector __g_ResVec;
Color __g_Color;
Color __g_clrNorm;
Transform __g_Absolute;
//==============================================================================
Mesh::Mesh()
{
    m_Vertexes = NULL;
	m_Indexes = NULL;
	m_SavedVertexes = NULL;
	m_QuadIndexesPreload = false;

	rectangleColorOffset[0] = rectangleColorOffset[1] = rectangleColorOffset[2] = rectangleColorOffset[3] = Color(0,0,0,0);
}

//==============================================================================
Mesh::~Mesh()
{
    if (m_Vertexes)
    {
        delete[] m_Vertexes;
        m_Vertexes = NULL;
    }

	if (m_Indexes)
	{
		delete[] m_Indexes;
		m_Indexes = NULL;
	}

	if (m_SavedVertexes)
	{
		delete[] m_SavedVertexes;
		m_SavedVertexes = NULL;
	}
}

//==============================================================================
void Mesh::Init(const int &vertexCount, const int &indexesCount)
{
    m_VertexCount = vertexCount;
    m_Vertexes = new Vertex[vertexCount];
	m_SavedVertexes = new Vertex[vertexCount];
    for (int i=0; i<vertexCount; i++)
    {
        Vertex * v = &m_Vertexes[i];
        v->x = 0.0f;
        v->y = 0.0f;
        v->z = 0.0f;
        v->u = 0.0f;
        v->v = 0.0f;
        v->color = 0xFFFFFFFF;
    }

	m_IndexesCount = indexesCount;
	m_Indexes = new unsigned short[indexesCount];
	memset(m_Indexes,0,sizeof(unsigned short)*indexesCount);
}

//==============================================================================
void Mesh::SetColor(Color &color)
{
    unsigned long clr = color.GetPackedColor();
    for (int i=0; i<m_VertexCount; i++)
    {
        m_Vertexes[i].color = clr;
    }
}

//==============================================================================
void Mesh::SetRectData(FRect * uv, Transform * trans, Vector size, Color clr, Texture * texture, bool isTileTexture, Vector offset)
{   	
	__g_Absolute = trans->GetAbsoluteTransform();
	bool flipU = false;
	bool flipV = false;
	if (__g_Absolute.scalex < 0.0f)flipU = true;
	if (__g_Absolute.scaley < 0.0f)flipV = true;
    matrix.CreateFromTransform(__g_Absolute);

    float txLeft	= uv->left;
    float txTop		= uv->top;
    float txRight	= uv->right;
    float txBottom	= uv->bottom;

	if (!m_QuadIndexesPreload)
	{
		m_QuadIndexesPreload = true;
		m_Indexes[0]=0;	
		m_Indexes[1]=1;
		m_Indexes[2]=2;
		m_Indexes[3]=3;
		m_Indexes[4]=2;
		m_Indexes[5]=1;
	}  

    float offsetX = 0;
    float offsetY = 0;

    if (size.y < 0)
    {
        size.y = fabs(size.y);
        offsetY = -size.y;
        txTop = uv->bottom;
        txBottom = uv->top;
    }
    if (size.x < 0)
    {
        size.x = fabs(size.x);
        offsetX = -size.x;
        txLeft = uv->right;
        txRight = uv->left;
    }

	if (flipU)
	{
		//offset.x *= -1;
	}
	if (flipV)
	{
		//offset.y *= -1;
	}

	offsetX += offset.x;
	offsetY += offset.y;

    m_Vertexes[0].x = 0.0f + offsetX;
    m_Vertexes[0].y = 0.0f + offsetY;

    m_Vertexes[1].x = size.x + offsetX;
    m_Vertexes[1].y = 0.0f + offsetY;

    m_Vertexes[2].x = 0.0f + offsetX;
    m_Vertexes[2].y = size.y + offsetY;

    m_Vertexes[3].x = size.x + offsetX;
    m_Vertexes[3].y = size.y + offsetY;	

	//unsigned long color = clr.GetPackedColor();
    __g_clrNorm = clr;
#ifndef NBG_WIN32
    __g_clrNorm.Normalize();
#endif
    m_Vertexes[0].color = __g_clrNorm;
    m_Vertexes[1].color = __g_clrNorm;
    m_Vertexes[2].color = __g_clrNorm;
    m_Vertexes[3].color = __g_clrNorm;

	if (flipU || flipV)
	{	
		__g_Vertexes[0] = m_Vertexes[0];
		__g_Vertexes[1] = m_Vertexes[1];
		__g_Vertexes[2] = m_Vertexes[2];
		__g_Vertexes[3] = m_Vertexes[3];
	}	
	
    for (int i=0; i<4; i++)
    {
		int index = i;
		if (flipU && !flipV)
		{
			if (i==0)index=1;
			else if(i==1)index=0;
			else if (i==2)index=3;
			else if(i==3)index=2;
		}
		else if (flipV && !flipU)
		{
			if (i==0)index=2;
			else if(i==1)index=3;
			else if (i==2)index=0;
			else if(i==3)index=1;
		}
		else if (flipV && flipU)
		{
			if (i==0)index=3;
			else if(i==1)index=2;
			else if (i==2)index=1;
			else if(i==3)index=0;			
		}
		if (flipV || flipU)
			__g_ResVec = matrix.TransformPoint(Vector(__g_Vertexes[i].x,__g_Vertexes[i].y));				
		else
			__g_ResVec = matrix.TransformPoint(Vector(m_Vertexes[i].x,m_Vertexes[i].y));				
		__g_ResVec += rectangleOffsets[i];			
        m_Vertexes[index].x = __g_ResVec.x;        		
#ifdef NBG_ANDROID
		m_Vertexes[index].y = __g_ResVec.y;						
#else
		m_Vertexes[index].y = g_Render->GetMatrixOffset().y -__g_ResVec.y;						
#endif
    } 
	
	if (flipU)
	{
		float tmp = txLeft;
		txLeft = txRight;
		txRight = tmp;
	}
	if (flipV)
	{
		float tmp = txTop;
		txTop = txBottom;
		txBottom = tmp;
	}

    m_Vertexes[0].u = txLeft; m_Vertexes[0].v = txTop;
    m_Vertexes[1].u = txRight; m_Vertexes[1].v = txTop;
    m_Vertexes[2].u = txLeft; m_Vertexes[2].v = txBottom;
    m_Vertexes[3].u = txRight; m_Vertexes[3].v = txBottom;
}

void Mesh::UpdateVertexes(Transform * trans, Texture * texture, bool isTileTexture)
{    
	/*trans->SetHotSpot(0,0,0);
    matrix.CreateFromTransform(trans->GetAbsoluteTransform());

    Vector textureSize;
    Vector scale(1.0f,1.0f);
    if (texture)
    {
        textureSize = texture->GetSize();
    }
    else
    {
        isTileTexture = false;
    }
    if (isTileTexture)
    {
        scale.x = m_Size.x/textureSize.x;
        scale.y = m_Size.y/textureSize.y;
    }

    for (int i=0; i<m_VertexCount; i++)
    {
        Vector res = matrix.TransformPoint(Vector(m_SavedVertexes[i].x,m_SavedVertexes[i].y));
        m_Vertexes[i].x = res.x;
        m_Vertexes[i].y = NBG::g_DevicePtr->GetMiddleY()-res.y;
        Vector uv = GetUV(Vector(m_SavedVertexes[i].x,m_SavedVertexes[i].y));
        if (isTileTexture)
        {
            uv *= scale;
        }
        m_Vertexes[i].u = uv.x;
        m_Vertexes[i].v = uv.y;
    }*/
}

NBG::Vector Mesh::GetUV(const Vector &pos)
{
    Vector _pos = pos;
    _pos -= m_MinPos;
    _pos /= m_Size;
    return _pos;
}

///Метод для задания кастомных вершин с последующей их триангуляцией
void Mesh::SetCustomVertexes(std::vector<Vertex> &vertexes)
{
   /* if (m_Vertexes)
    {
        delete[] m_Vertexes;
		m_Vertexes = NULL;
    }

    if (m_Indexes)
    {
        delete[] m_Indexes;
		m_Indexes = NULL;
    }

	if (m_SavedVertexes)
	{
		delete[] m_SavedVertexes;
		m_SavedVertexes = NULL;
	}

    m_VertexCount = vertexes.size();
	if (m_VertexCount < 3)return;

    TPPLPoly * poly = new TPPLPoly();
    poly->Init(m_VertexCount);
    for (int i=0; i<m_VertexCount; i++)
    {
        TPPLPoint * point = &poly->GetPoint(i);
        point->x = vertexes[i].x;
        point->y = vertexes[i].y;		
		point->u = vertexes[i].u;
		point->v = vertexes[i].v;
    }


    std::list<TPPLPoly>triangles;
    TPPLPartition partition;

    partition.Triangulate_EC(poly,&triangles);
    ///Скорее всего в неправильном порядке вершины, отразим их
    if (triangles.size() == 0)
    {
        int index = 0;
        for (int i=m_VertexCount-1; i>=0; i--)
        {
            TPPLPoint * point = &poly->GetPoint(index);
            point->x = vertexes[i].x;
            point->y = vertexes[i].y;
            index++;
        }
        partition.Triangulate_EC(poly,&triangles);
    }

    m_VertexCount =triangles.size()*3;
    m_Vertexes  = new Vertex[m_VertexCount];
	m_Indexes = new unsigned short[m_VertexCount];
	m_IndexesCount = m_VertexCount;
    m_SavedVertexes = new Vertex[m_VertexCount];

    m_MinPos = Vector(99999,99999);
    m_MaxPos = Vector(-99999,-99999);

    ///Расчёт текстурных координат
    for( std::list<TPPLPoly>::iterator listMyClassIter = triangles.begin(); // not listMyClass.begin()
        listMyClassIter != triangles.end(); // not listMyClass.end()
        listMyClassIter ++)
    {
        TPPLPoly * triangle = &(*listMyClassIter);
        TPPLPoint * pnt1 = &triangle->GetPoint(0);
        TPPLPoint * pnt2 = &triangle->GetPoint(1);
        TPPLPoint * pnt3 = &triangle->GetPoint(2);

        if (pnt1->x > m_MaxPos.x)m_MaxPos.x=pnt1->x;
        if (pnt2->x > m_MaxPos.x)m_MaxPos.x=pnt2->x;
        if (pnt3->x > m_MaxPos.x)m_MaxPos.x=pnt3->x;

        if (pnt1->x < m_MinPos.x)m_MinPos.x=pnt1->x;
        if (pnt2->x < m_MinPos.x)m_MinPos.x=pnt2->x;
        if (pnt3->x < m_MinPos.x)m_MinPos.x=pnt3->x;

        if (pnt1->y > m_MaxPos.y)m_MaxPos.y=pnt1->y;
        if (pnt2->y > m_MaxPos.y)m_MaxPos.y=pnt2->y;
        if (pnt3->y > m_MaxPos.y)m_MaxPos.y=pnt3->y;

        if (pnt1->y < m_MinPos.y)m_MinPos.y=pnt1->y;
        if (pnt2->y < m_MinPos.y)m_MinPos.y=pnt2->y;
        if (pnt3->y < m_MinPos.y)m_MinPos.y=pnt3->y;
    }

    m_Size = m_MaxPos-m_MinPos;

    int index = 0;
    for( std::list<TPPLPoly>::iterator listMyClassIter = triangles.begin(); // not listMyClass.begin()
        listMyClassIter != triangles.end(); // not listMyClass.end()
        listMyClassIter ++)
    {
        Vertex * v = &m_Vertexes[index];
        Vertex * v2 = &m_Vertexes[index+1];
        Vertex * v3 = &m_Vertexes[index+2];

		m_Indexes[index] = index;
		m_Indexes[index+1] = index+1;
		m_Indexes[index+2] = index+2;

        TPPLPoly * triangle = &(*listMyClassIter);
        triangle->SetOrientation(TPPL_CCW);

        m_SavedVertexes[index].x = triangle->GetPoint(0).x;
        m_SavedVertexes[index].y = triangle->GetPoint(0).y;
        m_SavedVertexes[index+1].x = triangle->GetPoint(1).x;
        m_SavedVertexes[index+1].y = triangle->GetPoint(1).y;
        m_SavedVertexes[index+2].x = triangle->GetPoint(2).x;
        m_SavedVertexes[index+2].y = triangle->GetPoint(2).y;

        TPPLPoint * pnt = &triangle->GetPoint(0);
        v->x = pnt->x;
        v->y = NBG::g_DevicePtr->GetMiddleY()-pnt->y;
        v->z = 0.0f;
        NBG::Vector uv = GetUV(Vector(pnt->x,pnt->y));
		uv.x = pnt->u;
		uv.y = pnt->v;

        v->u = uv.x;
        v->v = uv.y;
        v->color = 0xFFFFFFFF;

        pnt = &triangle->GetPoint(1);
        v2->x = pnt->x;
        v2->y = NBG::g_DevicePtr->GetMiddleY()-pnt->y;
        v2->z = 0.0f;
        uv = GetUV(Vector(pnt->x,pnt->y));
		uv.x = pnt->u;
		uv.y = pnt->v;
        v2->u = uv.x;
        v2->v = uv.y;
        v2->color = 0xFFFFFFFF;

        pnt = &triangle->GetPoint(2);
        v3->x = pnt->x;
        v3->y = NBG::g_DevicePtr->GetMiddleY()-pnt->y;
        v3->z = 0.0f;
        uv = GetUV(Vector(pnt->x,pnt->y));
		uv.x = pnt->u;
		uv.y = pnt->v;
        v3->u = uv.x;
        v3->v = uv.y;
        v3->color = 0xFFFFFFFF;
        index+=3;		
    }*/
}
}
