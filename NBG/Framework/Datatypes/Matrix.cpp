#include "Matrix.h"
#include <math.h>

#include <Framework/Utils/MathUtils.h>

namespace NBG
{

	Matrix __g_RotateMatrix;
	Matrix __g_ScalingMatrix;
	Matrix __g_TempMatrix;
	Matrix __g_ResultMatrix;	
	Matrix __g_TranslationMatrix;
	float __g_sinRoll = 0.0f;
	float __g_cosRoll = 0.0f;
//==============================================================================
Matrix::Matrix()
{

}

//==============================================================================
Matrix::~Matrix()
{

}

//==============================================================================
void Matrix::Identity()
{
	m[0] = 1.0;
	m[1] = 0.0;
	m[2] = 0.0;
	m[3] = 0.0;
	m[4] = 0.0;
	m[5] = 1.0;
	m[6] = 0.0;
	m[7] = 0.0;
	m[8] = 0.0;
	m[9] = 0.0;
	m[10] = 0.0;
	m[11] = 0.0;
	m[12] = 0.0;
	m[13] = 0.0;
	m[14] = 0.0;
	m[15] = 1.0;
}

//==============================================================================
void Matrix::CreateFromTransform(const Transform &trans)
{
   Identity();   

    __g_TempMatrix.Identity();
    __g_TempMatrix.Translate(-trans.hx, -trans.hy, trans.hz);
    Multiply(&__g_TempMatrix);

	__g_TempMatrix.Identity();
	__g_TempMatrix.Scale(trans.scalex, trans.scaley, trans.scalez);
	Multiply(&__g_TempMatrix);
	
	if (trans.rz != 0.0f)
	{
		__g_TempMatrix.Identity();
		__g_TempMatrix.RotateYawPitchRoll(trans.ry, trans.rx, trans.rz);
		Multiply(&__g_TempMatrix);
	}

    __g_TempMatrix.Identity();
    __g_TempMatrix.Translate(trans.x, trans.y, trans.z);
    Multiply(&__g_TempMatrix);
}

//==============================================================================
void Matrix::Multiply(Matrix * second)
{
	Matrix * m1;
	Matrix * m2;
	m1 = second;
	m2 = this;
	
	__g_ResultMatrix.m[0]  = m1->m[0] * m2->m[0]  + m1->m[4] * m2->m[1];
	__g_ResultMatrix.m[1]  = m1->m[1] * m2->m[0]  + m1->m[5] * m2->m[1];
	__g_ResultMatrix.m[2]  = 0;
	__g_ResultMatrix.m[3]  = 0;
	__g_ResultMatrix.m[4]  = m1->m[0] * m2->m[4]  + m1->m[4] * m2->m[5];
	__g_ResultMatrix.m[5]  = m1->m[1] * m2->m[4]  + m1->m[5] * m2->m[5];
	__g_ResultMatrix.m[7] = 0;
	__g_ResultMatrix.m[8] = 0;
	__g_ResultMatrix.m[9] = 0;
	__g_ResultMatrix.m[10] = 0;
	__g_ResultMatrix.m[11] = 0;
	__g_ResultMatrix.m[12] = m1->m[0] * m2->m[12] + m1->m[4] * m2->m[13] + m1->m[12];
	__g_ResultMatrix.m[13] = m1->m[1] * m2->m[12] + m1->m[5] * m2->m[13] + m1->m[13];
	__g_ResultMatrix.m[14] = 0;
	__g_ResultMatrix.m[15] = 1;

    for (int i=0; i<16; i++)
    {
        m[i] = __g_ResultMatrix.m[i];
    }

}

//==============================================================================
void Matrix::Translate(const float x, const float y, const float z)
{ 
    __g_TranslationMatrix.Identity();
	__g_TranslationMatrix.m[12] = x;
	__g_TranslationMatrix.m[13] = y;
	Multiply(&__g_TranslationMatrix);
}

//==============================================================================
void Matrix::RotateYawPitchRoll(float yaw, float pitch, float roll )
{    
    __g_RotateMatrix.Identity();
	
	__g_sinRoll = MathUtils::FastSin(roll);
	__g_cosRoll = MathUtils::FastCos(roll);

	__g_RotateMatrix.m[0] = __g_cosRoll ;
	__g_RotateMatrix.m[1] = __g_sinRoll;
	__g_RotateMatrix.m[2] = 0;
	__g_RotateMatrix.m[4] = -__g_sinRoll;
	__g_RotateMatrix.m[5] = __g_cosRoll;
	__g_RotateMatrix.m[6] = 0;
	__g_RotateMatrix.m[8] = 0;
	__g_RotateMatrix.m[9] = 0;
	__g_RotateMatrix.m[10]  = 0;
	__g_RotateMatrix.m[15] = 1;
	Multiply(&__g_RotateMatrix);
}

//==============================================================================
void Matrix::Scale(const float x, const float y, const float z)
{
	__g_ScalingMatrix.Identity();
	__g_ScalingMatrix.m[0] = x;
	__g_ScalingMatrix.m[5] = y;
	Multiply(&__g_ScalingMatrix);
}

//==============================================================================
Vector Matrix::TransformPoint(Vector point)
{
    Vector res;
	res.x = m[0] * point.x + m[4] * point.y + m[12];
    res.y = m[1] * point.x + m[5] * point.y + m[13];
    return res;
}

}
