#ifndef NBG_CORE_DATATYPES_MATRIX
#define NBG_CORE_DATATYPES_MATRIX

#include <string>
#include <ostream>

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
/** @brief Класс, реализующий работу матрицы 4х4
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Matrix
{
public:
    /// @name Конструкторы
    Matrix();
    ~Matrix();

    /** @brief Заполняет матрицу значениями по умолчанию
     *
     *
     */
    void Identity();

    /** @brief Создаёт матрицу из трансформации
     *
     *
     */
    void CreateFromTransform(const Transform &trans);

    /** @brief Перемножает матрицы
     *
     *
     */
    void Multiply(Matrix * second);

    /** @brief Перемещает матрицу
     *
     *
     */
    void Translate(const float x, const float y, const float z = 0.0f);

    /** @brief Вращает матрицу
     *
     *
     */
     void RotateYawPitchRoll(const float yaw, const float pitch, const float roll);

    /** @brief Масштабирует матрицу
     *
     *
     */
    void Scale(const float x, const float y, const float z = 0.0f);


    /** @brief Применяет матрицу к точке
     *
     *
     */
    Vector TransformPoint(Vector point);

    float m[16];
private:


};

}
#endif // NBG_CORE_DATATYPES_MATRIX
