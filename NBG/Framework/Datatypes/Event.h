#ifndef NBG_CORE_DATATYPES_EVENT
#define NBG_CORE_DATATYPES_EVENT

#include <string>

namespace NBG
{
	/** @brief Класс, реализующий работу с событиями.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class Event
	{
	public:
		/// @name Перечисления

		///Стандартные типы событий
		enum EventTypes
		{
			ET_MouseDown,
			ET_MouseUp,
			ET_MouseMove,
			ET_KeyDown,
			ET_KeyUp,
			ET_Over,
			ET_Out,
			ET_Check,
			ET_UnCheck,
			ET_Confirm,
			ET_FloatValueChanged,
			ET_StringValueChanged,
			ET_UserDefined
		};

		/// @name Конструктор/деструктор

		///Конструктор события по умолчанию
		Event(const EventTypes &type, void * data = NULL);
		///Конструктор пользовательского события
		Event(const std::string eventName, void * data = NULL);
		~Event();

		/// @name Получение основных свойств
		///Получение типа
		inline int GetType()
		{
			return m_Type;
		};
		///Получение пользовательского названия события
		inline std::string GetName()
		{
			return m_Name;
		};
		///Получение пользовательских данных
		inline void * GetData()
		{
			return m_Data;
		};


	protected:
		///Пользовательское название события
		std::string m_Name;
		///Пользовательские данные события
		void * m_Data;
		///Тип события
		int m_Type;
	private:
	};
}
#endif //NBG_CORE_DATATYPES_EVENT
