#ifndef NBG_CORE_MESH
#define NBG_CORE_MESH

#include <string>
#include <vector>
#include <ostream>

#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Vertex.h>

namespace NBG
{
/** @brief Класс, реализующий работу с мешом. Возможность задания любого количества вершин.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Mesh
{
public:
    /// @name Конструкторы
    ///Базовый конструктор
    Mesh();
    ~Mesh();

    void Init(const int &vertexCount, const int &indexesCount = 6);
    void SetColor(Color &color);

    ///Метод для заполнения данных прямоугольного меша
    void SetRectData(FRect * uv, Transform * trans, Vector size, Color clr, Texture * texture, bool isTileTexture = false, Vector offset = Vector());
    ///Обновление только текстурных координат
    void SetRectUV(FRect * uv);

    void UpdateVertexes(Transform * trans, Texture * texture, bool isTileTexture = false);
    ///Метод для задания кастомных вершин
    void SetCustomVertexes(std::vector<Vertex> &vertexes);

    inline Vertex * GetVertexes()
    {
        return m_Vertexes;
    };

    inline int GetVertexesSize()
    {
        return sizeof(Vertex)*m_VertexCount;
    };

    inline int GetVertexCount()
    {
        return m_VertexCount;
    };

	inline unsigned short * GetIndexes()
	{
		return m_Indexes;
	}

	inline int GetIndexesSize()
    {
        return sizeof(unsigned short)*m_IndexesCount;
    };

    inline int GetIndexesCount()
    {
        return m_IndexesCount;
    };


	Vector rectangleOffsets[4]; ///Смещения для прямоугольных спрайтов.
	Color rectangleColorOffset[4]; ///Смещения цвета для прямоугольных спрайтов

private:
    Vertex * m_Vertexes;
    int m_VertexCount;

	unsigned short * m_Indexes;
	int m_IndexesCount;
	bool m_QuadIndexesPreload;

	

    Vertex * m_SavedVertexes;

    Vector m_Size;
    Vector m_MinPos;
    Vector m_MaxPos;

    NBG::Vector GetUV(const Vector &pos);
};
}
#endif // NBG_CORE_MESH
