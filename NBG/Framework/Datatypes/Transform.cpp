#include <memory.h>
#include <string>
#include <vector>

#include "Transform.h"
#include <Framework/Utils/MathUtils.h>

namespace   NBG{

//==============================================================================
Transform::Transform()
{
    memset(this,0, sizeof(Transform));
    scalex=scaley=scalez=1;
    parent = NULL;
}
//==============================================================================
Transform::Transform(Transform* tr)
{
    this->x = tr->x;
    this->y = tr->y;
    this->z = tr->z;
    this->hx = tr->hx;
    this->hy = tr->hy;
    this->hz = tr->hz;
    this->rx = tr->rx;
    this->ry = tr->ry;
    this->rz = tr->rz;
    this->scalex = tr->scalex;
    this->scaley = tr->scaley;
    this->scalez = tr->scalez;
    this->parent = tr->parent;
}
//==============================================================================
void Transform::SetPosition(const float x, const float y, const float z)
{
    this->x = x; this->y = y; this->z = z;
}
//=================================================================//
void Transform::SetPosition(const Vector &point)
{
    SetPosition(point.x, point.y, point.z);
}

//==============================================================================
void Transform::SetHotSpot(const float x, const float y, const float z)
{
    hx = x; hy = y; hz = z;
}

//==============================================================================
void Transform::SetScale(const float x, const float y, const float z)
{
    scalex = x; scaley = y; scalez = z;
}
//==============================================================================
void Transform::SetRotation(const float rot)
{
    this->rz = rot;
}
//==============================================================================
void Transform::SetRotation(const float x, const float y, const float z)
{
    rx = x; ry = y; rz = z;
}
//==============================================================================
void Transform::SetRotation( const Vector& rotation)
{
    rx = rotation.x; ry = rotation.y; rz = rotation.z;
}

//=================================================================//
Vector Transform::GetPosition()
{
    Vector point;
    point.x = x;
    point.y = y;
    return point;
}

//=================================================================//
Transform Transform::GetAbsoluteTransform()
{   
    if(parent != NULL)
    {
		Transform result = *this;
        result.AddTransform(parent->GetAbsoluteTransform());		
		return result;
    }
    return this;
}

//=================================================================//
void Transform::AddTransform(Transform transf)
{
    this->scalex *= transf.scalex;
    this->scaley *= transf.scaley;

    x *= transf.scalex;
    y *= transf.scaley;

    x += transf.x;
    y += transf.y;

    MathUtils::RelativeRotate(transf.x, transf.y, x, y, transf.rz);
    this->rz += transf.rz;
}
}
