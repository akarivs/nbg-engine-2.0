#ifndef NBG_CORE_DATATYPES_TEXTURE
#define NBG_CORE_DATATYPES_TEXTURE

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
/** @brief Класс, реализующий работу с текстурами
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Texture
{
public:
    /// @name Конструкторы
    Texture();
    ~Texture();

    /// @name Методы

    /** @brief Устанавливает размеры текстуры
     *
     *@param float x
     *@param float y
     *
     */
     void SetSize(const float x, const float y);

    /** @brief Возвращает ширину/высоту текстуры
     *
     *
     */
    Vector GetSize();

    /** @brief Возвращает декодированные данные текстуры
     *
     *
     */
    void * GetTextureData();

    /** @brief Записывает декодированные данные текстуры
     *
     *@param void * data
     *
     */
    void SetTextureData(void * data);
    
    /** @brief Возвращает пользовательские данные текстуры
     *
     *
     */
    void * GetUserData();
    
    /** @brief Записывает пользовательские данные текстуры
     *
     *@param void * data
     *
     */
    void SetUserData(void * data);

private:
    /// @name Размеры
    Vector m_Size;

    ///Декодированные текстурные данные
    void * m_Data;
    
    ///Пользовательские данные
    void * m_UserData;
};

}

#endif // NBG_CORE_DATATYPES_VECTOR
