#ifndef NBG_CORE_DATATYPES_TRANSFORM
#define NBG_CORE_DATATYPES_TRANSFORM

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
/** @brief Класс, реализующий работу 3D трансформации.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Transform
{
public:
    /// @name Если true, то используется собственный хот спот + точка трансформации
    bool            blUseSelfHotSpot;
    /// @name Точка трансформации, т.е точка вокруг которой происходит трансформация
    float           hx,hy,hz;
    /// @name Вращение вокруг оси z
    float           rx,ry,rz;
    /// @name Масштаб в процентах
    float           scalex,scaley,scalez;
    /// @name Позиция
    float           x,y,z;
    /// @name Родительская трансформация
    Transform*  parent;

    /// @name Констурктор. Инициализирует тождественную трансформацию, т.е не изменяет ориентацию объекта
    Transform();
    /// @name Конструирует объект исходя из объекта *tr
    Transform(Transform* tr);

    /// @name Установка позиции
    void SetPosition(const float x, const float y, const float z = 0);
    void SetPosition(const Vector& point);

    /// @name Устанавливает точку трансформации, т.е hot spot
    void SetHotSpot(const float x, const float y, const float z = 0);

    /// @name Устанавливает масштаб относительно осей координат
    void SetScale(const float x, const float y, const float z = 0);

    /// @name Устанавливает двумерную трансформацию
    void SetRotation(const float rot);
    /// @name Установить трехмерную трансформацию
    void SetRotation(const float x, const float y, const float z);
	/// @name Установить трехмерную трансформацию
	void SetRotation( const Vector& rotation );

    /// @name Возвращает позицию
    Vector GetPosition();

    /// @name Получение абсолютной трансформации с учетом всех родительских
    Transform GetAbsoluteTransform();

    /// @name Сложение двух трансформаций
    void AddTransform(Transform trans);

    /// @name Устанавливает новую родительскую трансформацию, возвращая при этом старую
    Transform* SetParent(Transform* ptr){if(!this)return NULL; Transform* oldpar = parent; parent = ptr; return oldpar;}
};
}
#endif // Transform_H_INCLUDED
