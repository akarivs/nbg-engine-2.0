#include "Color.h"

#include <math.h>
#include <stdlib.h>     /* atoi */
#include <Framework.h>
#include <Framework/Utils/StringUtils.h>

namespace NBG
{
//==============================================================================
Color::Color()
{
    SetColor(255.0f,255.0f,255.0f,255.0f);
}

//==============================================================================
Color::Color(const float &a, const float &r, const float &g, const float &b)
{
    SetColor(a,r,g,b);
}

//==============================================================================
Color::Color(unsigned long color)
{
    SetColor(color);
}

//==============================================================================
Color::Color(const std::string &color)
{
    SetColor(color);
}

//////////////////////////////////////////////////////////////////////////
///�����������, ��������� ���� ��������� �� FloatColor
Color::Color(FloatColor &color)
{
	SetColor(color.a, color.r, color.g,color.b);
}

//==============================================================================
void Color::SetColor(unsigned long color)
{
    b = (float)(color & 0xFF); color >>= 8;
    g = (float)(color & 0xFF); color >>= 8;
    r = (float)(color & 0xFF); color >>= 8;
    a = (float)(color & 0xFF);
}

//==============================================================================
void Color::SetColor(const float &a, const float &r, const float &g, const float &b)
{
    this->a = a;
    this->r = r;
    this->g = g;
    this->b = b;
}

//==============================================================================
void Color::SetColor(const std::string &color)
{
    std::vector<std::string>colorArray;
    colorArray= StringUtils::ExplodeString(color,' ');
    if (colorArray.size() != 4)
    {
        CONSOLE("Error: color must contain 4 elements! - %s", color.c_str());
        return;
    }
    this->a = atoi(colorArray[0].c_str());
    this->r = atoi(colorArray[1].c_str());
    this->g = atoi(colorArray[2].c_str());
    this->b = atoi(colorArray[3].c_str());
}

//==============================================================================
void Color::Normalize()
{
#ifndef NBG_WIN32
    a/=255.0f;
    r/=255.0f;
    g/=255.0f;
    b/=255.0f;
#endif
}

//==============================================================================
unsigned long Color::GetPackedColor()
{
	return ((((((int)a << 8) + (int)r) << 8) + (int)g) << 8) + (int)b;
}


}
