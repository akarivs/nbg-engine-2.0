#include "Vertex.h"

namespace NBG
{
    //==============================================================================
    Vertex::Vertex()
    {
        x = y = z = 0.0f;
        color = 0xFFFFFFFF;
    }

    //==============================================================================
    Vertex::Vertex(const Vector &pos, Color color, const Vector &uv)
    {
        x = pos.x;
        y = pos.y;
        z = pos.z;
        this->color = color.GetPackedColor();
        u = uv.x;
        v = uv.y;
    }

    //==============================================================================
    Vertex::~Vertex()
    {

    }

}
