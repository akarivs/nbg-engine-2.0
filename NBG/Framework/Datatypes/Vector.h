#ifndef NBG_CORE_DATATYPES_VECTOR
#define NBG_CORE_DATATYPES_VECTOR

#include <string>
#include <ostream>

namespace NBG
{
/** @brief Класс, реализующий работу 3D вектора.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Vector
{
public:
    /// @name Конструкторы
    Vector();
    Vector(const float &x, const float &y);
    Vector(const float &x, const float &y, const float &z);
    ~Vector();

    /// @name Координаты

    float x;
    float y;
    float z;

    /// @name Методы

    /** @brief Возвращает длину вектора
     *
     *
     */
    float GetLength();
    /** @brief Нормализует вектор
     *
     *
     */
    void Normalize();
    /** @brief Возвращает результат скалярного произведения с входным вектором
     *
     *
     */
    float DotProduct(const Vector &in);


    /// @name Стандартные математические операторы
    Vector	operator + (const Vector &vec);
	Vector  operator + (const float scalar);
    Vector	operator - (const Vector &vec);
	Vector  operator - (const float scalar);
    Vector  operator * (const Vector &vec);
	Vector  operator * (const float scalar);
    Vector  operator / (const Vector &vec);
	Vector  operator / (const float scalar);

    void operator = (const Vector &vec);
	void operator = (const float scalar);
    void operator += (const Vector &vec);
	void operator += (const float scalar);
    void operator -= (const Vector &vec);
	void operator -= (const float scalar);
	void operator *= (const Vector &vec);
	void operator *= (const float scalar);
	void operator /= (const Vector &vec);
	void operator /= (const float scalar);

    /// @name Возможность вывода в лог-файл или консоль
    friend std::ostream& operator<< (std::ostream& stream, const Vector& vector) {
            stream << "x: ";
            stream << vector.x;
            stream << " y: ";
            stream << vector.y;
            return stream;
    }

private:


};

}




#endif // NBG_CORE_DATATYPES_VECTOR
