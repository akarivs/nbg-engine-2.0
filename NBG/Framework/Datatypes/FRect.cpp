#include "FRect.h"
#include <math.h>

namespace NBG
{
FRect::FRect()
{
    left = 0.0f;
    top = 0.0f;
    right = 0.0f;
    bottom = 0.0f;
}
FRect::FRect(const float &left, const float &top, const float &right, const float &bottom)
{
    this->left = left;
    this->top = top;
    this->right = right;
    this->bottom = bottom;
}

}
