#include "Texture.h"

namespace NBG
{
//==============================================================================
Texture::Texture()
{
    m_Data = NULL;
    m_UserData = NULL;
}

//==============================================================================
Texture::~Texture()
{
   
}

//==============================================================================
void Texture::SetSize(const float x, const float y)
{
    m_Size.x = x;
    m_Size.y = y;
}

//==============================================================================
Vector Texture::GetSize()
{
    return m_Size;
}

//==============================================================================
void * Texture::GetTextureData()
{
    return m_Data;
}

//==============================================================================
void Texture::SetTextureData(void* data)
{
    m_Data = data;
}
    
    //==============================================================================
    void * Texture::GetUserData()
    {
        return m_UserData;
    }
    
    //==============================================================================
    void Texture::SetUserData(void* data)
    {
        m_UserData = data;
    }

}
