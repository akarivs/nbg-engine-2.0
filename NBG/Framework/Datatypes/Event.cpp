#include "Event.h"

namespace NBG
{
    //==============================================================================
    Event::Event(const EventTypes &type, void * data)
    {
        m_Type = type;
        m_Data = data;
        m_Name = "Base Event";
    }

    //==============================================================================
    Event::Event(const std::string eventName, void * data)
    {
        m_Type = ET_UserDefined;
        m_Name = eventName;
        m_Data = data;
    }

    //==============================================================================
    Event::~Event()
    {

    }

}
