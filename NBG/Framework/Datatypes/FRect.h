#ifndef NBG_CORE_DATATYPES_RECTANGLE
#define NBG_CORE_DATATYPES_RECTANGLE

namespace NBG
{
/** @brief Класс, реализующий работу прямоугольника.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class FRect
{
public:
    /// @name Конструкторы
    FRect();
    FRect(const float &left, const float &top, const float &right, const float &bottom);
    ~FRect(){};

    /// @name Координаты

    float left;
    float top;
    float right;
    float bottom;

    /// @name Методы
private:

};
}
#endif // NBG_CORE_DATATYPES_RECTANGLE
