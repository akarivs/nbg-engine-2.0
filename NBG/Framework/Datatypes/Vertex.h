#ifndef NBG_CORE_DATATYPES_VERTEX
#define NBG_CORE_DATATYPES_VERTEX

#include <string>

#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
    /** @brief Класс, представляющий собой вершину
    *
    * @author Vadim Simonov <akari.vs@gmail.com>
    * @copyright 2013 New Bridge Games
    *
    */
    class Vertex
    {
    public:
        /// @name Конструктор/деструктор

        ///Конструктор вершины по умолчанию
        Vertex();
        ///Конструктор вершины с данными
        Vertex(const Vector &pos, Color color, const Vector &uv);
        ~Vertex();

         /// @name Координаты
        float x;
        float y;
        float z;
        /// @name Цвет
       Color color;
        /// @name Текстурные координаты
        float u;
        float v;
    protected:

    private:

    };
}
#endif //NBG_CORE_DATATYPES_EVENT
