#ifndef NBG_CORE_DATATYPES_COLOR
#define NBG_CORE_DATATYPES_COLOR

#include <string>
#include <ostream>

#include "FloatColor.h"

namespace NBG
{
/** @brief Класс, реализующий работу с цветом.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
struct Color
{
#if !defined (NBG_WIN32)
	float r;
	float g;
    float b;
    float a;
#else
	unsigned b      :8;
	unsigned g      :8;
	unsigned r      :8;
	unsigned a      :8;
#endif

public:
    /// @name Конструкторы
    ///Базовый конструктор, заполняет цвет по умолчанию (255,255,255,255)
    Color();
    ///Конструктор, заполняет цвет значениями
    Color(const float &a, const float &r, const float &g, const float &b);
    ///Конструктор, заполняет цвет значением из упакованного цвета
    Color(unsigned long color);
    ///Конструктор, заполняет цвет значением из строки в формате (255 255 255 255)
    Color(const std::string &color);
	///Конструктор, заполняет цвет значением из FloatColor
	Color(FloatColor &color);
    ~Color(){};

    /// @name Составляющие цвета
    

    /// @name Методы
    ///Установка цвета из заданных значений
    void SetColor(const float &a, const float &r, const float &g, const float &b);
    ///Установка цвета из упакованного long
    void SetColor(unsigned long color);
    ///Установка цвета из строки в формате  "255 255 255 255" (argb)
    void SetColor(const std::string &color);

	///Сеттеры
	void SetA(const float val){a=val;}
	void SetR(const float val){r=val;}
	void SetG(const float val){g=val;}
	void SetB(const float val){b=val;}
	///Геттеры
	float GetA(){return a;}
	float GetR(){return r;}
	float GetG(){return g;}
	float GetB(){return b;}

    ///Нормализует цвет (приводит в вид от 0 до 1)
    void Normalize();

    ///Упаковывает цвет в один unsigned long
    unsigned long GetPackedColor();
};
}
#endif // NBG_CORE_DATATYPES_COLOR
