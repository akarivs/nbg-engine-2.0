#ifndef _AK_FRAMEWORK_GAME_APPLICATION
#define _AK_FRAMEWORK_GAME_APPLICATION

#include <Framework/GUI/GraphicsObject.h>
#include <Framework/GUI/GuiFactory.h>
#include <Framework/GUI/CursorObject.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Utils/Config.h>


#include <Framework/Managers/ScenesManager.h>
#include <Framework/Managers/FontsManager.h>
#include <Framework/Managers/LocalizationManager.h>
#include <Framework/Managers/PlayersManager.h>
#include <Framework/Managers/SoundManager.h>

#include <Framework/Global/Render.h>
#include <Framework/Global/System.h>
#include <Framework/Global/Input.h>
#include <Framework/Global/Random.h>

#include <Framework/Global/OperatingSystem.h>
#include <Framework/Global/TimeManager.h>
#include <Framework/Global/TextureManager.h>
#include <Framework/Global/FileSystem.h>
#include <Framework/Global/ResourcesManager.h>

#include <Framework/Helpers/AtlasHelper.h>
#include <Framework/Helpers/ActionHelper.h>
#include <Framework/Helpers/Actions/ActionsFactory.h>
#include <Framework/Helpers/EditionHelper.h>
#include <Framework/Helpers/LuaHelper.h>
#include <Framework/Helpers/BinaryHelper.h>
#include <Framework/Helpers/StencilHelper.h>

#include <Framework/Utils/Tweener.h>

#include <../TheoraPlayer/include/TheoraPlayer/TheoraPlayer.h>
#include <../TheoraPlayer/OpenAL_AudioInterface.h>

#include <functional>

namespace NBG
{
	class CGameApplication
	{
	public:
		CGameApplication();
		~CGameApplication();            
		

		///загрузка игровой логики и запуск игры
		bool Init();
		bool Run();
        void AfterRun();
		void StopApp();
		bool IsStopped();
		bool IsPaused();
		void Pause();
		void Resume();

		bool IsCheatsEnabled();

		bool IsFullscreen();

		void SetWindowTitle(const std::wstring &title);
		


		CGameApplication * SetFullscreenSize(const int w, const int h);
		CGameApplication * SetLogicalSize(const int w, const int h);
		CGameApplication * SetWindowedSize(const int w, const int h);
		CGameApplication * SetFullscreen(const bool fullscreen);
		CGameApplication * SetFontsPath(const std::string &fontsPath);
		CGameApplication * SetStringsPath(const std::string &fontsPath);
		CGameApplication * SetConfigPath(const std::string &configPath);
		CGameApplication * SetAtlasesPath(const std::string &atlasesPath);
		CGameApplication * SetEditionsPath(const std::string &editionXML, const std::string &editionsFolder);
		CGameApplication * ConfigDebugLabel(const std::string &font, const Vector scale);

		NBG::CConfig			* GetConfig();
		CScenesManager			* GetScenesManager();
		CFontsManager			* GetFontsManager();
		CLocalizationManager	* GetLocalizationManager();
		CPlayersManager			* GetPlayersManager();
		TheoraVideoManager		* GetTheoraVideoManager();
		CSoundManager			* GetSoundManager();
		CCursorObject			* GetCursor();
		COperatingSystem		* GetOperatingSystem();
		CTimeManager			* GetTimeManager();
		CTextureManager			* GetTextureManager();
		CFileSystem				* GetFileSystem();
		CAtlasHelper			* GetAtlasHelper();
		CResourcesManager       * GetResourcesManager();
		CEditionHelper		    * GetEditionHelper();
		CLuaHelper				* GetLuaHelper();
		CRender					* GetRender();
		CSystem					* GetSystem();
		CInput					* GetInput();
		CRandom					* GetRandom();
		CTweener				* GetTweener();
		CGuiFactory				* GetGuiFactory();
		CActionHelper			* GetActionHelper();
		CActionFactory			* GetActionFactory();
		CBinaryHelper			* GetBinaryHelper();
		CStencilHelper			* GetStencilHelper();

		Vector GetLogicalSize()
		{
			return m_LogicalSize;
		}
		
		Vector & GetWindowSize();
		Vector & GetWindowedSize(){return m_WindowedSize;}
		double GetFrameTime();
		void SetFrameTime(double time){m_FrameTime = time;}
		double GetAppTime();

		void OnKeyUp( const int key );
		void OnKeyDown( const int key );
		void OnMouseUp(const int button);
		void OnMouseDown(const int button);
		void OnMouseMove();    

		void OnBeforeRenderChange();
		void OnAfterRenderChange();
		void Update();
		void Draw();
        
        void ShareContext();
        void UnShareContext();

		void ShowDebugInfo(bool show);

		void SetOnInitCallback(std::function<void(void)> callback){ m_OnInitCallback = callback; };
		void SetOnMouseDownCallback(std::function<void(void)> callback){ m_OnMouseMoveCallback = callback; };
		void SetUpdateCallback(std::function<void(void)> callback){ m_OnUpdateCallback = callback; };
		void SetDrawCallback(std::function<void(void)> callback){ m_OnDrawCallback = callback; };

		
	private:		
		bool m_IsApplicationActive;
		bool m_IsInitialized;
		NBG::Transform m_SavedGlobalMatrix;

		std::wstring m_DebugString;		
		std::string m_FontsPath;
		std::string m_StringsPath;
		std::string m_ConfigPath;
		std::string m_AtlasesPath;
		std::string m_EditionsXMLPath;
		std::string m_EditionsFolderPath;

		Vector m_FullscreenSize;
		Vector m_WindowedSize;
		Vector m_LogicalSize;
		Vector m_CurrentSize;
		bool m_IsFullscreen;
		bool m_IsStopped;
		bool m_IsPaused;

		Vector m_DebugLabelScale;
		std::string m_DebugLabelFont;

		double m_FrameTime;
		double m_AppTime;
		
		CConfig m_Config;
		CScenesManager m_ScenesManager;
		CFontsManager m_FontsManager;
		CLocalizationManager m_LocalizationManager;
		CPlayersManager m_PlayersManager;
		TheoraVideoManager *m_TheoraVideoManager;
		CSoundManager m_SoundManager;
		IOperatingSystem * m_OperatingSystem;
		ITimeManager * m_TimeManager;
		ITextureManager * m_TextureManager;
		IFileSystem * m_FileSystem;
		CRender		m_Render;
		CSystem		m_System;
		CInput		m_Input;
		CRandom		m_Random;
		CTweener	m_Tweener;
		CGuiFactory m_GuiFactory;
		CActionHelper m_ActionHelper;
		CActionFactory m_ActionFactory;

		CAtlasHelper m_AtlasHelper;
		CEditionHelper m_EditionHelper;
		CResourcesManager m_ResourcesManager;
		CBinaryHelper	m_BinaryHelper;
		CStencilHelper	m_StencilHelper;
		CLuaHelper		m_LuaHelper;

		CLabelObject * m_DebugLabel;

		CCursorObject * m_Cursor;

		float m_UpdateLimit;
		float m_UpdateCounter;
		float m_DrawLimit;
		float m_DrawCounter;

		std::function<void(void)> m_OnInitCallback;
		std::function<void(void)> m_OnUpdateCallback;
		std::function<void(void)> m_OnDrawCallback;
		std::function<void(void)>  m_OnMouseMoveCallback;		
	protected:

	};    
}

#endif
