#include "AtlasHelper.h"
#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CAtlasHelper::CAtlasHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CAtlasHelper::~CAtlasHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CAtlasHelper::LoadAtlases(const std::string &path)
	{		
		STRING_VECTOR atlasesDir = g_FileSystem->GetDirectoryContents(path);
		for (size_t i=0; i<atlasesDir.size(); i++)
		{
			if (StringUtils::GetExtension(atlasesDir[i]) != "xml")continue;
			AddAtlasesXML(path,path+"/"+atlasesDir[i], NBG::StringUtils::RemoveExtension(atlasesDir[i]));
		}
		
	}

	//////////////////////////////////////////////////////////////////////////
	void CAtlasHelper::PreloadResource(const std::string &name)
	{
		std::map<std::string, std::string>::iterator iter;
		std::string fullName = name+".xml";
		iter = m_AtlasTextures[fullName].begin();
		while (iter != m_AtlasTextures[fullName].end())
		{			
			g_ResManager->GetResource(iter->second);
			iter++;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CAtlasHelper::AddAtlasesXML(const std::string &basePath, const std::string &path, const std::string &name)
	{
		std::string xml = path;
		NBG::CXMLResource * xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(xml);

		xml_document* doc_level = xmlRes->GetXML();
		pugi::xml_node node_level = doc_level->first_child();

		float atlasWidth    = node_level.attribute("width").as_float();
		float atlasHeight   = node_level.attribute("height").as_float();       

		float texelWidth = 0.5/atlasWidth;
		float texelHeight = 0.5/atlasHeight;

		for (pugi::xml_node image = node_level.child("image"); image; image = image.next_sibling("image"))
		{
			std::string id = image.attribute("id").value();			

			float x = image.attribute("x").as_float();
			float y = image.attribute("y").as_float();
			float width = image.attribute("width").as_float();
			float height = image.attribute("height").as_float();

			float u1 = image.attribute("u1").as_float() - texelWidth;
			float u2 = image.attribute("u2").as_float() + texelWidth;
			float v1 = image.attribute("v1").as_float() - texelHeight;
			float v2 = image.attribute("v2").as_float() + texelHeight;

			AtlasTextureDescription desc;
			desc.atlasID = basePath + "/";
			desc.atlasID += image.attribute("atlas").value();
			desc.uv = NBG::FRect(u1,v1,u2,v2);
			desc.size.x = width;
			desc.size.y = height;
			desc.offset.x = image.attribute("ox").as_float();
			desc.offset.y = image.attribute("oy").as_float();
			desc.atlasName = name;
			m_Textures[id] = desc;
		}
		g_ResManager->ReleaseResource(xmlRes);
	}

	//////////////////////////////////////////////////////////////////////////
	std::string CAtlasHelper::GetAtlasID(const std::string &texture)
	{
		return m_Textures[texture].atlasID;
	}

	//////////////////////////////////////////////////////////////////////////
	NBG::FRect CAtlasHelper::GetUV(const std::string &texture)
	{
		return m_Textures[texture].uv;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector CAtlasHelper::GetSize(const std::string &texture)
	{
		return m_Textures[texture].size;
	}

	//////////////////////////////////////////////////////////////////////////
	AtlasTextureDescription * CAtlasHelper::GetTextureDescription(const std::string &texture)
	{		
		if (m_Textures.find(texture) == m_Textures.end())
		{
			return NULL;
		}
		return &m_Textures[texture];
	}
}