#ifndef _AK_FRAMEWORK_ACTION_HELPER
#define _AK_FRAMEWORK_ACTION_HELPER

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "Actions/BaseAction.h"

#include <queue>
#include <deque>


namespace NBG
{
	/** @brief Класс, реализующий работу с цепочками действий, которые можно назначить из xml.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/

	class CActionQueue : public CBaseObject
	{
	public:
		CActionQueue();
		~CActionQueue();

		void Start();		
		void Pause();
		void Stop();

		void AddAction(CBaseAction * action);
		void AddActions(CBaseAction * action,...);

		CActionQueue * SkipAction();

		CBaseAction * GetAction();
		void PopAction();

		bool IsEmpty();

		std::deque<CBaseAction*>& GetActions();

		virtual int Update();
	private:
		std::deque<CBaseAction*> m_Actions;
	};

#define ACTION_QUEUE CActionQueue
#define ACTION_QUEUE_VECTOR std::vector<ACTION_QUEUE*>
#define ACTION_QUEUE_ITERATOR ACTION_QUEUE_VECTOR::iterator



	class CActionHelper
	{
	public:
		/// @name Конструктор/деструктор		
		CActionHelper();
		~CActionHelper();   

		
		ACTION_QUEUE * CreateActionsQueue();
		void AddActionToQueue(ACTION_QUEUE * queue, CBaseAction * action);
		void AddActionsToQueue(ACTION_QUEUE * queue, ...);
		ACTION_QUEUE * SkipQueueAction(ACTION_QUEUE * queue);
		void StopQueue(ACTION_QUEUE * queue);

		bool IsActionActive(ACTION_QUEUE * queue);

		void LoadActionsFromNode(pugi::xml_node &node);
		void Update();
		void ClearActions();
	private:			
		ACTION_QUEUE_VECTOR m_ActionQueues;		
		ACTION_QUEUE_VECTOR m_LockedQueues;		
		bool m_Lock;
	};    
}
#endif
