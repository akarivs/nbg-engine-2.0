#ifndef FRAMEWORK_HELPERS_LUA_ACTIONS
#define FRAMEWORK_HELPERS_LUA_ACTIONS

#include <Framework/Datatypes/Vector.h>
#include <lua.hpp>

using namespace NBG;
namespace LUA_ACTIONS
{
	/** @brief Обёртка экшнов для LUA
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/	
	
	void Init(lua_State * s)
	{			
		/*ActionQueue*/
		SLB::Class<ACTION_QUEUE>("CActionQueue")			
			.constructor()
			.inherits<CBaseObject>()
			.set("Start",	&ACTION_QUEUE::Start)
			.set("Pause",	&ACTION_QUEUE::Pause)
			.set("Stop",	&ACTION_QUEUE::Stop)
			.set("AddAction",	&ACTION_QUEUE::AddAction);

		/*ActionQueue*/
		SLB::Class<VOID_CALLBACK>("VOID_CALLBACK")			
			.constructor();			

		/* ACTION HELPER */
		SLB::Class<CActionHelper>("CActionHelper")			
			.constructor()			
			.set("CreateActionsQueue",	&CActionHelper::CreateActionsQueue)
			.set("AddActionToQueue",	&CActionHelper::AddActionToQueue);			

		SLB::push(s,g_ActionHelper);		
		lua_setglobal(s, "g_ActionHelper");

		/* BASE ACTION*/		
		SLB::Class<CBaseAction>("CBaseAction")			
			.constructor();	 

		/* CALLBACK_LUA */
		SLB::Class<CCallbackLUAAction>("CCallbackAction")			
			.constructor()
			.inherits<CBaseAction>();

		/* HIDE */
		SLB::Class<CHideAction>("CHideAction")			
			.constructor<CBaseObject*, float,float>()
			.inherits<CBaseAction>();

		/* SHOW */
		SLB::Class<CShowAction>("CShowAction")			
			.constructor<CBaseObject*, float,float>()
			.inherits<CBaseAction>();

		/* SLEEP */
		SLB::Class<CSleepAction>("CSleepAction")			
			.constructor<float>()
			.inherits<CBaseAction>();

		/* MOVE BY */
		SLB::Class<CMoveByAction>("CMoveByAction")			
			.constructor<CBaseObject *, const float, Vector, const float>()
			.inherits<CBaseAction>();

		/* MOVE TO */
		SLB::Class<CMoveToAction>("CMoveToAction")			
			.constructor<CBaseObject *, const float, Vector, const float>()
			.inherits<CBaseAction>();

		/* MOVE TO */
		SLB::Class<CMoveToBezierAction>("CMoveToBezierAction")			
			.constructor<CBaseObject *, const float, Vector, Vector, const float>()
			.inherits<CBaseAction>();

		/* SCALE BY */
		SLB::Class<CScaleByAction>("CScaleByAction")			
			.constructor<CBaseObject *, const float, Vector, const float>()
			.inherits<CBaseAction>();

		/* SCALE TO */
		SLB::Class<CScaleToAction>("CScaleToAction")			
			.constructor<CBaseObject *, const float, Vector, const float>()
			.inherits<CBaseAction>();

		/* PLAY VIDEO */
		SLB::Class<CPlayVideoAction>("CPlayVideoAction")			
			.constructor<CBaseObject *, const bool>()
			.inherits<CBaseAction>();
	}
}

#endif ///FRAMEWORK_HELPERS_LUA
