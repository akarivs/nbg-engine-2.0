#ifndef FRAMEWORK_HELPERS_LUA_BASE
#define FRAMEWORK_HELPERS_LUA_BASE

#include <Framework/Datatypes/Vector.h>
#include <lua.hpp>

using namespace NBG;
namespace LUA_BASE
{
	/** @brief Обёртка базовых объектов для LUA
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/	

	class CGUILuaHelper
	{
	public:
		CGUILuaHelper(){};
		~CGUILuaHelper(){};
		
		CAnimatedObject * CastToAnimatedObject(CBaseObject  * obj)
		{
			return CAST(CAnimatedObject*, obj);
		}

		CParticleObject * CastToParticleObject(CBaseObject  * obj)
		{
			return CAST(CParticleObject*, obj);
		}

		CClip * CastToClip(CBaseObject  * obj)
		{
			return CAST(CClip*, obj);
		}
	};
	
	
	void Init(lua_State * s)
	{			
		/*CBaseObject*/
		SLB::Class<CBaseObject>("CBaseObject")			
			.constructor<std::string>()
			.set("GetChild",&CBaseObject::GetChild)
			.set("GetPosition",&CBaseObject::GetPosition)
			.set("SetPosition", ( void (CBaseObject::*)(const Vector&) )&CBaseObject::SetPosition)
			.set("SetPositionF", ( void (CBaseObject::*)(const float&,const float&,const float&) )&CBaseObject::SetPosition)
			.set("GetScale",&CBaseObject::GetScale)
			.set("SetScale", ( void (CBaseObject::*)(const Vector&) )&CBaseObject::SetScale)
			.set("SetScaleF", ( void (CBaseObject::*)(const float&,const float&,const float&) )&CBaseObject::SetScale)
			.set("GetSize",&CBaseObject::GetSize)
			.set("SetSize", ( void (CBaseObject::*)(const Vector&) )&CBaseObject::SetSize)
			.set("SetSizeF", ( void (CBaseObject::*)(const float&,const float&,const float&) )&CBaseObject::SetSize)
			.set("GetColor",&CBaseObject::GetColor)
			.set("SetColor", &CBaseObject::SetColor)
			.set("GetRotation",&CBaseObject::GetRotation)
			.set("SetRotation", &CBaseObject::SetRotation)
			.set("GetId",&CBaseObject::GetId)
			.set("GetHotSpot",&CBaseObject::GetHotSpot)
			.set("SetHotSpot", &CBaseObject::SetHotSpot)
			.set("IsContains", &CBaseObject::IsContains)
			.set("AddChild", &CBaseObject::AddChild)
			.set("RemoveChild", ( void (CBaseObject::*)(CBaseObject*) )&CBaseObject::RemoveChild)
			.set("RemoveAllChilds", &CBaseObject::RemoveAllChilds)
			.set("BringForward",  (void (CBaseObject::*)(void) )&CBaseObject::BringForward)
			.set("BringAfter", (void (CBaseObject::*)(CBaseObject*) )&CBaseObject::BringAfter)
			.set("BringBefore", (void (CBaseObject::*)(CBaseObject*) )&CBaseObject::BringBefore)
			.set("BringBackward", (void (CBaseObject::*)(void) )&CBaseObject::BringBackward)
			.set("SetLayer", &CBaseObject::SetLayer)
			.set("GetLayer", &CBaseObject::GetLayer)
			.set("Destroy", &CBaseObject::Destroy)
			.set("SetClickable", &CBaseObject::SetClickable)
			.set("SetDisabled", &CBaseObject::SetDisabled)
			.set("SetVisible", &CBaseObject::SetVisible)
			.set("DispatchEvent",&CBaseObject::DispatchEvent)
			.set("GetParent", &CBaseObject::GetParent);


		/*CAnimatedObject*/
		SLB::Class<CAnimatedObject>("CAnimatedObject")			
			.constructor<std::string>()
			.inherits<CBaseObject>()
			.set("Play", &CAnimatedObject::Play)
			.set("Pause", &CAnimatedObject::Pause)
			.set("Init", ( void (CAnimatedObject::*)(const std::string,const int,const float,const std::string,const int) )&CAnimatedObject::Init)
			.set("SetCallback", &CAnimatedObject::SetCallbackLua)
			.set("Stop", &CAnimatedObject::Stop);

		/*CParticleObject*/
		SLB::Class<CParticleObject>("CParticleObject")			
			.constructor<std::string>()
			.inherits<CBaseObject>()
			.set("Play", &CParticleObject::Play)
			.set("Shoot", &CParticleObject::Shoot)
			.set("Init", ( void (CParticleObject::*)(const std::string,const bool,const float) )&CParticleObject::Init)			
			.set("Stop", &CParticleObject::Stop);


		/*CAnimatedObject*/
		SLB::Class<CGraphicsObject>("CGraphicsObject")			
			.constructor<std::string>()
			.inherits<CBaseObject>()
			.set("Init", &CGraphicsObject::Init)
			.set("SetPosition", ( void (CGraphicsObject::*)(const float&,const float&,const float&) )&CGraphicsObject::SetPosition)
			.set("SetVisible", &CGraphicsObject::SetVisible);			


		/*CAnimatedObject*/
		SLB::Class<CClip>("CClip")			
			.constructor<std::string>()
			.inherits<CBaseObject>()
			.set("Play", &CClip::Play)			
			.set("Stop", &CClip::Stop);


		/*CGUILuaHelper*/
		SLB::Class<CGUILuaHelper>("CGUILuaHelper")			
			.constructor()			
			.set("CastToAnimatedObject", &CGUILuaHelper::CastToAnimatedObject)
			.set("CastToParticleObject",&CGUILuaHelper::CastToParticleObject)
			.set("CastToClip", &CGUILuaHelper::CastToClip);

		CGUILuaHelper guiLuaHelper;		
		SLB::push(s,&guiLuaHelper);
		lua_setglobal(s, "g_GuiHelper");

		/*CBaseScene*/
		SLB::Class<CBaseScene>("CBaseScene")			
			.constructor<std::string>()
			.inherits<CBaseObject>();

		/*CTimeManager*/
		SLB::Class<CTimeManager>("CTimeManager")			
			.constructor()
			.set("ReadTimer", &CTimeManager::ReadTimer);

		SLB::push(s,g_TimeManager);		
		lua_setglobal(s, "g_TimeManager");


		/*CSoundManager*/
		SLB::Class<CSoundManager>("CSoundManager")			
			.constructor()
			.set("Play", &CSoundManager::Play)
			.set("Stop", &CSoundManager::Stop);

		SLB::push(s,g_SoundManager);		
		lua_setglobal(s, "g_SoundManager");


		/*CRandom*/
		SLB::Class<CRandom>("CRandom")			
			.constructor()
			.set("RandB", &CRandom::RandB)
			.set("RandI", &CRandom::RandI)
			.set("RandF", &CRandom::RandF);

		SLB::push(s,g_Random);		
		lua_setglobal(s, "g_Random");	
	}
}

#endif ///FRAMEWORK_HELPERS_LUA
