#ifndef FRAMEWORK_HELPERS_LUA_DATATYPES
#define FRAMEWORK_HELPERS_LUA_DATATYPES

#include <Framework/Datatypes/Vector.h>
#include <lua.hpp>

using namespace NBG;
namespace LUA_DATATYPES
{
	/** @brief Обёртка вектора для LUA
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/		
	
	void Init(lua_State * s)
	{	
		/* VECTOR */
		SLB::Class<Vector>("Vector")			
			.constructor()
			.constructor<float, float>()
			.constructor<float, float, float>()
			.set("Normalize", &Vector::Normalize)
			.set("DotProduct", &Vector::DotProduct)
			.set("GetLength", &Vector::GetLength)
			.property("x",&Vector::x)
			.property("y",&Vector::y)
			.property("z",&Vector::z);

		/* COLOR */
		SLB::Class<FloatColor>("FloatColor")			
			.constructor()
			.constructor<float, float, float, float>()
			.constructor<unsigned long>()
			.constructor<const std::string>()			
			.set("SetColor", &FloatColor::SetColor)
			.set("Normalize", &FloatColor::Normalize)
			.set("GetPackedColor", &FloatColor::GetPackedColor)
			.property("a",&FloatColor::a)
			.property("r",&FloatColor::r)
			.property("g",&FloatColor::g)	
			.property("b",&FloatColor::b);	


		/* COLOR */
		SLB::Class<Color>("Color")			
			.constructor()
			.constructor<float, float, float, float>()
			.constructor<unsigned long>()
			.constructor<const std::string>()			
			.set("SetColor", &Color::SetColor)
			.set("Normalize", &Color::Normalize)
			.set("GetPackedColor", &Color::GetPackedColor)
			.set("GetA", &Color::GetA)
			.set("GetR", &Color::GetR)
			.set("GetG", &Color::GetG)
			.set("GetB", &Color::GetB)
			.set("SetA", &Color::SetA)
			.set("SetR", &Color::SetR)
			.set("SetG", &Color::SetG)
			.set("SetB", &Color::SetB);

		/* EVENT */
		SLB::Class<Event>("Event")						
			.constructor<const std::string, void *>();

	}
}

#endif ///FRAMEWORK_HELPERS_LUA_DATATYPES
