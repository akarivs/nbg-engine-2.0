#include "LuaHelper.h"

#include <Framework.h>

#include "LuaHelper/ActionsLua.h"
#include "LuaHelper/BaseLua.h"
#include "LuaHelper/DatatypesLua.h"

#include <lualib.h>

namespace NBG
{   	
	
	CLuaHelper * CLuaHelper::self = NULL;
	//////////////////////////////////////////////////////////////////////////
	CLuaHelper::CLuaHelper()
	{			
		self = this;		
	}

	//////////////////////////////////////////////////////////////////////////
	CLuaHelper::~CLuaHelper()
	{
		lua_close(m_LuaState);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLuaHelper::Init()
	{
		m_LuaState = luaL_newstate();		
		luaL_Reg lualibs[] =
		{
			{ "math", luaopen_math },
			{ "base", luaopen_base },			
			{ NULL, NULL}
		};
		
		const luaL_Reg *lib = lualibs;
		for(; lib->func != NULL; lib++)
		{	
			lib->func(m_LuaState);
			lua_settop(m_LuaState, 0);
		}
		luaL_openlibs(m_LuaState);
		SLB::Manager::defaultManager()->registerSLB(m_LuaState);					

		LUA_ACTIONS::Init(m_LuaState);
		LUA_BASE::Init(m_LuaState);
		LUA_DATATYPES::Init(m_LuaState);
		m_CurrentObject = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	void CLuaHelper::OnSceneChange(CBaseScene * scene)
	{
		SLB::push(m_LuaState,scene);		
		lua_setglobal(m_LuaState, "g_Scene");
	}

	//////////////////////////////////////////////////////////////////////////
	void CLuaHelper::RunScript(CBaseObject * obj, const std::string &script)
	{
		SLB::push(m_LuaState,obj);		
		lua_setglobal(m_LuaState, "this");
		m_CurrentObject = obj;
		int status = luaL_dostring(m_LuaState, script.c_str());		
		ReportErrors(m_LuaState,status);		
	}

	//////////////////////////////////////////////////////////////////////////
	void CLuaHelper::ReportErrors(lua_State *l,const int status)
	{
		if ( status!=0 ) {
			std::cerr << "-- " << lua_tostring(l, -1) << std::endl;
			lua_pop(l, 1); // remove error message		
		}
	}


	//////////////////////////////////////////////////////////////////////////
	CBaseObject * CLuaHelper::GetCurrentObject()
	{
		return m_CurrentObject;
	}	
}
