#ifndef _AK_FRAMEWORK_EDITION_HELPER
#define _AK_FRAMEWORK_EDITION_HELPER

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include <string>
#include <map>

namespace NBG
{
	/** @brief Класс, реализующий работу с разными сборками проекта.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CEditionHelper
	{
	public:
		/// @name Конструктор/деструктор		
		CEditionHelper();
		~CEditionHelper();        		

		/// Инициализация
		void Init(const std::string &path, const std::string &editionsPath);

		/// Получение текущей сборки.
		std::string GetEdition(){return m_Edition;};

		/// Сконвертировать путь к ресурсу в путь текущей сборки.
		std::string ConvertPath(const std::string &path);
	private:		
		/// Загрузить карту замены файлов.
		void LoadResourcesMap();

		/// Карта замены.
		std::map<std::string, std::string>m_ResMap;            

		/// Сборка
		std::string m_Edition;

		/// Путь к сборкам
		std::string m_EditionsPath;
	};    
}
#endif
