#ifndef _AK_FRAMEWORK_ATLAS_HELPER
#define _AK_FRAMEWORK_ATLAS_HELPER

#include <Framework/Structures.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include <string>
#include <map>

namespace NBG
{
	/** @brief Класс, реализующий работу с атласами.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/

    class CAtlasHelper
    {
        public:
			/// @name Конструктор/деструктор.            
			CAtlasHelper();
            ~CAtlasHelper();



			/// Получить ID атласа.
            std::string GetAtlasID(const std::string &texture);

			/// Получить текстурные координаты объекта.
            NBG::FRect GetUV(const std::string &texture);

			/// Получить размер объекта.
            NBG::Vector GetSize(const std::string &texture);

			/// Получить описание текстуры.
            AtlasTextureDescription * GetTextureDescription(const std::string &texture);

            /// Загрузить все атласы.
			void LoadAtlases(const std::string &path);

			/// Предзагрузить ресурсы текстурные.
            void PreloadResource(const std::string &name);

			/// Добавляет в карту объекты атласа
            void AddAtlasesXML(const std::string &basePath, const std::string &xml,const std::string &name); 
        private:



			/// Карта текстур в атласах.
            std::map<std::string, AtlasTextureDescription>m_Textures;

			/// Карта атласов и их текстур.
            std::map<std::string, std::map<std::string, std::string> > m_AtlasTextures;
    };    
}

#endif
