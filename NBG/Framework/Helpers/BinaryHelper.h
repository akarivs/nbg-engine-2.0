#ifndef FRAMEWORK_HELPERS_BINARY
#define FRAMEWORK_HELPERS_BINARY

#include <Framework/Datatypes.h>

namespace NBG
{
	/** @brief Класс, реализующий работу с бинарными данными..
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CBinaryHelper
	{
	public:
		/// @name Конструктор/деструктор
		CBinaryHelper();
		~CBinaryHelper();

		/// @name Перегруженные функции записи в буффер.
		void Write(char * buffer, bool data, int &offset)
		{
			Write(buffer,&data,offset,sizeof(bool));
		}
		void Write(char * buffer, int data, int &offset)
		{
			Write(buffer,&data,offset,sizeof(int));
		}
		void Write(char * buffer, float data, int &offset)
		{
			Write(buffer,&data,offset,sizeof(float));
		}

		void Write(char * buffer, void * data, int &offset, const int size)
		{
			memcpy(&buffer[offset],data,size); offset+=size;
		}

		/// @name Перегруженные функции чтения из буффера.
		void Read(char * buffer, bool &data, int &offset)
		{
			Read(buffer,&data,offset,sizeof(bool));
		}
		void Read(char * buffer, int &data, int &offset)
		{
			Read(buffer,&data,offset,sizeof(int));
		}
		void Read(char * buffer, float &data, int &offset)
		{
			Read(buffer,&data,offset,sizeof(float));
		}

		void Read(char * buffer, void * data, int &offset, const int size)
		{
			memcpy(data,&buffer[offset], size); offset+=size;
		}
	private:				
	};    
}

#endif //FRAMEWORK_HELPERS_BINARY
