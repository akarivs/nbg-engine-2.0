#ifndef FRAMEWORK_HELPERS_LUA
#define FRAMEWORK_HELPERS_LUA

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Mesh.h>

#include <../External/lua/src/SLB.hpp>

#include <Framework/GUI/BaseObject.h>
#include <Framework/BaseScene.h>

#include <lua.hpp>

namespace NBG
{
	/** @brief Класс, реализующий помощь в работе со скриптами.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CLuaHelper
	{
	public:
		/// @name Конструктор деструктор
		CLuaHelper();
		~CLuaHelper();	

		void Init();

		static CLuaHelper * self;

		CBaseObject * GetCurrentObject();
		void ReportErrors(lua_State *l,const int status);
		lua_State * GetLuaState()
		{
			return m_LuaState;
		}

		void OnSceneChange(CBaseScene * scene);


		void RunScript(CBaseObject * obj, const std::string &script);

		template <class Type>
		void PushObject(Type * obj, const std::string &name)
		{
			SLB::push(m_LuaState,obj);		
			lua_setglobal(m_LuaState, name.c_str());				
		}		

	private:		
		CBaseObject * m_CurrentObject;
		lua_State *m_LuaState;
	};    
}

#endif ///FRAMEWORK_HELPERS_LUA
