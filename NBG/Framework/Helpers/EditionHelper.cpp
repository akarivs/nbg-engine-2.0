#include "EditionHelper.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CEditionHelper::CEditionHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CEditionHelper::~CEditionHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CEditionHelper::Init(const std::string &path, const std::string &editionsPath)
	{
		NBG::CConfig conf;
		conf.LoadConfig(path);
		m_Edition = "";
		conf.GetValue("edition",m_Edition);
		if (m_Edition.empty())m_Edition="desktop_paid";

		m_EditionsPath = editionsPath;

		LoadResourcesMap();        
	}

	//////////////////////////////////////////////////////////////////////////
	std::string CEditionHelper::ConvertPath(const std::string &path)
	{
		if (m_ResMap.find(path) != m_ResMap.end())
		{
			return m_ResMap[path];
		}
		return path;
	}

	//////////////////////////////////////////////////////////////////////////
	void CEditionHelper::LoadResourcesMap()
	{
		std::string xml =m_EditionsPath+"/"+m_Edition+"/resources_map.xml";
		CXMLResource * xmlRes = (CXMLResource*)g_ResManager->GetResource(xml);
		xml_document* doc_level = xmlRes->GetXML();
		pugi::xml_node node_resources = doc_level->first_child();       

		for (pugi::xml_node resource = node_resources.child("resource"); resource; resource = resource.next_sibling("resource"))
		{
			m_ResMap[resource.attribute("path").value()] = resource.attribute("real_path").value();
		}
		g_ResManager->ReleaseResource(xmlRes);
	}  
}

