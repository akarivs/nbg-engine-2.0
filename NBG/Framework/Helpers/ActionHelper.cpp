#include "ActionHelper.h"
#include <Framework.h>

namespace NBG
{   
	//////////////////////////////////////////////////////////////////////////
	CActionQueue::CActionQueue():CBaseObject("ActionQueue")
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CActionQueue::~CActionQueue()
	{
		m_Actions.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::Start()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::Pause()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::Stop()
	{	
		CBaseAction * action;		
		while(IsEmpty() == false)
		{
			action = GetAction();
			action->Stop();
			PopAction();
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::AddAction(CBaseAction * action)
	{
		m_Actions.push_back(action);
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::AddActions(CBaseAction * action,...)
	{
		m_Actions.push_back(action);

		va_list ap; 
		va_start(ap,action);
		CBaseAction * act = NULL;
		do 
		{
			act = va_arg(ap,CBaseAction*);
			if (act)
			{
				m_Actions.push_back(act);
			}
		} while (act != NULL);				
		va_end(ap); 
	}

	//////////////////////////////////////////////////////////////////////////
	CActionQueue * CActionQueue::SkipAction()
	{
		CBaseAction * action = GetAction();
		action->Stop();
		PopAction();
		if (IsEmpty())
		{
			Destroy();
			return NULL;
		}
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CBaseAction * CActionQueue::GetAction()
	{
		return m_Actions.front();
	}

	//////////////////////////////////////////////////////////////////////////
	std::deque<CBaseAction*>& CActionQueue::GetActions()
	{
		return m_Actions;
	}
	
	//////////////////////////////////////////////////////////////////////////
	void CActionQueue::PopAction()
	{
		m_Actions.pop_front();
	}

	//////////////////////////////////////////////////////////////////////////
	bool CActionQueue::IsEmpty()
	{
		return m_Actions.size() == 0;
	}

	//////////////////////////////////////////////////////////////////////////
	int CActionQueue::Update()
	{
		int res = CBaseObject::Update();
		if (res > EventSkipDontProcess)return res;

		if (IsEmpty())
		{			
			return EventSkip;
		}

		auto queueIter = m_Actions.begin();

		CBaseAction * action = *queueIter;
		if (action->IsStarted() == false)
		{
			action->Start();								
		}	

		bool needContinue = false;
		while (action->IsComplete())
		{
			if (action->IsEnded())
			{
				bool needToNewUpdate = false;
				if (action->GetActionTime() == 0.0f)
				{
					needToNewUpdate = true;
				}
				queueIter = m_Actions.erase(queueIter);
				if (IsEmpty())
				{	
					needContinue = true;
				}		
				else
				{
					if (needToNewUpdate)
					{
						Update();
					}
				}			

				break;
			}
			else
			{
				action->Update();
				queueIter++;
				if (queueIter == m_Actions.end())
				{
					needContinue = false;
					break;
				}
				else
				{
					action = *queueIter;
					if (action->IsStarted() == false)
					{
						action->Start();								
					}	
				}					
			}
		}
		if (needContinue)
		{
			return EventSkip;
		}			
		action->Update();
		return EventSkip;
	}


	//////////////////////////////////////////////////////////////////////////
	CActionHelper::CActionHelper()
	{
		m_Lock = false;
	}

	//////////////////////////////////////////////////////////////////////////
	CActionHelper::~CActionHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	ACTION_QUEUE * CActionHelper::CreateActionsQueue()
	{
		return new ACTION_QUEUE();		
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::AddActionToQueue(ACTION_QUEUE * queue, CBaseAction * action)
	{
		queue->AddAction(action);
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::AddActionsToQueue(ACTION_QUEUE * queue, ...)
	{
		va_list ap; 
		va_start(ap,queue);
		CBaseAction * act = NULL;
		do 
		{
			act = va_arg(ap,CBaseAction*);
			if (act)
			{
				AddActionToQueue(queue,act);
			}
		} while (act != NULL);				
		va_end(ap); 
	}

	//////////////////////////////////////////////////////////////////////////
	ACTION_QUEUE * CActionHelper::SkipQueueAction(ACTION_QUEUE * queue)
	{
		ACTION_QUEUE_ITERATOR iter = m_ActionQueues.begin(); 
		bool found = false;
		while (iter != m_ActionQueues.end())
		{
			ACTION_QUEUE * _queue = (*iter);
			if (queue == _queue )
			{
				found = true;
				break;
			}
			iter++;
		}					
		if (found == false || queue == NULL)return NULL;
		CBaseAction * action = queue->GetAction();
		action->Stop();
		queue->PopAction();
		if (queue->IsEmpty())
		{
			delete queue;
			queue = NULL;
		}
		return queue;
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::StopQueue(ACTION_QUEUE * queue)
	{
		ACTION_QUEUE_ITERATOR iter = m_ActionQueues.begin(); 
		bool found = false;
		while (iter != m_ActionQueues.end())
		{
			ACTION_QUEUE * _queue = (*iter);
			if (queue == _queue )
			{
				found = true;
				break;
			}
			iter++;
		}					
		if (found == false || queue == NULL)return;
		CBaseAction * action = queue->GetAction();
		action->Stop();
		queue->PopAction();
		while(queue->IsEmpty() == false)
		{
			action = queue->GetAction();
			action->Stop();
			queue->PopAction();
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	bool CActionHelper::IsActionActive(ACTION_QUEUE * queue)
	{
		ACTION_QUEUE_ITERATOR iter = m_ActionQueues.begin(); 
		bool found = false;
		while (iter != m_ActionQueues.end())
		{
			ACTION_QUEUE * _queue = (*iter);
			if (queue == _queue )
			{
				found = true;
				break;
			}
			iter++;
		}					
		if (found == false || queue == NULL)return false;
		return true;
	}
	

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::LoadActionsFromNode(pugi::xml_node &node)
	{		
		for (pugi::xml_node actions = node.child("actions"); actions; actions = actions.next_sibling("actions"))
		{
			ACTION_QUEUE * queue = new ACTION_QUEUE();
			for (pugi::xml_node action = actions.child("action"); action; action = action.next_sibling("action"))
			{
				CBaseAction * actionObj = g_ActionsFactory->create(action.attribute("type").value());
				///���� ������ ������� ����
				if (actionObj)
				{
					///���� ������ ���������������� ����
					if (actionObj->LoadFromXMLNode(action))
					{
						/// ��������� ���� � �������
						queue->AddAction(actionObj);
					}
				}			
			}
			///���� �������� ���� �� ���� ���� � �������
			if (queue->IsEmpty() == false)
			{
				if (m_Lock)
				{
					m_LockedQueues.push_back(queue);					
				}
				else
				{
					m_ActionQueues.push_back(queue);					
				}		
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::ClearActions()
	{
		m_ActionQueues.clear();		
	}

	//////////////////////////////////////////////////////////////////////////
	void CActionHelper::Update()
	{
		
	}
}