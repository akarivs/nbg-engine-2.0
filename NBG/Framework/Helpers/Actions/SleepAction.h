#ifndef _AK_FRAMEWORK_ACTION_SLEEP
#define _AK_FRAMEWORK_ACTION_SLEEP

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - пауза
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CSleepAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CSleepAction();
		CSleepAction(const float blockTime);
		~CSleepAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(const float blockTime);
		virtual void Start();
	protected:					
		virtual void VirtualUpdate();		
	};    
}
#endif //_AK_FRAMEWORK_ACTION_SLEEP
