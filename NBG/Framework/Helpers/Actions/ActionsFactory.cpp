#include "ActionsFactory.h"

#include "HideAction.h"
#include "ShowAction.h"
#include "SleepAction.h"
#include "DisableAction.h"
#include "EnableAction.h"
#include "PlaySoundAction.h"

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CActionFactory::CActionFactory()
	{
		add<CHideAction>("hide");
		add<CShowAction>("show");
		add<CSleepAction>("sleep");
		add<CDisableAction>("disable");
		add<CEnableAction>("enable");
		add<CEnableAction>("play_sound");
	}
}