#ifndef _AK_FRAMEWORK_ACTIONS_FACTORY
#define _AK_FRAMEWORK_ACTIONS_FACTORY

#include <Framework/Datatypes.h>
#include "BaseAction.h"

#include <map>

namespace NBG
{
	class CBaseObject;

	/** @brief Фабрика, помогающая создать класс действия по его строковому имени
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class __abstractActionCreator
	{
	public:
		virtual void actionCreator() {}
		virtual CBaseAction * create() const = 0;
	};

	template <class C>
	class __actionCreator : public __abstractActionCreator
	{
	public:
		virtual CBaseAction * create() const { return new C(); }
	};

	class CActionFactory
	{
	protected:
		typedef std::map<std::string, __abstractActionCreator*> FactoryMap;
		FactoryMap _factory;

	public:
		CActionFactory();
		virtual ~CActionFactory(){};


		template <class C>
		void add(const std::string & id)
		{
			typename FactoryMap::iterator it = _factory.find(id);
			if (it == _factory.end())
			{
				_factory[id] = new __actionCreator<C>();
			}
		}

		CBaseAction * create(const std::string & id)
		{
			FactoryMap::iterator it = _factory.find(id);
			if (it != _factory.end())
				return it->second->create();
			std::string error = "Action is not registered! '";
			error += id;
			error += "'";			
			NBG_Assert(0,error);
			return NULL;
		}
	};
}
#endif //_AK_FRAMEWORK_ACTIONS_FACTORY




