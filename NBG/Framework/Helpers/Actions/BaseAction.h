#ifndef _AK_FRAMEWORK_ACTION_BASE
#define _AK_FRAMEWORK_ACTION_BASE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include <Framework/GUI/BaseObject.h>

#include <../xml/pugixml.hpp>

#include <queue>


namespace NBG
{
	/** @brief Класс, реализующий работу с цепочками действий, которые можно назначить из xml.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CBaseAction();
		~CBaseAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node){return true;};
		virtual void Start();		
		virtual void Stop();

		float GetActionTime()
		{
			return m_ActionTime;
		};

		///Передать ли управление следующему экшну
		bool IsComplete()
		{
			return m_IsComplete;
		};

		///Завершился ли полностью апдейт экшна (даже если передали управление другому, этот ещё нужно апдейтить)
		virtual bool IsEnded()
		{
			return true;
		}



		bool IsStarted()
		{
			return m_IsStarted;
		};


		void Update();
	protected:			
		virtual void VirtualUpdate(){};
		CBaseObject * GetSceneObject(const std::string &id);
		float m_Timer;
		float m_ActionTime;
		
		bool m_IsComplete;
		bool m_IsStarted;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_BASE
