#include "SleepAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CSleepAction::CSleepAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CSleepAction::CSleepAction(const float blockTime):CBaseAction()
	{
		Init(blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CSleepAction::~CSleepAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CSleepAction::LoadFromXMLNode(pugi::xml_node &node)
	{	
		return Init(node.attribute("time").as_float());				
	}

	//////////////////////////////////////////////////////////////////////////
	bool CSleepAction::Init(const float blockTime)
	{
		m_ActionTime = blockTime;		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CSleepAction::Start()
	{		
		CBaseAction::Start();		

	}	
	
	//////////////////////////////////////////////////////////////////////////
	void CSleepAction::VirtualUpdate()
	{
		
	}	
}

