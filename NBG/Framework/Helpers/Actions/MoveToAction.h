#ifndef _AK_FRAMEWORK_ACTION_MOVE_TO
#define _AK_FRAMEWORK_ACTION_MOVE_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - переместить объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CMoveToAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CMoveToAction();
		CMoveToAction(CBaseObject * object, const float time, Vector position, const float blockTime = -1.0f);
		~CMoveToAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, Vector position, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;		
		Vector m_Position;
		Vector m_StartPosition;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_MOVE_TO
