#ifndef _AK_FRAMEWORK_ACTION_ENABLE
#define _AK_FRAMEWORK_ACTION_ENABLE

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - заблокировать объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CEnableAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CEnableAction();
		~CEnableAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		virtual void Start();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_ENABLE
