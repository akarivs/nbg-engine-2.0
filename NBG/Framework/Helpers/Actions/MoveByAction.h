#ifndef _AK_FRAMEWORK_ACTION_MOVE_BY
#define _AK_FRAMEWORK_ACTION_MOVE_BY


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - переместить объект на какое-то значение 
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CMoveByAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CMoveByAction();
		CMoveByAction(CBaseObject * object, const float time, Vector position, const float blockTime = -1.0f);
		~CMoveByAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, Vector position, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;		
		Vector m_Position;
		Vector m_StartPosition;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_MOVE_BY
