#include "CallbackAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CCallbackAction::CCallbackAction():CBaseAction()
	{
		
	}

	//////////////////////////////////////////////////////////////////////////
	CCallbackAction::CCallbackAction(VOID_CALLBACK callback):CBaseAction()
	{
		Init(callback);		
	}

	//////////////////////////////////////////////////////////////////////////
	CCallbackAction::CCallbackAction(VOID_DATA_CALLBACK callback, void * data):CBaseAction()
	{
		Init(callback, data);		
	}

	//////////////////////////////////////////////////////////////////////////
	CCallbackAction::~CCallbackAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CCallbackAction::LoadFromXMLNode(pugi::xml_node &node)
	{	
		CONSOLE("Error: cannot load callback action from XML. Call it directly!");
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CCallbackAction::Init(VOID_CALLBACK callback)
	{
		m_Callback = callback;	
		m_Timer = 0.0f;
		m_ActionTime = 0.0f;		
		m_CallbackType = Callback_Void;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CCallbackAction::Init(VOID_DATA_CALLBACK callback, void * data)
	{
		m_DataCallback = callback;	
		m_Data = data;
		m_Timer = 0.0f;
		m_ActionTime = 0.0f;	
		m_CallbackType = Callback_VoidData;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CCallbackAction::Start()
	{
		CBaseAction::Start();	
		if (m_CallbackType == Callback_Void)
		{
			m_Callback();
		}
		else if (m_CallbackType == Callback_VoidData)
		{
			m_DataCallback(m_Data);
		}
	}	
	
	//////////////////////////////////////////////////////////////////////////
	void CCallbackAction::VirtualUpdate()
	{
		
	}	
}

