#ifndef _AK_FRAMEWORK_ACTION_CALLBACK_LUA
#define _AK_FRAMEWORK_ACTION_CALLBACK_LUA

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - вызвать функцию из LUA
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CCallbackLUAAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CCallbackLUAAction();
		~CCallbackLUAAction();		
		            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init();		
		virtual void Start();
	protected:					
		virtual void VirtualUpdate();		
		int m_CallbackId;		
	};    
}
#endif //_AK_FRAMEWORK_ACTION_CALLBACK
