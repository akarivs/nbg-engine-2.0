#include "PlayVideoAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CPlayVideoAction::CPlayVideoAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CPlayVideoAction::CPlayVideoAction(CBaseObject * object, bool waitForComplete)
	{
		Init(object, waitForComplete);
	}


	//////////////////////////////////////////////////////////////////////////
	CPlayVideoAction::~CPlayVideoAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CPlayVideoAction::LoadFromXMLNode(pugi::xml_node &node)
	{	
		//Init(node.attribute("sound").value(), node.attribute("time").as_float());		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayVideoAction::Init(CBaseObject * object, bool waitForComplete)
	{
		m_Object = CAST(CVideoObject*,object);
		if (waitForComplete)
		{
			m_ActionTime = m_Object->GetDuration()*2.0f;
			m_Object->SetCallback(CLOSURE_THIS(CPlayVideoAction::Stop));
		}
		else
		{
			m_ActionTime = 0.0f;
		}		
	}



	//////////////////////////////////////////////////////////////////////////
	void CPlayVideoAction::Start()
	{		
		CBaseAction::Start();	
		m_Object->Stop();
		m_Object->Play();
	}	

	//////////////////////////////////////////////////////////////////////////
	void CPlayVideoAction::Stop()
	{
		m_IsComplete = true;		
	}
	
	//////////////////////////////////////////////////////////////////////////
	void CPlayVideoAction::VirtualUpdate()
	{
		
	}	
}

