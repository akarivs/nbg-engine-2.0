#include "BaseAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CBaseAction::CBaseAction()
	{
		m_IsComplete = false;
		m_IsStarted = false;
	}

	//////////////////////////////////////////////////////////////////////////
	CBaseAction::~CBaseAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CBaseAction::Start()
	{
		m_Timer = 0.0f;   
		m_IsStarted = true;
	}	

	//////////////////////////////////////////////////////////////////////////
	void CBaseAction::Stop()
	{
		
	}	

	//////////////////////////////////////////////////////////////////////////
	CBaseObject * CBaseAction::GetSceneObject(const std::string &id)
	{
		CBaseScene * scene = g_ScenesManager->GetCurrentScene();
		STRING_VECTOR subIds = StringUtils::ExplodeString(id,'/');
		
		CBaseObject * parent = CAST(CBaseObject*,scene);
		for (int i=0; i<subIds.size(); i++)
		{
			parent = parent->GetChild(subIds[i]);
			if (!parent)
			{
				return NULL;
			}
		}
		return parent;
	}

	//////////////////////////////////////////////////////////////////////////
	void CBaseAction::Update()
	{		
		m_Timer += g_FrameTime;   
		if (m_Timer >= m_ActionTime)
		{
			m_IsComplete = true;
		}				
		VirtualUpdate();
	}	
}

