#include "DisableAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CDisableAction::CDisableAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CDisableAction::~CDisableAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CDisableAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		m_Object = GetSceneObject(node.attribute("object_id").value());
		if (!m_Object)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
			return false;
		}		
		m_ActionTime = 0.0f;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CDisableAction::Start()
	{
		CBaseAction::Start();
		m_Object->SetDisabled(true);		
	}	

	
	//////////////////////////////////////////////////////////////////////////
	void CDisableAction::VirtualUpdate()
	{
		
	}	
}

