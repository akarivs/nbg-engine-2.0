#include "ScaleByAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CScaleByAction::CScaleByAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CScaleByAction::CScaleByAction(CBaseObject * object, const float time, Vector scale, const float blockTime):CBaseAction()
	{
		Init(object,time,scale, blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CScaleByAction::~CScaleByAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CScaleByAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		Vector scale = Vector(node.attribute("x").as_float(),node.attribute("y").as_float());
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()),			
			 node.attribute("time").as_float(),
			 scale,
			 node.attribute("block_time").as_float()
			);
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CScaleByAction::Init(CBaseObject * object, const float time, Vector scale, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		m_Scale = scale;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CScaleByAction::Start()
	{
		CBaseAction::Start();

		Vector scale = m_Object->GetScale();
		if (m_Time == 0.0f)
		{
			m_Object->SetScale(scale+m_Scale);
		}
		else
		{
			m_StartScale = m_Object->GetScale();			
		}
	}	

	//////////////////////////////////////////////////////////////////////////
	void CScaleByAction::Stop()
	{
		m_Object->SetScale(m_StartScale+m_Scale);
	}	

	//////////////////////////////////////////////////////////////////////////
	bool CScaleByAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}

	//////////////////////////////////////////////////////////////////////////
	void CScaleByAction::VirtualUpdate()
	{
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;

		auto tween = CTweener::GetTween(EASE_LINEAR);	
		Vector scale = Vector(tween(m_UpdateTimer,m_StartScale.x,m_Scale.x,m_Time),
			tween(m_UpdateTimer,m_StartScale.y,m_Scale.y,m_Time));
		m_Object->SetScale(scale);
	}	
}

