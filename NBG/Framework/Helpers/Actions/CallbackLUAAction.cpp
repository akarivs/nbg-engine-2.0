#include "CallbackLUAAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CCallbackLUAAction::CCallbackLUAAction():CBaseAction()
	{
		m_CallbackId = -1;
		m_ActionTime = 0.0f;
		Init();	
	}

	
	//////////////////////////////////////////////////////////////////////////
	CCallbackLUAAction::~CCallbackLUAAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CCallbackLUAAction::LoadFromXMLNode(pugi::xml_node &node)
	{	
		CONSOLE("Error: cannot load callback action from XML. Call it directly!");
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CCallbackLUAAction::Init()
	{
		//save function for later use 		
		m_CallbackId = luaL_ref(g_LuaHelper->GetLuaState(),LUA_REGISTRYINDEX);			
		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	void CCallbackLUAAction::Start()
	{
		CBaseAction::Start();			
		if (m_CallbackId != -1)
		{			
			//retrive function and call it 
			lua_rawgeti(g_LuaHelper->GetLuaState(),LUA_REGISTRYINDEX,m_CallbackId);		
			int error = lua_pcall(g_LuaHelper->GetLuaState(), 0, 0, 0);
			if(error)
			{
				CONSOLE("%s",lua_tostring(g_LuaHelper->GetLuaState(), -1));
				lua_pop(g_LuaHelper->GetLuaState(), 1);	
			}
			//luaL_unref(g_LuaHelper->GetLuaState(), LUA_REGISTRYINDEX, m_CallbackId);
		}		
	}	
	
	//////////////////////////////////////////////////////////////////////////
	void CCallbackLUAAction::VirtualUpdate()
	{
		
	}	
}

