#ifndef _AK_FRAMEWORK_ACTION_CALLBACK
#define _AK_FRAMEWORK_ACTION_CALLBACK

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - вызвать функцию
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CCallbackAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CCallbackAction();
		CCallbackAction(VOID_CALLBACK callback);
		CCallbackAction(VOID_DATA_CALLBACK callback, void * data);
		~CCallbackAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(VOID_CALLBACK callback);
		bool Init(VOID_DATA_CALLBACK callback, void * data);
		virtual void Start();
	protected:					
		virtual void VirtualUpdate();		
		VOID_CALLBACK m_Callback;
		VOID_DATA_CALLBACK m_DataCallback;
		void * m_Data;

		enum CallbackType
		{
			Callback_Void,
			Callback_VoidData
		};
		CallbackType m_CallbackType;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_CALLBACK
