#ifndef _AK_FRAMEWORK_ACTION_MOVE_TO_BEZIER
#define _AK_FRAMEWORK_ACTION_MOVE_TO_BEZIER


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - переместить объект по кривой безье
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CMoveToBezierAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CMoveToBezierAction();
		CMoveToBezierAction(CBaseObject * object, const float time, Vector position, Vector bezier, const float blockTime = -1.0f);
		~CMoveToBezierAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, Vector position, Vector bezier, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;		
		int m_TweenId;
		Vector m_Position;
		Vector m_Bezier;
		Vector m_StartPosition;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_MOVE_TO_BEZIER
