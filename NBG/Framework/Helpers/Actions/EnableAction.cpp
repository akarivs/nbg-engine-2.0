#include "EnableAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CEnableAction::CEnableAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CEnableAction::~CEnableAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CEnableAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		m_Object = GetSceneObject(node.attribute("object_id").value());
		if (!m_Object)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
			return false;
		}		
		m_ActionTime = 0.0f;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CEnableAction::Start()
	{
		CBaseAction::Start();
		m_Object->SetDisabled(false);		
	}	

	
	//////////////////////////////////////////////////////////////////////////
	void CEnableAction::VirtualUpdate()
	{
		
	}	
}

