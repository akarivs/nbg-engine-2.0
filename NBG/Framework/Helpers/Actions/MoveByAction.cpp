#include "MoveByAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CMoveByAction::CMoveByAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CMoveByAction::CMoveByAction(CBaseObject * object, const float time, Vector position, const float blockTime):CBaseAction()
	{
		Init(object,time,position, blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CMoveByAction::~CMoveByAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CMoveByAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		Vector pos =  Vector(node.attribute("x").as_float(),node.attribute("y").as_float());
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()),			
			 node.attribute("time").as_float(),
			 pos,
			 node.attribute("block_time").as_float()
			);
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CMoveByAction::Init(CBaseObject * object, const float time, Vector position, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		m_Position = position;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CMoveByAction::Start()
	{
		CBaseAction::Start();

		Vector pos = m_Object->GetPosition();
		if (m_Time == 0.0f)
		{
			m_Object->SetPosition(pos+m_Position);
		}
		else
		{			
			m_StartPosition = m_Object->GetPosition();
		}
	}	

	//////////////////////////////////////////////////////////////////////////
	void CMoveByAction::Stop()
	{		
		Vector pos = m_Object->GetPosition();
		m_Object->SetPosition(m_StartPosition+m_Position);
	}	

	//////////////////////////////////////////////////////////////////////////
	bool CMoveByAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}

	//////////////////////////////////////////////////////////////////////////
	void CMoveByAction::VirtualUpdate()
	{
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;

		auto tween = CTweener::GetTween(EASE_LINEAR);	
		Vector pos = Vector(tween(m_UpdateTimer,m_StartPosition.x,m_Position.x,m_Time),
			tween(m_UpdateTimer,m_StartPosition.y,m_Position.y,m_Time));


		m_Object->SetPosition(pos);
	}	
}

