#include "HideAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CHideAction::CHideAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CHideAction::CHideAction(CBaseObject * object, const float time, const float blockTime):CBaseAction()
	{
		Init(object,time,blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CHideAction::~CHideAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CHideAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()),
			 node.attribute("time").as_float(),
			 node.attribute("block_time").as_float()
			);
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CHideAction::Init(CBaseObject * object, const float time, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CHideAction::Start()
	{
		CBaseAction::Start();

		if (m_Time == 0.0f)
		{
			Color clr = m_Object->GetColor();
			clr.a = 0.0f;
			m_Object->SetColor(clr);
			m_Object->SetVisible(false);
			m_UpdateTimer = m_Time;
		}
		else
		{
			Color clr = m_Object->GetColor();
			m_StartColor = clr.a;			
		}
	}	

	//////////////////////////////////////////////////////////////////////////
	void CHideAction::Stop()
	{		
		Color clr = m_Object->GetColor();
		clr.a = 0.0f;
		m_Object->SetColor(clr);
		m_Object->SetVisible(false);
	}	


	//////////////////////////////////////////////////////////////////////////
	bool CHideAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}


	//////////////////////////////////////////////////////////////////////////
	void CHideAction::VirtualUpdate()
	{
		if (m_Time == 0.0f)return;
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;	

		auto tween = CTweener::GetTween(EASE_LINEAR);	
		float alpha = tween(m_UpdateTimer,m_StartColor,-m_StartColor,m_Time);
		auto color = m_Object->GetColor();
		color.a = alpha;
		m_Object->SetColor(color);
	}		
}

