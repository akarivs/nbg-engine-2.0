#include "AlphaToAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CAlphaToAction::CAlphaToAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CAlphaToAction::CAlphaToAction(CBaseObject * object, const float time, float alpha, const float blockTime):CBaseAction()
	{
		Init(object,time,alpha, blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CAlphaToAction::~CAlphaToAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CAlphaToAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		float alpha = node.attribute("alpha").as_float();
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()),			
			 node.attribute("time").as_float(),
			 alpha,
			 node.attribute("block_time").as_float()
			);
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CAlphaToAction::Init(CBaseObject * object, const float time, float alpha, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		m_Alpha = alpha;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAlphaToAction::Start()
	{
		CBaseAction::Start();

		if (m_Time == 0.0f)
		{
			auto color = m_Object->GetColor();
			color.a = m_Alpha;
			m_Object->SetColor(color);
		}
		else
		{
			m_StartAlpha = m_Object->GetColor().a;
			m_Alpha -= m_StartAlpha;			
		}
	}	

	//////////////////////////////////////////////////////////////////////////
	void CAlphaToAction::Stop()
	{
		auto color = m_Object->GetColor();
		color.a = m_Alpha;
		m_Object->SetColor(color);
	}	

	//////////////////////////////////////////////////////////////////////////
	bool CAlphaToAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAlphaToAction::VirtualUpdate()
	{		
		if (m_Time == 0.0f)return;
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;

		auto tween = CTweener::GetTween(EASE_LINEAR);
		float alpha = tween(m_UpdateTimer,m_StartAlpha,m_Alpha,m_Time);
		auto color = m_Object->GetColor();
		color.a = alpha;
		m_Object->SetColor(color);
	}	
}

