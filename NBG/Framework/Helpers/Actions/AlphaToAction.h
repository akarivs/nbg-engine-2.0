#ifndef _AK_FRAMEWORK_ACTION_ALPHA_TO
#define _AK_FRAMEWORK_ACTION_ALPHA_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - сменить альфу объекту
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CAlphaToAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CAlphaToAction();
		CAlphaToAction(CBaseObject * object, const float time, float alpha, const float blockTime = -1.0f);
		~CAlphaToAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, float alpha, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;		
		float m_Alpha;
		float m_StartAlpha;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_ALPHA_TO
