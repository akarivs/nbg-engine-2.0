#include "PlaySoundAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CPlaySoundAction::CPlaySoundAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CPlaySoundAction::CPlaySoundAction(std::string id, const float time)
	{
		Init(id, time);
	}


	//////////////////////////////////////////////////////////////////////////
	CPlaySoundAction::~CPlaySoundAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CPlaySoundAction::LoadFromXMLNode(pugi::xml_node &node)
	{	
		Init(node.attribute("sound").value(), node.attribute("time").as_float());		
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlaySoundAction::Init(const std::string &id, const float time)
	{
		m_ActionTime = time;
		m_SoundId = id;	
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlaySoundAction::Start()
	{
		CBaseAction::Start();	
		g_SoundManager->Play(m_SoundId);
	}	
	
	//////////////////////////////////////////////////////////////////////////
	void CPlaySoundAction::VirtualUpdate()
	{
		
	}	
}

