#ifndef _AK_FRAMEWORK_ACTION_SCALE_BY
#define _AK_FRAMEWORK_ACTION_SCALE_BY


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - масштабировать объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CScaleByAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CScaleByAction();
		CScaleByAction(CBaseObject * object, const float time, Vector scale, const float blockTime = -1.0f);
		~CScaleByAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, Vector scale, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;
		int m_TweenId;
		Vector m_Scale;
		Vector m_StartScale;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_SCALE_BY
