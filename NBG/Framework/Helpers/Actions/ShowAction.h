#ifndef _AK_FRAMEWORK_ACTION_SHOW
#define _AK_FRAMEWORK_ACTION_SHOW

#include <Framework/Datatypes.h>


#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - показать объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CShowAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CShowAction();
		CShowAction(CBaseObject * object, const float time, const float blockTime = -1.0f);
		~CShowAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();


	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;		
		float m_UpdateTimer;
		float m_StartColor;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_SHOW
