#include "MoveToBezierAction.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CMoveToBezierAction::CMoveToBezierAction():CBaseAction()
	{
	
	}

	//////////////////////////////////////////////////////////////////////////
	CMoveToBezierAction::CMoveToBezierAction(CBaseObject * object, const float time, Vector position, Vector bezier, const float blockTime):CBaseAction()
	{
		Init(object,time,position,bezier, blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CMoveToBezierAction::~CMoveToBezierAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CMoveToBezierAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		Vector pos = Vector(node.attribute("x").as_float(),node.attribute("y").as_float());
		Vector bezier = Vector(node.attribute("bx").as_float(),node.attribute("by").as_float());
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()),			
			 node.attribute("time").as_float(),
			 pos,
			 bezier,
			 node.attribute("block_time").as_float()
			);
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CMoveToBezierAction::Init(CBaseObject * object, const float time, Vector position, Vector bezier, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		m_Position = position;
		m_Bezier = bezier;
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CMoveToBezierAction::Start()
	{
		CBaseAction::Start();

		if (m_Time == 0.0f)
		{
			m_Object->SetPosition(m_Position);
		}		
		m_StartPosition = m_Object->GetPosition();
	}	

	//////////////////////////////////////////////////////////////////////////
	void CMoveToBezierAction::Stop()
	{		
		m_Object->SetPosition(m_Position);
	}	

	//////////////////////////////////////////////////////////////////////////
	bool CMoveToBezierAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}

	//////////////////////////////////////////////////////////////////////////
	void CMoveToBezierAction::VirtualUpdate()
	{
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;

		float percent = m_UpdateTimer/m_Time;
		if (percent > 1.0f)percent = 1.0f;

		Vector pos = MathUtils::GetQuadBezierPoint(m_StartPosition,m_Bezier,m_Position,percent);
		m_Object->SetPosition(pos);
	}	
}

