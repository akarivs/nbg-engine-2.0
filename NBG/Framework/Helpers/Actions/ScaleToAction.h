#ifndef _AK_FRAMEWORK_ACTION_SCALE_TO
#define _AK_FRAMEWORK_ACTION_SCALE_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - масштабировать объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CScaleToAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CScaleToAction();
		CScaleToAction(CBaseObject * object, const float time, Vector scale, const float blockTime = -1.0f);
		~CScaleToAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, Vector scale, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:		
		CBaseObject * m_Object;
		float m_Time;		
		float m_UpdateTimer;
		Vector m_Scale;
		Vector m_StartScale;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_SCALE_TO
