#include "ShowAction.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CShowAction::CShowAction():CBaseAction()
	{
		m_UpdateTimer = 0.0f;
	}

	//////////////////////////////////////////////////////////////////////////
	CShowAction::CShowAction(CBaseObject * object, const float time, const float blockTime):CBaseAction()
	{
		Init(object,time,blockTime);
	}

	//////////////////////////////////////////////////////////////////////////
	CShowAction::~CShowAction()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CShowAction::LoadFromXMLNode(pugi::xml_node &node)
	{
		bool ret = Init(GetSceneObject(node.attribute("object_id").value()), node.attribute("time").as_float(), node.attribute("block_time").as_float());
		if (ret == false)
		{
			CONSOLE("Error: cannot find object for action: '%s'", node.attribute("object_id").value());
			return false;
		}		
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CShowAction::Init(CBaseObject * object, const float time, const float blockTime)
	{
		m_UpdateTimer = 0.0f;
		m_Object = object;
		if (m_Object == NULL)
		{			
			return false;
		}
		m_Time = time;		
		if (blockTime < 0.0f)
		{
			m_ActionTime = m_Time;
		}
		else
		{
			m_ActionTime = blockTime;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CShowAction::Start()
	{
		CBaseAction::Start();

		m_Object->SetVisible(true);		
		if (m_Time == 0.0f)
		{
			Color clr = m_Object->GetColor();
			clr.a = 255.0f;
			m_Object->SetColor(clr);	
			m_UpdateTimer = m_Time;
		}
		else
		{	 
			Color clr = m_Object->GetColor();
			m_StartColor = clr.a;
			clr.a = 0.0f;
			m_Object->SetColor(clr);	
		}		
	}	

	//////////////////////////////////////////////////////////////////////////
	void CShowAction::Stop()
	{		
		Color clr = m_Object->GetColor();
		clr.a = 255.0f;
		m_Object->SetColor(clr);
		m_Object->SetVisible(true);
	}	

	//////////////////////////////////////////////////////////////////////////
	bool CShowAction::IsEnded()
	{
		return m_UpdateTimer >= m_Time;
	}

	
	//////////////////////////////////////////////////////////////////////////
	void CShowAction::VirtualUpdate()
	{
		if (m_Time == 0.0f)return;
		m_UpdateTimer += g_FrameTime;
		if (m_UpdateTimer > m_Time)m_UpdateTimer= m_Time;	

		auto tween = CTweener::GetTween(EASE_LINEAR);	
		float alpha = tween(m_UpdateTimer,0.0f,255.0f,m_Time);
		auto color = m_Object->GetColor();
		color.a = alpha;
		m_Object->SetColor(color);
	}	
}

