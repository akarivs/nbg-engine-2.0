#ifndef _AK_FRAMEWORK_ACTION_HIDE
#define _AK_FRAMEWORK_ACTION_HIDE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - спрятать объект
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CHideAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CHideAction();
		CHideAction(CBaseObject * object, const float time, const float blockTime = -1.0f);
		~CHideAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		bool Init(CBaseObject * object, const float time, const float blockTime = -1.0f);
		virtual void Start();
		virtual void Stop();
		virtual bool IsEnded();
	protected:					
		virtual void VirtualUpdate();	
	private:
		CBaseObject * m_Object;
		float m_Time;
		float m_UpdateTimer;
		float m_StartColor;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_HIDE
