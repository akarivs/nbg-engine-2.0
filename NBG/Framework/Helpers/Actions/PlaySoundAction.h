#ifndef _AK_FRAMEWORK_ACTION_PLAY_SOUND
#define _AK_FRAMEWORK_ACTION_PLAY_SOUND

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	/** @brief Действие - проиграть звук
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CPlaySoundAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CPlaySoundAction();
		CPlaySoundAction(std::string id, const float time);
		~CPlaySoundAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		virtual void Start();
		void Init(const std::string &id, const float time);
	protected:					
		virtual void VirtualUpdate();		
		std::string m_SoundId;
	};    
}
#endif //_AK_FRAMEWORK_ACTION_PLAY_SOUND
