#ifndef _AK_FRAMEWORK_ACTION_PLAY_VIDEO
#define _AK_FRAMEWORK_ACTION_PLAY_VIDEO

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseAction.h"

namespace NBG
{
	class CVideoObject;

	/** @brief Действие - проиграть видео
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CPlayVideoAction : public CBaseAction
	{
	public:
		/// @name Конструктор/деструктор
		CPlayVideoAction();
		CPlayVideoAction(CBaseObject * object, bool waitForComplete);
		~CPlayVideoAction();            				

		virtual bool LoadFromXMLNode(pugi::xml_node &node);
		virtual void Start();
		virtual void Stop();
		void Init(CBaseObject* object, bool waitForComplete);
	protected:		
		CVideoObject * m_Object;
		bool waitForComplete;
		virtual void VirtualUpdate();				
	};    
}
#endif //_AK_FRAMEWORK_ACTION_PLAY_VIDEO
