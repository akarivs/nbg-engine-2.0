#ifndef FRAMEWORK_HELPERS_STENCIL
#define FRAMEWORK_HELPERS_STENCIL

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Mesh.h>

namespace NBG
{
	/** @brief Класс, реализующий работу с буфером глубины.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CStencilHelper
	{
	public:
		/// @name Конструктор деструктор
		CStencilHelper();
		~CStencilHelper();

		/// После вызова Begin нужно отрисовать маску, в границах которой будет идти отрисовка.
		void Begin();
		/// После этого нужно сделать Switch, после этого рисуются объекты, которые должны попасть в маску.
		void Switch();
		/// После того, как отрисовали обрезаемые объекты, нужно сделать End
		void End();

		/// Отрисовать прямоугольник для обрезания.
		void DrawRectangle(NBG::Transform * transform, const NBG::Vector size);
	private:				

		/// Меш для обрезающего прямоугольника.
		NBG::Mesh m_Rectangle;
	};    
}

#endif ///FRAMEWORK_HELPERS_STENCIL
