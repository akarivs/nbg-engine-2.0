#include "StencilHelper.h"

#include <Framework.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CStencilHelper::CStencilHelper()
	{
		m_Rectangle.Init(4,6);
		unsigned short * index = m_Rectangle.GetIndexes();
		index[0] = 0;
		index[1] = 1;
		index[2] = 2;
		index[3] = 3;
		index[4] = 2;
		index[5] = 1;

		Vertex * vertex = m_Rectangle.GetVertexes();
		vertex[0].color = 0xFF000000;
		vertex[1].color = 0xFF000000;
		vertex[2].color = 0xFF000000;
		vertex[3].color = 0xFF000000;		
	}

	//////////////////////////////////////////////////////////////////////////
	CStencilHelper::~CStencilHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::Begin()
	{
		g_Render->DrawBatch();
		g_Render->StencilClear();		
		g_Render->StencilEnable();		
		g_Render->StencilSetCmpFunc(CMP_NEVER);
		g_Render->StencilLevelSet(0x2);
		g_Render->StencilSetFailOpFunc(OP_REPLACE);
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::Switch()
	{
		g_Render->DrawBatch();
		g_Render->StencilSetCmpFunc(CMP_LESS);
		g_Render->StencilLevelSet(0x1);
		g_Render->StencilSetFailOpFunc(OP_REPLACE);
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::End()
	{
		g_Render->DrawBatch();		
		g_Render->StencilDisable();
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::DrawRectangle(Transform * transform, const Vector size)
	{		
		Transform trans = transform->GetAbsoluteTransform();
		Vertex * vertexes = m_Rectangle.GetVertexes();
		vertexes->x = trans.x - size.x/2.0f;
		vertexes->y = g_Render->GetMatrixOffset().y-(trans.y - size.y/2.0f);
		vertexes++;
		vertexes->x = trans.x + size.x/2.0f;
		vertexes->y = g_Render->GetMatrixOffset().y-(trans.y - size.y/2.0f);
		vertexes++;
		vertexes->x = trans.x - size.x/2.0f;
		vertexes->y = g_Render->GetMatrixOffset().y-(trans.y + size.y/2.0f);
		vertexes++;
		vertexes->x = trans.x + size.x/2.0f;
		vertexes->y = g_Render->GetMatrixOffset().y-(trans.y + size.y/2.0f);
		g_Render->DrawMesh(g_GameApplication->GetTextureManager()->GetDefaultTexture(),&m_Rectangle);
	}
}
