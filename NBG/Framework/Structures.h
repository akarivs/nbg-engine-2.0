#ifndef NBG_CORE_STRUCTURES
#define NBG_CORE_STRUCTURES

#include <string>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/FRect.h>

/// Структура описания одной текстуры в атласе.
struct AtlasTextureDescription
{
    std::string atlasID;
    std::string atlasName;
    NBG::FRect uv;
    NBG::Vector size;
	NBG::Vector offset;
};

#endif // NBG_CORE_STRUCTURES
