#ifndef _AK_FRAMEWORK_FONTS_MANAGER
#define _AK_FRAMEWORK_FONTS_MANAGER

#include <Framework/GUI/LabelObject.h>

#include <map>


namespace NBG
{
	/** @brief Класс, реализующий работу со шрифтами
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CFontsManager
	{
	public:
		/// @name Конструктор/деструктор
		CFontsManager();
		~CFontsManager();

		/// Загрузка шрифтов из XML
		void LoadFromXML(const std::string &path);

		/// Получение описания шрифта по ID из xml.
		FontDescription * GetFont(const std::string &id);
		FontDescription * GetFirstFont(){return &m_Fonts.begin()->second;};
	private:
		/// Карта шрифтов
		std::map <std::string, FontDescription>m_Fonts;

		/// Загрузка описания шрифта.
		void LoadFont(pugi::xml_node &node);
	};    
}
#endif
