//
//  File created by Vadim Simonov.
//  Company New Bridge Games
//  Project Patchworks
//  Date 20130827
//  Класс, реализующий логику для хранения/загрузки игроков
//============================================================================//

#ifndef NBG_CORE_PLAYERS_MANAGER
#define NBG_CORE_PLAYERS_MANAGER

#include <../xml/pugixml.hpp>
#include <string>
#include <vector>

namespace NBG
{
    class CBasePlayer
    {
    public:
        CBasePlayer(){SetName(L"Player");};
        ~CBasePlayer(){};

        virtual void InitDefaultParams();
        virtual void Save(pugi::xml_document * doc);
        virtual void Load(pugi::xml_document * doc);

        virtual void AfterInit(){};


        void SetFullscreen(bool isFullscreen){m_IsFullscreen = isFullscreen;};
        bool IsFullscreen(){return m_IsFullscreen;};
        void SetCustomCursor(bool isCustomCursor){m_IsCustomCursor = isCustomCursor;};
        bool IsCustomCursor(){return m_IsCustomCursor;};
        void SetSoundVolume(float volume){m_SoundVolume = volume;};
        float GetSoundVolume(){return m_SoundVolume;};
        void SetMusicVolume(float volume){m_MusicVolume = volume;};
        float GetMusicVolume(){return m_MusicVolume;};

        void SetName(const std::wstring name);
        std::wstring GetName(){return m_Name;};
        virtual int GetSize();

        int GetId(){return m_Id;};
        void SetId(const int id){m_Id = id;};

        bool isCreated;
    private:
        std::wstring m_Name;
        int m_Id;
        bool m_IsFullscreen;
        float m_SoundVolume;
        float m_MusicVolume;
        bool m_IsCustomCursor;
    };

    class CPlayersManager
    {
        public:
            CPlayersManager();
            ~CPlayersManager();

            void InitSavePath(const std::string company, const std::string product);
            void SavePlayers();
            void SaveCurrentPlayer();
            void LoadPlayers();
            CBasePlayer * GetCurrentPlayer();
            void SetActivePlayer(const int id);

            template <class C>
            void InitPlayers(const int count)
            {
                m_PlayersCount = count;
                m_Players.resize(count);
                for (int i=0; i<count; i++)
                {
                    C * c = new C();
                    c->isCreated = false;
                    c->SetId(i);
                    if(i==0)m_PlayersSize = c->GetSize();
                    m_Players[i] = c;
                }
                m_DefaultPlayer = new C();
                m_DefaultPlayer->SetId(-1);
                m_DefaultPlayer->isCreated = true;
                m_DefaultPlayer->InitDefaultParams();
                m_DefaultPlayer->SetName(L"");
            }

            void AddPlayer(const std::wstring name);
            void DeletePlayer(const int id);
            bool IsNameDuplicates(const std::wstring name, const bool ignoreCurrentPlayer = false);
            CBasePlayer * GetPlayer(const int id){return m_Players[id];};
            int GetPlayersCount();
			int GetMaxPlayersCount(){return m_Players.size();};

            void AfterInit();
        private:
            int m_PlayersSize;
            int m_PlayersCount;
            int m_CurrentPlayer;
            std::string m_SavePath;
            std::string m_PlayersSave;
            std::vector<CBasePlayer*>m_Players;
            bool m_IsUseDefaultPlayer;

            std::string m_CorruptedProfiles;

            CBasePlayer * m_DefaultPlayer;

            void SaveSystemConfig();
            void LoadSystemConfig();
        protected:
    };    
}

#endif //NBG_CORE_PLAYERS_MANAGER
