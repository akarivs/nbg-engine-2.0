#include "FontsManager.h"

#include <Framework.h>

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CFontsManager::CFontsManager()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CFontsManager::~CFontsManager()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CFontsManager::LoadFromXML(const std::string &path)
	{
		std::string docPath = path;
		NBG::CXMLResource* xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(docPath);
		pugi::xml_document* doc_window = xmlRes->GetXML();

		pugi::xml_node node_fonts = doc_window->first_child();

		for (pugi::xml_node font = node_fonts.child("font"); font; font = font.next_sibling("font"))
		{
			LoadFont(font);
		}
		g_ResManager->ReleaseResource(xmlRes);
	}

	//////////////////////////////////////////////////////////////////////////
	void CFontsManager::LoadFont(pugi::xml_node &node)
	{
		FontDescription fntDesc;

		std::string path = node.attribute("descPath").value();
		NBG::CXMLResource* xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(path);
		pugi::xml_document* doc = xmlRes->GetXML();

		pugi::xml_node common = doc->first_child().first_child().next_sibling();
		pugi::xml_node page = common.next_sibling().first_child();
		std::string texture = node.attribute("texturePath").value();

		float textureWidth  = common.attribute("scaleW").as_float();
		float textureHeight = common.attribute("scaleH").as_float();

		float widthTexel    = 0.5f/textureWidth;
		float heightTexel   = 0.5f/textureHeight;

		pugi::xml_node chars = doc->child("font").child("chars");
		fntDesc.charsCount = chars.attribute("count").as_int();
		fntDesc.texture = texture;
		fntDesc.chars = new CharOffsets[fntDesc.charsCount];
		fntDesc.lineHeight = common.attribute("lineHeight").as_float();
		fntDesc.characterPadding = 1;
		fntDesc.spaceSize = 8;
		fntDesc.baseLine =  doc->child("font").child("info").attribute("size").as_int()/2.0f;
		int currentChar = 0;
		for (pugi::xml_node chr = chars.child("char"); chr; chr = chr.next_sibling("char"))
		{
			fntDesc.chars[currentChar].id = (unsigned short)chr.attribute("id").as_int();
			fntDesc.chars[currentChar].arrayId  = currentChar;
			fntDesc.chars[currentChar].xOffset  = chr.attribute("xoffset").as_int();
			fntDesc.chars[currentChar].yOffset  = chr.attribute("yoffset").as_int();
			fntDesc.chars[currentChar].xAdvance = chr.attribute("xadvance").as_int();

			float x = chr.attribute("x").as_float();
			float y = chr.attribute("y").as_float();
			float width = chr.attribute("width").as_float();
			float height = chr.attribute("height").as_float();

			fntDesc.chars[currentChar].uv.left = x/textureWidth-widthTexel;
			fntDesc.chars[currentChar].uv.top = y/textureHeight-heightTexel;
			fntDesc.chars[currentChar].uv.right = (x+width)/textureWidth+widthTexel;
			fntDesc.chars[currentChar].uv.bottom = (y+height)/textureHeight+heightTexel;

			fntDesc.chars[currentChar].size.x = width;
			fntDesc.chars[currentChar].size.y = height;
			currentChar++;
		}
		pugi::xml_node kernings = doc->child("font").child("kernings");
		fntDesc.kerningsCount = kernings.attribute("count").as_int();
		fntDesc.kernings = new FontKernPair[fntDesc.kerningsCount];
		currentChar = 0;
		for (pugi::xml_node chr = kernings.child("kerning"); chr; chr = chr.next_sibling("kerning"))
		{
			fntDesc.kernings[currentChar].first   = chr.attribute("first").as_int();
			fntDesc.kernings[currentChar].second  = chr.attribute("second").as_int();
			fntDesc.kernings[currentChar].offset  = chr.attribute("amount").as_int();
			currentChar++;
		}
		std::string name = node.attribute("name").value();		
		m_Fonts[name] = fntDesc;
		g_ResManager->ReleaseResource(xmlRes);
	}

	//////////////////////////////////////////////////////////////////////////
	FontDescription * CFontsManager::GetFont(const std::string &id)
	{
		return &m_Fonts[id];
	}
}
