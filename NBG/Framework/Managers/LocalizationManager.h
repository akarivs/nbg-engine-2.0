#ifndef NBG_CORE_LOCALIZATION_MANAGER
#define NBG_CORE_LOCALIZATION_MANAGER

#include <map>
#include <vector>
#include <string>

namespace NBG
{
	/** @brief Класс, для работы с локализацией.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CLocalizationManager
	{
	public:
		/// @name Конструктор/деструктор
		CLocalizationManager();
		~CLocalizationManager();

		/// Инициализация локализации.
		void Init(const std::string &fileName);

		/// Получить текущую локаль.
		const std::string & GetLocale();

		/// Получить текст по ключу.
		const std::wstring & GetText(const std::string &id);
	private:
		/// Текущая локаль.
		std::string m_Locale;
		std::wstring m_DefaultText;

		/// Карта строк.
		std::map <std::string,std::vector<std::wstring> >m_Strings;

		/// Карта ключей.
		std::map<std::string, int>m_Keys;

		/// Карта языков.
		std::map<int,std::string>m_Languages;
	};	
}
#endif //NBG_CORE_LOCALIZATION_MANAGER
