#include "SoundManager.h"

#include <cstring>
#include <Framework.h>
#include <Framework/Utils/StringUtils.h>
#include <Framework/GLobal/ResourcesManager.h>
#include <Framework/Global/Resources/OGGResource.h>

#define DYNBUF_SIZE  128000
#define NUM_OF_DYNBUF 10

#ifdef NBG_ANDROID
	#include <unistd.h>
#endif

namespace NBG
{
	//==============================================================================
	CSound::CSound()
	{
		m_IsLoaded	= false;
		m_IsPlaying = false;
		m_IsPaused	= false;
		m_Buffers.resize(NUM_OF_DYNBUF);
	}

	//==============================================================================
	CSound::~CSound()
	{
        m_IsLoaded = false;
	}

	//==============================================================================
	bool CSound::Open(const std::string &filename, bool looped, bool streamed, float volume)
	{
		m_IsLooped   = looped;
		m_IsStreamed = streamed;
		m_Volume = volume;

		m_SourceID = 0;

		std::string ext = StringUtils::GetExtension(filename);
		if (ext == "ogg")
		{
			return LoadOggFile(filename,streamed);
		}
		return false;
	}

	//==============================================================================
	void CSound::SetVolume(const float volume)
	{
		m_VolumeCoeff = volume;
		alSourcef(m_SourceID, AL_GAIN, m_Volume*volume);
	}

	//==============================================================================
	void CSound::Play()
	{		
		bool isPlay = true;
		if (m_SourceID == 0)
		{
			isPlay = false;
			m_SourceID = g_SoundManager->GetFreeSourceId();
			
			alSourcef (m_SourceID, AL_PITCH,    1.0f);
			alSourcef (m_SourceID, AL_GAIN,     m_Volume*m_VolumeCoeff);
			alSourcefv(m_SourceID, AL_POSITION,  mPos);
			alSourcefv(m_SourceID, AL_VELOCITY,  mVel);			
			if (m_IsStreamed) // Помещаем буфер в очередь.
			{
				alSourcei(m_SourceID, AL_LOOPING, AL_FALSE);
				for (int i = 0; i < NUM_OF_DYNBUF; i++)
				{
					// Создаем буфер
					alGenBuffers(1, &m_BufferId);
					// Считываем блок данных
					ReadOggBlock(m_BufferId, DYNBUF_SIZE,mVF);
					alSourceQueueBuffers(m_SourceID, 1, &m_BufferId);

					m_Buffers[i] = m_BufferId;
				}				
			}
			else
			{
				alSourcei (m_SourceID, AL_LOOPING,  m_IsLooped);			
				alSourcei(m_SourceID, AL_BUFFER,m_BufferId);
			}
		}		
		SetVolume(m_VolumeCoeff);
		if (m_IsStreamed)
		{
			ALenum state;			
			alGetSourcei(m_SourceID,AL_SOURCE_STATE,&state);
			if( state == AL_PLAYING)
			{
				return;
			}
			alSourceStop(m_SourceID);		
			alSourceRewind(m_SourceID);		
			alSourcePlay(m_SourceID);		
			m_IsPlaying = true;
		}
		else
		{
			alSourcePlay(m_SourceID);
			m_IsPlaying = true;
		}
	}

	//==============================================================================
	void CSound::Close()
	{
		alSourceStop(m_SourceID);
		if (alIsSource(m_SourceID)) 
		{
			m_SourceID = 0;			
		}
	}

	//==============================================================================
	void CSound::Pause()
	{
		ALint state;
		alGetSourcei(m_SourceID, AL_SOURCE_STATE, &state);
		if (state == AL_PLAYING)
		{
			m_IsPaused = true;
			alSourcePause(m_SourceID);
		}
	}

	//==============================================================================
	void CSound::Resume()
	{
		if (m_IsPaused)
		{
			m_IsPaused = false;
			alSourcePlay(m_SourceID);
		}
	}

	//==============================================================================
	void CSound::Stop()
	{
		if (m_IsStreamed && m_SourceID != 0)
		{
			int      Processed = 0;

			// Получаем количество отработанных буферов
			alGetSourcei(m_SourceID, AL_BUFFERS_PROCESSED, &Processed);
			// Если таковые существуют то
			while (Processed--)
			{
				// Исключаем их из очереди
				alSourceUnqueueBuffers(m_SourceID, 1, &m_BufferId);						
			}				
			for (int i=0; i<NUM_OF_DYNBUF; i++)
			{
				alDeleteBuffers(1,&m_Buffers[i]);				
			}			
			ov_raw_seek(mVF,0.0f);
		}		
		g_SoundManager->FreeSourceId(m_SourceID);
		alSourceStop(m_SourceID);
		alSourcei(m_SourceID, AL_BUFFER, NULL);
		m_SourceID = 0;
		m_IsPlaying = false;
	}

	//==============================================================================
	void CSound::Move(float x, float y, float z)
	{
		ALfloat pos[3] = { x, y, z };
		alSourcefv(m_SourceID, AL_POSITION, pos);
	}

	//==============================================================================
	float CSound::GetPosition()
	{
		float result = ov_time_tell( mVF );
		return result;
	}

	//==============================================================================
	void CSound::Update()
	{
		if (!m_IsLoaded)return;
		if (m_IsStreamed && m_SourceID != 0)
		{
			int      Processed = 0;

			// Получаем количество отработанных буферов
			alGetSourcei(m_SourceID, AL_BUFFERS_PROCESSED, &Processed);
			// Если таковые существуют то
			while (Processed--)
			{
				// Исключаем их из очереди
				alSourceUnqueueBuffers(m_SourceID, 1, &m_BufferId);				
				// Читаем очередную порцию данных и включаем буфер обратно в очередь
				if (ReadOggBlock(m_BufferId, DYNBUF_SIZE,mVF) != 0)
				{
					alSourceQueueBuffers(m_SourceID, 1, &m_BufferId);					
				}
				else
				{
					// Если зацикленное воспроизведение, тогда перематываем на начало звук.
					if (m_IsLooped)
					{
						ov_time_seek(mVF,0.0f);
						alSourceQueueBuffers(m_SourceID, 1, &m_BufferId);
						alSourcePlay(m_SourceID);
					}
				}
			}				
		}
		else
		{
			if (m_SourceID != 0)
			{
				ALenum state;
				alGetSourcei(m_SourceID,AL_SOURCE_STATE,&state);

				if( state != AL_PLAYING && state != -1)
				{
					alSourcei(m_SourceID, AL_BUFFER, NULL);
					g_SoundManager->FreeSourceId(m_SourceID);
					m_SourceID = 0;
				}					
			}		
		}
		
	}

	//==============================================================================
	struct ogg_file
	{
		char* curPtr;
		char* filePtr;
		size_t fileSize;
	};

	//==============================================================================
	size_t AR_readOgg(void* dst, size_t size1, size_t size2, void* fh)
	{
		ogg_file* of = reinterpret_cast<ogg_file*>(fh);
		size_t len = size1 * size2;
		if ( of->curPtr + len > of->filePtr + of->fileSize )
		{
			len = of->filePtr + of->fileSize - of->curPtr;
		}
		memcpy( dst, of->curPtr, len );		
		of->curPtr += len;
		return len;
	}

	//==============================================================================
	int AR_seekOgg( void *fh, ogg_int64_t to, int type ) {
		ogg_file* of = reinterpret_cast<ogg_file*>(fh);
		switch( type ) {
		case SEEK_CUR:
			of->curPtr += to;
			break;
		case SEEK_END:
			of->curPtr = of->filePtr + of->fileSize - to;
			break;
		case SEEK_SET:
			of->curPtr = of->filePtr + to;
			break;
		default:
			return -1;
		}
		if ( of->curPtr < of->filePtr ) {
			of->curPtr = of->filePtr;
			return -1;
		}
		if ( of->curPtr > of->filePtr + of->fileSize ) {
			of->curPtr = of->filePtr + of->fileSize;
			return -1;
		}
		return 0;
	}

	//==============================================================================
	int AR_closeOgg(void* fh)
	{
		return 0;
	}

	//==============================================================================
	long AR_tellOgg( void *fh )
	{
		ogg_file* of = reinterpret_cast<ogg_file*>(fh);
		return (of->curPtr - of->filePtr);
	}

	//==============================================================================
	bool CSound::ReadOggBlock(ALuint BufID, size_t Size, OggVorbis_File* VF)
	{
		// Переменные
		char    eof = 0;
		int    current_section;
		long    TotalRet = 0, ret;
		// Буфер данных
		char    *PCM;

		if (Size < 1) return false;
		PCM = new char[Size];

		// Цикл чтения
		while ((size_t)TotalRet < Size)
		{
			ret = ov_read(VF, PCM + TotalRet, Size - TotalRet, 0, 2, 1,
				& current_section);

			// Если достигнут конец файла
			if (ret == 0) break;
			else if (ret < 0)     // Ошибка в потоке
			{
				//
			}
			else
			{
				TotalRet += ret;
			}
		}
		if (TotalRet > 0)
		{
			alBufferData(BufID, m_Format, (void *)PCM,
				TotalRet,m_Rate);
		}
		delete [] PCM;
		return (ret > 0);
	}

	//==============================================================================
	bool CSound::LoadOggFile(const std::string &filename, bool streamed)
	{
		int        i, DynBuffs = 1, BlockSize;
		// OAL specific
		ALuint      BufID = 0;
		// Структура с функциями обратного вызова.
		ov_callbacks  cb;

		// Заполняем структуру cb
		cb.read_func = AR_readOgg;
		cb.seek_func = AR_seekOgg;
		cb.close_func = AR_closeOgg;
		cb.tell_func = AR_tellOgg;

		// Создаем структуру OggVorbis_File
		mVF = new OggVorbis_File();

		// Инициализируем файл средствами vorbisfile
		COGGResource * res = CAST(COGGResource*,g_ResManager->GetResource(filename));
		if (!res || res->GetSize() == 0)return false;	

		ogg_file * t = new ogg_file();
		t->curPtr = t->filePtr = res->GetData();
		t->fileSize = res->GetSize();

		if (ov_open_callbacks(t, mVF, NULL, -1, cb) < 0)
		{
			CONSOLE("Error: this is not OGG file! %s", filename.c_str());
			return false;
		}				

		// Начальные установки в зависимости от того потоковое ли проигрывание
		// затребовано
		if (!streamed)
		{
			// Размер блока – весь файл
			BlockSize = ov_pcm_total(mVF, -1) * 4;
		}
		else
		{
			// Размер блока задан
			BlockSize  = DYNBUF_SIZE;
			// Количество буферов в очереди задано
			DynBuffs  = NUM_OF_DYNBUF;			
		}

		// Получаем комментарии и информацию о файле
		mComment    = ov_comment(mVF, -1);
		mInfo      = ov_info(mVF, -1);

		// Заполняем SndInfo структуру данными
		m_Rate    = mInfo->rate;
		m_Format = (mInfo->channels == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;

		// Если потоковое проигрывание, или буфер со звуком не найден то
		if (streamed || !BufID)
		{
			for (i = 0; i < DynBuffs; i++)
			{
				if (!streamed) // Помещаем буфер в очередь.
				{
					alGenBuffers(1, &m_BufferId);				
					ReadOggBlock(m_BufferId, BlockSize,mVF);					
				}
			}
		}
		else
		{
			//alSourcei(m_SourceID, AL_BUFFER, m_BufferId);
		}
		m_IsLoaded = true;
		return true;
	}

	CSoundManager::CSoundManager()
	{

	}

	CSoundManager::~CSoundManager()
	{
	}

	//==============================================================================
	void CSoundManager::Init()
	{
		// Позиция слушателя.
		ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };
		// Скорость слушателя.
		ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };
		// Ориентация слушателя. (Первые 3 элемента – направление «на», последние 3 – «вверх»)
		ALfloat ListenerOri[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };

		/// Открываем заданное по умолчанию устройство
		m_Device = alcOpenDevice(NULL);
		/// Проверка на ошибки
		if (!m_Device)
		{
			CONSOLE("Default sound device not present");
			return;
		}
		/// Создаем контекст рендеринга
		m_Context = alcCreateContext(m_Device, NULL);

		/// Делаем контекст текущим
		alcMakeContextCurrent(m_Context);
		alListenerfv(AL_POSITION,    ListenerPos);
		alListenerfv(AL_VELOCITY,    ListenerVel);
		alListenerfv(AL_ORIENTATION, ListenerOri);

		
		while (true)
		{
			SSources src;			
			src.free = true;
			alGenSources(1, &src.sourceId);
			if (alGetError() == 0)
			{
				m_Sources.push_back(src);
			}
			else
			{
				break;
			}			
		}
		CONSOLE("Sound Buffers count: %d", (int)m_Sources.size());
		m_IsLoaded = false;
		m_UpdateTimer = 0.0f;
	}

	//==============================================================================
	void CSoundManager::Destroy()
	{
		alcCloseDevice(m_Device);
		m_IsLoaded = false;		
		m_Sounds.clear();
	}
	

	//==============================================================================
	void CSoundManager::LoadSounds(const std::string &xml)
	{
		std::string xmlPath = xml;
		NBG::CXMLResource * res = CAST(CXMLResource*,g_ResManager->GetResource(xmlPath));
		pugi::xml_document * doc = res->GetXML();
		pugi::xml_node node_sounds = doc->first_child();
		for (pugi::xml_node sound = node_sounds.child("sound"); sound; sound = sound.next_sibling("sound"))
		{
			CSound * soundObj = new CSound();
			float volume = 1.0f;
			if (!sound.attribute("volume").empty())
			{
				volume = sound.attribute("volume").as_float();
			}
			soundObj->Open(sound.attribute("path").value(),sound.attribute("looped").as_bool(),sound.attribute("streamed").as_bool(), volume);
			soundObj->SetGroup(sound.attribute("group").value());
			AddSound(sound.attribute("name").value(), soundObj);
		}
		g_ResManager->ReleaseResource(res);
		m_IsLoaded = true;
	}

	//==============================================================================
	void CSoundManager::AddSound(const std::string &name, CSound * sound)
	{
		m_Sounds[name] = sound;
	}

	//==============================================================================
	void CSoundManager::Play(const std::string &name)
	{
		if (m_Sounds.find(name) == m_Sounds.end())
		{
			return;
		}		
		m_Sounds[name]->Play();
	}

	//==============================================================================
	void CSoundManager::Stop(const std::string &name)
	{
		if (m_Sounds.find(name) == m_Sounds.end())
		{
			return;
		}
		m_Sounds[name]->Stop();
	}

	//==============================================================================
	void CSoundManager::Pause(const std::string &name)
	{
		if (m_Sounds.find(name) == m_Sounds.end())
		{
			return;
		}
		m_Sounds[name]->Pause();
	}

	//==============================================================================
	void CSoundManager::Resume(const std::string &name)
	{
		if (m_Sounds.find(name) == m_Sounds.end())
		{
			return;
		}
		m_Sounds[name]->Resume();
	}

	//==============================================================================
	float CSoundManager::GetPosition(const std::string &name)
	{
		if (m_Sounds.find(name) == m_Sounds.end())
		{
			return 0.0f;
		}
		return m_Sounds[name]->GetPosition();
	}

	//==============================================================================
	void CSoundManager::Update()
	{
		if (!m_IsLoaded)return;
		m_UpdateTimer -= g_FrameTime;
		if (m_UpdateTimer > 0.0f)return;
		m_UpdateTimer = 0.5f;

		auto m_SoundsIter = m_Sounds.begin();
		while (m_SoundsIter != m_Sounds.end())
		{
			m_SoundsIter->second->Update();
			m_SoundsIter++;
		}
	}

	//==============================================================================	
	void CSoundManager::SetGroupVolume(const std::string &group, const float val)
	{
		if (!m_IsLoaded)return;
		auto m_SoundsIter = m_Sounds.begin();
		while (m_SoundsIter != m_Sounds.end())
		{
			if (m_SoundsIter->second->GetGroup() == group)
			{
				m_SoundsIter->second->SetVolume(val);
			}
			m_SoundsIter++;
		}
	}

	//////////////////////////////////////////////////////////////////////////	
	void CSoundManager::StopGroup(const std::string &group)
	{
		if (!m_IsLoaded)return;
		auto m_SoundsIter = m_Sounds.begin();
		while (m_SoundsIter != m_Sounds.end())
		{
			if (m_SoundsIter->second->GetGroup() == group)
			{
				m_SoundsIter->second->Stop();
			}
			m_SoundsIter++;
		}
	}

	//==============================================================================
	void CSoundManager::OnLostFocus()
	{
		auto m_SoundsIter = m_Sounds.begin();
		while (m_SoundsIter != m_Sounds.end())
		{
			m_SoundsIter->second->Pause();
			m_SoundsIter++;
		}
	}

	//==============================================================================
	void CSoundManager::OnGetFocus()
	{
		auto m_SoundsIter = m_Sounds.begin();
		while (m_SoundsIter != m_Sounds.end())
		{
			m_SoundsIter->second->Resume();
			m_SoundsIter++;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	ALuint & CSoundManager::GetFreeSourceId()
	{
		for (int i=0; i<m_Sources.size(); i++)
		{
			auto &source = m_Sources[i];
			if( source.free == false)
			{
				continue;
			}
			source.free = false;
			return source.sourceId;			
		}
		ALuint s = 0;
		return s;
	}

	//////////////////////////////////////////////////////////////////////////
	void CSoundManager::FreeSourceId(ALuint sourceId)
	{
		for (int i=0; i<m_Sources.size(); i++)
		{
			auto &source = m_Sources[i];
			if( source.sourceId == sourceId)
			{
				source.free = true;
			}						
		}
	}
}
