#include "ScenesManager.h"

#include <framework.h>

#include <Framework/BaseScene.h>
#include <Framework/GUI/ContainerObject.h>


namespace NBG
{    
	CScenesManager::CScenesManager()
	{
		m_Container = new CContainerObject("ScenesContainer");		
	}

	CScenesManager::~CScenesManager()
	{
		delete m_Container;
	}

	void CScenesManager::AddScene(const std::string id)
	{	
		if (m_Container->GetChild(id)) //если нашли сцену, кидаем её на верх
		{
			CBaseScene * scene = GetScene(id);
			if (scene)
			{
				scene->BringForward();
				scene->Init();
				return;
			}
		}
		CBaseScene * scene = m_ScenesFactory.create(id);        
		NBG_Assert(scene,"Scene is not registered!");
		
		m_Scenes[id] = scene;				
		

		m_Scenes[id] = scene;		
		m_Container->AddChild(scene);

		scene->Init();
		scene->SetId(id);

		g_LuaHelper->OnSceneChange(scene);
        
	}

	CBaseScene * CScenesManager::GetScene(const std::string id)
	{
		if (!m_Container->GetChild(id))
		{
			CONSOLE("Scene not found! %s",id.c_str());
			return NULL;
		}
		return CAST(CBaseScene*,m_Container->GetChild(id));
	}

	CBaseScene * CScenesManager::GetCurrentScene()
	{
		return CAST(CBaseScene*,m_Container->GetChilds().back());		
	}

	void CScenesManager::RemoveScene(const std::string id)
	{	
		g_ActionHelper->ClearActions();
		g_Tweener->StopAll();

		if (m_Scenes.find(id) != m_Scenes.end())
		{
			CBaseScene * scene = m_Scenes[id];		    
			scene->Release();
			scene->Destroy();
		}
	}

	void CScenesManager::OnOrientationChange(const bool isLandscape)
	{		
		m_Container->UpdateAnchors();        
	}

	void CScenesManager::OnBeforeRenderChange()
	{
		m_Container->UpdateAnchors();        
		m_Container->BeforeRenderChange();        

	}

	void CScenesManager::OnAfterRenderChange()
	{
		m_Container->UpdateAnchors();        
		m_Container->AfterRenderChange();        		
	}

	void CScenesManager::OnMouseMove()
	{
		m_Container->OnMouseMove();
	}

	void CScenesManager::OnMouseDown(const int button)
	{
		m_Container->OnMouseDown(button);        
	}

	void CScenesManager::OnMouseUp(const int button)
	{
		m_Container->OnMouseUp(button);
	}

	void CScenesManager::OnKeyDown(const int key)
	{        
		int res = m_Container->OnKeyDown(key);
		if (res != CBaseObject::EventSkip)return;

		/*if (key == KEY_R && g_Input->IsKeyDown(KEY_SHIFT))
		{		
			
			g_Config->LoadConfig("xml/config.xml");
			g_LocaleManager->Init("xml/strings.xml");			
			g_GameApplication->SetWindowTitle(g_LocaleManager->GetText("common_window_title"));
			g_Tweener->StopAll();
			g_ActionHelper->ClearActions();				
			for (m_ScenesRIter=m_Scenes.rbegin(); m_ScenesRIter!=m_Scenes.rend(); m_ScenesRIter++)
			{	
				std::string id = m_ScenesRIter->second->GetId();
				m_ScenesRIter->second->Init();
				m_ScenesRIter->second->SetId(id);
				m_ScenesRIter->second->DispatchEvent(Event("debug_restart",NULL),NULL);
			}				
			g_LuaHelper->OnSceneChange(m_Scenes.rbegin()->second);


			g_ResManager->ReleaseUnusedResources();
		}*/
	}

	void CScenesManager::OnKeyUp(const int key)
	{	
		int res = m_Container->OnKeyUp(key);
		if (res != CBaseObject::EventSkip)return;
	}

	int CScenesManager::Update()
	{		
		m_ScenesToDestroy.clear();
		for (m_ScenesRIter=m_Scenes.rbegin(); m_ScenesRIter!=m_Scenes.rend(); m_ScenesRIter++)
		{
			if (m_ScenesRIter->second->IsDestroyed())
			{
				m_ScenesToDestroy[m_ScenesRIter->first] = m_ScenesRIter->second;
				continue;
			}            
		}
		int res = m_Container->Update();		
		if (m_ScenesToDestroy.size() > 0)
		{	
			for (m_ScenesRIter=m_ScenesToDestroy.rbegin(); m_ScenesRIter!=m_ScenesToDestroy.rend(); m_ScenesRIter++)
			{				
				CONSOLE("Scene destroyed %s", m_ScenesRIter->first.c_str());
				m_Scenes.erase(m_ScenesRIter->first);
			}
			g_ResManager->ReleaseUnusedResources();
			
		}
		return res;
	}

	int CScenesManager::Draw()
	{
		m_Container->Draw();
		return CBaseObject::EventSkip;
	}
}
