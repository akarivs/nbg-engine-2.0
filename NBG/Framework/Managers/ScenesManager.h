#ifndef _AK_FRAMEWORK_SCENES_MANAGER
#define _AK_FRAMEWORK_SCENES_MANAGER

#include <Framework/BaseScene.h>
#include <map>

namespace NBG
{
    class abstractSceneCreator
    {
    public:
        virtual void sceneCreator() {}
        virtual CBaseScene * create() const = 0;
    };

    template <class C>
    class sceneCreator : public abstractSceneCreator
    {
    public:
        virtual CBaseScene * create() const { return new C("_unnamed scene"); }
    };

    class SceneFactory
    {
    protected:
        typedef std::map<std::string, abstractSceneCreator*> FactoryMap;
        FactoryMap _factory;

    public:
        SceneFactory(){};
        virtual ~SceneFactory(){};

        template <class C>
        void add(const std::string & id)
        {
            typename FactoryMap::iterator it = _factory.find(id);
            if (it == _factory.end())
                _factory[id] = new sceneCreator<C>();
        }


        CBaseScene * create(const std::string & id)
        {
            FactoryMap::iterator it = _factory.find(id);
            if (it != _factory.end())
                return it->second->create();
            return 0;
        }
    };

    class CContainerObject;
    class CScenesManager
    {
        public:
            CScenesManager();
            ~CScenesManager();

            //добавление/получение/удаление сцен
            void AddScene(const std::string id);
            CBaseScene * GetScene(const std::string id);
			CBaseScene * GetCurrentScene();
            void RemoveScene(const std::string id);


            //сортировка
            void MoveSceneOnTop(const std::string id);
            void MoveSceneOnBottom(const std::string id);

            //события
            void OnMouseMove();
            void OnMouseDown(const int button);
            void OnMouseUp(const int button);
            void OnKeyDown(const int key);
            void OnKeyUp(const int key);
            void OnOrientationChange(const bool isLandscape);
			void OnBeforeRenderChange();
			void OnAfterRenderChange();

            int Update();
            int Draw();

            SceneFactory * GetFactory() {return &m_ScenesFactory;};
        private:
            std::map <std::string, CBaseScene *>m_Scenes;
            std::map <std::string, CBaseScene *>m_ScenesToDestroy;
            std::map <std::string, CBaseScene *>::iterator m_ScenesIter;
            std::map <std::string, CBaseScene *>::reverse_iterator m_ScenesRIter;

            CContainerObject  * m_Container;
            SceneFactory m_ScenesFactory;
        protected:

        };        
}

#endif
