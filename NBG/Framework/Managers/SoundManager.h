#ifndef NBG_CORE_SOUND_MANAGER_BASE
#define NBG_CORE_SOUND_MANAGER_BASE

#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#include <../TheoraPlayer/include/openal/al.h>
#include <../TheoraPlayer/include/openal/alc.h>
#include <../TheoraPlayer/include/vorbis/vorbisenc.h>
#include <../TheoraPlayer/include/vorbis/vorbisfile.h>

#include <Framework/Global/Thread.h>

#include <Framework/Datatypes.h>

namespace NBG
{
	/** @brief Базовый класс звуковой. Общий для многих систем.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CSound
	{
	public:
		/// @name Конструкторы
		CSound();
		virtual ~CSound();

		/// Скорость воспроизведения звука
		ALfloat mVel[3];

		/// Позиция звука в пространстве
		ALfloat mPos[3];

		/// Зациклен ли звук.
		bool  m_IsLooped;

		/// @name Методы

		/// Открытие звука.
		bool Open(const std::string &filename, bool looped, bool streamed, float volume);

		/// Установка громкости звука.
		void SetVolume(const float volume);

		/// Стримится ли звук во время проигрывания или загружается полностью.
		bool IsStreamed();

		/// Воспроизвести звук.
		void Play();

		/// Поставить звук на паузу.
		void Pause();

		/// Восстановить воспроизведение после паузы.
		void Resume();

		/// Закрыть звук.
		void Close();

		/// Обновить звук.
		void Update();

		/// Передвинуть звук в пространстве.
		void Move(float x, float y, float z);

		/// Остановить воспроизведение звука.
		void Stop();

		/// Получить позицию.
		float GetPosition();

		/// Установить группу воспроизведения звука.
		void SetGroup(const std::string &group)
		{
			m_Group = group;
		};

		/// Получить группу воспроизведения звука.
		std::string GetGroup()
		{
			return m_Group;
		}
	private:
		///вектор буферов
		std::vector<ALuint> m_Buffers;

		/// Идентификатор источника.
		ALuint m_SourceID;

		/// Идентификатор буфера.
		ALuint m_BufferId;

		/// Формат звука.
		unsigned int m_Format;

		/// Частота звука.
		unsigned int m_Rate;

		/// Потоковый ли наш звук?
		bool m_IsStreamed;

		/// Загрузили ли уже звук.
		bool m_IsLoaded;

		/// Если пытались воспроизвести незагруженный звук.
		bool m_IsPlaying;

		/// Стоит ли звук на паузе.
		bool m_IsPaused;

		/// Загрузить OGG файл.
		bool LoadOggFile(const std::string &filename, bool streamed);

		
		/// Звуковая группа.
		std::string m_Group;

		/// Громкость звка.
		float m_Volume;

		/// Коэффициэнт громкости звука
		float m_VolumeCoeff;

		/// @name OGG PART

		/// Считать OGG блок.
		bool ReadOggBlock(ALuint BufID, size_t Size,OggVorbis_File* VF);

		/// Указатель на файл.
		OggVorbis_File  *mVF;

		/// Мета-информация к файлу.
		vorbis_comment  *mComment;

		/// Инфо о файле.
		vorbis_info    *mInfo;
	};


	class CSoundManager
	{
	public:
		CSoundManager();
		~CSoundManager();

		ALCdevice * GetDevice()
		{
			return m_Device;
		}

		
		ALCcontext * GetContext()
		{
			return m_Context;
		}


		/// Инициализирует поток для обновления менеджера.
		void Init();
		
		/// Разрушает объекты менеджера.
		void Destroy();
		
		/// Загрузка списка звуков из xml.
		void LoadSounds(const std::string &xml);
		
		/// Добавляет звук в список.
		void AddSound(const std::string &name, CSound * sound);
		
		/// Проигрывает звук из списка.
		void Play(const std::string &name);
		
		/// Останавливает звук из списка.
		void Stop(const std::string &name);
		
		/// Ставит на паузу звук из списка.
		void Pause(const std::string &name);
		
		/// Снимает с паузы звук из списка.
		void Resume(const std::string &name);
		
		/// Обновление всех звуков.
		void Update();
		
		/// Получение позиции звука.
		float GetPosition(const std::string &name);

		///Установка звука группе
		void SetGroupVolume(const std::string &group, const float val);
		
		///Остановить всю группу звуков
		void StopGroup(const std::string &group);
		
		///События потери/получения фокуса
		void OnLostFocus();
		void OnGetFocus();

		/// Получить путь к файлу.
		std::string GetXmlPath()
		{
			return m_XmlPath;
		};
		ALuint &GetFreeSourceId();
		void FreeSourceId(ALuint sourceId);

	protected:
		/// Девайс звуковой.
		ALCdevice * m_Device;

		/// Контекст звуковой.
		ALCcontext * m_Context;

	
		/// Получить путь к файлу.
		std::string m_XmlPath;

		/// Загружены ли звуки.
		bool m_IsLoaded;

		/// Карта звуков.
		std::map<std::string, CSound* >m_Sounds;

		/// Карта звуков, которые необходимо проиграть
		std::vector<std::string>m_SoundsToPlay;

		/// Итератор для карты звуков.
		std::vector<std::string>::iterator  m_SoundsToPlayIter;

		/// Поток для обновления звуков.
		CThread * m_Thread;

		/// Таймер обновление звука
		float m_UpdateTimer;		

		struct SSources
		{
			ALuint sourceId;
			bool free;
		};
		std::vector<SSources> m_Sources;

	};
};
#endif //NBG_CORE_SOUND_MANAGER_BASE
