#include "LocalizationManager.h"

#include <Framework.h>

#include <Framework/Global/ResourcesManager.h>
#include <Framework/Global/Resources/XMLResource.h>
#include <Framework/Utils/StringUtils.h>


namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CLocalizationManager::CLocalizationManager()
	{
		m_DefaultText = L"default_text";
	}

	//////////////////////////////////////////////////////////////////////////
	CLocalizationManager::~CLocalizationManager()
	{
		std::map <std::string,std::vector<std::wstring> >stringsSwap;
		std::map<std::string, int>keysSwap;
		std::map<int,std::string>languagesSwap;
		
		m_Strings.swap(stringsSwap);
		m_Keys.swap(keysSwap);
		m_Languages.swap(languagesSwap);

	}

	//////////////////////////////////////////////////////////////////////////
	void CLocalizationManager::Init(const std::string &fileName)
	{
		//Получаем системную локаль
		m_Locale = g_System->GetLocale();
		if (m_Locale.empty()) ///если система не поддерживает локали, получаем локаль из настроечного файла
		{
			g_Config->GetValue("locale",m_Locale);			
		}
		///Если нет папки с указанной локалью, локаль ставим по умолчанию
		std::string path = "localization/"+m_Locale;

		if (g_FileSystem->IsDirExists(path) == false)
		{
			m_Locale = "en";
		}	

		std::map <std::string,std::vector<std::wstring> >stringsSwap;
		std::map<std::string, int>keysSwap;
		std::map<int,std::string>languagesSwap;
		
		m_Strings.swap(stringsSwap);
		m_Keys.swap(keysSwap);
		m_Languages.swap(languagesSwap);

		///Читаем из формата Excel XML Table 2003        
		CXMLResource* xmlRes = (CXMLResource*)g_ResManager->GetResource(fileName);
		pugi::xml_document* doc = xmlRes->GetXML();
		if (!doc)
		{
			CONSOLE("Error: no such file - %s", fileName.c_str());
			return;
		}

		pugi::xml_node node = doc->first_child().child("Worksheet").child("Table");		

		int counter = 0;
		int stringsId = 0;
		//получаем строки
		for (pugi::xml_node Row = node.child("Row"); Row; Row = Row.next_sibling("Row"))
		{			

			if (counter == 1)
			{
				int cnt = 0;
				for (pugi::xml_node Cell = Row.child("Cell"); Cell; Cell = Cell.next_sibling("Cell"))
				{								
					if (cnt>1)
					{
						std::string id = Cell.first_child().child_value();
						if (!id.empty())
						{
							m_Languages[cnt] = id;								
						}
					}
					cnt++;
				}		
			}			
			counter++;
			if (counter<=2)continue;
			int cnt = 0;
			for (pugi::xml_node Cell = Row.child("Cell"); Cell; Cell = Cell.next_sibling("Cell"))
			{				
				std::string id = Cell.first_child().child_value();				
				if (cnt == 0)
				{			
					if (!id.empty() && Cell.attribute("ss:MergeAcross").empty())
					{
						m_Keys[id] = stringsId;
						stringsId++;
					}
					else
					{
						break;
					}
				}				
				cnt++;
				if (cnt<=2)continue;
				if (id.empty())
				{
					id = "!no_text!";
				}
				///Разбиваем по языкам				
				m_Strings[m_Languages[cnt-1]].push_back(pugi::as_wide(id));				
			}						           
		}	
		g_ResManager->ReleaseResource(xmlRes);
	}

	//////////////////////////////////////////////////////////////////////////
	const std::string & CLocalizationManager::GetLocale()
	{
		return m_Locale;
	}

	//////////////////////////////////////////////////////////////////////////
	const std::wstring & CLocalizationManager::GetText(const std::string &id)
	{
		if (m_Keys.find(id) == m_Keys.end())
		{
			return m_DefaultText;
		}
		auto &str =  m_Strings[m_Locale][m_Keys[id]];
		if (str[0] == L'#')
		{
			str = GetText(pugi::as_utf8(str.substr(1)));
		}
		return str;

	}
}
