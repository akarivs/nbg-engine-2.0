#include "PlayersManager.h"

#include <Framework.h>
#include <algorithm>

namespace NBG
{
	///Используется для простой защиты "от дурака" для шифрования сейвов.
	std::string players_version = "player";

    int CBasePlayer::GetSize()
    {
        int size = 0;
        size += sizeof(m_Name); //name
        size += sizeof(bool); //isCreated
        size += sizeof(bool); //isFullscreen
        size += sizeof(float); //soundVolume
        size += sizeof(float); //musicVolume
        size += sizeof(bool); //isCustomCursor        
        return size;
    }
    void CBasePlayer::SetName(const std::wstring name)
    {
		m_Name = name;        
    }

    void CBasePlayer::InitDefaultParams()
    {
        isCreated = true;
        m_IsFullscreen = true;
        m_SoundVolume = 50;
        m_MusicVolume = 50;
        m_IsCustomCursor = true;
    }

    void CBasePlayer::Save(pugi::xml_document * doc)
    {
		pugi::xml_node system = doc->append_child("system");	
		system.append_child("name").append_attribute("value").set_value(pugi::as_utf8(m_Name.c_str()).c_str());
		system.append_child("created").append_attribute("value").set_value(isCreated);
		system.append_child("fullscreen").append_attribute("value").set_value(m_IsFullscreen);
		system.append_child("sound_volume").append_attribute("value").set_value(m_SoundVolume);
		system.append_child("music_volume").append_attribute("value").set_value(m_MusicVolume);
		system.append_child("custom_cursor").append_attribute("value").set_value(m_IsCustomCursor);
    }	

    void CBasePlayer::Load(pugi::xml_document * doc)
    {		
		pugi::xml_node system = doc->child("system");
		m_Name =				pugi::as_wide(system.child("name").attribute("value").value());
		isCreated =				system.child("created").attribute("value").as_bool();
		m_IsFullscreen =		system.child("fullscreen").attribute("value").as_bool();
		m_SoundVolume =			system.child("sound_volume").attribute("value").as_float();
		m_MusicVolume =			system.child("music_volume").attribute("value").as_float();
		m_IsCustomCursor =		system.child("custom_cursor").attribute("value").as_bool();		
    }
    
    CPlayersManager::CPlayersManager()
    {
        m_CurrentPlayer = -1;	
    }

    CPlayersManager::~CPlayersManager()
    {

    }

    void CPlayersManager::InitSavePath(const std::string company, const std::string product)
    {	
		m_SavePath =  g_System->GetSavePath(company, product);
		m_PlayersSave = m_SavePath+"players.dat";
    }

    void CPlayersManager::SetActivePlayer(const int id)
    {
        m_CurrentPlayer = id;
        SaveSystemConfig();
    }


    void CPlayersManager::SavePlayers()
    {	
        SaveSystemConfig();
        for (int i=0; i<m_PlayersCount; i++)
        {
            std::string path = m_SavePath+"player_"+StringUtils::ToString(i)+".dat";
            if (!m_Players[i]->isCreated)
            {        
				g_FileSystem->DeleteFile(path);
                continue;
            }
			pugi::xml_document doc;            
            m_Players[i]->Save(&doc);      			           

			std::ostringstream out;
			doc.save(out);			
			std::string docStr = out.str();
			/*for(int j = 0; j < docStr.size(); j++) 
			{
				docStr[j] = docStr[j] ^ players_version[j%players_version.size()];				
			}	*/				
			FILE * ops = fopen(path.c_str(),"w");
			fwrite(docStr.c_str(),docStr.size(),1,ops);
			fclose(ops);
        }		
    }

    void CPlayersManager::SaveCurrentPlayer()
    {
        SaveSystemConfig();
        int i = m_CurrentPlayer;
        if (m_CurrentPlayer <0)m_CurrentPlayer=0;
        {
            std::string path = m_SavePath+"player_"+StringUtils::ToString(i)+".dat";
            if (!m_Players[i]->isCreated)
            {
				
				g_FileSystem->DeleteFile(path);					
            }
			pugi::xml_document doc;            
            m_Players[i]->Save(&doc);      			

			std::ostringstream out;
			doc.save(out);			
			std::string docStr = out.str();
			/*for(int i = 0; i < docStr.size(); i++) 
			{
				docStr[i] = docStr[i] ^ players_version[i%players_version.size()];				
			}*/					
			FILE * ops = fopen(path.c_str(),"w");
			fwrite(docStr.c_str(),docStr.size(),1,ops);
			fclose(ops);
        }
    }

    void CPlayersManager::LoadPlayers()
    {
        std::string checkPath = m_SavePath+"player_0.dat";
		if (!g_FileSystem->IsFileExists(checkPath.c_str()))
        {			
            g_Config->GetValue("use_default_player",m_IsUseDefaultPlayer);
            if (m_IsUseDefaultPlayer)
            {
                m_Players[0]->InitDefaultParams();
                m_CurrentPlayer = 0;
                SavePlayers();
            }
            else
            {
                m_CurrentPlayer = -1;
            }
			g_GameApplication->SetFullscreen(GetCurrentPlayer()->IsFullscreen());
			g_Cursor->Show(GetCurrentPlayer()->IsCustomCursor());
			g_SoundManager->SetGroupVolume("ambience",GetCurrentPlayer()->GetSoundVolume()/100.0f);
			g_SoundManager->SetGroupVolume("sounds",GetCurrentPlayer()->GetSoundVolume()/100.0f);
			g_SoundManager->SetGroupVolume("music",GetCurrentPlayer()->GetMusicVolume()/100.0f);
            return;
        }
        LoadSystemConfig();

        for (int i=0; i<m_PlayersCount; i++)
        {            
            std::string path = m_SavePath+"player_"+StringUtils::ToString(i)+".dat";
			int offset = 0;
			FILE * ops = fopen(path.c_str(),"r");
			if (ops)
			{
				fseek(ops,0,SEEK_END);
				int size  = ftell(ops);
				fseek(ops,0,SEEK_SET);	
				char * data = new char[size];
				fread(data,size,1,ops);				
				fclose(ops);
				/*for(int j = 0; j < size; j++) 
				{
					data[j] = data[j] ^ players_version[j%players_version.size()];													
				}	*/					 				
				pugi::xml_document doc;
				doc.load_buffer(data,size);
				m_Players[i]->Load(&doc);
				delete[] data;					
			}            
        }		
		g_Cursor->Show(GetCurrentPlayer()->IsCustomCursor());		
		g_GameApplication->SetFullscreen(GetCurrentPlayer()->IsFullscreen());
		g_SoundManager->SetGroupVolume("ambience",GetCurrentPlayer()->GetSoundVolume()/100.0f);
		g_SoundManager->SetGroupVolume("sounds",GetCurrentPlayer()->GetSoundVolume()/100.0f);
		g_SoundManager->SetGroupVolume("music",GetCurrentPlayer()->GetMusicVolume()/100.0f);
    }

    int CPlayersManager::GetPlayersCount()
    {
        int playersCount = 0;
        for (size_t i=0; i<m_Players.size(); i++)
        {
            if (m_Players[i]->isCreated)playersCount++;
        }
        return playersCount;
    }

    void CPlayersManager::SaveSystemConfig()
    {
		pugi::xml_document doc;
		pugi::xml_node config = doc.append_child("config");
		config.append_attribute("current_player").set_value(m_CurrentPlayer);		
		std::ostringstream out;
		doc.save(out);			
		std::string docStr = out.str();
		/*for(int i = 0; i < docStr.size(); i++) 
		{
			docStr[i] = docStr[i] ^ players_version[i%players_version.size()];			
		}		*/
		g_FileSystem->SaveFile(m_PlayersSave,docStr.c_str(),docStr.size());
	}

    void CPlayersManager::LoadSystemConfig()
    {
		auto file = g_FileSystem->ReadFile(m_PlayersSave,"r");		
		if (file.status == IFileSystem::FileOk)
		{
			char * data = file.data;
			/*for(int i = 0; i < file.size; i++) 
			{
				data[i] = data[i] ^ players_version[i%players_version.size()];												
			}*/						 		
			pugi::xml_document doc;
			doc.load_buffer(data,file.size);
			m_CurrentPlayer = doc.first_child().attribute("current_player").as_int();			
			delete[] file.data;			
		}                
    }

    CBasePlayer * CPlayersManager::GetCurrentPlayer()
    {
        if (m_CurrentPlayer == -1)
        {
            return m_DefaultPlayer;
        }

        return m_Players[m_CurrentPlayer];

    }

    void CPlayersManager::AddPlayer(const std::wstring name)
    {
        int id = GetPlayersCount();
        m_Players[id]->InitDefaultParams();
        m_Players[id]->SetName(name);
		if (m_CurrentPlayer != -1)
		{
			m_Players[id]->SetMusicVolume(m_Players[m_CurrentPlayer]->GetMusicVolume());
			m_Players[id]->SetSoundVolume(m_Players[m_CurrentPlayer]->GetSoundVolume());
			m_Players[id]->SetFullscreen(m_Players[m_CurrentPlayer]->IsFullscreen());
			m_Players[id]->SetCustomCursor(m_Players[m_CurrentPlayer]->IsCustomCursor());
		}
        m_CurrentPlayer = id;
        SavePlayers();
    }

    void CPlayersManager::DeletePlayer(const int id)
    {	
		m_Players[id]->isCreated = false;
        for (int i=id;i<m_PlayersCount-1;i++)
        {
            std::swap(m_Players[i],m_Players[i+1]);
            m_Players[i]->SetId(i);
            m_Players[i+1]->SetId(i+1);			
        }
		
        if (m_CurrentPlayer > id)m_CurrentPlayer--;		
		if (m_CurrentPlayer >= GetPlayersCount())m_CurrentPlayer--;		
        if (m_CurrentPlayer < 0)m_CurrentPlayer=0;				
        
        SavePlayers();
    }

    bool CPlayersManager::IsNameDuplicates(const std::wstring name, const bool ignoreCurrentPlayer)
    {
		auto name1 = name;
		std::transform(name1.begin(),name1.end(), name1.begin(),towlower);
        for (int i=0; i<m_PlayersCount; i++)
        {
			if (ignoreCurrentPlayer && i==GetCurrentPlayer()->GetId())continue;
            if (!m_Players[i]->isCreated)continue;
			auto name2 = m_Players[i]->GetName();
			std::transform(name2.begin(),name2.end(), name2.begin(),towlower);
            if (name2 == name1)return true;
        }
        return false;
    }

    void CPlayersManager::AfterInit()
    {
        for (int i=0; i<m_PlayersCount; i++)
        {
            m_Players[i]->AfterInit();
        }
    }
}
