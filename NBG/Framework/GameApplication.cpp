#include "GameApplication.h"

#include <Framework.h>

namespace NBG
{
	CGameApplication::CGameApplication()
	{        
		m_IsApplicationActive = true;
		m_IsInitialized = false;

		m_FullscreenSize = Vector(1024,768);
		m_LogicalSize = Vector(1024,768);
		m_WindowedSize = Vector(1024,768);
		m_IsFullscreen = false;

		m_CurrentSize = m_WindowedSize;

		m_FontsPath				= "xml/fonts.xml";
		m_StringsPath			= "xml/strings.xml";
		m_ConfigPath			= "xml/config.xml";
		m_AtlasesPath			= "atlases";
		m_EditionsXMLPath		= "edition.xml";
		m_EditionsFolderPath	= "xml/editions";

		m_AppTime		= 0.0f;
		m_IsStopped		= false;
		m_IsPaused		= false;
		m_Cursor		= new CCursorObject("Cursor");

		m_OperatingSystem	= new COperatingSystem();
		m_TimeManager		= new CTimeManager();
		m_TextureManager	= new CTextureManager();
		m_FileSystem		= new CFileSystem();
		m_DebugLabel		= NULL;

		m_UpdateLimit = 1.0f/30.0f;
		m_DrawLimit = 1.0f/120.0f;

		m_UpdateCounter = m_DrawCounter = 0.0f;

		m_OnInitCallback = nullptr;
		m_OnDrawCallback = nullptr;
		m_OnUpdateCallback = nullptr;
		m_OnMouseMoveCallback = nullptr;
	}

	CGameApplication::~CGameApplication()
	{

	}

	bool CGameApplication::Init()
	{		
		MathUtils::Init();
		m_System.Init();		
		m_EditionHelper.Init(m_EditionsXMLPath,m_EditionsFolderPath);		
		m_FileSystem->Init();		
		m_Config.LoadConfig(m_ConfigPath);
        return true;
	}

	void CGameApplication::StopApp()
	{
		m_IsStopped = true;
		m_PlayersManager.SaveCurrentPlayer();	
	}

	bool CGameApplication::IsStopped()
	{
		return m_IsStopped;
	}
	
	bool CGameApplication::IsPaused()
	{
		return m_IsPaused;
	}
	
	void CGameApplication::Pause()
	{
		m_IsPaused = true;
	}
	
	void CGameApplication::Resume()
	{
		m_IsPaused = false;
	}
    
    void CGameApplication::ShareContext()
    {
        g_Render->ShareContext();        
    }
    
    void CGameApplication::UnShareContext()
    {
        g_Render->UnShareContext();
    }		

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::IsCheatsEnabled()
	{
		bool cheats = false;
		m_Config.GetValue("cheats",cheats);
		return cheats;
	}

	bool CGameApplication::Run()
	{			
		m_CurrentSize = m_WindowedSize;		
		Vector displaySize =  m_WindowedSize;		
        if (m_System.IsMobileSystem())
        {
             m_CurrentSize = m_FullscreenSize;
			 displaySize =  g_System->GetSystemResolution();
        }		
		
		m_System.UpdateSize(displaySize, m_CurrentSize, m_IsFullscreen);
		m_OperatingSystem->CreateAppWindow();		
		m_TimeManager->Init();		
		m_Random.Init();
#ifdef NBG_IOS
#elif defined(NBG_ANDROID)
		m_SoundManager.Init();		
		m_SoundManager.LoadSounds("xml/sounds.xml");
#else
		m_Render.Init();
        AfterRun();
#endif
		m_OperatingSystem->MainLoop();	
#ifndef NBG_ANDROID		
		m_SoundManager.Destroy();
#endif
		return true;
	}
    
    void CGameApplication::AfterRun()
    {				
        m_TextureManager->InitDefaultTexture();		
		m_FontsManager.LoadFromXML(m_FontsPath);		
		m_LocalizationManager.Init(m_StringsPath);		
		m_AtlasHelper.LoadAtlases(m_AtlasesPath);		
#ifndef NBG_ANDROID
		m_SoundManager.Init();		
#ifndef NBG_WIN32	
		m_SoundManager.LoadSounds("xml/sounds.xml");
#endif
#endif

#ifdef NBG_DEBUG
		CBaseObject * debugContainer = new CBaseObject("DebugContainer");
		debugContainer->SetSize(g_System->GetGameBounds());
		m_DebugString = L"Debug info.\nDraw Calls: %s\nPoly Count: %s\nMouse: %s\nFPS: %s";
		m_DebugLabel = new CLabelObject("DebugLabel");
		m_DebugLabel->Init(m_FontsManager.GetFont(m_DebugLabelFont),m_DebugString);
		m_DebugLabel->SetHotSpot(HS_UL);
		m_DebugLabel->SetAnchor(HS_UL);
		m_DebugLabel->SetScale(m_DebugLabelScale);
		debugContainer->AddChild(m_DebugLabel);
#endif

		
        
		m_TheoraVideoManager = new TheoraVideoManager();		
		OpenAL_AudioInterfaceFactory *	iface_factory = new OpenAL_AudioInterfaceFactory();		

        SetWindowTitle(g_LocaleManager->GetText("common_window_title"));
       
		std::string company, product;
		m_Config.GetValue("company_name",company);
		m_Config.GetValue("product_name",product);
        
		m_PlayersManager.InitSavePath(company,product);
		m_PlayersManager.LoadPlayers();
		m_PlayersManager.AfterInit();

		m_LuaHelper.Init();
        
		std::string scene;
		m_Config.GetValue("start_scene",scene);
		m_ScenesManager.AddScene(scene);

		if (m_OnInitCallback != nullptr)
		{
			m_OnInitCallback();
		}
    }

	void CGameApplication::OnMouseUp(const int button)
	{
		m_ScenesManager.OnMouseUp(button);
	}
	void CGameApplication::OnMouseDown(const int button)
	{
		m_ScenesManager.OnMouseDown(button);
	}

	void CGameApplication::OnKeyUp( const int key )
	{
		if (key == KEY_F8)
		{
			SetFullscreen(!IsFullscreen());
		}
		m_ScenesManager.OnKeyUp(key);
	}
	void CGameApplication::OnKeyDown( const int key )
	{
		m_ScenesManager.OnKeyDown(key);
	}
	void CGameApplication::OnMouseMove()
	{
		m_ScenesManager.OnMouseMove();
		if (m_OnMouseMoveCallback)
		{
			m_OnMouseMoveCallback();
		}
	}

	void CGameApplication::OnBeforeRenderChange()
	{
		m_ScenesManager.OnBeforeRenderChange();
	}

	void CGameApplication::OnAfterRenderChange()
	{
		m_ScenesManager.OnAfterRenderChange();
#ifdef NBG_DEBUG
		if (m_DebugLabel)
		{				
			m_DebugLabel->GetParent()->SetSize(g_System->GetGameBounds());
			m_DebugLabel->UpdateAnchors();
		}
#endif;
	}       

	
	void CGameApplication::Update()
	{		
		m_FrameTime = m_TimeManager->Reset();
		if (m_FrameTime > 0.04f)m_FrameTime = 0.04f;
		m_AppTime += m_FrameTime;

		if (m_FrameTime < m_DrawLimit)
		{
			/*m_DrawLimit = 1/40.0f;
			m_System.Sleep((m_DrawLimit-m_FrameTime)*1000.0f);		
			m_FrameTime += m_TimeManager->Reset();
			*/
		}

		m_SoundManager.Update();
		if (!m_IsApplicationActive)return;
		m_Cursor->ResetState();

		g_Tweener->Update();	
		m_TheoraVideoManager->update(m_FrameTime);		

		int res = m_ScenesManager.Update();

		if (m_OnUpdateCallback != nullptr)
		{
			m_OnUpdateCallback();
		}
		

		if (res > CBaseObject::EventSkipDontProcess)return;
	}

	void CGameApplication::Draw()
	{
		g_Render->Begin();
		m_ScenesManager.Draw();

		if (m_OnDrawCallback != nullptr)
		{
			m_OnDrawCallback();
		}
       

		if (m_Cursor->IsVisible() && g_System->IsMobileSystem() == false)
		{
			m_Cursor->SetPosition(g_MousePos);
			m_Cursor->Draw();
		}	 
#ifdef NBG_DEBUG
		static float time = 0.0f;
		static int frames = 0;
		static int fps = 0;

		frames++;
		time += g_FrameTime;
		if (time >= 1.0f)
		{
			fps = frames;
			time = 0.0f;
			frames = 0;
		}						

		std::wstring text = m_DebugLabel->GetBaseText();
		text = StringUtils::StringReplace(text,std::wstring(L"%s"),StringUtils::ToWString(g_Render->GetDrawCalls()));
		text = StringUtils::StringReplace(text,std::wstring(L"%s"),StringUtils::ToWString(g_Render->GetPolyCount()));
		std::wstring mousePos = StringUtils::ToWString((int)g_MousePos.x);
		mousePos += L" ";
		mousePos += StringUtils::ToWString((int)g_MousePos.y);			
		text = StringUtils::StringReplace(text,std::wstring(L"%s"),mousePos);
		text = StringUtils::StringReplace(text,std::wstring(L"%s"),StringUtils::ToWString(fps));		
		m_DebugLabel->SetText(text);
		m_DebugLabel->Draw();
#endif;				
		g_Render->End();		
	}


	void CGameApplication::ShowDebugInfo(bool show)
	{
		if (m_DebugLabel)
			m_DebugLabel->SetVisible(show);
	}

	CGameApplication * CGameApplication::SetFullscreenSize(const int w, const int h)
	{
		m_FullscreenSize.x = w;
		m_FullscreenSize.y = h;
		return this;
	}

	CGameApplication * CGameApplication::SetLogicalSize(const int w, const int h)
	{		
		m_LogicalSize.x = w;
		m_LogicalSize.y = h;
		return this;
	}

	CGameApplication * CGameApplication::SetWindowedSize(const int w, const int h)
	{
		m_WindowedSize.x = w;
		m_WindowedSize.y = h;
		return this;
	}


	CGameApplication * CGameApplication::SetFontsPath(const std::string &path)
	{
		m_FontsPath = path;
		return this;
	}

	CGameApplication * CGameApplication::SetStringsPath(const std::string &path)
	{
		m_StringsPath = path;
		return this;
	}

	CGameApplication * CGameApplication::SetConfigPath(const std::string &configPath)
	{
		m_ConfigPath = configPath;
		return this;
	}

	CGameApplication * CGameApplication::SetAtlasesPath(const std::string &configPath)
	{
		m_AtlasesPath = configPath;
		return this;
	}


	CGameApplication * CGameApplication::SetEditionsPath(const std::string &xml, const std::string &folder)
	{
		m_EditionsXMLPath = xml;
		m_EditionsFolderPath = folder;
		return this;
	}


	CGameApplication * CGameApplication::SetFullscreen(const bool fullscreen)
	{
		m_IsFullscreen = fullscreen;
		if (fullscreen)
		{
			m_CurrentSize = m_FullscreenSize;
		}
		else
		{
			m_CurrentSize = m_WindowedSize;
		}		
		m_OperatingSystem->SetFullscreenMode(fullscreen);
		return this;
	}

	CGameApplication * CGameApplication::ConfigDebugLabel(const std::string &font, const Vector scale)
	{
		m_DebugLabelFont = font;
		m_DebugLabelScale = scale;
		return this;
	}

	NBG::CConfig * CGameApplication::GetConfig()
	{
		return &m_Config;
	}

	CScenesManager * CGameApplication::GetScenesManager()
	{
		return &m_ScenesManager;
	}

	CFontsManager * CGameApplication::GetFontsManager()
	{
		return &m_FontsManager;
	}

	CLocalizationManager * CGameApplication::GetLocalizationManager()
	{
		return &m_LocalizationManager;
	}

	CPlayersManager * CGameApplication::GetPlayersManager()
	{
		return &m_PlayersManager;
	}

	TheoraVideoManager * CGameApplication::GetTheoraVideoManager()
	{
		return m_TheoraVideoManager;
	}

	CSoundManager * CGameApplication::GetSoundManager()
	{
		return &m_SoundManager;
	}
	
	CCursorObject * CGameApplication::GetCursor()
	{
		return m_Cursor;
	}

	COperatingSystem * CGameApplication::GetOperatingSystem()
	{
		return CAST(COperatingSystem*,m_OperatingSystem);
	}

	CTimeManager * CGameApplication::GetTimeManager()
	{
		return CAST(CTimeManager*,m_TimeManager);
	}

	CTextureManager * CGameApplication::GetTextureManager()
	{
		return CAST(CTextureManager*,m_TextureManager);
	}

	CFileSystem * CGameApplication::GetFileSystem()
	{
		return CAST(CFileSystem*,m_FileSystem);
	}

	CAtlasHelper * CGameApplication::GetAtlasHelper()
	{
		return &m_AtlasHelper;
	}

	CResourcesManager * CGameApplication::GetResourcesManager()
	{
		return &m_ResourcesManager;
	}

	CEditionHelper * CGameApplication::GetEditionHelper()
	{
		return &m_EditionHelper;
	}

	CLuaHelper * CGameApplication::GetLuaHelper()
	{
		return &m_LuaHelper;
	}	

	CRender * CGameApplication::GetRender()
	{
		return &m_Render;
	}	

	CSystem * CGameApplication::GetSystem()
	{
		return &m_System;
	}

	CInput * CGameApplication::GetInput()
	{
		return &m_Input;
	}

	CRandom * CGameApplication::GetRandom()
	{
		return &m_Random;
	}

	CTweener * CGameApplication::GetTweener()
	{
		return &m_Tweener;
	}

	CGuiFactory * CGameApplication::GetGuiFactory()
	{
		return &m_GuiFactory;
	}

	CActionHelper * CGameApplication::GetActionHelper()
	{
		return &m_ActionHelper;
	}

	CActionFactory * CGameApplication::GetActionFactory()
	{
		return &m_ActionFactory;
	}

	CBinaryHelper * CGameApplication::GetBinaryHelper()
	{
		return &m_BinaryHelper;
	}

	CStencilHelper * CGameApplication::GetStencilHelper()
	{
		return &m_StencilHelper;
	}


	void CGameApplication::SetWindowTitle(const std::wstring &title)
	{
        m_OperatingSystem->SetTitle(title);
	}

	double CGameApplication::GetFrameTime()
	{
		return m_FrameTime;
	}

	double CGameApplication::GetAppTime()
	{
		return m_AppTime;
	}

	Vector & CGameApplication::GetWindowSize()
	{
		return m_CurrentSize;
	}

	bool CGameApplication::IsFullscreen()
	{
		return m_IsFullscreen;
	}
}
