#include "InputTextObject.h"

#include <Framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CInputTextObject::CInputTextObject(const std::string id):CBaseObject(id)
	{
		m_Symbols = L"";
		m_CursorPos = 0;
		m_CursorAnimTime = 0.0f;
		m_IsShowCursor = false;
		m_MaxSymbols = 10;

		m_IsFocused = false;
	}

	//////////////////////////////////////////////////////////////////////////
	CInputTextObject::~CInputTextObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::OnLoadFromXMLNode(pugi::xml_node node)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::AfterLoadFromXMLNode()
	{
		m_Background = CAST(CGraphicsObject*,GetChild("bg"));	
		m_Label = CAST(CLabelObject*,m_Background->GetChild("input_label"));	
		m_CursorLabel = CAST(CLabelObject*,m_Background->GetChild("cursor_label"));	

		Init(m_Label->GetText());
	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::Init(const std::wstring str)
	{   
		m_Symbols = str;
		m_CursorPos = m_Symbols.size();
		m_DefaultSymbols = m_Symbols;
		UpdateCursor();
	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::SetText(const std::wstring text)
	{
		m_Label->SetText(text);
		if (GetParent())
		{
			GetParent()->DispatchEvent(Event(Event::ET_StringValueChanged),this);
		}
		m_Symbols = text;
	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::MoveCursorStart()
	{
		m_CursorPos = 0;
		UpdateCursor();
	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::MoveCursorEnd()
	{
		m_CursorPos = m_Symbols.size();
		UpdateCursor();
	}

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::SetPosition(const float x, const float y, const float z)
	{
		CBaseObject::SetPosition(x,y,z);
	}

	//////////////////////////////////////////////////////////////////////////
	int CInputTextObject::OnMouseMove()
	{        
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CInputTextObject::OnMouseUp(const int button)
	{
		if (m_Background->IsContains(g_MousePos))
		{
			m_IsFocused = true;
		}
		else
		{
			m_IsFocused = false;			
		}
		UpdateCursor();
		m_CursorLabel->SetVisible(m_IsFocused);
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CInputTextObject::OnKeyDown(const int key)
	{
		if (!m_IsFocused)return EventSkip;
        
		switch (key)
		{
		case KEY_LEFT:
			m_CursorPos--;
			m_IsShowCursor = true;
			m_CursorAnimTime = 0.7f;
			if (m_CursorPos<0)m_CursorPos=0;
			UpdateCursor();
			g_Input->SetLastInputChar(0);
			break;
		case KEY_RIGHT:
			m_CursorPos++;
			m_IsShowCursor = true;
			m_CursorAnimTime = 0.7f;
			if ((size_t)m_CursorPos>m_Symbols.size())m_CursorPos=m_Symbols.size();
			UpdateCursor();
			g_Input->SetLastInputChar(0);
			break;
		case KEY_BACK:
			m_Symbols = m_Symbols.substr(0,m_Symbols.size()-1);
			SetText(m_Symbols);
			m_CursorPos--;
			m_IsShowCursor = true;
			m_CursorAnimTime = 0.7f;
			if (m_CursorPos < 0)m_CursorPos=0;
			UpdateCursor();
			g_Input->SetLastInputChar(0);
			break;
		case KEY_DELETE:
			if ((size_t)m_CursorPos < m_Symbols.size())
			{
				m_Symbols.erase(m_CursorPos,1);
				m_IsShowCursor = true;
				m_CursorAnimTime = 0.7f;
				SetText(m_Symbols);
				UpdateCursor();
			}
			g_Input->SetLastInputChar(0);
			break;
		case KEY_SPACE:
			g_Input->SetLastInputChar(0);
			break;
		default:			
			if (m_Symbols.size() < (size_t)m_MaxSymbols)
			{				
				std::wstring str = g_Input->GetLastInput();				
				if (str.size() > 0)
				{
					m_Symbols.insert(m_CursorPos, str);
					SetText(m_Symbols);
					m_CursorPos++;
					m_IsShowCursor = true;
					m_CursorAnimTime = 0.7f;
					UpdateCursor();
					g_Input->SetLastInputChar(0);
					return EventBlock;
				}
			}
			break;
		}
		return EventSkip;
	}    

	//////////////////////////////////////////////////////////////////////////
	int CInputTextObject::Update()
	{		
		if (!m_IsFocused)return EventSkip;
		m_CursorAnimTime -= g_FrameTime;
		if (m_CursorAnimTime <= 0.0f)
		{			
			m_CursorAnimTime = 0.7f;
			m_IsShowCursor = !m_IsShowCursor;
			m_CursorLabel->SetVisible(m_IsShowCursor);
		}
		return EventSkip;
	}    

	//////////////////////////////////////////////////////////////////////////
	void CInputTextObject::UpdateCursor()
	{
		if (!m_IsFocused)return;
		Vector pos = m_Label->GetPosition();
		m_CursorLabel->SetPosition(pos.x+m_Label->GetStringWidth(0,m_CursorPos)-m_CursorLabel->GetStringWidth()/2,pos.y);
		m_CursorLabel->SetVisible(true);
	}
}
