#ifndef _AK_FRAMEWORK_ANIMATED_OBJECT
#define _AK_FRAMEWORK_ANIMATED_OBJECT

#include <Framework.h>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Texture.h>

#include <Framework/GUI/GraphicsObject.h>
/*
Графический объект.
*/

namespace NBG
{
	class CBaseObject;

	///Типы анимаций
	enum AnimType
	{
		AT_ForwardOnce,
		AT_BackwardOnce,
		AT_ForwardLoop,
		AT_BackwardLoop,
		AT_PingPong
	};

	/** @brief Класс, реализующий работу с анимированным 2D объектом.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CAnimatedObject : public CBaseObject
	{
	public:
		/// @name Конструктор
		CAnimatedObject(const std::string id);
		virtual ~CAnimatedObject();

		/// @name Загрузка объекта из XML ноды
		virtual void OnLoadFromXMLNode(pugi::xml_node node);

		/// Инициализация объекта
		/// @param texture		- задаёт анимированную текстуру. Текстуры должны лежать в одной папке и быть последовательно названы. Если путь к папке images/anim/test, то нужно передать images/anim/test.png.
		/// @param framesCount	- количество кадров в анимации.
		/// @param interval		- интервал между кадрами.
		/// @param animType		- тип анимации.
		/// @param startFrame	- номер стартового кадра анимации.
		void Init(const std::string texture, const int framesCount, const float interval, const std::string animType, const int startFrame);
		void Init(const std::string texture, const int framesCount, const float interval, AnimType animType, const int startFrame);

		/// Установка анимации.
		void SetAnimation(const std::string texture);

		/// Переключиться на следующий кадр анимации.
		void NextFrame();			

		/// Получить текущий кадр анимации.
		int GetCurrentFrame()
		{
			return m_CurrentFrame;
		};

		/// Приостановить анимацию.
		void Pause();

		/// Остановить анимацию.
		void Stop();

		/// Проиграть анимацию.
		void Play();

		/// Остановлена ли анимация.
		bool IsStoped()
		{
			return m_IsStoped;
		};

		/// Попадает ли точка.
		virtual bool IsContains(const Vector &pos);

		/// Получить эвенты.
		virtual void DispatchEvent(Event event, CBaseObject * object);

		/// Обновить анимацию.
		virtual int Update();

		/// Эвент, когда меняем цвет объекта.
		virtual void OnColorChanged();

		/// Установить текстуру по пути.
		void SetTexture(const std::string texturePath);
		/// Установить текстуру по указателю на ресурс.
		void SetTexture(NBG::CTextureResource * textureRes);

		/// Установить режим смешивания - Additive.
		void SetAdditiveBlending(const bool additive);

		/// Установить режим смешивания - Screen.
		void SetScreenBlending(const bool screen);

		/// Установить текстурные координаты
		void SetUV(FRect uv);
        
        ///Установить повтор текстур
        void SetRepeatedTexture();

		///Установить коллбек на кадр
		void SetCallback(const int frame, VOID_CALLBACK callback);

		///Установить коллбек из LUA
		void SetCallbackLua(const int frame, CCallbackLUAAction * callback);

		///Свойство: множитель скорости анимации
		GETTER_SETTER(float, m_SpeedMultiplier, SpeedMultiplier);

	private:
		/// Объект, в который рисуется графика.
		CGraphicsObject * m_Graphics;

		/// Текущий кадр анимации.
		int m_CurrentFrame;

		/// Количество кадров анимации.
		int m_AnimFrames;

		/// Время между показом кадра.
		float m_PauseTime;

		/// Текущее время.вто
		float m_CurrentTime;

		/// Стартовый кадр.
		int m_StartFrame;

		/// Стоит ли анимация на пауза.
		bool m_IsPaused;

		/// Название анимации, без номера кадра.
		std::string m_AnimName;

		/// Расширение файла.
		std::string m_AnimExtension;

		/// Тип анимации.
		AnimType m_AnimType;

		/// Остановлена ли анимация.
		bool m_IsStoped;

		/// Если режим PingPong - тогда смотрим в какую сторону воспроизводить.
		bool m_IsPongForward;

		/// UV координаты
		FRect m_UV;

		///Карта с коллбеками
		std::map<int,VOID_CALLBACK>m_Callbacks;

		///Карта с коллбеками LUA
		std::map<int,CCallbackLUAAction*>m_CallbacksLua;
	};
}
#endif //_AK_FRAMEWORK_ANIMATED_OBJECT

