#include "CheckboxObject.h"

#include <Framework.h>



namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CCheckBoxObject::CCheckBoxObject(const std::string id):CBaseObject(id)
	{
		m_NormalState = m_OverState = m_ClickedState = m_ClickedOverState = m_DisabledState = m_ActiveState = NULL;
		m_State = ButtonNo;
	}

	//////////////////////////////////////////////////////////////////////////
	CCheckBoxObject::~CCheckBoxObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		std::string texture = node.attribute("texture").value();
		///грузим с диска
		if (texture.find("/") != std::string::npos)
		{
			if (texture.substr(texture.length()-1,1) != "/")texture+="/";
			LoadFromDrive(texture);
		}
		else ///грузим с атласа
		{
			LoadFromAtlas(texture);
		}
		SetHotSpot(m_HotSpot);
	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::LoadFromAtlas(std::string texture)
	{
		m_NormalState = new CGraphicsObject("normal");
		m_NormalState->Init(texture+"_normal");
		m_OverState = new CGraphicsObject("over");
		m_OverState->Init(texture+"_over");
		m_ClickedState = new CGraphicsObject("clicked");
		m_ClickedState->Init(texture+"_clicked");
		m_ClickedOverState = new CGraphicsObject("clicked_over");
		m_ClickedOverState->Init(texture+"_clicked_over");
		m_DisabledState = new CGraphicsObject("disabled");
		m_DisabledState->Init(texture+"_disabled");

		SetSize(m_NormalState->GetSize().x,m_NormalState->GetSize().y);

		AddChild(m_NormalState);
		AddChild(m_OverState);
		AddChild(m_ClickedState);
		AddChild(m_ClickedOverState);
		AddChild(m_DisabledState);


		SetState(ButtonNormal);
	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::LoadFromDrive(std::string folder)
	{
		m_NormalState = new CGraphicsObject("normal");
		m_NormalState->Init(folder+"normal.png");
		m_OverState = new CGraphicsObject("over");
		m_OverState->Init(folder+"over.png");
		m_ClickedState = new CGraphicsObject("clicked");
		m_ClickedState->Init(folder+"clicked.png");
		m_ClickedOverState = new CGraphicsObject("clicked_over");
		m_ClickedOverState->Init(folder+"clicked_over.png");
		m_DisabledState = new CGraphicsObject("disabled");
		m_DisabledState->Init(folder+"disabled.png");


		SetSize(m_NormalState->GetSize().x,m_NormalState->GetSize().y);
		AddChild(m_NormalState);
		AddChild(m_OverState);
		AddChild(m_ClickedState);
		AddChild(m_ClickedOverState);
		AddChild(m_DisabledState);


		SetState(ButtonNormal);
	}

	//////////////////////////////////////////////////////////////////////////
	int CCheckBoxObject::OnMouseMove()
	{
		float x = g_MousePos.x;
		float y = g_MousePos.y;

		if (IsContains(Vector(x,y)))
		{
			if (m_State == ButtonNormal)
			{
				SetState(ButtonOver);
			}
			else if (m_State == ButtonClicked)
			{
				SetState(ButtonClickedOver);
			}
			return EventBlock;
		}
		else
		{
			if (m_State == ButtonOver)
			{
				SetState(ButtonNormal);
			}
			else if (m_State == ButtonClickedOver)
			{
				SetState(ButtonClicked);
			}
		}
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CCheckBoxObject::OnMouseDown(const int button)
	{
		float x = g_MousePos.x;
		float y = g_MousePos.y;

		if (m_State == ButtonOver)
		{
			//SetState(ButtonOver);
			return EventBlock;
		}
		else if (m_State == ButtonClickedOver)
		{
			SetState(ButtonClickedOver);
			return EventBlock;
		}

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CCheckBoxObject::OnMouseUp(const int button)
	{
		float x = g_MousePos.x;
		float y = g_MousePos.y;

		if (IsDisabled())
		{
			if (IsContains(Vector(x,y)))
			{
				GetParent()->DispatchEvent(Event("disabled_click"),this);
			}
		}
		else
		{
			if (m_State == ButtonOver)
			{
				if (IsContains(Vector(x,y)))
				{
					SetState(ButtonClickedOver);
					GetParent()->DispatchEvent(Event(Event::ET_Check),this);
					return EventBlock;
				}
				else
				{
					SetState(ButtonNormal);
				}
			}
			else if (m_State == ButtonClickedOver)
			{
				if (IsContains(Vector(x,y)))
				{
					SetState(ButtonOver);
					GetParent()->DispatchEvent(Event(Event::ET_UnCheck),this);
					return EventBlock;
				}
				else
				{
					SetState(ButtonClicked);
				}
			}
		}

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::Check(const bool isCheck)
	{
		if (isCheck)
		{
			SetState(ButtonClicked);
		}
		else
		{
			SetState(ButtonNormal);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::SetState(ButtonState state)
	{
		if (m_State == state)return;
		m_State = state;		

		m_NormalState->SetVisible(false);
		m_OverState->SetVisible(false);
		m_ClickedState->SetVisible(false);
		m_ClickedOverState->SetVisible(false);
		m_DisabledState->SetVisible(false);

		switch (m_State)
		{
		case ButtonNormal:
			m_ActiveState = m_NormalState;
			break;
		case ButtonOver:
			m_ActiveState = m_OverState;
			break;
		case ButtonClicked:
			m_ActiveState = m_ClickedState;
			break;
		case ButtonClickedOver:			
			m_ActiveState = m_ClickedOverState;
			break;
		case ButtonDisabled:
			m_ActiveState = m_DisabledState;
			break;
		}
		m_ActiveState->SetVisible(true);
	}

	//////////////////////////////////////////////////////////////////////////
	void CCheckBoxObject::OnSetDisabled()
	{
		if (m_IsDisabled)
		{
			SetState(ButtonDisabled);
		}
		else
		{
			SetState(ButtonNormal);
		}
	}
}
