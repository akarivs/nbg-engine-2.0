#ifndef _AK_FRAMEWORK_BASE_OBJECT
#define _AK_FRAMEWORK_BASE_OBJECT

#include <Framework/Utils/MathUtils.h>

#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Transform.h>

#include <../xml/pugixml.hpp>

#include <Framework/Datatypes.h>

using namespace NBG;

namespace NBG
{
	class CBaseObject;
#define BASE_CHILD_VECTOR std::vector<CBaseObject*>

	/** @brief Класс, реализующий работу базового объекта системы.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CBaseObject
	{
	public:
		/// @name Перечисления
		/// Режимы сортировки
		enum SortModes
		{
			SM_Manual,
			SM_ByLayer
		};

		/// Обработка системных сообщений
		enum EventReturnCodes
		{
			///Пропустить сообщение дальше
			EventSkip,
			///Пропустить сообщение дальше, не обрабатывать в классе-наследнике
			EventSkipDontProcess,
			///Заблокировать движение сообщения
			EventBlock,
			///Заблокировать движение сообщения, не посылать события
			EventBlockDontDispatch
		};

		/// @name Конструктор/деструктор
		CBaseObject(const std::string &id);
		virtual ~CBaseObject();


		void LoadFromXML(const std::string path);

		/// @name Загрузка объекта из XML ноды
		void LoadFromXMLNode(pugi::xml_node node, CBaseObject * parent = NULL);

		/// @name Работа с сообщениями
		///Обработать входящее сообщение, посылают либо чайлды, либо из других классов
		virtual void DispatchEvent(Event event, CBaseObject * object);


		/// @name Установка и получение основных свойств объекта
		///Установка ID
		inline void SetId(const std::string &id)
		{
			m_Id = id;
		};
		///Получение ID
		inline std::string GetId()
		{
			return m_Id;
		};

		///Получение трансформации
		inline Transform * GetTransform()
		{
			return &m_Transform;
		};

		///Установка позиции
		void SetPosition(const Vector &position);
		inline void SetPosition(const float &x,const float &y,const float &z=0.0f)
		{
			SetPosition(Vector(x,y,z));
		};
		///Получение позиции
		virtual Vector GetPosition();

		///Установка позиции
		void SetScale(const Vector &scale);
		inline void SetScale(const float &x,const float &y,const float &z=0.0f)
		{
			SetScale(Vector(x,y,z));
		};
		///Получение позиции
		Vector GetScale();

		///Установка поворота в градусах
		inline void SetRotation(const float &angle)
		{
			m_Degree = angle;
			if (m_Degree >= 360.0f)
			{
				m_Degree -= 360.0f;
			}
			else if (m_Degree < 0.0f)
			{
				m_Degree += 360.0f;
			}
			m_Transform.rz = MathUtils::ToRadian(m_Degree);
			OnRotationChanged();
			SendOnChangeParentTransform(this);
		};
		///Получение поворота в градусах
		inline float GetRotation()
		{
			return m_Degree;
		};

		///Установка якоря привязки к родителю
		void SetAnchor(const int &anchor);

		///Установка размера
		inline void SetSize(const Vector &size)
		{
			m_Size.x = size.x;
			m_Size.y = size.y;
			m_Size.z = size.z;
			UpdateHotSpot();
			OnSizeChanged();
			UpdateAnchors();
		}
		inline void SetSize(const float &x,const float &y,const float &z=0.0f)
		{
			SetSize(Vector(x,y,z));
		};
		///Получение размера
		inline Vector GetSize()
		{
			return m_Size;
		};

		///Установка цвета
		void SetColor(Color clr, const bool changeBaseColor = true);
		///Получение цвета
		inline Color GetColor()
		{
			return m_Color;
		};
		inline Color GetBaseColor()
		{
			return m_BaseColor;
		};


		///Установки точки привязки объекта
		void SetHotSpot(const int &type, const Vector offset = Vector(0,0,0));
		///Получение смещения объекта, в зависимости от точки привязки
		Vector GetHotSpotOffset(const int &hotSpot);
		inline int GetHotSpot(){return m_HotSpot;};

		///Установка видимости
		inline void SetVisible(const bool &visible)
		{
			m_IsVisible = visible;
		};
		inline bool IsVisible()
		{
			return m_IsVisible;
		};

		///Установка доступности
		inline void SetDisabled(const bool &disabled)
		{
			m_IsDisabled = disabled;
			OnSetDisabled();
		};
		inline bool IsDisabled()
		{
			return m_IsDisabled;
		};


		///Установка флага для определения кликабельного объекта
		inline void SetClickable(const bool clickable)
		{
			m_Clickable = clickable;
		}

		///Установка флага для удаления объекта
		void Destroy();

		inline bool IsDestroyed()
		{
			return m_IsDestroyed;
		};

		///Попадает ли точка
		virtual bool IsContains(const Vector &pos);


		/// @name Cистема родителей/потомков
		///Добавляет ребенка к текущему объекту
		void AddChild(CBaseObject * child);
		///Получение ребенка по его строковому идентификатору.
		CBaseObject * GetChild(const std::string &id, const bool &isRecursive = false);
		///Удаление ребенка по ID или указателю
		void RemoveChild(const std::string id);
		void RemoveChild(CBaseObject * child);
		///Установить объекту родителя
		void SetParent(CBaseObject * parent);
		///Получение родителя объекта
		inline CBaseObject * GetParent()
		{
			return m_Parent;
		}
		///Получение всех детей объекта
		inline BASE_CHILD_VECTOR & GetChilds()
		{
			return m_Childs;
		}
		///Удаляет всех детей
		void RemoveAllChilds();

		/// @name Cистема сортировки
		///Установка режима сортировки
		inline void SetSortMode(SortModes mode)
		{
			m_SortMode = mode;
		};
		///Получение режима сортировки
		inline SortModes GetSortMode()
		{
			return m_SortMode;
		};

		///Система ручной сортировки
		///Кинуть объект на самый верх
		void BringForward();
		///Кинуть объект на самый низ
		void BringBackward();
		///Кинуть объект перед объектом place
		void BringBefore(CBaseObject * place);
		///Кинуть объект после объекта place
		void BringAfter(CBaseObject * place);

		///Кинуть объект на самый верх
		void BringForward(CBaseObject * child);
		///Кинуть объект на самый низ
		void BringBackward(CBaseObject * child);
		///Кинуть объект перед объектом place
		void BringBefore(CBaseObject * child, CBaseObject * place);
		///Кинуть объект после объекта place
		void BringAfter(CBaseObject * child, CBaseObject * place);

		///Система сортировки по слоям
		///Получить текущий максимальный слой
		int GetTopLayer();
		///Установить слой объекту
		void SetLayer(const int &layer);
		///Получить слой объекта
		inline int GetLayer()
		{
			return m_Layer;
		};
		///Сортировщик, для сортировки по слоям
		static bool SortByLayer(CBaseObject *a, CBaseObject *b);
		///Событие в момент, когда у чайлда меняется слой.
		void OnChildLayerChange(CBaseObject * child);

		/// @name Получение системных сообщений
		///Мышь
		virtual int OnMouseMove();
		virtual int OnMouseDown(const int button);
		virtual int OnMouseUp(const int button);
		///Клавиатура
		virtual int OnKeyUp(const int keyCode);
		virtual int OnKeyDown(const int keyCode);

		/// @name Обновление/отрисовка
		virtual void BeforeRenderChange();
		virtual void AfterRenderChange();
		virtual int Update();
		virtual int Draw();

		/// @name Виртуальные функции-события для наследования
		virtual void OnPositionChanged(){};
		virtual void OnRotationChanged(){};
		virtual void OnScaleChanged(){};
		virtual void OnSizeChanged(){};
		virtual void OnColorChanged(){};
		virtual void OnHotSpotChanged(){};
		virtual void OnLoadFromXMLNode(pugi::xml_node node){};
		virtual void AfterLoadFromXMLNode(){};
		virtual void OnParentTransformChanged(){};
		virtual void OnSetDisabled(){};
		virtual void OnChildAdded(CBaseObject * child){};
		virtual void OnAddedToParent(){};
		virtual bool IsIgnoreEvents(){return false;}; ///Функция, указывающая на то, что чайлд не должен ловить и обрабатывать события

		void SendOnChangeParentTransform(CBaseObject * object); ///сообщаем всем чайлдам, что сменился трансформ у их парента

		void UpdateAnchors(); ///обновление позиции на основе якорей

		///Реализация отложенной сортировки чайлдов.
		enum EDelayedSortType
		{
			Delayed_BringForward,
			Delayed_BringBackward,
			Delayed_BringBefore,
			Delayed_BringAfter,
			Delayed_BringForwardSelf,
			Delayed_BringBackwardSelf,
			Delayed_BringBeforeSelf,
			Delayed_BringAfterSelf,
		};
		void AddDelayedSort(EDelayedSortType sortType, CBaseObject * obj1, CBaseObject * obj2 = NULL)
		{
			SDelayedSortItem item;
			item.type = sortType;
			item.obj1 = obj1;
			item.obj2 = obj2;
			m_DelayedSortItems.push_back(item);
		}
		struct SDelayedSortItem
		{
			EDelayedSortType type;
			CBaseObject * obj1;
			CBaseObject * obj2;
		};
		void ProceedDelayedSort();
		std::vector<SDelayedSortItem>m_DelayedSortItems;

		void SetUseParentColor(bool val);		
		bool GetUseParentColor()
		{
			return m_UseParentColor;
		}
	protected:
		///Основные свойства объекта
		std::string         m_Id;               ///строковый идентификатор объекта, может быть не уникальным
		Transform           m_Transform;        ///трансформация, которая хранит позицию, масштаб, поворот
		Vector              m_Size;             ///размер объекта
		Color               m_Color;            ///цвет объекта
		Color               m_BaseColor;        ///установленный цвет объекта, необходимо для системы родитель/ребенок
		int                 m_Anchor;           ///якорь привязки к родителю
		bool                m_IsVisible;        ///видим ли объект, если не видим - не ловит сообщения и не рисуется.
		bool                m_IsDisabled;       ///получает ли объект сообщения
		bool                m_IsDestroyed;      ///флаг, который помечает объект для удаления
		int                 m_HotSpot;          ///точка привязки объекта
		Vector				m_HotSpotOffset;	///точка привязки, установленная вручную
		float               m_Degree;           ///угол поворота объекта, в градусах
		bool				m_Clickable;		///Объект ловит события мыши
		bool				m_IsFocused;		///Объект в фокусе

		///Система родителей и потомков
		CBaseObject * m_Parent;

		//////////////////////////////////////////////////////////////////////////
		bool m_UseParentColor;

		/**
		* Система сортировки.
		* Сортировать можно двумя способами (первый по умолчанию):
		* 1.  Вручную, добавлять объекты и сортировать их методами BringForward,
		*     BringBackward, BringBefore, BringAfter.
		* 2.  Через систему слоёв. Объекту назначается слой и он вызывает родительскую
		*     функцию OnChildLayerChange, которая пересортирвывает все слои родителя.
		*     Для того, чтобы активировать данный функционал, нужно вызвать функцию
		*     родителя SetSortMode() с параметром SM_ByLayer;
		**/
		int m_TopLayer; ///наибольший слой среди чайлдов
		int m_Layer;    ///слой объекта
		SortModes m_SortMode; ///режим сортировки объектов        
	private:
		Transform m_SavedTransform; ///сохранённая трансформация, для сохранения истинной позиции
		std::vector<Vector> m_BoundPoints; ///ограничивающий прямоугольник

		std::vector<CBaseObject*> m_Childs;
		std::vector<CBaseObject*> m_ChildsToAdd;		
		std::vector<CBaseObject*>::iterator m_ChildsIter;
		std::vector<CBaseObject*>::reverse_iterator m_ChildsRIter;

		///Залочен ли вектор для добавления чайлдов
		bool m_ChildsLock;

		void RemoveDestroyedChilds(); ///удаление чайлдов, которые помечены особым флагом
		void UpdateHotSpot(); ///обновляем позицию на основе хот-спота при смене размера		
	};
}
#endif
