#include "ListObject.h"

#include <Framework.h>


#include <Framework/Utils/Tweener.h>
#include <Framework/GameApplication.h>

#include <Framework/GUI/GraphicsObject.h>
#include <Framework/GUI/ButtonObject.h>
#include <Framework/GUI/LabelObject.h>


namespace NBG
{
    CListObject::CListObject(const std::string id):CBaseObject(id)
    {
		m_IsDrag		= false;   
		m_IsSliderDrag	= false;
		m_Inertion		= 0.0f;
    }

    CListObject::~CListObject()
    {

    }

    void CListObject::DispatchEvent(Event event, CBaseObject * object)
    {		
		if (event.GetType() == Event::ET_MouseUp)  
		{
			for (int i=0; i<m_Elements.size(); i++)
			{
				if (m_Elements[i] == object)
				{
					m_Inertion = 0.0f;
					m_SelectorContainer->SetPosition(object->GetPosition());
					break;
				}
			}
		}
    }
	
    void CListObject::OnLoadFromXMLNode(pugi::xml_node node)
    {
		m_TemplateNode = node.child("template");		
		
		m_ItemHeight = m_TemplateNode.attribute("elementHeight").as_int();

		SetSize(
			m_TemplateNode.attribute("width").as_int(),				
			m_TemplateNode.attribute("height").as_int()
			);
		
		std::ostringstream out;
		m_TemplateNode.first_child().print(out);
		m_TemplateNodePlain = out.str();

		m_PageSize = GetSize().y/m_ItemHeight;

		m_SelectorContainer = new CContainerObject("ListItemsSelector");
		m_SelectorContainer->LoadFromXMLNode(node.child("selector").first_child());	
    }	

	void CListObject::AfterLoadFromXMLNode()
	{
		m_ItemsContainer = new CContainerObject("ListItemsContainer");
		m_ItemsContainer->SetSize(GetSize());		
		AddChild(m_ItemsContainer);		
		m_ItemsContainer->AddChild(m_SelectorContainer);

		m_Slider = CAST(CGraphicsObject*,GetChild("slider"));
		m_Slider->SetRepeatedTexture(true);
	}

    int CListObject::OnMouseDown(const int button)
    {	
		if (m_Slider->IsContains(g_MousePos))
		{
			m_IsSliderDrag = true;
			m_MouseDownPos = g_MousePos;
			m_MouseDownContainerPos = m_Slider->GetPosition();
			m_Inertion = 0.0f;
			return EventBlock;
		}
		if (IsContains(g_MousePos))
		{
			m_IsDrag = true;
			m_Inertion = 0.0f;
			m_MouseDownPos = g_MousePos;
			m_MouseDownContainerPos = m_ItemsContainer->GetPosition();
			m_MouseDownTime = g_GameApplication->GetAppTime();
			return EventBlock;
		}

        int res = CBaseObject::OnMouseDown(button);
        if (res != EventSkip)return res;		

        return EventSkip;
    }

    int CListObject::OnMouseUp(const int button)
    {
		if (m_IsDrag)
		{
			Vector diff = g_MousePos - m_MouseDownPos;
			m_IsDrag = false;
			if (fabs(diff.y) > 5.0f)
			{				
				m_Inertion = (diff.y / ((g_GameApplication->GetAppTime()-m_MouseDownTime)))*3.0f;				
				m_Inertion *= 0.05f;
				return EventBlock;
			}			
		}
		if (m_IsSliderDrag)
		{
			m_IsSliderDrag = false;
		}
		int res = CBaseObject::OnMouseUp(button);
        if (res != EventSkip)return res;				

        return EventSkip;
    }

    int CListObject::OnMouseMove()
    {        
        int res = CBaseObject::OnMouseMove();
        if (res != EventSkip)return res;
       
		if (m_IsDrag)
		{
			Vector diff = g_MousePos - m_MouseDownPos;
			Vector pos = m_MouseDownContainerPos;
			auto trans = GetTransform()->GetAbsoluteTransform();
			diff.x /= trans.scalex;
			diff.y /= trans.scaley;
			pos.y +=  diff.y;
			if (pos.y > 0.0f)pos.y = 0.0f;
			if (pos.y < -m_MaxHeight)pos.y = -m_MaxHeight;			
			m_ItemsContainer->SetPosition(pos);

			if (m_MaxHeight > 0)
			{
				Vector posSlider = m_Slider->GetPosition();
				posSlider.y = fabs(pos.y)/m_MaxHeight * m_SliderPathHeight;
				m_Slider->SetPosition(posSlider);
			}
			return EventBlock;
		}

		if (m_IsSliderDrag)
		{
			Vector diff = g_MousePos - m_MouseDownPos;
			Vector pos = m_MouseDownContainerPos;
			auto trans = GetTransform()->GetAbsoluteTransform();
			diff.x /= trans.scalex;
			diff.y /= trans.scaley;

			pos.y +=  diff.y;
			if (pos.y < 0.0f)pos.y = 0.0f;
			if (pos.y > m_SliderPathHeight)pos.y = m_SliderPathHeight;			
			m_Slider->SetPosition(pos);



			float percent = pos.y / m_SliderPathHeight;

			Vector containerPos = m_ItemsContainer->GetPosition();
			containerPos.y = -(m_MaxHeight*percent);
			m_ItemsContainer->SetPosition(containerPos);
			return EventBlock;
		}
        return EventSkip;
    }	  

	void CListObject::AddElement(STRING_VECTOR  &keys,STRING_VECTOR &values)
	{
		CContainerObject * obj = new CContainerObject("ListItem");

		std::string nodeString = m_TemplateNodePlain;
		for (int i=0; i<keys.size(); i++)
		{
			nodeString = StringUtils::StringReplace(nodeString,keys[i],values[i],true);			
		}
		pugi::xml_document doc;
		doc.load_buffer(nodeString.c_str(),nodeString.size());		
		obj->LoadFromXMLNode(doc.first_child());		
		obj->SetPosition(0.0f,m_Elements.size()*m_ItemHeight);
		m_ItemsContainer->AddChild(obj);
		m_Elements.push_back(obj);	

		m_PagesCount = m_Elements.size()*m_ItemHeight/m_Size.y;		
		m_MaxHeight = (m_Elements.size()-1)*m_ItemHeight;
		m_MaxHeight -= (GetSize().y - m_ItemHeight);
		if (m_MaxHeight <0)
		{
			m_MaxHeight = 0;
		}

		UpdateSliderSize();
	}

	//////////////////////////////////////////////////////////////////////////
	void CListObject::UpdateSliderSize()
	{
		if (m_MaxHeight <= 0)
		{
			m_Slider->SetSize(m_Slider->GetSize().x, GetSize().y);		
			m_SliderPathHeight = 0;		
		}
		else
		{
			float percent = GetSize().y / (GetSize().y+m_MaxHeight);
			float sizeOfItem = GetSize().y * percent;		

		
			m_Slider->SetSize(m_Slider->GetSize().x, sizeOfItem);		
			m_SliderPathHeight = GetSize().y - sizeOfItem;		
		}
		
	}

	int CListObject::Draw()
	{
		if (m_Inertion > 0.0f)
		{
			m_Inertion -= 50.0f*g_FrameTime;
			if (m_Inertion <= 0.0f)m_Inertion=0.0f;
		}
		else if (m_Inertion < 0.0f)
		{
			m_Inertion += 50.0f*g_FrameTime;
			if (m_Inertion >= 0.0f)m_Inertion=0.0f;
		}
		Vector pos = m_ItemsContainer->GetPosition();
		pos.y += m_Inertion*g_FrameTime;
		if (pos.y > 0.0f)pos.y = 0.0f;
		if (pos.y < -m_MaxHeight)pos.y = -m_MaxHeight;

		if (m_MaxHeight > 0)
		{
			Vector posSlider = m_Slider->GetPosition();
			posSlider.y = fabs(pos.y)/m_MaxHeight * m_SliderPathHeight;
			m_Slider->SetPosition(posSlider);
		}
		auto absTrans = GetTransform()->GetAbsoluteTransform();
		auto size = GetSize();
		size.x *= absTrans.scalex;
		size.y *= absTrans.scaley;
		m_ItemsContainer->SetPosition(pos);
		g_StencilHelper->Begin();		
		g_StencilHelper->DrawRectangle(&absTrans,size);
		g_StencilHelper->Switch();
		CBaseObject::Draw();		
		g_StencilHelper->End();
		return EventSkip;
	}
}
