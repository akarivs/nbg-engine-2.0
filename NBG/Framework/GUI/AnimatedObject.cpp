#include "AnimatedObject.h"	

#include <Framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CAnimatedObject::CAnimatedObject(const std::string id):CBaseObject(id)
	{
		m_Graphics = new CGraphicsObject(id);
		AddChild(m_Graphics);
		m_IsStoped = true;
		m_IsPongForward = true;

		m_IsPaused = true;
		m_UV.left = -1.0f;
		m_SpeedMultiplier = 1.0f;
	}

	//////////////////////////////////////////////////////////////////////////
	CAnimatedObject::~CAnimatedObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		m_AnimName = node.attribute("texture").value();

		m_AnimFrames    = node.attribute("frames").as_int();
		m_StartFrame    = node.attribute("start_frame").as_int();		
		m_PauseTime     = node.attribute("interval").as_float();        
		std::string animType = node.attribute("animType").value();

		Init(m_AnimName,m_AnimFrames,m_PauseTime,animType,m_StartFrame);

		if (node.attribute("play_from_start").as_bool())
		{
			if (node.attribute("randomStartFrame").as_bool())
			{
				m_CurrentFrame = m_StartFrame + g_Random->RandI(0,m_AnimFrames-1);
			}
			Play();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetAnimation(const std::string texture)
	{
		m_AnimExtension = ".";
		m_AnimExtension += StringUtils::GetExtension(texture);
		m_AnimName = StringUtils::RemoveExtension(texture);

		SetTexture(m_AnimName + "/" + NBG::StringUtils::ToString(m_CurrentFrame) + m_AnimExtension);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetAdditiveBlending(const bool additive)
	{
		m_Graphics->SetBlendMode(BLENDMODE_ADD);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetScreenBlending(const bool screen)
	{
		m_Graphics->SetBlendMode(BLENDMODE_MOD);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::Pause()
	{
		m_IsPaused = true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::Stop()
	{
		m_IsPaused = true;
		if (m_AnimType == AT_ForwardOnce)
		{			
			m_CurrentFrame = m_StartFrame;
		}
		else if (m_AnimType == AT_ForwardLoop)
		{
			m_CurrentFrame = m_StartFrame;
		}
		else if (m_AnimType == AT_BackwardOnce)
		{			
			m_CurrentFrame = m_AnimFrames-1;		
		}
		else if (m_AnimType == AT_BackwardLoop)
		{			
			m_CurrentFrame = m_AnimFrames-1;			
		}
		else if (m_AnimType == AT_PingPong)
		{
			m_CurrentFrame = m_StartFrame;			
		}
		SetTexture(m_AnimName + "/" + StringUtils::ToString(m_CurrentFrame)+m_AnimExtension);		
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::Play()
	{
		m_IsPaused = false;
		m_IsStoped = false;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetUV(FRect uv)
	{
		m_UV = uv;
		m_Graphics->SetUV(uv);
	}
    
    //////////////////////////////////////////////////////////////////////////
    void CAnimatedObject::SetRepeatedTexture()
    {
        for (int i=m_StartFrame; i<m_AnimFrames; i++)
        {
            CTextureResource * res = CAST(CTextureResource*, g_ResManager->GetResource(m_AnimName + "/" + StringUtils::ToString(i)+m_AnimExtension));
            if (res)
            {
                g_GameApplication->GetTextureManager()->SetRepeatTexture(res->GetTexture());
            }
			m_Graphics->SetRepeatedTexture(true);
        }
    }

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::Init(const std::string texture, const int framesCount, const float interval, const std::string animType, const int startFrame)
	{
		if (animType == "forwardOnce")
		{
			m_AnimType = AT_ForwardOnce;			
		}
		else if (animType == "forwardLoop")
		{
			m_AnimType = AT_ForwardLoop;			
		}
		else if (animType == "backwardOnce")
		{
			m_AnimType = AT_BackwardOnce;			
		}
		else if (animType == "backwardLoop")
		{
			m_AnimType = AT_BackwardLoop;
		}
		else if (animType == "pingPong")
		{
			m_AnimType = AT_PingPong;		
		}		
		Init(texture,framesCount,interval, m_AnimType,startFrame);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::Init(const std::string texture, const int framesCount, const float interval, AnimType animType, const int startFrame)
	{
		m_AnimExtension = ".";
		m_AnimExtension += StringUtils::GetExtension(texture);
		m_AnimName = StringUtils::RemoveExtension(texture);



		m_AnimFrames = framesCount;
		m_PauseTime = interval;
		m_StartFrame = startFrame;

		m_AnimFrames += m_StartFrame;

		m_CurrentTime   = 0.0f;
		m_CurrentFrame  = m_StartFrame;

		m_AnimType = animType;

		if (m_AnimType == AT_ForwardOnce)
		{			
			m_CurrentFrame = m_StartFrame;			
		}
		else if (m_AnimType == AT_ForwardLoop)
		{
			m_CurrentFrame = m_StartFrame;
		}
		else if (m_AnimType == AT_BackwardOnce)
		{
			m_CurrentFrame = m_AnimFrames-1;			
		}
		else if (m_AnimType == AT_BackwardLoop)
		{
			m_CurrentFrame = m_AnimFrames-1;

		}
		else if (m_AnimType == AT_PingPong)
		{
			m_CurrentFrame = m_StartFrame;
		}
		SetTexture(m_AnimName + "/" + StringUtils::ToString(m_CurrentFrame) + m_AnimExtension);
	}

	//////////////////////////////////////////////////////////////////////////
	bool CAnimatedObject::IsContains(const Vector &pos)
	{
		return m_Graphics->IsContains(pos);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::DispatchEvent(Event event, CBaseObject * object)
	{
		GetParent()->DispatchEvent(event,object);
	}

	//////////////////////////////////////////////////////////////////////////
	int CAnimatedObject::Update()
	{
		CBaseObject::Update();
		if (m_IsStoped || m_IsPaused)return EventSkip;
		m_CurrentTime += g_FrameTime * m_SpeedMultiplier;
		while (m_CurrentTime > m_PauseTime)
		{
			m_CurrentTime -= m_PauseTime;			
			NextFrame();           
		}
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::NextFrame()
	{
		switch (m_AnimType)
		{
		case AT_ForwardOnce:
			m_CurrentFrame++;
			if(m_CurrentFrame >= m_AnimFrames)
			{
				m_CurrentFrame = m_AnimFrames-1;
				m_IsStoped = true;
			}			
			break;
		case AT_ForwardLoop:
			m_CurrentFrame++;
			if(m_CurrentFrame >= m_AnimFrames)
			{
				m_CurrentFrame = m_StartFrame;
			}
			break;
		case AT_BackwardOnce:
			m_CurrentFrame--;
			if(m_CurrentFrame < m_StartFrame)
			{
				m_CurrentFrame = m_StartFrame;
				m_IsStoped = true;
			}
			break;
		case AT_BackwardLoop:
			m_CurrentFrame--;
			if(m_CurrentFrame < m_StartFrame)
			{
				m_CurrentFrame = m_AnimFrames-1;
			}
			break;
		case AT_PingPong:
			if (m_IsPongForward)
			{
				m_CurrentFrame++;
				if(m_CurrentFrame >= m_AnimFrames)
				{
					m_CurrentFrame = m_AnimFrames-1;
					m_IsPongForward = false;
				}
			}
			else
			{
				m_CurrentFrame--;
				if(m_CurrentFrame < m_StartFrame)
				{
					m_CurrentFrame = m_StartFrame;
					m_IsPongForward = true;
				}
			}
			break;
		}		
		if (m_Callbacks.size() > 0)
		{
			auto iter = m_Callbacks.find(m_CurrentFrame);
			if (iter != m_Callbacks.end())
			{
				iter->second();				
			}
		}
		if (m_CallbacksLua.size() > 0)
		{
			auto iter = m_CallbacksLua.find(m_CurrentFrame);
			if (iter != m_CallbacksLua.end())
			{
				iter->second->Start();				
			}
		}
		SetTexture(m_AnimName + "/" + StringUtils::ToString(m_CurrentFrame) + m_AnimExtension);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetTexture(const std::string texturePath)
	{		
		m_Graphics->SetTexture(texturePath);
		SetSize(m_Graphics->GetSize());
		if (m_UV.left != -1.0f)
		{
			m_Graphics->SetUV(m_UV);
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetTexture(NBG::CTextureResource * textureRes)
	{
		m_Graphics->SetTexture(textureRes);
		SetSize(m_Graphics->GetSize());
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::OnColorChanged()
	{
		//m_Graphics->SetColor(GetColor());
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetCallback(const int frame, VOID_CALLBACK callback)
	{
		m_Callbacks[frame] = callback;
	}

	//////////////////////////////////////////////////////////////////////////
	void CAnimatedObject::SetCallbackLua(const int frame, CCallbackLUAAction * callback)
	{
		m_CallbacksLua[frame] = callback;
	}
}
