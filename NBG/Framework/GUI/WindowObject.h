#ifndef _AK_FRAMEWORK_WINDOW_OBJECT
#define _AK_FRAMEWORK_WINDOW_OBJECT

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Texture.h>

#include <Framework/GUI/GraphicsObject.h>
/*
    Графический объект.
*/

namespace NBG
{
    class CBaseObject;

    class CWindowObject : public CBaseObject
    {
        public:           
            CWindowObject(const std::string id);
            virtual ~CWindowObject();

            /// @name Загрузка объекта из XML ноды
            virtual void OnLoadFromXMLNode(pugi::xml_node node);

			virtual void Init(){};


            
			void Show();
			void Hide();


            virtual int OnMouseMove();
            virtual int OnMouseUp(const int button);
            virtual int OnMouseDown(const int button);

			virtual int Update();
			virtual int Draw();

			
			virtual void OnShow(){};
			virtual void OnHide(){};

			virtual void OnShowComplete(){};
			virtual void OnHideComplete(){};

			GETTER_SETTER(bool,m_DestroyAtHide,DestroyAtHide);
        protected:  
			static void EventOnHided(void * userData);
			static void EventOnShowed(void * userData);
			bool m_IsModal;
			bool m_ShowDarker;
			float m_DarkerAlpha;
        protected:			
        };
}

#endif //_AK_FRAMEWORK_WINDOW_OBJECT
