#include "BaseObject.h"


#include <algorithm>


#include <Framework/Utils/StringUtils.h>
#include <Framework/Utils/MathUtils.h>

#include <Framework.h>

using namespace NBG;
namespace NBG
{
    //==============================================================================
    CBaseObject::CBaseObject(const std::string &id)
    {
        ///Основные параметры
        m_Id            = id;
        m_IsVisible     = true;
        m_IsDisabled    = false;
        m_IsDestroyed   = false;
        m_HotSpot       = HS_MID;
        m_Degree        = 0.0f;
		m_Clickable		= false;
		m_IsFocused		= false;

		m_UseParentColor = false;

        m_Anchor        = HS_MID;

        ///Система parent/child
        m_Parent = NULL;

        ///Система сортировки
        m_Layer     = 0;
        m_TopLayer  = 0;
        SetSortMode(SM_ByLayer);

        ///Другое
        m_BoundPoints.resize(4);

		m_ChildsLock = false;
    }

//#define DISPLAY_ALL_DELETIONS

    //==============================================================================
    CBaseObject::~CBaseObject()
    {
#ifdef DISPLAY_ALL_DELETIONS
		int count = 0;
		CBaseObject * parent = GetParent();
		while (parent)
		{
			parent = parent->GetParent();
			count++;
		}
		std::string t = "";
		for (int i=0; i<count; i++)t+="\t";
		CONSOLE("%s %s",t.c_str(), GetId().c_str());
#endif
		m_ChildsIter=m_Childs.begin();
        while (m_ChildsIter!=m_Childs.end())
        {
            CBaseObject * obj = (*m_ChildsIter);
			if (obj)
            {
                delete obj;
                obj = NULL;
                m_ChildsIter++;
            }
        }
    }

	void CBaseObject::Destroy()
	{
		m_IsDestroyed = true;
		if (!m_Parent)
		{
			delete this;
		}
	}

	//==============================================================================
	void CBaseObject::LoadFromXML(const std::string path)
	{
		CXMLResource * xml = CAST(CXMLResource*, g_ResManager->GetResource(g_EditionHelper->ConvertPath(path)));              
		pugi::xml_document * doc = xml->GetXML();
		LoadFromXMLNode(doc->first_child());	
		g_ResManager->ReleaseResource(xml);
		
	}

    //==============================================================================
    void CBaseObject::LoadFromXMLNode(pugi::xml_node node, CBaseObject * parent)
    {
        if (node.empty())return;
        CBaseObject * obj = this;
        ///Если есть парент, значит нужно создать его чайлда
        if (parent)
        {
            while(!node.empty())
            {				
				if (std::string(node.name()) == "object")
				{					
					obj = g_GuiFactory->create(node.attribute("type").value());
					obj->LoadFromXMLNode(node);
					parent->AddChild(obj);					
				}
				node = node.next_sibling();
            }
        }
        else ///иначе ставим параметры текущему объекту
        {			
			if (!node.attribute("id").empty())
			{
				SetId(node.attribute("id").value());
			}
			
            float x = node.attribute("x").as_float();
            float y = node.attribute("y").as_float();
            float z = node.attribute("z").as_float();
            float width = node.attribute("width").as_float();
            float height = node.attribute("height").as_float();
			int layer = node.attribute("layer").as_int();
			float angle = node.attribute("angle").as_float();

            float sx = 1.0f;
            float sy = 1.0f;
            float sz = 1.0f;

            if (!node.attribute("sx").empty())
                 sx = node.attribute("sx").as_float();
            if (!node.attribute("sy").empty())
                 sy = node.attribute("sy").as_float();
            if (!node.attribute("sz").empty())
                 sz = node.attribute("sz").as_float();

            Color clr;
            if (!node.attribute("color").empty())
            {
                clr.SetColor(node.attribute("color").value());
            }

			if (!node.attribute("hide").empty())
			{
				SetVisible(!node.attribute("hide").as_bool());
			}            

			m_Clickable = node.attribute("clickable").as_bool();

            SetPosition(x,y,z);
            SetSize(width,height);
            SetScale(sx,sy,sz);
            SetRotation(angle);
            SetColor(clr);
			SetLayer(layer);

            SetHotSpot(StringUtils::StringToHotSpot(node.attribute("hot_spot").value()));
            SetAnchor(StringUtils::StringToHotSpot(node.attribute("anchor").value()));
            OnLoadFromXMLNode(node);

			if (!node.attribute("disable").empty())
			{
				SetDisabled(node.attribute("disable").as_bool());				
			}  

            LoadFromXMLNode(node.first_child(),this);
			AfterLoadFromXMLNode();
			if (m_SortMode == SM_ByLayer)
			{
				///stable_sort не меняет порядок одинаковых элементов. То что нам нужно :)
				std::stable_sort(m_Childs.begin(),m_Childs.end(),CBaseObject::SortByLayer);				
			}
        }
		UpdateAnchors();
    }

    //==============================================================================
    void CBaseObject::DispatchEvent(Event event, CBaseObject * object)
    {
        switch (event.GetType())
        {
        case Event::ET_MouseUp:

            break;
        }
    }

    //==============================================================================
    void CBaseObject::SetPosition(const Vector &position)
    {
        m_Transform.x = position.x;
        m_Transform.y = position.y;
        m_Transform.z = position.z;

        m_SavedTransform = m_Transform;
		if (!m_Parent)
		{
			OnPositionChanged();
		}
        UpdateAnchors();
    }

    //==============================================================================
    Vector CBaseObject::GetPosition()
    {
        return Vector(m_SavedTransform.x,m_SavedTransform.y,m_SavedTransform.z);
    }

    //==============================================================================
    void CBaseObject::SetScale(const Vector &scale)
    {
        m_Transform.scalex = scale.x;
        m_Transform.scaley = scale.y;
        m_Transform.scalez = scale.z;

		OnScaleChanged();
        SendOnChangeParentTransform(this);
    }

    //==============================================================================
    Vector CBaseObject::GetScale()
    {
        return Vector(m_Transform.scalex,m_Transform.scaley,m_Transform.scalez);
    }

    //==============================================================================
    void CBaseObject::SetAnchor(const int &anchor)
    {
        m_Anchor = anchor;
        UpdateAnchors();
    }

	//////////////////////////////////////////////////////////////////////////
	void CBaseObject::SetUseParentColor(bool val)
	{
		m_UseParentColor = val;
		OnColorChanged();
		for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
		{
			CBaseObject * obj = (*m_ChildsIter);
			obj->SetUseParentColor(val);			
		}		
	}

	//==============================================================================
	void CBaseObject::SetColor(Color clr, const bool changeBaseColor)
	{     
		if (changeBaseColor)
        {
            m_BaseColor = clr;
        }
	    m_Color = clr;		

		OnColorChanged();		
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
        {
            CBaseObject * obj = (*m_ChildsIter);
            Color baseColor = obj->GetBaseColor();
            baseColor.a *= m_Color.a/255.0f;
			if (m_UseParentColor)
			{
				baseColor.r *= m_Color.r/255.0f;
				baseColor.g *= m_Color.g/255.0f;
				baseColor.b *= m_Color.b/255.0f;
			}
            obj->SetColor(baseColor, false);
        }
    }

    //==============================================================================
    void CBaseObject::SetHotSpot(const int &type, const Vector offset)
    {
        m_HotSpot = type;
		m_HotSpotOffset = offset;
        UpdateHotSpot();
        OnHotSpotChanged();
    }

    //==============================================================================
    Vector CBaseObject::GetHotSpotOffset(const int &hotSpot)
    {
        Vector ret;
        switch (hotSpot)
        {
        case HS_UL:
            ret.x = 0.0f;
            ret.y = 0.0f;
            ret.z = 0.0f;
            break;
        case HS_MU:
            ret.x = GetSize().x/2.0f;
            ret.y = 0.0f;
            ret.z = 0.0f;
            break;
        case HS_UR:
            ret.x = GetSize().x;
            ret.y = 0.0f;
            ret.z = 0.0f;
            break;
        case HS_ML:
            ret.x = 0.0f;
            ret.y = GetSize().y/2.0f;
            ret.z = 0.0f;
            break;
        case HS_MID:
            ret.x = GetSize().x/2.0f;
            ret.y = GetSize().y/2.0f;
            ret.z = 0.0f;
            break;
        case HS_MR:
            ret.x = GetSize().x;
            ret.y = GetSize().y/2.0f;
            ret.z = 0.0f;
            break;
        case HS_DL:
            ret.x = 0.0f;
            ret.y = GetSize().y;
            ret.z = 0.0f;
            break;
        case HS_MD:
            ret.x = GetSize().x/2.0f;
            ret.y = GetSize().y;
            ret.z = 0.0f;
            break;
        case HS_DR:
            ret.x = GetSize().x;
            ret.y = GetSize().y;
            ret.z = 0.0f;
            break;
		case HS_CUSTOM:
			return m_HotSpotOffset;
			break;
        }
        return ret;
    }

    //==============================================================================
    bool CBaseObject::IsContains(const Vector &pos)
    {
        ///Создаём матрицу из текущий трансформации
        Matrix m;
        m.CreateFromTransform(m_Transform.GetAbsoluteTransform());

        ///Заполняем вершины ограничивающего прямоугольника
        Vector pUL(0.0f,0.0f);
        Vector pUR(GetSize().x,0.0f);
        Vector pDL(0.0f,GetSize().y);
        Vector pDR(GetSize().x,GetSize().y);

        ///Применяем матрицу к вершинам
        pUL = m.TransformPoint(pUL);
        pUR = m.TransformPoint(pUR);
        pDL = m.TransformPoint(pDL);
        pDR = m.TransformPoint(pDR);

        ///Проверяем, попала ли точка в прямоугольник
        m_BoundPoints[0] = pUL;
        m_BoundPoints[1] = pUR;
        m_BoundPoints[2] = pDR;
        m_BoundPoints[3] = pDL;
        return MathUtils::IsPointInsidePoly(pos, m_BoundPoints);
    }

    //==============================================================================
    void CBaseObject::AddChild(CBaseObject * child)
    {
        if (!child)
        {
            CONSOLE("Child is NULL!");
            return;
        }
        if (child == this)
        {
            CONSOLE("You try to add object as his child! %s");
            return;
        }

		///Если вектор защищён от записи, тогда добавляем объект в вектор
		if (m_ChildsLock)
		{
			m_ChildsToAdd.push_back(child);
			return;
		}

		if (child->GetParent())
		{
			CONSOLE("Object - %s was already added to parent %s!'", child->GetId().c_str(), child->GetParent()->GetId().c_str());
			child->GetParent()->RemoveChild(child);			
		}
		
		m_Childs.push_back(child);
		child->SetParent(this);
		child->UpdateAnchors();
		OnChildAdded(child);			
		child->OnAddedToParent();
		///Обновление закешированного значения максимального уровня
		if (m_SortMode == SM_ByLayer)
		{
			int layer = child->GetLayer();
			if (layer > m_TopLayer)
			{
				m_TopLayer = layer;
			}
			///stable_sort не меняет порядок одинаковых элементов. То что нам нужно :)
			std::stable_sort(m_Childs.begin(),m_Childs.end(),CBaseObject::SortByLayer);	
		}       
    }

    //==============================================================================
    CBaseObject * CBaseObject::GetChild(const std::string &id, const bool &isRecursive)
    {        
        for (size_t i=0; i<m_Childs.size(); ++i)
        {
            CBaseObject * child = m_Childs[i];
            if (child->GetId() == id)
            {
                return child;
            }
			else
			{
				if (isRecursive)
				{
					child = child->GetChild(id,true);
					if (child)return child;
				}
			}
        }
		for (size_t i=0; i<m_ChildsToAdd.size(); ++i)
        {
            CBaseObject * child = m_ChildsToAdd[i];
            if (child->GetId() == id)
            {
                return child;
            }
			else
			{
				if (isRecursive)
				{
					child = child->GetChild(id,true);
					if (child)return child;
				}
			}
        }		
        return NULL;
    }

    //==============================================================================
    void CBaseObject::RemoveChild(const std::string id)
    {
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
        {
            if ((*m_ChildsIter)->GetId() == id)
            {
                (*m_ChildsIter)->SetParent(NULL);
                m_Childs.erase(m_ChildsIter);
                return;
            }
        }
    }

    //==============================================================================
    void CBaseObject::RemoveChild(CBaseObject * child)
    {
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
        {
            if ((*m_ChildsIter) == child)
            {
                (*m_ChildsIter)->SetParent(NULL);
                m_Childs.erase(m_ChildsIter);
                return;
            }
        }
    }

    //==============================================================================
    void CBaseObject::SetParent(CBaseObject * parent)
    {
        m_Parent = parent;
        if (m_Parent)
        {
            m_Transform.SetParent(parent->GetTransform());
        }
        SendOnChangeParentTransform(this);
    }

    //==============================================================================
    void CBaseObject::RemoveAllChilds()
    {
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
        {
            CBaseObject * obj = (*m_ChildsIter);
            if (obj)
            {
                delete obj;
                obj = NULL;
            }
        }
        m_Childs.clear();
    }

	//==============================================================================
    void CBaseObject::BringForward()
	{	
		if (!m_Parent)
		{
			AddDelayedSort(Delayed_BringForwardSelf,NULL);
			return;
		}
		m_Parent->BringForward(this);
	}
    
	//==============================================================================
	void CBaseObject::BringBackward()
	{
		if (!m_Parent)
		{
			AddDelayedSort(Delayed_BringBackwardSelf,NULL);
			return;
		}
		m_Parent->BringBackward(this);
	}

	//==============================================================================
	void CBaseObject::BringBefore( CBaseObject * place)
	{
		if (!m_Parent)
		{
			AddDelayedSort(Delayed_BringBeforeSelf,place);
			return;
		}
		m_Parent->BringBefore(this,place);	
	}
	
	//==============================================================================
	void CBaseObject::BringAfter(CBaseObject * place)
	{
		if (!m_Parent)
		{
			AddDelayedSort(Delayed_BringAfterSelf,place);
			return;
		}
		m_Parent->BringAfter(this,place);		
	}

	//==============================================================================
    void CBaseObject::BringForward(CBaseObject * child)
	{			
		if (m_ChildsLock)
		{
			AddDelayedSort(Delayed_BringForward,child);
			return;
		}

		if (m_SortMode == SM_Manual)
		{
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
			{
				if ((*m_ChildsIter) == child)
				{
					m_Childs.erase(m_ChildsIter);
					break;
				}
			}
			m_Childs.push_back(child);
		}
		else if (m_SortMode == SM_ByLayer)
		{
			child->SetLayer(m_TopLayer+1);
		}

        
	}
    
	//==============================================================================
	void CBaseObject::BringBackward(CBaseObject * child)
	{
		if (m_ChildsLock)
		{
			AddDelayedSort(Delayed_BringBackward,child);
			return;
		}
		if (m_SortMode == SM_Manual)
		{
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
			{
				if ((*m_ChildsIter) == child)
				{
					m_Childs.erase(m_ChildsIter);
					break;
				}
			}
			m_Childs.insert(m_Childs.begin(), child);
		}
		else if (m_SortMode == SM_ByLayer)
		{
			child->SetLayer(0);
		}		
	}

	//==============================================================================
	void CBaseObject::BringBefore(CBaseObject * child, CBaseObject * place)
	{
		if (m_ChildsLock)
		{
			AddDelayedSort(Delayed_BringBefore,child,place);
			return;
		}
		if (m_SortMode == SM_Manual)
		{
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
			{
				if ((*m_ChildsIter) == child)
				{
					m_Childs.erase(m_ChildsIter);
					break;
				}
			}
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
			{
				if ((*m_ChildsIter) == place)
				{
					m_ChildsIter++;
					m_Childs.insert(m_ChildsIter, child);
					break;
				}
			}
		}
		else if (m_SortMode == SM_ByLayer)
		{
			child->SetLayer(place->GetLayer()-1);
		}	
       
	
	}
	
	//==============================================================================
	void CBaseObject::BringAfter(CBaseObject * child, CBaseObject * place)
	{
		if (m_ChildsLock)
		{
			AddDelayedSort(Delayed_BringAfter,child,place);
			return;
		}
		if (m_SortMode == SM_Manual)
		{
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
			{
				if ((*m_ChildsIter) == child)
				{
					m_Childs.erase(m_ChildsIter);
					break;
				}
			}
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); m_ChildsIter++)
			{
				if ((*m_ChildsIter) == place)
				{
					m_Childs.insert(m_ChildsIter, child);
					break;
				}
			}	
		}
		else if (m_SortMode == SM_ByLayer)
		{
			child->SetLayer(place->GetLayer()+1);
		}	
	}
	
    //==============================================================================
    int CBaseObject::GetTopLayer()
    {
        return m_TopLayer;
    }

    //==============================================================================
    void CBaseObject::SetLayer(const int &layer)
    {
        m_Layer = layer;

        if (m_Parent && m_Parent->GetSortMode() == SM_ByLayer)
        {
            m_Parent->OnChildLayerChange(this);
        }
    }

    //==============================================================================
    bool CBaseObject::SortByLayer(CBaseObject *a, CBaseObject *b)
    {
        return (a->GetLayer() < b->GetLayer());
    }

    //==============================================================================
    void CBaseObject::OnChildLayerChange(CBaseObject * child)
    {
        if (child->GetLayer() > m_TopLayer)
        {
            m_TopLayer = child->GetLayer();
        }
		std::stable_sort(m_Childs.begin(),m_Childs.end(),CBaseObject::SortByLayer);
    }		

	//==============================================================================
    int CBaseObject::OnMouseMove()
    {			
        if (!m_IsVisible || m_IsDisabled)
        {
            return EventSkipDontProcess;
        }
		
		if (IsIgnoreEvents())return EventSkip;				
		if (m_Clickable)
        {   			
			bool contains = IsContains(g_MousePos);
			if (contains && m_IsFocused == false)
			{
				DispatchEvent(Event(Event::ET_Over),this);
				m_IsFocused = true;
				return EventSkip;
			}  
			else if (!contains && m_IsFocused == true)
			{
				DispatchEvent(Event(Event::ET_Out),this);
				m_IsFocused = false;
				return EventBlock;
			}
			else if (contains && m_IsFocused)
			{
				return EventBlock;
			}			
        }
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
			if (IsIgnoreEvents())continue;					
            EventReturnCodes code = (EventReturnCodes)obj->OnMouseMove();		
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                if (code == EventBlock)
                {
					m_ChildsLock = false;
                    DispatchEvent(Event(Event::ET_MouseMove),obj);					
                    return EventBlockDontDispatch;					
                }
				m_ChildsLock = false;
                return code;
            }			
        }		
		m_ChildsLock = false;		
        return EventSkip;
    }

    //==============================================================================
    int CBaseObject::OnMouseDown(const int button)
    {
        if (!m_IsVisible || m_IsDisabled)
        {
            return EventSkipDontProcess;
        }

		if (m_Clickable && m_IsFocused)
        {
			DispatchEvent(Event(Event::ET_MouseDown),this);
            return EventBlock;
        }


		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            EventReturnCodes code = (EventReturnCodes)obj->OnMouseDown(button);
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                if (code == EventBlock)
                {
					m_ChildsLock = false;
                    DispatchEvent(Event(Event::ET_MouseDown),obj);					
                    return EventBlockDontDispatch;
                }
				m_ChildsLock = false;
                return code;
            }
        }
		m_ChildsLock = false;
        return EventSkip;
    }

    //==============================================================================
    int CBaseObject::OnMouseUp(const int button)
    {
        if (!m_IsVisible || m_IsDisabled)
        {
            return EventSkipDontProcess;
        }

		if (m_Clickable && m_IsFocused)
		{   
			//TODO: мега тщательную проверку сделать игр, может что-то сломать.
			DispatchEvent(Event(Event::ET_MouseUp),this);
			return EventBlockDontDispatch;
		}

		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            EventReturnCodes code = (EventReturnCodes)obj->OnMouseUp(button);
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                if (code == EventBlock)
                {
					m_ChildsLock = false;
                    DispatchEvent(Event(Event::ET_MouseUp),obj);					
                    return EventBlockDontDispatch;
                }
				m_ChildsLock = false;
                return code;
            }
        }
		m_ChildsLock = false;
        return EventSkip;
    }

    //==============================================================================
    int CBaseObject::OnKeyUp(const int keyCode)
    {
        if (!m_IsVisible || m_IsDisabled)
        {
            return EventSkipDontProcess;
        }
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            EventReturnCodes code = (EventReturnCodes)obj->OnKeyUp(keyCode);
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                if (code == EventBlock)
                {
					m_ChildsLock = false;
                    DispatchEvent(Event(Event::ET_KeyUp),obj);					
                    return EventBlockDontDispatch;
                }
				m_ChildsLock = false;
                return code;
            }
        }
		m_ChildsLock = false;
        return EventSkip;
    }

    //==============================================================================
    int CBaseObject::OnKeyDown(const int keyCode)
    {
        if (!m_IsVisible || m_IsDisabled)
        {
            return EventSkipDontProcess;
        }
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            EventReturnCodes code = (EventReturnCodes)obj->OnKeyDown(keyCode);
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                if (code == EventBlock)
                {
					m_ChildsLock = false;
                    DispatchEvent(Event(Event::ET_KeyDown),obj);					
                    return EventBlockDontDispatch;
                }
				m_ChildsLock = false;
                return code;
            }
        }
		m_ChildsLock = false;
        return EventSkip;
    }

	void CBaseObject::BeforeRenderChange()
	{
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            obj->BeforeRenderChange();          
        }
		m_ChildsLock = false;        
	}

	void CBaseObject::AfterRenderChange()
	{
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            obj->AfterRenderChange();          
        }
		m_ChildsLock = false;   
	}

	//////////////////////////////////////////////////////////////////////////
	void CBaseObject::ProceedDelayedSort()
	{
		for (int i=0; i<m_DelayedSortItems.size(); i++)
		{
			auto &item = m_DelayedSortItems[i];
			switch (item.type)
			{
			case Delayed_BringForward:
				BringForward(item.obj1);
				break;
			case Delayed_BringBackward:
				BringBackward(item.obj1);
				break;
			case Delayed_BringBefore:
				BringBefore(item.obj1,item.obj2);
				break;
			case Delayed_BringAfter:
				BringAfter(item.obj1,item.obj2);
				break;
			case Delayed_BringForwardSelf:
				if (m_Parent)BringForward();
				break;
			case Delayed_BringBackwardSelf:
				if (m_Parent)BringBackward();
				break;
			case Delayed_BringBeforeSelf:
				if (m_Parent)BringBefore(item.obj1);
				break;
			case Delayed_BringAfterSelf:
				if (m_Parent)BringAfter(item.obj1);
				break;
			}
		}
		m_DelayedSortItems.clear();
	}


    //==============================================================================
    int CBaseObject::Update()
    {	
		if (m_ChildsToAdd.size() > 0)
		{
			for (size_t i=0; i<m_ChildsToAdd.size(); i++)
			{
				AddChild(m_ChildsToAdd[i]);
			}
			m_ChildsToAdd.clear();
		}
		if (m_Childs.size() == 0)return EventSkip;
		if (m_DelayedSortItems.size() > 0)ProceedDelayedSort();


        if (!m_IsVisible)
        {
            return EventSkipDontProcess;
        }		
        EventReturnCodes code = EventSkip;
        bool isNeedToDestroy = true;		
		m_ChildsLock = true;
        for (m_ChildsRIter=m_Childs.rbegin(); m_ChildsRIter!=m_Childs.rend(); ++m_ChildsRIter)
        {
            CBaseObject * obj = (*m_ChildsRIter);
            if (obj->IsDestroyed())
            {
                isNeedToDestroy = true;
                continue;
            }
            code = (EventReturnCodes)obj->Update();
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
                break;
            }
        }

		m_ChildsLock = false;
        if (isNeedToDestroy)
        {			
            RemoveDestroyedChilds();
        }	
        return code;
    }

    //==============================================================================
    int CBaseObject::Draw()
    {
        if (!m_IsVisible)
        {
            return EventSkipDontProcess;
        }
		m_ChildsLock = true;
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
        {
            CBaseObject * obj = (*m_ChildsIter);
            EventReturnCodes code = (EventReturnCodes)obj->Draw();
            if (!(code == EventSkip || code == EventSkipDontProcess))
            {
				m_ChildsLock = false;
                return code;
            }
        }
		m_ChildsLock = false;
        return EventSkip;
    }

    //==============================================================================
    void CBaseObject::UpdateAnchors()
    {		
        SendOnChangeParentTransform(this);
        ///Если нет родителя, тогда нет смысла менять что-то
        if (!m_Parent)
		{
			for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
			{
				CBaseObject * obj = (*m_ChildsIter);
				obj->UpdateAnchors();
			}
			return;
		}

        ///Получение размеров родителя
        Vector parentSize = m_Parent->GetSize();

        ///Если у родителя не указан размер - привязываемся к экрану
        if (parentSize.x == 0.0f && parentSize.y == 0.0f)
        {
			parentSize = g_GameApplication->GetWindowSize();			
        }

        //if (m_Anchor != HS_MID)
        {
			Vector parentOffset = m_Parent->GetHotSpotOffset(m_Parent->GetHotSpot());

            ///Получаем смещение относительно родителя, на основе якоря привязки
            switch (m_Anchor)
            {
            case HS_UL:
                m_Transform.x = m_SavedTransform.x - parentSize.x/2.0f;
                m_Transform.y = m_SavedTransform.y - parentSize.y/2.0f;
                break;
            case HS_MU:
                m_Transform.y = m_SavedTransform.y - parentSize.y/2.0f;
                break;
            case HS_UR:
                m_Transform.x = m_SavedTransform.x + parentSize.x/2.0f;
                m_Transform.y = m_SavedTransform.y - parentSize.y/2.0f;
                break;
            case HS_ML:
                m_Transform.x = m_SavedTransform.x - parentSize.x/2.0f;
                break;
            case HS_MR:
                m_Transform.x = m_SavedTransform.x + parentSize.x/2.0f;
                break;
            case HS_DL:
                m_Transform.x = m_SavedTransform.x - parentSize.x/2.0f;
                m_Transform.y = m_SavedTransform.y + parentSize.y/2.0f;
                break;
            case HS_MD:
                m_Transform.y = m_SavedTransform.y + parentSize.y/2.0f;
                break;
            case HS_DR:
                m_Transform.x = m_SavedTransform.x + parentSize.x/2.0f;
                m_Transform.y = m_SavedTransform.y + parentSize.y/2.0f;
                break;
            case HS_MID:
                m_Transform.x = m_SavedTransform.x;
                m_Transform.y = m_SavedTransform.y;
                break;						
            }

            ///А теперь корректируем позицию, на основе хот-спота родителя			
            switch (m_Parent->GetHotSpot())
            {
            case HS_UL:
                m_Transform.x += parentSize.x/2.0f;
                m_Transform.y += parentSize.y/2.0f;
                break;
            case HS_MU:
                m_Transform.y += parentSize.y/2.0f;
                break;
            case HS_UR:
                m_Transform.x -= parentSize.x/2.0f;
                m_Transform.y += parentSize.y/2.0f;
                break;
            case HS_ML:
                m_Transform.x += parentSize.x/2.0f;
                break;
            case HS_MR:
                m_Transform.x -= parentSize.x/2.0f;
                break;
            case HS_DL:
                m_Transform.x += parentSize.x/2.0f;
                m_Transform.y -= parentSize.y/2.0f;
                break;
            case HS_MD:
                m_Transform.y -= parentSize.y/2.0f;
                break;
            case HS_DR:
                m_Transform.x -= parentSize.x/2.0f;
                m_Transform.y -= parentSize.y/2.0f;
                break;
            }
        }
        OnPositionChanged();
        for (m_ChildsIter=m_Childs.begin(); m_ChildsIter!=m_Childs.end(); ++m_ChildsIter)
        {
            CBaseObject * obj = (*m_ChildsIter);
            obj->UpdateAnchors();
        }
    }

    //==============================================================================
    void CBaseObject::RemoveDestroyedChilds()
    {
        m_ChildsIter=m_Childs.begin();
        while (m_ChildsIter!=m_Childs.end())
        {
            CBaseObject * obj = (*m_ChildsIter);
            if (obj->IsDestroyed())
            {				
                delete obj;
                m_ChildsIter = m_Childs.erase(m_ChildsIter);
                continue;
            }
            m_ChildsIter++;
        }
    }

    //==============================================================================
    void CBaseObject::UpdateHotSpot()
    {
        Vector offset = GetHotSpotOffset(m_HotSpot);		
        m_Transform.hx = offset.x;
        m_Transform.hy = offset.y;
        m_Transform.hz = offset.z;
        SendOnChangeParentTransform(this);		
    }

    //==============================================================================
    void CBaseObject::SendOnChangeParentTransform(CBaseObject * object)
    {		
        BASE_CHILD_VECTOR * childs = &object->GetChilds();
        m_ChildsIter=childs->begin();
        while (m_ChildsIter!=childs->end())
        {
            CBaseObject * obj = (*m_ChildsIter);
            obj->OnParentTransformChanged();
            obj->SendOnChangeParentTransform(obj);
            m_ChildsIter++;
        }
    }
}