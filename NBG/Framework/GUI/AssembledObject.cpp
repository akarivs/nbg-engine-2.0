#include "AssembledObject.h"

#include <framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CAssembledObject::CAssembledObject(const std::string id):CBaseObject(id)
	{
	}

	//////////////////////////////////////////////////////////////////////////
	CAssembledObject::~CAssembledObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CAssembledObject::DispatchEvent(Event event, CBaseObject * object)
	{
		GetParent()->DispatchEvent(event,object);
	}

	//////////////////////////////////////////////////////////////////////////
	void CAssembledObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		Vector size = GetSize();
		Init(node.attribute("texture").value(),node.attribute("width").as_float(),node.attribute("height").as_float(), node.attribute("stretch").as_bool());
	}

	//////////////////////////////////////////////////////////////////////////
	void CAssembledObject::Init(const std::string &texture, const float width, const float height, const bool stretched)
	{
		RemoveAllChilds();

		CGraphicsObject * ul = new CGraphicsObject(texture+"/ul.png");
		CGraphicsObject * ur = new CGraphicsObject(texture+"/ur.png");
		CGraphicsObject * dl = new CGraphicsObject(texture+"/dl.png");
		CGraphicsObject * dr = new CGraphicsObject(texture+"/dr.png");

		ul->Init(texture+"/ul.png");
		ur->Init(texture+"/ur.png");
		dl->Init(texture+"/dl.png");
		dr->Init(texture+"/dr.png");

		ul->SetAnchor(HS_UL);
		ul->SetHotSpot(HS_UL);
		ur->SetAnchor(HS_UR);
		ur->SetHotSpot(HS_UR);
		dl->SetAnchor(HS_DL);
		dl->SetHotSpot(HS_DL);
		dr->SetAnchor(HS_DR);
		dr->SetHotSpot(HS_DR);

		Vector midPartSize = ul->GetSize();        
		Vector cornerPartSize = ul->GetSize();

		float w,h;
		w = width;
		h = height;
		if (width < cornerPartSize.x*2.0f)
		{
			w = cornerPartSize.x*2.0f;
		}
		if (height < cornerPartSize.y*2.0f)
		{
			h = cornerPartSize.y*2.0f;
		}
		SetSize(w, h);

		AddChild(ul);
		AddChild(ur);
		AddChild(dl);
		AddChild(dr);

		if (stretched)
		{
			float difference = w-cornerPartSize.x*2.0f;

			CGraphicsObject * mu = new CGraphicsObject(texture+"_mu");
			mu->Init(texture+"/mu.png");
			mu->SetHotSpot(HS_UL);
			mu->SetAnchor(HS_UL);
			mu->SetPosition(cornerPartSize.x,0.0f);
			mu->SetScale(difference,1.0f);
			AddChild(mu);

			CGraphicsObject * md = new CGraphicsObject(texture+"_mu");
			md->Init(texture+"/md.png");
			md->SetHotSpot(HS_DL);
			md->SetAnchor(HS_DL);
			md->SetPosition(cornerPartSize.x,0.0f);
			md->SetScale(difference,1.0f);
			AddChild(md);

			float differenceH = h-cornerPartSize.y*2.0f;

			CGraphicsObject * ml = new CGraphicsObject(texture+"_ml");
			ml->Init(texture+"/ml.png");
			ml->SetHotSpot(HS_UL);
			ml->SetAnchor(HS_UL);
			ml->SetPosition(0.0f,cornerPartSize.y);
			ml->SetScale(1.0f, differenceH);
			AddChild(ml);

			CGraphicsObject * mr = new CGraphicsObject(texture+"_md");
			mr->Init(texture+"/mr.png");
			mr->SetHotSpot(HS_UR);
			mr->SetAnchor(HS_UR);
			mr->SetPosition(0.0f,cornerPartSize.y);
			mr->SetScale(1.0f, differenceH);
			AddChild(mr);

			CGraphicsObject * mid = new CGraphicsObject(texture+"_mid");
			mid->Init(texture+"/mid.png");
			mid->SetHotSpot(HS_UL);
			mid->SetAnchor(HS_UL);
			mid->SetPosition(cornerPartSize.x,cornerPartSize.y);
			mid->SetScale(difference, differenceH);
			AddChild(mid);
		}
		else
		{
			int widthBlocksCount = 0;
			int heightBlocksCount = 0;

			//заполняем блоки по ширине
			if (w > cornerPartSize.x*2.0f)
			{
				float difference = w-cornerPartSize.x*2.0f;
				int count = ceil(difference/midPartSize.x);
				if (count==0)count=1;
				widthBlocksCount = count;
				for (int i=0; i<count; i++)
				{
					CGraphicsObject * mu = new CGraphicsObject(texture+"_mu");
					mu->Init(texture+"/mu.png");
					mu->SetHotSpot(HS_UL);
					mu->SetAnchor(HS_UL);
					mu->SetPosition(cornerPartSize.x+(midPartSize.x*i),0.0f);

					CGraphicsObject * md = new CGraphicsObject(texture+"_md");
					md->Init(texture+"/md.png");
					md->SetHotSpot(HS_DL);
					md->SetAnchor(HS_DL);
					md->SetPosition(cornerPartSize.x+(midPartSize.x*i),0.0f);

					difference -= midPartSize.x;
					if (difference < 0)
					{
						difference = midPartSize.x + difference;
						mu->CutImage(difference/midPartSize.x, 1.0f);
						md->CutImage(difference/midPartSize.x, 1.0f);
					}
					AddChild(mu);
					AddChild(md);
				}
			}

			//заполняем блоки по высоте
			if (h > cornerPartSize.y*2.0f)
			{
				float difference = h-cornerPartSize.y*2.0f;
				int count = ceil(difference/midPartSize.y);
				if (count==0)count=1;
				heightBlocksCount = count;
				for (int i=0; i<count; i++)
				{
					CGraphicsObject * ml = new CGraphicsObject(texture+"_ml");
					ml->Init(texture+"/ml.png");
					ml->SetHotSpot(HS_UL);
					ml->SetAnchor(HS_UL);
					ml->SetPosition(0.0f,cornerPartSize.y+(midPartSize.y*i));

					CGraphicsObject * mr = new CGraphicsObject(texture+"_md");
					mr->Init(texture+"/mr.png");
					mr->SetHotSpot(HS_UR);
					mr->SetAnchor(HS_UR);
					mr->SetPosition(0.0f,cornerPartSize.y+(midPartSize.y*i));

					difference -= midPartSize.y;
					if (difference < 0)
					{
						difference = midPartSize.y + difference;
						ml->CutImage(1.0f,difference/midPartSize.y);
						mr->CutImage(1.0f,difference/midPartSize.y);
					}
					AddChild(ml);
					AddChild(mr);
				}
			}

			//заполняем центральные блоки
			float xDiff = w-cornerPartSize.x*2.0f;
			for (int x=0; x<widthBlocksCount; x++)
			{
				xDiff -= midPartSize.x;
				float yDiff = h-cornerPartSize.y*2.0f;
				for (int y=0; y<heightBlocksCount; y++)
				{
					CGraphicsObject * mid = new CGraphicsObject(texture+"_ml");
					mid->Init(texture+"/mid.png");
					mid->SetHotSpot(HS_UL);
					mid->SetAnchor(HS_UL);
					mid->SetPosition(cornerPartSize.x+(midPartSize.x*x),cornerPartSize.y+(midPartSize.y*y));

					float xPercent = 1.0f;
					float yPercent = 1.0f;
					yDiff -= midPartSize.y;

					if (xDiff < 0)
					{
						xPercent = (midPartSize.x + xDiff)/midPartSize.x;
					}
					if (yDiff < 0)
					{
						yPercent = (midPartSize.y + yDiff)/midPartSize.y;
					}

					mid->CutImage(xPercent,yPercent);

					AddChild(mid);
				}
			}
		}
	}
}
