#ifndef _AK_FRAMEWORK_CHECKBOX_OBJECT
#define _AK_FRAMEWORK_CHECKBOX_OBJECT

#include <Framework/BaseScene.h>
#include <Framework/GUI/GraphicsObject.h>

namespace NBG
{
	class CBaseObject;

	/** @brief Класс, реализующий работу с чекбоксом.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CCheckBoxObject : public CBaseObject
	{
	public:
		/// @name Конструктор/деструктор
		CCheckBoxObject(const std::string id);
		virtual ~CCheckBoxObject();

		/// Событие при загрузке объекта с XML ноды.
		virtual void OnLoadFromXMLNode(pugi::xml_node node);

		/// Состояния кнопки.
		enum ButtonState
		{
			ButtonNo,
			ButtonNormal,
			ButtonOver,
			ButtonClicked,
			ButtonClickedOver,
			ButtonDisabled
		};

		/// @name Системные события
		virtual int OnMouseMove();
		virtual int OnMouseUp(const int button);
		virtual int OnMouseDown(const int button);

		/// Событие, когда блокируют кнопку.
		virtual void OnSetDisabled();

		/// Поставить или снять флаг.
		void Check(const bool isCheck);

		/// DEPRECATED: загрузка с атласа.
		virtual void LoadFromAtlas(std::string texture);

		/// REFACTOR_THIS: Загрузка с диска или атласа.
		virtual void LoadFromDrive(std::string folder);
	private:
		/// @name Состояния кнопки.
		NBG::CGraphicsObject * m_NormalState;
		NBG::CGraphicsObject * m_OverState;
		NBG::CGraphicsObject * m_ClickedState;
		NBG::CGraphicsObject * m_ClickedOverState;
		NBG::CGraphicsObject * m_DisabledState;

		/// Текущее состояние
		NBG::CGraphicsObject * m_ActiveState;

		/// Установить состояние кнопки.
		void SetState(ButtonState state);

		/// Текущее состояние кнопки.
		ButtonState m_State;	
	};
}
#endif

