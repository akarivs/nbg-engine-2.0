#include "WindowObject.h"

#include <Framework.h>
#include <Framework/GameApplication.h>
#include <Framework/Global/Resources/XMLResource.h>

namespace NBG
{
	CWindowObject::CWindowObject(const std::string id):CBaseObject(id)
	{
		m_IsModal = false;
		m_ShowDarker = false;
		m_DarkerAlpha = 0.0f;
		m_DestroyAtHide = true;
	}

	CWindowObject::~CWindowObject()
	{

	}

	/// @name Загрузка объекта из XML ноды
	void CWindowObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		m_IsModal = node.attribute("modal").as_bool();
		m_ShowDarker = node.attribute("darker").as_bool();
		SetScale(0.0f,0.0f);
		SetDisabled(true);
		SetVisible(false);
	}    

	int CWindowObject::OnMouseMove()
	{
		int res = CBaseObject::OnMouseMove();
		if (res != EventSkip)return res;
		if (m_IsModal)return EventBlock;
		return EventSkip;
	}

	int CWindowObject::OnMouseDown(const int button)
	{
		int res = CBaseObject::OnMouseDown(button);
		if (res != EventSkip)return res;
		if (m_IsModal)return EventBlock;
		return EventSkip;
	}
	int CWindowObject::OnMouseUp(const int button)
	{
		int res = CBaseObject::OnMouseUp(button);
		if (res != EventSkip)return res;
		if (m_IsModal)return EventBlock;
		return EventSkip;
	}

	int CWindowObject::Update()
	{
		int res = CBaseObject::Update();
		if (res != EventSkip)return res;

		if (m_IsModal)
		{
			return EventBlock;
		}
		return EventSkip;
	}

	int CWindowObject::Draw()
	{
		if (m_ShowDarker)
		{			
			g_Render->DrawLine(Vector(-683,0),Vector(683,0),384,Color(m_DarkerAlpha,0,0,0));
		}
		
		int res = CBaseObject::Draw();
		if (res != EventSkip)return res;		
		return EventSkip;
	}

	void CWindowObject::Show()
	{
		BringForward();
		OnShow();
		SetDisabled(false);	
		SetVisible(true);
		SetColor(Color(0,255,255,255));
		NBG::g_Tweener->ScaleTo(this,0.15f,1.0f,1.0f,0.0f,CWindowObject::EventOnShowed,EASE_LINEAR,this);
		NBG::g_Tweener->AlphaTo(this,0.15f,255.0f);
		NBG::g_Tweener->FloatValueTo(&m_DarkerAlpha,0.15f,100.0f);
	}

	void CWindowObject::Hide()
	{
		OnHide();
		SetDisabled(true);
		NBG::g_Tweener->ScaleTo(this,0.15f,1.2f,1.2f,0.0f,CWindowObject::EventOnHided,EASE_LINEAR,this);
		NBG::g_Tweener->AlphaTo(this,0.15f,0.0f);
		NBG::g_Tweener->FloatValueTo(&m_DarkerAlpha,0.15f,0.0f);
	}

	void CWindowObject::EventOnShowed(void * userData)
	{
		CWindowObject * window = CAST(CWindowObject*, userData);
		window->OnShowComplete();		
	}

	void CWindowObject::EventOnHided(void * userData)
	{
		CWindowObject * window = CAST(CWindowObject*, userData);
		window->OnHideComplete();
		window->SetVisible(false);
		window->SetDisabled(true);
		if (window->GetDestroyAtHide())
		{
			window->Destroy();		
		}
		
	}
}
