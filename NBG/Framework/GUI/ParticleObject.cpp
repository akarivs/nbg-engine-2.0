#include "ParticleObject.h"



#include <Framework.h>


namespace NBG
{
	CParticleObject::CParticleObject(const std::string id):CBaseObject(id)
	{
		m_Emmiter = NULL;
	}

	CParticleObject::~CParticleObject()
	{
		
	}

	/// @name Загрузка объекта из XML ноды
	void CParticleObject::OnLoadFromXMLNode(pugi::xml_node node)
	{		
		std::string path = node.attribute("path").value();		
		bool playFromStart = node.attribute("play_from_start").as_bool();
		float playTime= node.attribute("opaque").as_float();

		Init(path,playFromStart, playTime);
	}


	void CParticleObject::Init(const std::string &path, const bool playFromStart, const float playTime)
	{		
		m_Path = path;		

		NBG::CXMLResource * xml = CAST(NBG::CXMLResource*, NBG::g_ResManager->GetResource(path));
		pugi::xml_document * doc = xml->GetXML();
		pugi::xml_node level_node = doc->first_child();
		int count;
		std::string texture = level_node.attribute("texture").value();
		
		count = level_node.attribute("count").as_int();	

		m_Emmiter = new CEmmiter(level_node.attribute("id").value());
		m_Emmiter->Init(count,texture);

		pugi::xml_node object = level_node.find_child_by_attribute("id","velocity");
		if(object.empty())
		{
			CONSOLE("velocity param is not exist");
		}
		else
		{
			m_Emmiter->SetVelocity(object.attribute("velocity").as_float(),
				object.attribute("max_velocity").as_float());
		}
		object = level_node.find_child_by_attribute("id","angle");
		if(object.empty())
		{
			CONSOLE("angle param is not exist");
		}
		else
		{
			m_Emmiter->SetAngle(object.attribute("angle").as_float(),
				object.attribute("max_angle").as_float());
		}
		object = level_node.find_child_by_attribute("id","size");
		if(object.empty())
		{
			CONSOLE("size param is not exist");
		}
		else
		{
			Vector size = Vector(object.attribute("sizex").as_float(),
				object.attribute("sizey").as_float(), object.attribute("sizez").as_float());
			Vector maxSize = Vector(object.attribute("maxsizex").as_float(),
				object.attribute("maxsizey").as_float(), object.attribute("maxsizez").as_float());
			m_Emmiter->SetSize(size,maxSize);
		}
		object = level_node.find_child_by_attribute("id","color");
		if(object.empty())
		{
			CONSOLE("color param is not exist");
		}
		else
		{
			pugi::xml_node color = object.find_child_by_attribute("id","start");
			Color start = Color(color.attribute("a").as_int(),
				color.attribute("r").as_int(),
				color.attribute("g").as_int(),
				color.attribute("b").as_int());
			color = object.find_child_by_attribute("id","mid");
			Color mid = Color(color.attribute("a").as_int(),
				color.attribute("r").as_int(),
				color.attribute("g").as_int(),
				color.attribute("b").as_int());
			color = object.find_child_by_attribute("id","end");
			Color end = Color(color.attribute("a").as_int(),
				color.attribute("r").as_int(),
				color.attribute("g").as_int(),
				color.attribute("b").as_int());
			m_Emmiter->SetEmmiterColor(start,mid,end);
		}
		object = level_node.find_child_by_attribute("id","life_time");
		if(object.empty())
		{
			CONSOLE("life_time param is not exist");
		}
		else
		{
			m_Emmiter->SetLifeTime(object.attribute("time").as_float(),
				object.attribute("max_time").as_float());
		}
		object = level_node.find_child_by_attribute("id","angular_velocity");
		if(object.empty())
		{
			CONSOLE("angular_velocity param is not exist");
		}
		else
		{
			m_Emmiter->SetAngularVelocity(object.attribute("velocity").as_float(),
				object.attribute("max_velocity").as_float());
		}
		object = level_node.find_child_by_attribute("id","speed");
		if(object.empty())
		{
			CONSOLE("speed param is not exist");
		}
		else
		{
			m_Emmiter->SetSpeed(object.attribute("speed").as_float(),
				object.attribute("max_speed").as_float());
		}
		object = level_node.find_child_by_attribute("id","emission_rate");
		if(object.empty())
		{
			CONSOLE("emission_rate param is not exist");
		}
		else
		{
			
			m_Emmiter->SetEmmisionRate(object.attribute("rate").as_int());			
		}
		object = level_node.find_child_by_attribute("id","additive");
		if(object.empty())
		{
			CONSOLE("additive param is not exist");
		}
		else
		{
			m_Emmiter->SetAdditive(object.attribute("additive").as_bool());
		}	

		object = level_node.find_child_by_attribute("id","zone");
		if(object.empty())
		{
			CONSOLE("zone param is not exist");
		}
		else
		{
			std::string type = object.attribute("type").value();
			if (type == "rectangle")
			{
				m_Emmiter->SetRectangleZone(object.attribute("p1").as_float(), object.attribute("p2").as_float());
			}
			else if (type == "circle")
			{
				m_Emmiter->SetCircleZone(object.attribute("p1").as_float(), object.attribute("p2").as_float());
			}			
		}	
		NBG::g_ResManager->ReleaseResource(xml);
		if (playFromStart)
		{
			m_Emmiter->Play(playTime);
		}
		AddChild(m_Emmiter);
	}

	void CParticleObject::BeforeRenderChange()
	{		
	}

	void CParticleObject::AfterRenderChange()
	{
	
	}

	void CParticleObject::Play(const float time)
	{
		m_Emmiter->Play(time);
	}

	void CParticleObject::Shoot(const int count)
	{
		m_Emmiter->Shoot(count);
	}

	void CParticleObject::Stop()
	{		
		m_Emmiter->Stop();
	}

	int CParticleObject::Update()
	{
		int res = CBaseObject::Update();
		if (res != EventSkip)return EventSkip;	
		return EventSkip;
	}

	int CParticleObject::Draw()
	{
		CBaseObject::Draw();
		return EventSkip;
	}
}
