#ifndef _AK_FRAMEWORK_LABEL_OBJECT
#define _AK_FRAMEWORK_LABEL_OBJECT

#include <Framework/Datatypes/FRect.h>

#include <Framework/GUI/BaseObject.h>
#include <Framework/GUI/ContainerObject.h>

#include <Framework/Global/Resources/TextureResource.h>

namespace NBG
{
	/// Настройки символов.
	struct CharOffsets
	{
		int xOffset;
		int yOffset;
		int xAdvance;
		NBG::FRect uv;
		unsigned short id;
		int arrayId;
		Vector size;
	};

	/// Пары для кернинга (смещения символов относительно друг друга)
	struct FontKernPair
	{
		int first;
		int second;
		int offset;
	};

	/// Описание шрифта.
	struct FontDescription
	{
		CharOffsets *chars;
		FontKernPair * kernings;
		int charsCount;
		int kerningsCount;
		float characterPadding;
		int baseLine;
		int spaceSize;
		int lineHeight;
		std::string texture;
	};

	/** @brief Класс, реализующий вывод форматированной строки.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CLabelObject : public CBaseObject
	{
	public:
		/// @name Конструктор/деструктор
		CLabelObject(const std::string id);
		virtual ~CLabelObject();

		/// Виртуальные наследуемые функции
		virtual void OnLoadFromXMLNode(pugi::xml_node node);
		virtual void OnScaleChanged();
		virtual void OnHotSpotChanged();
		virtual void OnColorChanged();
		virtual bool IsIgnoreEvents(){return true;}; ///Функция, указывающая на то, что чайлд не должен ловить и обрабатывать события

		///Инициализация лейбла шрифтом и строкой
		void Init(FontDescription * fontDesc, const std::wstring str = L"");

		///Установка текста
		void SetText(const std::wstring text);

		///Получение текущего текста
		inline std::wstring GetText()
		{
			return m_Text;
		};

		///Получение текста, установленного при создании объекта
		///Нужно для случаев, когда текст передаётся в виде: "Coins: %s"
		inline std::wstring GetBaseText()
		{
			return m_BaseText;
		};

		CBaseObject * GetTextContainer(){return m_TextContainer;}

		///Методы получения ширины строки, можно получить либо по индексам, либо полную строку.
		///WARNING: работает только для строк без переносов
		int GetStringWidth(const int &sIndex, const int &eIndex);
		int GetStringWidth();

		/// Установка межсимвольного расстояния
		inline void SetLetterSpacing(float letterSpacing)
		{
			m_BaseCharacterPadding = letterSpacing;
			m_CharacterPadding = m_BaseCharacterPadding;
			SetText(m_Text);
		};

		/// Установка высоты строки.
		inline void SetLineHeight(float height)
		{
			m_LineHeight = height;
			SetText(m_Text);
		};

		/// Получение высоты строки.
		inline float GetLineHeight()
		{
			return m_LineHeight;
		};

		/// Установить базовое положение строки.
		void SetBasePosition(const float &x, const float &y)
		{
			m_BasePosition.x = x;
			m_BasePosition.y = y;
		}

		/// Установить ограничивающий прямоугольник
		void SetBoundingBox(const FRect &boundingBox)
		{
			m_BoundingBox = boundingBox;
			SetText(m_Text);
		}

	private:
		/// Текст выводимый.
		std::wstring m_Text;

		/// Базовый текст.
		std::wstring m_BaseText;

		/// Описание шрифта.
		FontDescription * m_FontDesc;

		/// Смещение текста.
		Vector m_Offset;

		/// Базовое положение текста.
		Vector m_BasePosition;

		/// Контейнер, который хранит текст.
		CContainerObject * m_TextContainer;

		/// @name Константы для спец. символов.
		static const int SPACE_ID = 10000001;
		static const int NEW_LINE_ID = 10000002;

		/// Символы.
		CharOffsets *m_Chars;

		/// Количество символов.
		int m_CharsCount;

		/// Пары кернинга.
		FontKernPair * m_Kernings;

		/// Ограниченивающий прямоугольник
		FRect m_BoundingBox;

		/// Количество пар.
		int m_KerningsCount;

		///Методы получения ширины строки, можно получить либо по индексам, либо полную строку.
		///WARNING: работает только для строк без переносов
		int _GetStringWidth(const int &sIndex, const int &eIndex);
		int _GetStringWidth();


		/// Ограничение по ширине строки
		int m_MaxWidth;

		/// Функция быстрого поиска по сортированному массиву
		CharOffsets* GetCharById(const unsigned short &id);

		/// Закешированные символы, чтобы не нужно было искать каждый раз ID символов в массиве.
		unsigned int *m_CachedSymbols;

		/// ID текстуры шрифта.
		std::string	m_TextureId;

		/// Ресурс текстуры шрифта.
		CTextureResource * m_Texture;

		/// Межсимвольное расстояние.
		float m_CharacterPadding;

		/// Начальное межсимвольное расстояние.
		float m_BaseCharacterPadding;

		/// Точка привязки символа.
		int m_BaseLine;

		/// Размер пробела.
		int m_SpaceSize;

		/// Высота строки.
		int m_LineHeight;

		/// Закеширован ли рендер (?)
		bool m_RenderCache;

		/// Для оптимизации, чтобы лишний раз не перестраивать символы.
		int m_PrevHotSpot; 

		///Отрисовать линию символов (в текущей реализации - один раз создаёт массив спрайтов и больше к этой функции не возвращается)
		void DrawLine(int x, int y, const int &sIndex, const int &eIndex);

		///TODO: Неоптимизированный вариант поиска кернинг пар. Сделать поиск более быстрым.
		float GetKerningAmount(const unsigned short first, const unsigned short second);
	};
}

#endif
