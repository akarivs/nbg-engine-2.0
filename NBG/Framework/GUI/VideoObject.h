#ifndef _AK_FRAMEWORK_VIDEO_OBJECT
#define _AK_FRAMEWORK_VIDEO_OBJECT

#include <Framework/BaseScene.h>
#include <Framework/GUI/GraphicsObject.h>
#include <../TheoraPlayer/include/TheoraPlayer/TheoraPlayer.h>
/*
    Видео.
*/



namespace NBG
{
    class CBaseObject;

	class CVideoObject : public CGraphicsObject
    {
        public:
            CVideoObject(const std::string id);
            virtual ~CVideoObject();

            virtual void OnLoadFromXMLNode(pugi::xml_node node);
			void Init(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque);			

			virtual void BeforeRenderChange();
			virtual void AfterRenderChange();

			void SetCallback(VOID_CALLBACK callback);


			void Play();
			void Pause();
			void Stop();

			virtual int Update();
			virtual int Draw();

			void SetMusic(const std::string &music)
			{
				m_Music = music;						
			}

			float GetVideoTime();

			GETTER_SETTER(float,m_Duration,Duration);
        private:  
			TheoraVideoClip *m_Clip;
			Texture m_Texture;

			bool m_IsOpaque;
			bool m_IsDestroyAtEnd;	

			VOID_CALLBACK m_Callback;
			bool m_HaveCallback;

			std::string m_Path;
			std::string m_Music;
        protected:
        };
}

#endif

 