#ifndef _AK_FRAMEWORK_GUY_FACTORY
#define _AK_FRAMEWORK_GUY_FACTORY

#include <Framework/Datatypes.h>
#include <Framework/Gui/BaseObject.h>

#include <map>
#include <string>

namespace NBG
{
	class CBaseObject;

	/** @brief Фабрика, помогающая создать класс GUI по его строковому имени
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class __abstractGuiCreator
	{
	public:
		virtual void guiCreator() {}
		virtual CBaseObject * create() const = 0;
	};

	template <class C>
	class __guiCreator : public __abstractGuiCreator
	{
	public:
		virtual CBaseObject * create() const { return new C("UnnamedObject"); }
	};

	class CGuiFactory
	{
	protected:
		typedef std::map<std::string, __abstractGuiCreator*> FactoryMap;
		FactoryMap _factory;	
		
	public:		
		CGuiFactory();
		~CGuiFactory(){};

	




		template <class C>
		void add(const std::string & id)
		{
			typename FactoryMap::iterator it = _factory.find(id);
			if (it == _factory.end())
			{
				_factory[id] = new __guiCreator<C>();
			}
		}

		CBaseObject * create(const std::string & id)
		{
			FactoryMap::iterator it = _factory.find(id);
			if (it != _factory.end())
				return it->second->create();
			CONSOLE("Type is not registered! '%s'",id.c_str());
			SDL_assert(0);
			return NULL;
		}
	};
}
#endif //_AK_FRAMEWORK_GUI_FACTORY




