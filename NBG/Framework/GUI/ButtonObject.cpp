#include "ButtonObject.h"

#include <Framework.h>

#include <Framework/GameApplication.h>


namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CButtonObject::CButtonObject(const std::string id):CBaseObject(id)
	{
		m_NormalState = m_OverState = m_ClickedState = m_DisabledState = m_ActiveState = NULL;
		m_State = ButtonNo;

		///Установка дефолт значений
		m_Sound = "button_click";
		m_DisabledSound = "button_disabled_click";

		m_IsOnlyText = false;
	}

	//////////////////////////////////////////////////////////////////////////
	CButtonObject::~CButtonObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		std::string texture = node.attribute("texture").value();
		std::string sound = node.attribute("sound").value();
		std::string disabledSound = node.attribute("disabled_sound").value();
		m_IsOnlyText = node.attribute("only_text").as_bool();

		if (!sound.empty())m_Sound=sound;
		if (!disabledSound.empty())m_DisabledSound=disabledSound;

		///грузим с диска
		if (texture.find("/") != std::string::npos)
		{
			if (texture.substr(texture.length()-1,1) != "/")texture+="/";
			LoadFromDrive(texture);
		}
		else ///грузим с атласа
		{
			LoadFromAtlas(texture);
		}
		SetHotSpot(m_HotSpot);
	}

	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::LoadFromAtlas(std::string texture)
	{
		m_NormalState = new CGraphicsObject("normal");
		m_NormalState->Init(texture+"_normal");
		m_OverState = new CGraphicsObject("over");
		m_OverState->Init(texture+"_over");
		m_ClickedState = new CGraphicsObject("clicked");
		m_ClickedState->Init(texture+"_clicked");
		m_DisabledState = new CGraphicsObject("disabled");
		m_DisabledState->Init(texture+"_disabled");

		SetSize(m_NormalState->GetSize().x,m_NormalState->GetSize().y);

		AddChild(m_NormalState);
		AddChild(m_OverState);
		AddChild(m_ClickedState);
		AddChild(m_DisabledState);


		SetState(ButtonNormal);
	}


	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::LoadFromDrive(std::string folder)
	{
		m_NormalState = new CGraphicsObject("normal");
		m_NormalState->Init(folder+"normal.png");
		m_OverState = new CGraphicsObject("over");
		m_OverState->Init(folder+"over.png");
		m_ClickedState = new CGraphicsObject("clicked");
		m_ClickedState->Init(folder+"clicked.png");
		m_DisabledState = new CGraphicsObject("disabled");
		m_DisabledState->Init(folder+"disabled.png");


		SetSize(m_NormalState->GetSize().x,m_NormalState->GetSize().y);
		AddChild(m_NormalState);
		AddChild(m_OverState);
		AddChild(m_ClickedState);
		AddChild(m_DisabledState);


		SetState(ButtonNormal);
	}


	//////////////////////////////////////////////////////////////////////////
	int CButtonObject::OnMouseMove()
	{
		float x = g_MousePos.x;
		float y = g_MousePos.y;

		if (IsContains(Vector(x,y)))
		{
			if (m_State == ButtonNormal)
			{
				SetState(ButtonOver);
				GetParent()->DispatchEvent(Event(Event::ET_Over),this);
				return EventBlock;
			}            
		}
		else
		{
			if (m_State == ButtonOver)
			{
				GetParent()->DispatchEvent(Event(Event::ET_Out),this);
				SetState(ButtonNormal);
				return EventBlock;				
			}
		}
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CButtonObject::OnMouseDown(const int button)
	{
		if (IsContains(g_MousePos) && !IsDisabled())
		{			
			SetState(ButtonClicked);
			return EventBlock;
		}

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CButtonObject::OnMouseUp(const int button)
	{
		float x = g_MousePos.x;
		float y = g_MousePos.y;

		if (IsDisabled())
		{
			if (IsContains(Vector(x,y)))
			{
				GetParent()->DispatchEvent(Event("disabled_click"),this);
				g_SoundManager->Play(m_DisabledSound);
			}
		}
		else
		{
			if (m_State == ButtonClicked)
			{
				if (IsContains(Vector(x,y)))
				{				
					g_SoundManager->Play(m_Sound);
					SetState(ButtonOver);
					
					if (m_Callback)
					{
						m_Callback(this);
					}
					return EventBlock;
				}
				else
				{
					SetState(ButtonNormal);
				}
			}
		}

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CButtonObject::Update()
	{
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CButtonObject::Draw()
	{
		int event = CBaseObject::Draw();
		if (event != EventSkip)return event;
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::SetOnlyText(const bool onlyText)
	{
		m_IsOnlyText = onlyText;
		if (m_IsOnlyText)
		{
			Color clr;
			clr.a = clr.r = clr.g = clr.b = 0;          
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::SetState(ButtonState state)
	{
		if (m_State == state)return;
		m_State = state;

		m_NormalState->SetVisible(false);
		m_OverState->SetVisible(false);
		m_ClickedState->SetVisible(false);
		m_DisabledState->SetVisible(false);


		switch (m_State)
		{
		case ButtonNormal:
			m_ActiveState = m_NormalState;
			break;
		case ButtonOver:
			m_ActiveState = m_OverState;
			break;
		case ButtonClicked:
			m_ActiveState = m_ClickedState;
			break;
		case ButtonDisabled:
			m_ActiveState = m_DisabledState;
			break;
		}
		if (m_IsOnlyText == false)
		{
			m_ActiveState->SetVisible(true);
		}
		else
		{
			m_ActiveState->SetVisible(false);
		}
		
	}

	//////////////////////////////////////////////////////////////////////////
	void CButtonObject::OnSetDisabled()
	{
		if (m_IsDisabled)
		{
			SetState(ButtonDisabled);
		}
		else
		{
			SetState(ButtonNormal);
		}

	}
}
