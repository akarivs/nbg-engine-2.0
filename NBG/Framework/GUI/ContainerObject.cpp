#include "ContainerObject.h"

#include <framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CContainerObject::CContainerObject(const std::string id):CBaseObject(id)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CContainerObject::~CContainerObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CContainerObject::OnLoadFromXMLNode(pugi::xml_node node)
	{        
	}

	//////////////////////////////////////////////////////////////////////////
	void CContainerObject::DispatchEvent(Event event, CBaseObject * object)
	{		
		if (!m_Clickable)
		{
			if (GetParent())
			{
				GetParent()->DispatchEvent(event,object);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	int CContainerObject::OnMouseMove()
	{
		int res = CBaseObject::OnMouseMove();
		return res;
	}

	//////////////////////////////////////////////////////////////////////////
	int CContainerObject::OnMouseDown(const int button)
	{
		int res = CBaseObject::OnMouseDown(button);
		if (res != EventSkip)return res;

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CContainerObject::OnMouseUp(const int button)
	{
		int res = CBaseObject::OnMouseUp(button);
		if (res != EventSkip)return res;

		return EventSkip;
	}

}
