//
//  File created by Vadim Simonov.
//  Company New Bridge Games
//  Project Patchworks
//  Date 20130829
//  Класс, реализующий логику для слайдера
//============================================================================//

#ifndef _AK_FRAMEWORK_SLIDER_OBJECT
#define _AK_FRAMEWORK_SLIDER_OBJECT

#include <Framework/GUI/ButtonObject.h>
#include <Framework/GUI/GraphicsObject.h>

namespace NBG
{
    class CBaseObject;
    class CSliderObject : public CBaseObject
    {
        public:
            CSliderObject(const std::string id);
            virtual ~CSliderObject();

            virtual void DispatchEvent(Event event, CBaseObject * object);
			/// @name Загрузка объекта из XML ноды
            virtual void OnLoadFromXMLNode(pugi::xml_node node);
			virtual void AfterLoadFromXMLNode();

            virtual int OnMouseDown(const int button);
            virtual int OnMouseUp(const int button);
            virtual int OnMouseMove();

            void InitSlider(const bool isStatic = false);
            void SetValue(const float percent);			
        private:
            void ChangeValue(const bool onlyChange = false);

            bool m_IsKeyDown;
            bool m_IsDrag;

            bool m_IsStatic;

            CButtonObject * m_Button;
            CGraphicsObject * m_Back;
            CGraphicsObject * m_Fill;

            Vector m_DragStartPos;
            Vector m_DragOffset;
        protected:

        };
}

#endif
