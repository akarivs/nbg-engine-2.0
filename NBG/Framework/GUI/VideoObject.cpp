#include "VideoObject.h"
#include <../TheoraPlayer/include/TheoraPlayer/TheoraDataSource.h>


#include <Framework.h>


namespace NBG
{
	class TheoraMemoryDataSource : public TheoraDataSource
	{		
		unsigned long mSize,mReadPointer;
		unsigned char* mData;
	public:
		TheoraMemoryDataSource(unsigned char * data, unsigned long size);
		~TheoraMemoryDataSource();

		int read(void* output,int nBytes);
		void seek(unsigned long byte_index);		
		unsigned long size();
		unsigned long tell();	

		std::string repr() { return ""; }
		std::string getFilename() { return ""; }
	};

	TheoraMemoryDataSource::TheoraMemoryDataSource(unsigned char * data, unsigned long size) :
		mReadPointer(0),
		mData(0)
	{		
		mSize=size;
		mData=data;				
	}

	TheoraMemoryDataSource::~TheoraMemoryDataSource()
	{
		if (mData) delete [] mData;
	}

	int TheoraMemoryDataSource::read(void* output,int nBytes)
	{
		int n=(mReadPointer+nBytes <= mSize) ? nBytes : mSize-mReadPointer;
		if (!n) return 0;
		memcpy(output,mData+mReadPointer,n);
		mReadPointer+=n;
		return n;
	}

	void TheoraMemoryDataSource::seek(unsigned long byte_index)
	{
		mReadPointer=byte_index;
	}

	unsigned long TheoraMemoryDataSource::size()
	{
		return mSize;
	}

	unsigned long TheoraMemoryDataSource::tell()
	{
		return mReadPointer;
	}


	CVideoObject::CVideoObject(const std::string id):CGraphicsObject(id)
	{
		m_Clip = NULL;
		m_Texture.SetTextureData(NULL);
		m_HaveCallback = false;
	}

	CVideoObject::~CVideoObject()
	{
		if (m_Clip)
		{
			g_GameApplication->GetTheoraVideoManager()->destroyVideoClip(m_Clip);
			m_Clip = NULL;
		}
		if (m_Texture.GetTextureData() != NULL)
		{
			g_Render->ReleaseTexture(&m_Texture);
		}
	}

	/// @name Загрузка объекта из XML ноды
	void CVideoObject::OnLoadFromXMLNode(pugi::xml_node node)
	{		
		std::string path = node.attribute("video").value();		
		bool playFromStart = node.attribute("play_from_start").as_bool();
		bool loop = node.attribute("loop").as_bool();
		m_Music = node.attribute("music").value();
		bool destroyAtEnd = node.attribute("destroy_at_end").as_bool();
		bool isOpaque = node.attribute("opaque").as_bool();

		Init(path,playFromStart, loop, destroyAtEnd,isOpaque);
	}

	void CVideoObject::SetCallback(VOID_CALLBACK callback)
	{
		m_Callback = callback;
		m_HaveCallback = true;
	}

	void CVideoObject::Init(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque)
	{
		m_IsOpaque = isOpaque;
		m_Path = path;
		m_IsDestroyAtEnd = destroyAtEnd;

		
		TheoraVideoManager * mgr = g_GameApplication->GetTheoraVideoManager();
		
		if (mgr)
		{
			std::string res = g_System->GetAppPath() + path;		


			auto f = g_FileSystem->ReadFile(res,"rb");
			auto ds =  new TheoraMemoryDataSource((unsigned char*)f.data,f.size);

			m_Clip = mgr->createVideoClip(ds);
			m_Clip->setAutoRestart(loop);
			m_Duration = m_Clip->getDuration();
			TheoraVideoFrame *frame= m_Clip->getNextFrame();
			            
			if (!isOpaque)
			{
				Vector scale = GetScale();
				SetScale(scale.x * 0.5f, scale.y);
			}
			SetSize(m_Clip->getWidth(), m_Clip->getHeight());
			CONSOLE("Video size: %d %d",int(m_Clip->getWidth()), int(m_Clip->getHeight()));

            m_Texture.SetSize(GetSize().x, GetSize().y);
            m_Texture.SetTextureData(NULL);

            
          	if (frame)
			{			
				if (m_IsOpaque)
				{
					g_Render->CreateOpaqueTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);
				}
				else
				{
					g_Render->CreateTransparentTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);
				}
				m_Clip->popFrame();
			}			

			if (playFromStart == false)
			{
				m_Clip->stop();
			}
			else
			{
				if (m_Music.empty() == false)
				{
					g_SoundManager->Stop(m_Music);				
					g_SoundManager->Play(m_Music);				
				}
			}
		}
	}

	void CVideoObject::BeforeRenderChange()
	{		
	}

	void CVideoObject::AfterRenderChange()
	{
		if (m_Music.empty() == false)
		{
			if (m_Clip->isPaused())
				g_SoundManager->Pause(m_Music);			
		}
	}

	void CVideoObject::Play()
	{
		m_Clip->play();
		m_Clip->waitForCache();
		if (m_Music.empty() == false)
		{
			g_SoundManager->Play(m_Music);			
		}
	}

	void CVideoObject::Pause()
	{
		m_Clip->pause();
		if (m_Music.empty() == false)
		{
			g_SoundManager->Pause(m_Music);			
		}
	}

	void CVideoObject::Stop()
	{		
		m_Clip->stop();
		m_Clip->waitForCache();	
		if (m_Music.empty() == false)
		{
			g_SoundManager->Stop(m_Music);			
		}
	}

	int CVideoObject::Update()
	{
		int res = CBaseObject::Update();
		if (res != EventSkip)return EventSkip;
		

		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	float CVideoObject::GetVideoTime()
	{
		return m_Clip->getTimePosition();
	}


	//////////////////////////////////////////////////////////////////////////
	int CVideoObject::Draw()
	{
		if (!IsVisible())return EventSkip;
        
        
		if (!m_Clip)return EventSkip;
		bool needToCallCallback = false;
		if (m_Clip->isDone())
		{
			if (m_HaveCallback)
			{
				needToCallCallback = true;				
			}
			if (m_IsDestroyAtEnd)
			{
				if (needToCallCallback)
				{
					m_Callback();
				}
				g_Render->ReleaseTexture(&m_Texture);
				g_GameApplication->GetTheoraVideoManager()->destroyVideoClip(m_Clip);
				SetVisible(false);
				m_Clip = NULL;
				return EventSkip;
			}			
		}
		else
		{
			TheoraVideoFrame *frame= m_Clip->getNextFrame();
			if (frame)
			{
				if (m_IsOpaque)
				{
					g_Render->CreateOpaqueTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);
				}
				else
				{
					g_Render->CreateTransparentTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);
				}
				m_Clip->popFrame();
			}
		}	
		

		if (m_IsNeedToUpdateMesh)
		{
			UpdateMeshData();
			m_IsNeedToUpdateMesh = false;
		}		
		g_Render->SetBlendMode(BLENDMODE_BLEND);
		g_Render->DrawMesh(&m_Texture,&m_Mesh);
		if (needToCallCallback)
		{
			if (m_Callback.p_this != NULL)
				m_Callback();			
		}
		return EventSkip;
	}
}
