#include "SliderObject.h"

#include <Framework.h>

#include <Framework/Utils/Tweener.h>

#include <Framework/GUI/GraphicsObject.h>
#include <Framework/GUI/ButtonObject.h>
#include <Framework/GUI/LabelObject.h>


namespace NBG
{
    CSliderObject::CSliderObject(const std::string id):CBaseObject(id)
    {
        m_IsDrag    = false;
        m_IsKeyDown = false;
        m_IsStatic  = false;
    }

    CSliderObject::~CSliderObject()
    {

    }

    void CSliderObject::DispatchEvent(Event event, CBaseObject * object)
    {
		switch (event.GetType())
		{
		case Event::ET_MouseDown:
			if (object->GetId() == "button")
            {
                m_IsKeyDown = true;
            }
            else
            {
                GetParent()->DispatchEvent(event,object);
            }
			break;
		}        
    }

	/// @name Загрузка объекта из XML ноды
    void CSliderObject::OnLoadFromXMLNode(pugi::xml_node node)
    {
		m_IsStatic = node.attribute("static").as_bool();
    }

	void CSliderObject::AfterLoadFromXMLNode()
	{
		InitSlider(m_IsStatic);        
	}

    int CSliderObject::OnMouseDown(const int button)
    {		
        if (m_IsStatic)
        {
            return CBaseObject::OnMouseDown(button);            
        }        
        if (m_Back->IsContains(g_MousePos))
        {
            m_IsKeyDown = true;
            m_DragOffset.x = g_MousePos.x - GetTransform()->GetAbsoluteTransform().x;
            if (m_DragOffset.x < -(GetSize().x/2.0f))m_DragOffset.x = -(GetSize().x/2.0f);
            else if (m_DragOffset.x > (GetSize().x/2.0f))m_DragOffset.x = (GetSize().x/2.0f);
            m_Button->SetPosition(m_DragOffset.x,m_Button->GetPosition().y);
            ChangeValue();
            return EventBlock;
        }
        int res = CBaseObject::OnMouseDown(button);
        if (res != EventSkip)return res;
        return EventSkip;
    }

    int CSliderObject::OnMouseUp(const int button)
    {   if (m_IsStatic)
        {
            return CBaseObject::OnMouseUp(button);            
        }        
		m_IsKeyDown = false;
        int res = CBaseObject::OnMouseUp(button);
        if (m_IsDrag)
        {
            m_IsDrag = false;
        }
	    if (res != EventSkip)return res;
        return EventSkip;
    }

    int CSliderObject::OnMouseMove()
    {
        if (m_IsStatic)return EventSkip;
        int res = CBaseObject::OnMouseMove();
        if (res != EventSkip)return res;

        if (m_IsKeyDown)
        {
            if (!m_IsDrag)
            {
                m_IsDrag = true;
            }
            m_DragOffset.x = g_MousePos.x  - GetTransform()->GetAbsoluteTransform().x;
			m_DragOffset.x /= GetTransform()->GetAbsoluteTransform().scalex;

            if (m_DragOffset.x < -(GetSize().x/2.0f))m_DragOffset.x = -(GetSize().x/2.0f);
            else if (m_DragOffset.x > (GetSize().x/2.0f))m_DragOffset.x = (GetSize().x/2.0f);

            if (m_IsDrag)
            {
                m_Button->SetPosition(m_DragOffset.x,m_Button->GetPosition().y);
                ChangeValue();
            }
        }
        return EventSkip;
    }

    void CSliderObject::InitSlider(bool isStatic)
    {
        m_IsStatic = isStatic;
        m_Button = CAST(CButtonObject *, GetChild("button"));
        if (!m_Button && !isStatic)
        {
            //CONSOLE("No 'button' object in slider: %s ", GetId().c_str());
        }
        m_Back =  CAST(CGraphicsObject *, GetChild("back"));
        if (!m_Back)
        {
            CONSOLE("No 'back' object in slider: %s", GetId().c_str());
        }
        m_Fill =  CAST(CGraphicsObject *, GetChild("fill"));
        if (!m_Back)
        {
            CONSOLE("No 'fill' object in slider: %s" ,GetId().c_str());
        }
    }

    void CSliderObject::ChangeValue(const bool onlyChange)
    {
		float sizeX = GetSize().x;		
		float percent = (sizeX/2.0f+m_Button->GetPosition().x)/sizeX;
        m_Fill->CutImage(percent, 1.0f);
        if (!onlyChange)
        {
            GetParent()->DispatchEvent(Event(Event::ET_FloatValueChanged,&percent),this);
        }
    }

    void CSliderObject::SetValue(const float percent)
    {
        if (m_IsStatic)
        {
            m_Fill->CutImage(percent, 1.0f);
        }
        else
        {
            m_Button->SetPosition(-GetSize().x/2.0f + GetSize().x*percent,m_Button->GetPosition().y);
            ChangeValue();
        }
    }
}
