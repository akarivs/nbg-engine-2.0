#ifndef _AK_FRAMEWORK_ASSEMBLED_OBJECT
#define _AK_FRAMEWORK_ASSEMBLED_OBJECT

#include <framework.h>

#include "Framework/GUI/GraphicsObject.h"


namespace NBG
{
	class CBaseObject;

	/** @brief Класс, реализующий работу с графическим объектом, собирающимся из 9 кусков.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CAssembledObject : public CBaseObject
	{
	public:
		/// @name Конструктор
		CAssembledObject(const std::string id);
		virtual ~CAssembledObject();

		///Инициализация объекта.
		/// @param texture	- путь к папке, где лежат части текстуры
		/// @param width	- ширина объекта
		/// @param height	- высота объекта
		void Init(const std::string &texture, const float width, const float height, const bool stretched = false);

		virtual void DispatchEvent(Event event, CBaseObject * object);

		/// @name Загрузка объекта из XML ноды
		virtual void OnLoadFromXMLNode(pugi::xml_node node);
	private:
		bool m_IsStretched;
	};
}

#endif
