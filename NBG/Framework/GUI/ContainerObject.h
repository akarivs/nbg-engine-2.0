#ifndef _AK_FRAMEWORK_CONTAINER_OBJECT
#define _AK_FRAMEWORK_CONTAINER_OBJECT

#include <Framework/GUI/BaseObject.h>

namespace NBG
{	
	/** @brief Класс, реализующий работу с контейнером для других объектов.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CContainerObject : public CBaseObject
	{
	public:
		/// @name конструктор/деструктор
		CContainerObject(const std::string id);
		virtual ~CContainerObject();

		/// @name Системные события
		virtual int OnMouseMove();
		virtual int OnMouseUp(const int button);
		virtual int OnMouseDown(const int button);

		/// @name Загрузка с XML и обработка кликов.
		virtual void OnLoadFromXMLNode(pugi::xml_node node);
		virtual void DispatchEvent(Event event, CBaseObject * object);
	};
}
#endif
