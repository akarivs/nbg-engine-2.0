#ifndef _AK_FRAMEWORK_INPUT_TEXT_OBJECT
#define _AK_FRAMEWORK_INPUT_TEXT_OBJECT

#include <Framework/GUI/GraphicsObject.h>
#include <Framework/GUI/LabelObject.h>


namespace NBG
{
	class CBaseObject;
	/** @brief Класс, реализующий работу с вводом текста.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CInputTextObject : public CBaseObject
	{
	public:
		/// @name Конструктор/деструктор
		CInputTextObject(const std::string id);
		virtual ~CInputTextObject();

		///Инициализация объекта строкой.
		void Init(const std::wstring str = L"");

		/// @name Загрузка объекта из XML ноды
		virtual void OnLoadFromXMLNode(pugi::xml_node node);
		virtual void AfterLoadFromXMLNode();

		/// @name Системные события
		virtual int OnMouseMove();
		virtual int OnKeyDown(const int key);            			
		virtual int OnMouseUp(const int button);            

		/// Установить текст.		
		void SetText(const std::wstring text);

		/// Получить текст
		std::wstring GetText(){return m_Symbols;};

		/// Установить позицию.
		virtual void SetPosition(const float x, const float y, const float z = 0.0f);

		/// Обновить.
		virtual int Update();            

		/// Установить фокус.
		void SetFocused(const bool focused){m_IsFocused = focused;};			

		/// Переместить курсор в начало.
		void MoveCursorStart();

		/// Переместить курсор в конец.
		void MoveCursorEnd();

		/// Сбросить форму.
		void Reset(){m_Symbols = m_DefaultSymbols; m_CursorPos = m_DefaultSymbols.size(); SetText(m_Symbols); UpdateCursor();};
	private:
		/// Обновить курсор.
		void UpdateCursor();

		/// Позиция курсора.
		int m_CursorPos;

		/// Время текущего состояния курсора - вкл/выкл.
		float m_CursorAnimTime;

		/// Показуется ли в данный момент курсор.
		bool m_IsShowCursor;

		/// Строка
		CLabelObject * m_Label;

		/// Курсор
		CLabelObject * m_CursorLabel;

		/// Фон
		CGraphicsObject * m_Background;

		/// Символы введённые.
		std::wstring m_Symbols;

		/// Символы по умолчанию.
		std::wstring m_DefaultSymbols;


		/// В фокусе ли.
		bool m_IsFocused;

		/// Максимальное количество символов.
		int m_MaxSymbols;
	};
}

#endif
