#include "GraphicsObject.h"

#include <Framework.h>

namespace NBG
{
	
	//////////////////////////////////////////////////////////////////////////
	CGraphicsObject::CGraphicsObject(const std::string id):CBaseObject(id)
	{
		m_UseAtlas  = false;
		m_UV.left   = 0.0f;
		m_UV.top    = 0.0f;
		m_UV.right  = 1.0f;
		m_UV.bottom = 1.0f;

		m_Mesh.Init(4);
		m_MeshType = MT_Rectangle;

		m_Resource              = NULL;
		m_IsNeedToUpdateMesh    = false;

		m_IsTileTexture = false;
		m_IsAdditive = false;
		m_IsScreen = false;

		m_BlendMode = BLENDMODE_BLEND;

        m_IsRepeatedTexture = false;
		m_Desc = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		Vector size = GetSize();
		Init(node.attribute("texture").value());
		m_IsTileTexture = node.attribute("tile").as_bool();
		if (m_IsTileTexture)
		{
			float width = node.attribute("width").as_float();
			float height = node.attribute("height").as_float();
			SetSize(width,height);
		}
		if (size.x != 0.0f && size.y != 0.0f)
		{
			SetSize(size);
		}
		m_IsAdditive = node.attribute("additive").as_bool();
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGraphicsObject::IsContains(const Vector &pos)
	{
		if (m_MeshType == MT_Rectangle)
		{
			return CBaseObject::IsContains(pos);
		}
		else
		{
			///Создаём матрицу из текущей трансформации
			Matrix m;
			m.CreateFromTransform(m_Transform.GetAbsoluteTransform());

			for (size_t i=0; i<m_Vertexes.size(); ++i)
			{
				m_Vertexes[i] = m.TransformPoint(m_SavedVertexes[i]);
			}
			return MathUtils::IsPointInsidePoly(pos, m_Vertexes);
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::Init(const std::string texturePath)
	{
		SetTexture(texturePath);
	}

	//////////////////////////////////////////////////////////////////////////
	CGraphicsObject::~CGraphicsObject()
	{
		if (m_Resource)
		{
			g_ResManager->ReleaseResource(m_Resource);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::DispatchEvent(Event event, CBaseObject * object)
	{
		GetParent()->DispatchEvent(event,object);
	}

	//////////////////////////////////////////////////////////////////////////
	int CGraphicsObject::Update()
	{
		CBaseObject::Update();
		return EventSkip;
	}

	//////////////////////////////////////////////////////////////////////////
	int CGraphicsObject::Draw()
	{
		if (!IsVisible())return EventSkip;
        if (m_Color.a == 0.0f)return EventSkip;

		if (m_IsNeedToUpdateMesh)
		{			
			UpdateMeshData();
			m_IsNeedToUpdateMesh = false;
		}
		g_Render->SetBlendMode(m_BlendMode);
#ifdef NBG_WIN32
		g_Render->SetTextureRepeat(m_IsRepeatedTexture);
#endif


		
		if (m_Resource)
		{ 
			g_Render->DrawMesh(m_Resource->GetTexture(),&m_Mesh);
		}
		else
		{
			g_Render->DrawMesh(NULL,&m_Mesh);
		}	
	
		CBaseObject::Draw();
		return EventSkip;
	}

	char ** CGraphicsObject::GetAlphaMask()
	{
		if (m_Resource)
		{
			NBG::FRect rectangle;
			rectangle.left		= 0;
			rectangle.right		= GetSize().x;
			rectangle.top		= 0;
			rectangle.bottom	= GetSize().y;

			if (m_UseAtlas)
			{
				Vector atlasSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
				rectangle.left		= m_Desc->uv.left * atlasSize.x;
				rectangle.right		= m_Desc->uv.right * atlasSize.x;
				rectangle.top		= m_Desc->uv.top * atlasSize.y;
				rectangle.bottom	= m_Desc->uv.bottom * atlasSize.y;
			}
			return g_Render->GetAlphaFromRegion(m_Resource->GetTexture(),rectangle);
		}
		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::SetTexture(const std::string texturePath)
	{	
		//Освобождаем предыдущую текстуру.
		if (m_Resource != NULL)
		{
			g_ResManager->ReleaseResource(m_Resource);
		}

		std::string path = g_EditionHelper->ConvertPath(texturePath);		
		path = StringUtils::StringReplace(path,std::string("%LOCALE%"),g_LocaleManager->GetLocale());	

		m_Desc = g_AtlasHelper->GetTextureDescription(path);

		///Если нашли текстуру в атласе
		if (m_Desc)
		{
			path = g_EditionHelper->ConvertPath(m_Desc->atlasID);			
			m_Resource = CAST(CTextureResource*,g_ResManager->GetResource(path));
			SetSize(m_Desc->size.x,m_Desc->size.y);
			m_UV = m_Desc->uv;
			m_UseAtlas = true;
		}
		///иначе пробуем подтянуть напрямую с диска
		else
		{
			m_Resource = CAST(CTextureResource*,g_ResManager->GetResource(path.c_str()));			
			if (m_Resource)
			{
				SetSize(m_Resource->GetTextureWidth(),m_Resource->GetTextureHeight());
				m_UseAtlas = false;
				/*float atlasHalfWidthTexel = 0.5f/(float)m_Resource->GetTextureWidth();
				float atlasHalfHeightTexel = 0.5f/(float)m_Resource->GetTextureHeight();*/


				m_UV.left   = 0.0f;
				m_UV.top    = 0.0f;
				m_UV.right  = 1.0f;
				m_UV.bottom = 1.0f;

				/*m_UV.left	+= atlasHalfWidthTexel;
				m_UV.right	+= atlasHalfWidthTexel;
				m_UV.top	-= atlasHalfHeightTexel;
				m_UV.bottom -= atlasHalfHeightTexel;*/
			}
			else
			{
				SetTexture(NBG::CTextureResource::GetDefaultTexture());
			}
		}
		m_IsNeedToUpdateMesh = true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::SetTexture(NBG::CTextureResource * textureRes)
	{
		if (textureRes)
		{
			m_Resource = textureRes;
			textureRes->IncreaseRefsCount();			
			SetSize(m_Resource->GetTextureWidth(),m_Resource->GetTextureHeight());
			m_UseAtlas = false;
			m_IsNeedToUpdateMesh = true;
		}
		else
		{
			m_Resource = NULL;
		}
	} 

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::SetCustomShape(std::vector<Vertex> &vertexes, const bool modifyPos)
	{
		m_Vertexes.clear();
		Vector minPos(99999,99999),maxPos(-99999,-99999);
		for (size_t i=0; i<vertexes.size(); i++)
		{
			if (vertexes[i].x > maxPos.x)maxPos.x = vertexes[i].x;
			if (vertexes[i].y > maxPos.y)maxPos.y = vertexes[i].y;

			if (vertexes[i].x < minPos.x)minPos.x = vertexes[i].x;
			if (vertexes[i].y < minPos.y)minPos.y = vertexes[i].y;
		}
		Vector size = maxPos-minPos;
		SetSize(size);
		if (modifyPos)
		{
			SetPosition(minPos+size/2);
			for (size_t i=0; i<vertexes.size(); i++)
			{
				vertexes[i].x = vertexes[i].x-GetPosition().x;
				vertexes[i].y = vertexes[i].y-GetPosition().y;
				m_Vertexes.push_back(Vector(vertexes[i].x,vertexes[i].y));
			}
		}
		else
		{
			for (size_t i=0; i<vertexes.size(); i++)
			{
				m_Vertexes.push_back(Vector(vertexes[i].x,vertexes[i].y));
			}
		}
		m_SavedVertexes = m_Vertexes;

		m_MeshType = MT_Custom;
		m_Mesh.SetCustomVertexes(vertexes);
	}

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::UpdateMeshData()
	{
		Texture * texture = NULL;
		if (m_Resource)
		{
			texture = m_Resource->GetTexture();
		}
		if (m_MeshType == MT_Rectangle)
		{			
			if ((m_Mesh.GetIndexesCount() == 6 && m_Mesh.GetVertexCount() == 4) == false)return;
			Vector offset;
			if (m_Desc)
			{
				offset = m_Desc->offset;				
			}			
			m_Mesh.SetRectData(&m_UV,GetTransform(), GetSize(), GetColor(),texture,m_IsTileTexture, offset);
		}
		else if (m_MeshType == MT_Custom)
		{
			m_Mesh.UpdateVertexes(GetTransform(),texture,m_IsTileTexture);
		}
	}
    
    void CGraphicsObject::SetRepeatedTexture(const bool isRepeated)
    {
        m_IsRepeatedTexture = isRepeated;
        if (m_IsRepeatedTexture)
        {
            g_GameApplication->GetTextureManager()->SetRepeatTexture(m_Resource->GetTexture());            
        }
    }

	//////////////////////////////////////////////////////////////////////////
	void CGraphicsObject::CutImage(float xPercent, float yPercent, bool xFlip, bool yFlip)
	{
		if (m_UseAtlas)
		{
			float width = m_Desc->uv.right - m_Desc->uv.left;
			float height = m_Desc->uv.bottom - m_Desc->uv.top;
			width *= xPercent;
			height *= yPercent;
			m_UV.right = m_Desc->uv.left+width;			
			m_UV.bottom =  m_Desc->uv.top+height;						
			SetSize(m_Desc->size.x*xPercent, m_Desc->size.y*yPercent); 						
		}
		else
		{			
			float atlasHalfWidthTexel = 0.5f/(float)m_Resource->GetTextureWidth();
			float atlasHalfHeightTexel = 0.5f/(float)m_Resource->GetTextureHeight();

			if (xFlip)
			{
				m_UV.left   = 1.0f-xPercent;
				m_UV.right  = 1.0f;
			}
			else
			{
				m_UV.left   = 0.0f;
				m_UV.right  = xPercent;
			}
			

			if (yFlip)
			{
				m_UV.top    = 1.0f-yPercent;		
				m_UV.bottom = 1.0f;
			}
			else
			{
				m_UV.top    = 0.0f;		
				m_UV.bottom = yPercent;
			}
			

			m_UV.left += atlasHalfWidthTexel;
			m_UV.right -= atlasHalfWidthTexel;
			m_UV.top += atlasHalfHeightTexel;
			m_UV.bottom -= atlasHalfHeightTexel;

			Vector size = m_Resource->GetTexture()->GetSize();
			SetSize(size.x*xPercent,size.y*yPercent);
		}
		m_IsNeedToUpdateMesh = true;
	}
}