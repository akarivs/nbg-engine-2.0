#ifndef _AK_FRAMEWORK_PARTICLE_OBJECT
#define _AK_FRAMEWORK_PARTICLE_OBJECT

#include <Framework/BaseScene.h>
#include <Framework/Particles/Emmiter.h>
/*
    Частицы.
*/



namespace NBG
{
    class CBaseObject;
	class CParticleObject : public CBaseObject
    {
        public:
            CParticleObject(const std::string id);
            virtual ~CParticleObject();

            virtual void OnLoadFromXMLNode(pugi::xml_node node);
			void Init(const std::string &path, const bool playFromStart, const float playTime);			

			virtual void BeforeRenderChange();
			virtual void AfterRenderChange();			

			void Play(const float time = 0.0f);			
			void Stop();
			void Shoot(const int count);

			virtual int Update();
			virtual int Draw();			
        private:  
			CEmmiter * m_Emmiter;						
			std::string m_Path;			
        protected:
        };
}
#endif

 