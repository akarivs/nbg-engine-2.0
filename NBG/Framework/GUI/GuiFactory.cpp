#include "GuiFactory.h"

#include "AnimatedObject.h"
#include "AssembledObject.h"
#include "BaseObject.h"
#include "ContainerObject.h"
#include "CheckboxObject.h"
#include "ButtonObject.h"
#include "InputTextObject.h"
#include "GraphicsObject.h"
#include "LabelObject.h"
#include "ListObject.h"
#include "ParticleObject.h"
#include "SliderObject.h"
#include "VideoObject.h"
#include "WindowObject.h"

#include "../Animation/Clip.h"

namespace NBG
{	
	//////////////////////////////////////////////////////////////////////////
	CGuiFactory::CGuiFactory()
	{
		add<CAnimatedObject>("animated");
		add<CAssembledObject>("assembled");
		add<CBaseObject>("base");
		add<CCheckBoxObject>("checkbox");
		add<CButtonObject>("button");
		add<CGraphicsObject>("image");
		add<CInputTextObject>("input_text");
		add<CContainerObject>("container");
		add<CLabelObject>("label");
		add<CListObject>("list");
		add<CParticleObject>("particle");
		add<CSliderObject>("slider");
		add<CVideoObject>("video");
		add<CWindowObject>("window");

		add<CClip>("clip");
	}
}


