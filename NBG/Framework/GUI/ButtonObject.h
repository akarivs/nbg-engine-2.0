#ifndef _AK_FRAMEWORK_BUTTON_OBJECT
#define _AK_FRAMEWORK_BUTTON_OBJECT


#include <Framework/GUI/GraphicsObject.h>
#include <../xml/pugixml.hpp>

namespace NBG
{
	class CBaseObject;

	/** @brief Класс, реализующий работу с кнопкой.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CButtonObject : public CBaseObject
	{
	public:
		/// @name Конструктор/деструктор
		CButtonObject(const std::string id);
		virtual ~CButtonObject();

		virtual void OnLoadFromXMLNode(pugi::xml_node node);

		/// Состояния кнопки
		enum ButtonState
		{
			ButtonNo,
			ButtonNormal,
			ButtonOver,
			ButtonClicked,
			ButtonDisabled
		};

		/// @name События системные
		virtual int OnMouseMove();
		virtual int OnMouseUp(const int button);
		virtual int OnMouseDown(const int button);

		/// Событие, когда кнопку дисейблят.
		virtual void OnSetDisabled();

		/// DEPRECATED: загрузка из атласа.
		virtual void LoadFromAtlas(std::string texture);
		/// Загрузка с диска или атласа.
		virtual void LoadFromDrive(std::string folder);

		/// @name Обновление/отрисовка
		virtual int Update();
		virtual int Draw();

		/// Поставить режим, когда кнопка только выводит текст и не рисует графику.
		void SetOnlyText(const bool onlyText);

		///Установить Callback
		GETTER_SETTER(VOID_DATA_CALLBACK, m_Callback, Callback);


	private:
		/// @name Объекты для разных состояний кнопки.
		NBG::CGraphicsObject * m_NormalState;
		NBG::CGraphicsObject * m_OverState;
		NBG::CGraphicsObject * m_ClickedState;
		NBG::CGraphicsObject * m_DisabledState;

		/// Текущее состояние кнопки
		NBG::CGraphicsObject * m_ActiveState;

		/// Звук при клике на кнопку.
		std::string m_Sound;

		/// Звук при клике на заблокированную кнопку.
		std::string m_DisabledSound;          

		/// Установить состояние кнопки.
		void SetState(ButtonState state);

		/// Текущее состояние кнопки.
		ButtonState m_State;

		/// Режим вывода только текста.
		bool m_IsOnlyText;
	protected:
	};
}

#endif

