#ifndef _AK_FRAMEWORK_CURSOR_OBJECT
#define _AK_FRAMEWORK_CURSOR_OBJECT

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Texture.h>

#include <Framework/GUI/GraphicsObject.h>

#include <map>

namespace NBG
{
	class CBaseObject;
	/** @brief Класс, реализующий работу с курсором
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CCursorObject : public CGraphicsObject
	{
	public:          
		/// @name Конструктор/деструктор
		CCursorObject(const std::string id);
		virtual ~CCursorObject();           

		void AddState(const std::string &state, const std::string &texture);
		void ChangeState(const std::string &state);
		void ResetState();

		/// Показать/спрятать курсор.
		void Show(const bool show);
	private:
		std::string m_State;
		std::map<std::string, std::string>m_StatesMap;
	};
}

#endif //_AK_FRAMEWORK_CURSOR_OBJECT

