#include "LabelObject.h"

#include <framework.h>

#include <Framework/Managers/FontsManager.h>
#include <Framework/GUI/GraphicsObject.h>


namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CLabelObject::CLabelObject(const std::string id):CBaseObject(id)
	{
		m_CharacterPadding = 0.0f;
		m_Chars			= NULL;
		m_CachedSymbols = NULL;
		m_BaseLine = 10.0f;
		m_BaseCharacterPadding = 10.0f;
		m_SpaceSize = 8.0f;
		m_LineHeight = 22.0f;
		m_Offset.x = m_Offset.y = 0.0f;
		m_PrevHotSpot = m_HotSpot;
		m_TextContainer = new CContainerObject("TextContainer");

		m_BoundingBox.right = 0.0f;
		m_BoundingBox.bottom = 0.0f;

	}

	//////////////////////////////////////////////////////////////////////////
	CLabelObject::~CLabelObject()
	{
		if (m_CachedSymbols)
		{
			delete[] m_CachedSymbols;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::OnLoadFromXMLNode(pugi::xml_node node)
	{
		std::string font = node.attribute("font").value();
		FontDescription * desc = g_FontsManager->GetFont(font);
		std::string text = node.attribute("text").value();
		std::wstring resText = L"";
		if (text.substr(0,1) == "#")
		{
			resText = g_LocaleManager->GetText(text.substr(1).c_str());
		}
		else
		{
			resText = pugi::as_wide(text);
		}
		Init(desc,resText);

		bool changeText = false;

		if (node.attribute("letter_spacing").empty() == false)
		{
			m_CharacterPadding = node.attribute("letter_spacing").as_float();
			m_BaseCharacterPadding = m_CharacterPadding;
			changeText = true;			
		}

		if (node.attribute("space_size").empty() == false)
		{
			m_SpaceSize = node.attribute("space_size").as_float();			
			changeText = true;
		}

		if (node.attribute("max_width").empty() == false)
		{
			FRect r;
			r.right = node.attribute("max_width").as_float();
			m_BoundingBox = r;
			changeText = true;
		}

		if (changeText)
		{
			SetText(resText);
		}
		
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::Init(FontDescription * fontDesc, const std::wstring str)
	{
		AddChild(m_TextContainer);
		m_FontDesc = fontDesc;

		m_Chars = fontDesc->chars;
		m_CharacterPadding = fontDesc->characterPadding;
		m_CharsCount = fontDesc->charsCount;
		m_Kernings = fontDesc->kernings;
		m_KerningsCount = fontDesc->kerningsCount;
		m_BaseLine = fontDesc->baseLine;
		m_SpaceSize = fontDesc->spaceSize;
		m_LineHeight = fontDesc->lineHeight;
		m_TextureId = fontDesc->texture;

		m_BaseCharacterPadding = m_CharacterPadding;
		m_BaseText = str;

		m_Texture = CAST(NBG::CTextureResource*, g_ResManager->GetResource(m_TextureId));

		SetText(str);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::OnHotSpotChanged()
	{
		if (m_HotSpot == m_PrevHotSpot)return;
		m_PrevHotSpot = m_HotSpot;
		SetText(m_Text);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::OnColorChanged()
	{
		BASE_CHILD_VECTOR childs = m_TextContainer->GetChilds();
		for (size_t i=0; i<childs.size(); i++)
		{
			childs[i]->SetColor(GetColor());
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::OnScaleChanged()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::SetText(const std::wstring text)
	{
		//if (text == m_Text)return;
		if (!m_Chars)return;
		///Уничтожаем все предыдущие символы
		m_TextContainer->RemoveAllChilds();
		m_TextContainer->SetPosition(0.0f,0.0f);

		m_Offset.x = 0.0f;
		m_Offset.y = 0.0f;
		m_Text = text;
		int size = m_Text.length();
		///Если уже было что-то закешировано - удалить массив и пересоздать его заново
		if (m_CachedSymbols)
		{
			delete[] m_CachedSymbols;
		}
		m_CachedSymbols = new unsigned int[size];
		unsigned int id;
		///Пройтись в цикле по всем символам и найти им соответствие в массиве символов шрифта
		for (size_t i=0; i<m_Text.length(); i++)
		{
			id = (int)*m_Text.substr(i,1).c_str();
			if (id == 32) //SPACE
			{
				m_CachedSymbols[i] = SPACE_ID;
				continue;
			}
			else if (id == 10) //NEW LINE
			{
				m_CachedSymbols[i] = NEW_LINE_ID;
				continue;
			}
			CharOffsets* character = GetCharById(id);
			if (character)
			{
				m_CachedSymbols[i] = character->arrayId;
			}
			else
			{
				m_CachedSymbols[i] = -1;
			}
		}

		int posX = 0;
		int posY = 0;
		int previousSpace = -1;
		int previousId = 0;
		int width = 0;
		int lineWidth = 0;
		int prevRectI = -1;

		NBG::Vector sizeLabel;

		for (size_t i=0; i<m_Text.length(); ++i)
		{
			id = m_CachedSymbols[i];
			if (id == SPACE_ID)
			{
				previousSpace = i;
			}

			
			if (m_BoundingBox.right > 0.0f)
			{
				if (id == SPACE_ID)
				{
					lineWidth  += m_SpaceSize;
				}
				else if (id == NEW_LINE_ID)
				{
					lineWidth = 0;
				}
				else
				{
					CharOffsets* character = &m_Chars[id];
					lineWidth += character->xOffset+character->xAdvance+m_CharacterPadding;
				}

				if (lineWidth >= m_BoundingBox.right)
				{
					if (prevRectI == i)
					{
						id = NEW_LINE_ID;						
						lineWidth = 0;
					}
					else
					{
						id = NEW_LINE_ID;
						prevRectI = i;
						i = previousSpace;
						lineWidth = 0;
					}					
				}
			}

			if (id == NEW_LINE_ID)
			{
				if (m_HotSpot == HS_ML)
				{
					DrawLine(posX,posY,previousId,i);
				}
				else if (m_HotSpot == HS_UL)
				{
					DrawLine(posX,posY,previousId,i);
				}
				else if (m_HotSpot == HS_UR)
				{
					width = _GetStringWidth(previousId,i);
					DrawLine(posX-width,posY,previousId,i);
				}
				else if (m_HotSpot == HS_MU)
				{
					width = _GetStringWidth(previousId,i);
					DrawLine(posX-width/2,posY-m_BaseLine,previousId,i);
				}
				else if (m_HotSpot == HS_MID)
				{
					width = _GetStringWidth(previousId,i);
					DrawLine(posX-width/2,posY-m_BaseLine,previousId,i);
				}
				else if (m_HotSpot == HS_MR)
				{
					width = _GetStringWidth(previousId,i);
					DrawLine(posX-width,posY-m_BaseLine,previousId,i);
				}
				else if (m_HotSpot == HS_MD)
				{
					width = _GetStringWidth(previousId,i);
					DrawLine(posX-width/2.0f,posY-m_BaseLine,previousId,i);
				}
				else if (m_HotSpot == HS_DL)
				{				    
					DrawLine(posX,posY-m_BaseLine,previousId,i);
				}
				int w = _GetStringWidth(previousId,i);
				if (w>sizeLabel.x)sizeLabel.x=w;

				previousId = i+1;
				posY += m_LineHeight;
				sizeLabel.y+=m_LineHeight;
				m_Offset.y -= (m_LineHeight/2.0f);
				if (m_HotSpot == HS_UL || m_HotSpot == HS_MU ||  m_HotSpot == HS_UR)
				{
					m_Offset.y = 0;
				}
				posX = 0.0f;
				continue;
			}
		}
		if (m_HotSpot == HS_ML)
		{		
			DrawLine(posX,posY,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_UL)
		{
			DrawLine(posX,posY,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_MU)
		{
			width = _GetStringWidth(previousId,m_Text.length());
			DrawLine(posX-width/2,posY,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_UR)
		{
			width = _GetStringWidth(previousId,m_Text.length());			
			DrawLine(posX-width,posY,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_MID)
		{
			width = _GetStringWidth(previousId,m_Text.length());			
			DrawLine(posX-width/2,posY-m_BaseLine,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_MD)
		{
			width = _GetStringWidth(previousId,m_Text.length());
			DrawLine(posX-width/2.0f,posY-m_BaseLine,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_DL)
		{
			width = _GetStringWidth(previousId,m_Text.length());
			DrawLine(posX,posY-m_BaseLine,previousId,m_Text.length());
		}
		else if (m_HotSpot == HS_MR)
		{
			width = _GetStringWidth(previousId,m_Text.length());
			DrawLine(posX-width,posY-m_BaseLine,previousId,m_Text.length());
		}

		int w = _GetStringWidth(previousId,m_Text.length());
		if (w>sizeLabel.x)sizeLabel.x=w;

		if (m_HotSpot == HS_UL)
		{		
			m_TextContainer->SetAnchor(HS_UL);
		}
		else if (m_HotSpot == HS_MU)
		{			
			m_TextContainer->SetAnchor(HS_MU);
		}
		else if (m_HotSpot == HS_DL)
		{
			m_TextContainer->SetPosition(-sizeLabel.x/2.0f, -sizeLabel.y/2.0f-m_LineHeight);
		}
		else if (m_HotSpot == HS_ML)
		{			
			m_TextContainer->SetAnchor(HS_ML);			
			m_TextContainer->SetPosition(0.0f, -sizeLabel.y/2.0f-m_LineHeight);
		}
		else if (m_HotSpot == HS_MD)
		{
			m_TextContainer->SetPosition(0.0f, -sizeLabel.y/2.0f-m_LineHeight);
		}
		else if (m_HotSpot == HS_UR)
		{			
			m_TextContainer->SetAnchor(HS_UR);
		}
		else if (m_HotSpot == HS_MR)
		{			
			m_TextContainer->SetAnchor(HS_MR);
		}
		else
		{
			m_TextContainer->SetPosition(0.0f, m_Offset.y);			
		}
		SetSize(sizeLabel.x,sizeLabel.y+m_BaseLine);	
	}

	//////////////////////////////////////////////////////////////////////////
	CharOffsets* CLabelObject::GetCharById(const unsigned short &id)
	{
		///Функция быстрого поиска по сортированному массиву
		int n1 = 0;
		int n2 = m_CharsCount -1;
		int m = 0;
		if(n2 > n1)
		{
			do
			{
				m = (n1 + n2)>>1; //делим сдвигом на 2

				if(m_Chars[m].id == id)
				{
					return &m_Chars[m];
				}
				if(m_Chars[m].id > id)
				{
					n2 = m - 1;
				}
				else
				{
					n1 = m + 1;
				}
			} while(n1 <= n2);
		}

		if(n1 >= 0 && n1 < m_CharsCount && m_Chars[n1].id == id)
			return &m_Chars[n1];
		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	int CLabelObject::GetStringWidth()
	{
		return _GetStringWidth(0, m_Text.length())*GetScale().x;
	}

	//////////////////////////////////////////////////////////////////////////
	int CLabelObject::_GetStringWidth()
	{
		return GetStringWidth(0, m_Text.length());
	}

	//////////////////////////////////////////////////////////////////////////
	int CLabelObject::GetStringWidth(const int &sIndex, const int &eIndex)
	{
		return _GetStringWidth(sIndex, eIndex)*GetScale().x;
	}

	//////////////////////////////////////////////////////////////////////////
	int CLabelObject::_GetStringWidth(const int &sIndex, const int &eIndex)
	{
		int maxWidth = 0;

		unsigned int id = 0;
		int width = 0;
		int prevId = -1;
		for (int i=sIndex; i<eIndex; ++i)
		{
			id = m_CachedSymbols[i];
			if (id == -1)continue;
			if (id == SPACE_ID)
			{
				width += m_SpaceSize;
				continue;
			}
			if (id == NEW_LINE_ID)
			{
				if (width > maxWidth)
				{
					maxWidth = width;
				}
				width = 0;
				continue;
			}
			CharOffsets* character = &m_Chars[m_CachedSymbols[i]];
			if (character)
			{
				width += character->xOffset+character->xAdvance+m_CharacterPadding;
				if (prevId != -1)
				{
					width += GetKerningAmount(prevId,character->id);
				}
				prevId = character->id;
			}			
		}
		if (width > maxWidth)
		{
			maxWidth = width;
		}
		
		return maxWidth;
	}

	//////////////////////////////////////////////////////////////////////////
	void CLabelObject::DrawLine(int x, int y, const int &sIndex, const int &eIndex)
	{
		unsigned int id = 0;
		unsigned short prevId = -1;

		for (int i=sIndex; i<eIndex; ++i)
		{
			id = m_CachedSymbols[i];
			if (id == -1)continue;
			if (id == SPACE_ID)
			{
				x += (float)(m_SpaceSize+(int)m_CharacterPadding);
				continue;
			}
			CharOffsets* character = &m_Chars[m_CachedSymbols[i]];
			if (character)
			{
				CGraphicsObject * obj = new CGraphicsObject("char");				

				obj->SetTexture(m_Texture);
				obj->SetUV(character->uv);
				obj->SetSize(character->size);
				obj->SetHotSpot(HS_UL);
				if (prevId != -1)
				{
					x += GetKerningAmount(prevId,character->id);
				}
				obj->SetPosition((float)x,(float)y+(float)character->yOffset);
				obj->SetColor(GetColor());
				m_TextContainer->AddChild(obj);
				x += (float)(character->xOffset+character->xAdvance+m_CharacterPadding);
				prevId = character->id;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	float CLabelObject::GetKerningAmount(const unsigned short first, const unsigned short second)
	{
		for (int i=0; i<m_KerningsCount; i++)
		{
			FontKernPair * kernPair = &m_Kernings[i];
			if (kernPair->first == first && kernPair->second == second)return kernPair->offset;
		}
		return 0.0f;
	}
}
