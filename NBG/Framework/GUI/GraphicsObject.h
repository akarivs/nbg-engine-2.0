#ifndef _AK_FRAMEWORK_GRAPHICS_OBJECT
#define _AK_FRAMEWORK_GRAPHICS_OBJECT


#include <Framework/Structures.h>
#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Vertex.h>
#include <Framework/GUI/BaseObject.h>


#include <Framework/Global/Resources/TextureResource.h>


namespace NBG
{
	class CBaseObject;
	/** @brief Класс, реализующий работу со спрайтом
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CGraphicsObject : public CBaseObject
	{
	public:
		/// Типы меша
		enum MeshType
		{
			MT_Rectangle, ///обычный прямоугольный спрайт
			MT_Custom ///кастомная форма
		};

		/// @name Конструктор/деструктор
		CGraphicsObject(const std::string id);
		virtual ~CGraphicsObject();

		/// @name Загрузка объекта из XML ноды
		virtual void OnLoadFromXMLNode(pugi::xml_node node);

		/// @name Переопределённые функции базового объекта.
		inline virtual void OnPositionChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnScaleChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnRotationChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnColorChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnSizeChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnHotSpotChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};
		inline virtual void OnParentTransformChanged()
		{
			m_IsNeedToUpdateMesh = true;
		};

		/// Установить текстурные координаты объекта.
		inline void SetUV(const NBG::FRect uv )
		{
			m_UV = uv;
			m_IsNeedToUpdateMesh = true;
		}

		NBG::FRect GetUV()
		{
			return m_UV;
		}

		/// Установить режим блендинга - Additive
		inline void SetBlendMode(BlendMode mode)
		{
			m_BlendMode = mode;
		}

		char ** GetAlphaMask();

		AtlasTextureDescription * GetAtlasDesc()
		{
			return m_Desc;
		}
		void SetAtlasDesc(AtlasTextureDescription * desc)
		{
			m_Desc = desc;
		}


		/// Попадает ли точка в объект.
		virtual bool IsContains(const Vector &pos);

		/// Инициализация объекта.
		/// @param texturePath - путь к текстуре, относительно папки data.
		void Init(const std::string texturePath);
		virtual void DispatchEvent(Event event, CBaseObject * object);

		/// @name Обновление/отрисовка
		virtual int Update();
		virtual int Draw();

		/// Установка текстуры по её пути.
		void SetTexture(const std::string texturePath);

		/// Установка текстуры по указателю на ресурс.
		void SetTexture(NBG::CTextureResource * textureRes);			

		/// Установить вручную заданную форму для спрайта.
		void SetCustomShape(std::vector<Vertex> &vertexes, const bool modifyPos = true);

		/// Получить мэш спрайта.
		inline Mesh * GetMesh()
		{
			return &m_Mesh;
		}

		/// Установить режим тайлинга текстуры - растягивать/тайлить.
		void SetTextureTiling(bool tile)
		{
			m_IsTileTexture = tile;
			m_IsNeedToUpdateMesh = true;
		};

		/// Получить вершины, если объект создавался вручную.
		std::vector<Vector> * GetVertexes()
		{
			return &m_SavedVertexes;
		};

		/// Режет текстуру.
		void CutImage(const float xPercent, const float yPercent, bool xFlip = false, bool yFlip = false); 

		/// @name ANGEL SCRIPT PART
		void AddRef() { /* do nothing */ }
		void ReleaseRef() { /* do nothing */ }
        
        CTextureResource * GetTexture(){return m_Resource;}
        
        void SetRepeatedTexture(const bool isRepeated);

		/// Обновить мэш.
		void UpdateMeshData();
	protected:
		/// Мэш.
		Mesh m_Mesh;

		/// Тип мэша.
		MeshType m_MeshType;

		/// Используется ли атлас.
		bool m_UseAtlas;
        
        ///Повторяется ли текстура
        bool m_IsRepeatedTexture;

		/// Описание текстуры из атласа.
		AtlasTextureDescription * m_Desc;

		/// Текстурные координаты.
		NBG::FRect m_UV;

		/// Нужно ли при этом рендере обновить мэш.
		bool m_IsNeedToUpdateMesh;

		/// Тайлить ли текстуру.
		bool m_IsTileTexture; 

		/// Режим смешивания.
		bool m_IsAdditive; 
		bool m_IsScreen; 
	protected:
		/// Указатель на ресурс.
		CTextureResource * m_Resource;

		BlendMode m_BlendMode;


		/// Вершины заданные вручную.
		std::vector<Vector>m_Vertexes;

		/// Вершины заданные вручную, сохраненные без изменений.
		std::vector<Vector>m_SavedVertexes;
	};
}

#endif //_AK_FRAMEWORK_GRAPHICS_OBJECT

