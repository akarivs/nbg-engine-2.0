//
//  File created by Vadim Simonov.
//  Company New Bridge Games
//  Project Patchworks
//  Date 20130829
//  Класс, реализующий логику для списка
//============================================================================//

#ifndef _AK_FRAMEWORK_LIST_OBJECT
#define _AK_FRAMEWORK_LIST_OBJECT

#include <Framework/GUI/ButtonObject.h>
#include <Framework/GUI/ContainerObject.h>
#include <Framework/GUI/GraphicsObject.h>

namespace NBG
{
	class CBaseObject;
	class CListObject : public CBaseObject
	{
	public:
		CListObject(const std::string id);
		virtual ~CListObject();

		virtual void DispatchEvent(Event event, CBaseObject * object);			
		virtual void OnLoadFromXMLNode(pugi::xml_node node);
		virtual void AfterLoadFromXMLNode();

		virtual int OnMouseDown(const int button);
		virtual int OnMouseUp(const int button);
		virtual int OnMouseMove();          

		int GetSelectedIndex()
		{
			return m_SelectedIndex;
		}

		CBaseObject * GetSelectedItem()
		{
			return m_Elements[m_SelectedIndex];
		}

		virtual int Draw();

		void AddElement(STRING_VECTOR &keys,STRING_VECTOR &values);
	protected:
	private:            
		void UpdateSliderSize();
		pugi::xml_node m_TemplateNode;
		std::string m_TemplateNodePlain;

		int m_ItemHeight;
		int m_SelectedIndex;
		int m_PagesCount;
		float m_PageSize;

		CContainerObject * m_ItemsContainer;
		CContainerObject * m_SelectorContainer;
		Vector m_MouseDownPos;
		Vector m_MouseDownContainerPos;
		double m_MouseDownTime;
		float m_MaxHeight;
		bool m_IsDrag;
		bool m_IsSliderDrag;

		float m_Inertion;
		
		CGraphicsObject * m_Slider;
		float m_SliderPathHeight;

		std::vector<CBaseObject*>m_Elements;
	};
}

#endif //_AK_FRAMEWORK_LIST_OBJECT
