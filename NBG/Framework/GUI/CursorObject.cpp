#include "CursorObject.h"

#include <Framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CCursorObject::CCursorObject(const std::string id):CGraphicsObject(id)
	{
		m_State = "";
		SetHotSpot(HS_UL);
		SetScale(0.75f,0.75f);
	}

	//////////////////////////////////////////////////////////////////////////
	CCursorObject::~CCursorObject()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CCursorObject::Show(const bool show)
	{
		SetVisible(show);
		g_System->ShowCursor(!show);		
	}

	//////////////////////////////////////////////////////////////////////////
	void CCursorObject::AddState(const std::string &state, const std::string &texture)
	{
		m_StatesMap[state] = texture;
	}

	//////////////////////////////////////////////////////////////////////////
	void CCursorObject::ChangeState(const std::string &state)
	{
		if (m_State == state)return;
		SetTexture(m_StatesMap[state]);
		m_State = state;
	}

	//////////////////////////////////////////////////////////////////////////
	void CCursorObject::ResetState()
	{
		if (m_StatesMap.size() == 0)return;
		SetTexture(m_StatesMap.begin()->second);
	}
}
