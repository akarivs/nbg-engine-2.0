#ifndef _AK_FRAMEWORK_BASE_SCENE
#define _AK_FRAMEWORK_BASE_SCENE

#include <Framework/GUI/BaseObject.h>

namespace NBG
{
    class CBaseScene : public CBaseObject
    {
        public:
            CBaseScene(const std::string id);
            virtual ~CBaseScene();

            virtual void Init(){};
			virtual void Release(){};
        private:

        protected:
    };
}

#endif

