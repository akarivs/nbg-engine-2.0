#ifndef NBG_FRAMEWORK_ANIMATION_CLIP
#define NBG_FRAMEWORK_ANIMATION_CLIP

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Texture.h>

#include <Framework/GUI/GraphicsObject.h>

#include <Framework/Utils/Tweener.h>
/*
    Графический объект.
*/

namespace NBG
{
    class CBaseObject;

    class CClip : public CBaseObject
    {
        public:   
            CClip(const std::string id);
            virtual ~CClip();

            /// @name Загрузка объекта из XML ноды
            virtual void OnLoadFromXMLNode(pugi::xml_node node);

			void LoadFromXML(const std::string &path, const std::string &animation = "");

            ///Попадает ли точка
            virtual void DispatchEvent(Event event, CBaseObject * object);

			void LoadNode(pugi::xml_node * object, CClip * parent, int &maxFrame);
			void MoveToTime(const float time);
			void Play(const bool restart = false);
			void Stop();

			///Типы анимаций
            enum AnimType
            {
                AT_ForwardOnce,
                AT_BackwardOnce,
                AT_ForwardLoop,
                AT_BackwardLoop,
                AT_PingPong
            };
			void SetAnimationType(const int type, VOID_CALLBACK finishCallback);
			void SetAnimationType(const int type);
			

			struct KeyFrameData
			{
				Vector anchor;
				Vector pos;
				Vector scale;
				float rotation;
				float opacity;
				float duration;
			};

			std::vector<KeyFrameData>m_KeyFrames;			
			std::vector<CClip*>m_ChildClips;

			virtual int Update();
			virtual int Draw();

			void SetImage(CGraphicsObject * im)
			{
				m_Image = im;
			}

			GETTER_SETTER(float, m_SpeedMultiplier, SpeedMultiplier);

			float GetClipTime()
			{
				return m_AllTime;
			}
        private:         			
			bool m_IsTopClip;
			bool m_IsStoped;
			int m_CurrentFrame;
			float m_FrameInterval;
			float m_Timer;

			CGraphicsObject * m_Image;
			
			float m_AllTime;
			float m_CurrentTime;

			bool m_IsPongForward;

			

			int m_AnimType;			
			VOID_CALLBACK m_FinishCallback;
			bool m_IsUseCallback;
			
        protected:
        };
}

#endif //NBG_FRAMEWORK_ANIMATION_CLIP

