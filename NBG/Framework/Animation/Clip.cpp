#include "Clip.h"

#include <framework.h>
#include <Framework/Utils/Tweener.h>


namespace NBG
{
	CClip::CClip(const std::string id):CBaseObject(id)
	{     
		m_IsTopClip = false;
		m_Timer = 0.0f;
		m_IsUseCallback = false;
		m_Image = NULL;
		m_SpeedMultiplier = 1.0f;
	}

	CClip::~CClip()
	{   
		m_KeyFrames.clear();
	}

	/// @name Загрузка объекта из XML ноды
	void CClip::OnLoadFromXMLNode(pugi::xml_node node)
	{
		LoadFromXML(node.attribute("path").value(),
					node.attribute("animation").value());

		int animType = 0;
		std::string animTypeS = node.attribute("animType").value();

		if (animTypeS == "forwardOnce")animType = AT_ForwardOnce;
		else if (animTypeS == "backwardOnce")animType = AT_BackwardOnce;
		else if (animTypeS == "forwardLoop")animType = AT_ForwardLoop;
		else if (animTypeS == "backwardLoop")animType = AT_BackwardLoop;
		else if (animTypeS == "pingPong")animType = AT_PingPong;
		
		SetAnimationType(animType);
		
		if (node.attribute("playFromStart").as_bool())
		{
			Play();
		}

	}

	void CClip::LoadFromXML(const std::string &path, const std::string &animation)
	{
		m_IsTopClip = true; 
		std::string docPath = path;
		NBG::CXMLResource* xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(docPath);
		pugi::xml_document* doc_window = xmlRes->GetXML();



		m_CurrentFrame = 0;
		m_FrameInterval = 1.0f/30.0f;
		m_Timer = 0.0f;
		m_IsStoped = true;
		m_IsPongForward = true;

		pugi::xml_node objects = doc_window->first_child().first_child();;


		pugi::xml_node childNode = objects.first_child();
		if (animation.empty() == false)
		{
			objects = doc_window->first_child().find_child_by_attribute("id", animation.c_str());
		}

		m_AllTime = objects.attribute("time").as_float();
		m_CurrentTime = 0.0f;


		std::string modifier = objects.attribute("modifier").value();		

		m_AnimType = AT_ForwardLoop;	



		for (pugi::xml_node obj = objects.first_child(); obj; obj= obj.next_sibling())
		{
			int m;
			LoadNode(&obj,this,m);       		
		}
		MoveToTime(0.0f);

		g_ResManager->ReleaseResource(xmlRes);
	}

	void CClip::SetAnimationType(const int type, VOID_CALLBACK finishCallback)
	{
		m_AnimType = type;
		m_FinishCallback = finishCallback;		
		m_IsUseCallback = true;
	}

	void CClip::SetAnimationType(const int type)
	{
		m_AnimType = type;		
	}

	void CClip::LoadNode(pugi::xml_node * object, CClip * parent, int &maxFrame)
	{		
		CClip * clip = new CClip("Clip");
		Vector anchor;
		anchor.x = object->attribute("centerX").as_float();
		anchor.y = object->attribute("centerY").as_float();

		CGraphicsObject * graph = new CGraphicsObject("image");		
		std::string texturePath = object->attribute("texture").value();
		texturePath = StringUtils::StringReplace(texturePath,std::string("\\"),std::string("/"),true);
		graph->SetTexture(texturePath);
		clip->SetId(object->attribute("name").value());		
		clip->AddChild(graph);
		clip->SetImage(graph);

		parent->AddChild(clip);
		parent->m_ChildClips.push_back(clip);

		clip->SetLayer(object->attribute("order").as_int());

		for (pugi::xml_node obj = object->first_child(); obj; obj= obj.next_sibling())
		{
			if (std::string(obj.name()) == "movingPart")
			{
				LoadNode(&obj,clip,maxFrame);
				continue;
			}
			float time = obj.attribute("time").as_float();
			float x = obj.attribute("x").as_float();
			float y = obj.attribute("y").as_float();
			float scaleX = obj.attribute("scaleX").as_float();
			float scaleY = obj.attribute("scaleY").as_float();
			if (obj.attribute("scaleX").empty())
			{
				scaleX =  1.0f;				
			}
			if (obj.attribute("scaleY").empty())
			{
				scaleY =  1.0f;				
			}

			float angle = MathUtils::ToDegree(obj.attribute("angle").as_float());
			if ((scaleX < 0.0f && scaleY > 0.0f) ||  (scaleX > 0.0f && scaleY < 0.0f))
			{
				//angle *= -1.0f;
			}

			float alpha = obj.attribute("alpha").as_float()*255.0f;


			KeyFrameData kf;
			kf.pos.x		= x;
			kf.pos.y		= y;
			kf.anchor.x		= anchor.x;
			kf.anchor.y		= anchor.y;
			kf.scale.x		= scaleX;
			kf.scale.y		= scaleY;
			kf.rotation		= angle;
			kf.opacity		= alpha;
			kf.duration		= time;
			clip->m_KeyFrames.push_back(kf);


		}		
	}

	void CClip::MoveToTime(const float time)
	{		
		KeyFrameData * prevFrame = NULL;
		KeyFrameData * nextFrame = NULL;
		float prevTime = -1.0f;
		for (int i=m_KeyFrames.size()-1; i>=0; i--)
		{
			KeyFrameData * f = &m_KeyFrames[i];
			if (f->duration < time)
			{
				prevFrame = f;
				break;
			}
		}
		for (int i=0; i<m_KeyFrames.size(); i++)
		{
			KeyFrameData * f = &m_KeyFrames[i];
			if (f->duration >= time)
			{
				nextFrame = f;
				break;
			}
		}

		if (m_KeyFrames.size() > 0)
		{
			if (prevFrame == NULL)prevFrame = &m_KeyFrames[0];
			if (nextFrame == NULL)nextFrame = &m_KeyFrames[m_KeyFrames.size()-1];
		}


		if (prevFrame && nextFrame)
		{
			if (prevFrame == nextFrame)
			{
				SetPosition(prevFrame->pos);
				SetScale(prevFrame->scale);
				CBaseObject * im = GetChild("image");			
				im->SetHotSpot(HS_CUSTOM,prevFrame->anchor);
				SetRotation(prevFrame->rotation);
				SetColor(Color(prevFrame->opacity,m_Color.r,m_Color.g,m_Color.b));
			}
			else
			{			
				EaseFunc ease = g_Tweener->GetTween(EASE_LINEAR);

				float t = time-prevFrame->duration;
				float d = nextFrame->duration - prevFrame->duration;
				float c = nextFrame->pos.x-prevFrame->pos.x;
				float b = prevFrame->pos.x;

				Vector pos;
				pos.x = ease(t,b,c,d);
				c = nextFrame->pos.y-prevFrame->pos.y;
				b = prevFrame->pos.y;
				pos.y = ease(t,b,c,d);
				SetPosition(pos);


				Vector scale;
				c = nextFrame->scale.x-prevFrame->scale.x;
				b = prevFrame->scale.x;
				scale.x = ease(t,b,c,d);
				c = nextFrame->scale.y-prevFrame->scale.y;
				b = prevFrame->scale.y;
				scale.y = ease(t,b,c,d);
				SetScale(scale);			

				m_Image->SetHotSpot(HS_CUSTOM,prevFrame->anchor);			


				c = nextFrame->rotation-prevFrame->rotation;
				b = prevFrame->rotation;			
				SetRotation(ease(t,b,c,d));

				c = nextFrame->opacity-prevFrame->opacity;
				b = prevFrame->opacity;			

				SetColor(Color(ease(t,b,c,d),m_Color.r,m_Color.g,m_Color.b));
			}
		}


		for (int i=0; i<m_ChildClips.size(); i++)
		{
			CClip * clip = m_ChildClips[i];			
			clip->MoveToTime(time);
		}
	}

	void CClip::Play(const bool restart)
	{
		m_IsStoped = false;
		if (restart)
		{
			if (m_AnimType == AT_BackwardLoop || m_AnimType == AT_BackwardOnce)
			{
				m_CurrentTime = m_AllTime;
			}
			else
			{
				m_CurrentTime = 0.0f;
			}
			MoveToTime(m_CurrentTime);
		}
	}

	void CClip::Stop()
	{
		m_IsStoped = true;
		if (m_AnimType == AT_BackwardLoop || m_AnimType == AT_ForwardOnce)
		{
			m_CurrentTime = m_AllTime;
		}
		else if (m_AnimType == AT_BackwardOnce || m_AnimType == AT_ForwardLoop)
		{
			m_CurrentTime = 0.0f;
		}		
		else
		{
			m_CurrentTime = 0.0f;
		}
		if (m_IsUseCallback)
		{
			m_FinishCallback();		
		}		
		MoveToTime(m_CurrentTime);
	}



	void CClip::DispatchEvent(Event event, CBaseObject * object)
	{
		GetParent()->DispatchEvent(event,object);
	}


	int CClip::Update()
	{

		CBaseObject::Update();   

		if (m_IsTopClip && !m_IsStoped)
		{	
			m_Timer += g_FrameTime * m_SpeedMultiplier;
			while (m_Timer > m_FrameInterval)
			{		
				m_Timer -= m_FrameInterval;

				if (m_AnimType == AT_ForwardLoop || m_AnimType == AT_ForwardOnce)
				{				
					m_CurrentTime += m_FrameInterval;	
					if (m_CurrentTime >= m_AllTime)
					{
						if (m_AnimType == AT_ForwardLoop)
						{
							m_CurrentTime = 0;
						}
						else if (m_AnimType == AT_ForwardOnce)
						{						
							Stop();
						}
					}
				}
				else if (m_AnimType == AT_PingPong)
				{
					if (m_IsPongForward)
					{					
						m_CurrentTime+=m_FrameInterval;
						if (m_CurrentTime >= m_AllTime)
						{
							m_CurrentTime = m_AllTime;
							m_IsPongForward = false;
						}
					}
					else
					{
						m_CurrentTime-=m_FrameInterval;
						if (m_CurrentTime < 0.0f)
						{
							m_CurrentTime = 0.0f;
							m_IsPongForward = true;
						}
					}
				}	
				MoveToTime(m_CurrentTime/m_AllTime);	
				//SetColor(GetColor());
			}
		}
		return EventSkip;
	}

	int CClip::Draw()
	{
		CBaseObject::Draw();			
		return EventSkip;
	}
}
