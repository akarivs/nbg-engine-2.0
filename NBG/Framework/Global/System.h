#ifndef NBG_FRAMEWORK_GLOBAL_SYSTEM
#define NBG_FRAMEWORK_GLOBAL_SYSTEM

#ifdef NBG_WIN32
	#include "System/win32/System.h"
#elif defined(NBG_OSX)
	#include "System/osx/System.h"
#elif defined(NBG_IOS)
    #include "System/ios/System.h"
#elif defined(NBG_ANDROID)
    #include "System/android/System.h"
#endif

#endif // NBG_FRAMEWORK_GLOBAL_SYSTEM
