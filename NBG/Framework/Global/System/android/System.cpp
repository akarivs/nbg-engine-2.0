#ifdef NBG_ANDROID

#include "System.h" 
#include <Framework.h>


namespace NBG
{
	//==============================================================================
	CSystem::CSystem()
	{
		m_Locale = "";
		m_AppPath = "";
	}

	//==============================================================================
	CSystem::~CSystem()
	{

	}	

	//==============================================================================
	std::string CSystem::GetSavePath(const std::string &company, const std::string &product)
	{
		return "";

	}

	//==============================================================================
	std::string &CSystem::GetLocale()
	{
		return m_Locale;
	}

	//==============================================================================
	bool CSystem::IsFileExists(const std::string &file)
	{
		return true;
	}

	//==============================================================================
	bool CSystem::IsFolderExists(const std::string &folder)
	{
		return true;   // this is a directory!
	}

	//==============================================================================
	void CSystem::SysDeleteFile(const std::string &file)
	{
		//_unlink(file.c_str());
	}

	//==============================================================================
	bool CSystem::SysCreateFolder(const std::string &path)
	{
		return true;
	}

	//==============================================================================
	STRING_VECTOR CSystem::GetFolderContents(const std::string &folder)
	{
		std::vector<std::string> dirContents;
		return dirContents;
	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		logicalSize = g_GameApplication->GetLogicalSize();
		m_SystemResolution = windowSize;		

		m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;		

		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

		m_GameBounds = m_SystemResolution / m_ResolutionScale;						
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}	


		if (m_GameBounds.x < 1024)
		{
			m_SystemResolution.x = 1024;
			m_SystemResolution.y = 768;	

			m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
			m_ResolutionScale.x = m_ResolutionScale.y;		

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

			m_GameBounds = m_SystemResolution / m_ResolutionScale;				
		}
		


		m_XOffset = (m_GameResolution.x - m_SystemResolution.x)/2.0f;			
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y)/2.0f;
	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
		
		float _x = g_GameApplication->GetLogicalSize().x / m_GameResolution.x;
		float _y = g_GameApplication->GetLogicalSize().y / m_GameResolution.y;
		vec.x -= m_GameResolution.x/2.0f;
		vec.y -= m_GameResolution.y/2.0f;
		

		vec.x += m_XOffset;
		vec.y += m_YOffset;
		vec.x *= _x;
		vec.y *= _y;

		if (vec.y < -m_GameBounds.y/2.0f)
		{
			vec.y = -m_GameBounds.y/2.0f;
		}
		else if (vec.y > m_GameBounds.y/2.0f)
		{
			vec.y = m_GameBounds.y/2.0f;
		}

	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{		
		return true;
	}

	//==============================================================================
	void CSystem::ShowCursor(const bool show)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CSystem::Sleep(const int miliseconds)
	{
		//usleep(miliseconds);
	}
}
#endif
