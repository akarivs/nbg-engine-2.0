#ifdef NBG_WIN32
#include <windows.h>

#include "System.h" 
#include <Framework.h>
#include <Shlobj.h>


namespace NBG
{
	//==============================================================================
	CSystem::CSystem()
	{
		m_Locale = "";

		m_AppPath = "";
		char str[255];
		GetCurrentDirectoryA(255,&str[0]);
		m_AppPath = str;
		m_AppPath += "\\Resources";
		bool res = SetCurrentDirectoryA(m_AppPath.c_str());
		if (res == false)
		{
			CONSOLE("Failed to change current directory!");
		}

		m_SystemResolution.x = GetSystemMetrics(SM_CXSCREEN);
		m_SystemResolution.y = GetSystemMetrics(SM_CYSCREEN);
	}

	//==============================================================================
	CSystem::~CSystem()
	{

	}	

	//==============================================================================
	std::string CSystem::GetSavePath(const std::string &company, const std::string &product)
	{
		std::string res = g_FileSystem->GetSaveDir();
		res += "/"+company;
		g_FileSystem->CreateDirectory(res);
		res += "/"+product;
		g_FileSystem->CreateDirectory(res);
		res += "/";
		return res;

	}

	//==============================================================================
	std::string &CSystem::GetLocale()
	{

			if (m_Locale == "")
			{				
				g_Config->GetValue("locale",m_Locale);			
			}
			return m_Locale;
	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		logicalSize = g_GameApplication->GetLogicalSize();

		m_SystemResolution = windowSize;		

		m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;		

		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

		m_GameBounds = m_SystemResolution / m_ResolutionScale;						
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}

		/*if (m_GameBounds.x < 1024)
		{
			m_SystemResolution.x = 1024;
			m_SystemResolution.y = 768;	

			m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
			m_ResolutionScale.x = m_ResolutionScale.y;		

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

			m_GameBounds = m_SystemResolution / m_ResolutionScale;				
		}*/
		m_XOffset = (m_GameResolution.x - m_SystemResolution.x)/2.0f;			
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y)/2.0f;
	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
		float _x = g_GameApplication->GetWindowSize().x / m_GameResolution.x;
		float _y = g_GameApplication->GetWindowSize().y / m_GameResolution.y;
		vec.x -= m_GameResolution.x/2.0f;
		vec.y -= m_GameResolution.y/2.0f;

		vec.x += m_XOffset;
		vec.y += m_YOffset;
		vec.x *= _x;
		vec.y *= _y;

		if (vec.y < -m_GameBounds.y/2.0f)
		{
			vec.y = -m_GameBounds.y/2.0f;
		}
		else if (vec.y > m_GameBounds.y/2.0f)
		{
			vec.y = m_GameBounds.y/2.0f;
		}
	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{		
		return false;
	}

	//==============================================================================
	void CSystem::ShowCursor(const bool show)
	{
		if (show)
		{
			while (::ShowCursor(true)<0);
		}
		else
		{
			while (::ShowCursor(false)>=0);
		}
		
	}

	//////////////////////////////////////////////////////////////////////////
	void CSystem::Sleep(const int miliseconds)
	{
		::Sleep(miliseconds);
	}
}
#endif
