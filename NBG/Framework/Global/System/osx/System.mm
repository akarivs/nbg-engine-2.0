#ifdef NBG_OSX


#include "System.h" 
#include <Framework.h>

#import <Foundation/Foundation.h>
#include <CoreFoundation/CoreFoundation.h>

namespace NBG
{
	//==============================================================================
	CSystem::CSystem()
	{
		m_Locale = "";

		m_AppPath = "";
        
        static char path[1024];
        CFBundleRef mainBundle = CFBundleGetMainBundle();
        assert(mainBundle);
        
        CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
        assert(mainBundleURL);
        
        CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
        assert(cfStringRef);
        
        CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
        CFRelease(mainBundleURL);
        CFRelease(cfStringRef);
        path[1023] = 0;
        
        strcat( path, "/" );
        
        m_AppPath = path;
        m_AppPath += "Contents/Resources/";

        
        BOOL result = [[NSFileManager defaultManager] changeCurrentDirectoryPath: [ NSString stringWithUTF8String:m_AppPath.c_str() ] ];
        
        if (result == NO)
        {
            CONSOLE("FAILED TO CHANGE DIRECTORY!");
        }
    }

	//==============================================================================
	CSystem::~CSystem()
	{

	}	

	//==============================================================================
	std::string &CSystem::GetLocale()
	{

			if (m_Locale == "")
			{				
				g_Config->GetValue("locale",m_Locale);			
			}
			return m_Locale;
	}

	//==============================================================================
	bool CSystem::IsFileExists(const std::string &file)
	{
		/*SDL_RWops * fileOp = SDL_RWFromFile(file.c_str(),"r");
		if (fileOp)
		{
			fileOp->close(fileOp);
			return true;
		}*/
		return false;
	}

	//==============================================================================
	bool CSystem::IsFolderExists(const std::string &folder)
	{
		std::string path = folder;
		/*DWORD ftyp = GetFileAttributesA(path.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!*/
        return true;
	}

	//==============================================================================
	void CSystem::SysDeleteFile(const std::string &file)
	{
		//unlink(file.c_str());
	}

	//==============================================================================
	STRING_VECTOR CSystem::GetFolderContents(const std::string &folder)
	{
		std::vector<std::string> dirContents;
		return dirContents;
	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		m_SystemResolution = windowSize;		

		m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;		

		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

		m_GameBounds = m_SystemResolution / m_ResolutionScale;						
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}

		if (m_GameBounds.x < 1024)
		{
			m_SystemResolution.x = 1024;
			m_SystemResolution.y = 768;	

			m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
			m_ResolutionScale.x = m_ResolutionScale.y;		

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

			m_GameBounds = m_SystemResolution / m_ResolutionScale;				
		}
		m_XOffset = (m_GameResolution.x - m_SystemResolution.x)/2.0f;			
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y)/2.0f;
	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
		float _x = g_GameApplication->GetWindowSize().x / m_GameResolution.x;
		float _y = g_GameApplication->GetWindowSize().y / m_GameResolution.y;
		vec.x -= m_GameResolution.x/2.0f;
		vec.y -= m_GameResolution.y/2.0f;

		vec.x += m_XOffset;
		vec.y += m_YOffset;
		vec.x *= _x;
		vec.y *= _y;

		if (vec.y < -m_GameBounds.y/2.0f)
		{
			vec.y = -m_GameBounds.y/2.0f;
		}
		else if (vec.y > m_GameBounds.y/2.0f)
		{
			vec.y = m_GameBounds.y/2.0f;
		}
	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{		
		return false;
	}
    
    //==============================================================================
    std::string CSystem::GetSavePath(const std::string & company,const std::string & product)
    {
       /* static std::string buffer;
        NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        buffer = [ns_path UTF8String];
        
        
        std::string res = buffer;
        res += "/"+company;
		g_System->SysCreateFolder(res);
        res += "/"+product;
        g_System->SysCreateFolder(res);
		res += "/";
        return res;*/
        return "";
    }
    
    ///
    void CSystem::Sleep(const int miilseconds)
    {
        //sleep(miliseconds);
    }
}
#endif
