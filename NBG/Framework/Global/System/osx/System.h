#ifndef NBG_FRAMEWORK_GLOBAL_SYSTEM_OSX
#define NBG_FRAMEWORK_GLOBAL_SYSTEM_OSX

#ifdef NBG_OSX

#include <string>
#include <ostream>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
	/** @brief Класс, реализующий доступ к информации о системе.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CSystem
	{
	public:
        CSystem();
		~CSystem();

		void Init(){};

		std::string &GetLocale();		

		///*** FILE SYSTEM ***///
		bool IsFileExists(const std::string &file);
		bool IsFolderExists(const std::string &folder);
		void SysDeleteFile(const std::string &file);
        std::string & GetAppPath(){return m_AppPath;};
        std::string GetSavePath(const std::string & company,const std::string & product);
		STRING_VECTOR GetFolderContents(const std::string &folder);

		void UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen);

		void ConvertMousePos(Vector & vec);

		/// Получить разрешение экрана.
		Vector GetSystemResolution()
		{
			return m_SystemResolution;
		};

		/// Установить разрешение экрана.
		void SetSystemResolution(const int width, const int height)
		{
			m_SystemResolution = Vector(width,height);
		};

		/// Получение разрешение экрана.
		Vector GetGameResolution()
		{
			return m_GameResolution;
		};

		/// Получить коэффициент масштабирования экрана
		Vector GetResolutionScale()
		{
			return m_ResolutionScale;
		};

		/// Получить границы экрана.
		Vector GetGameBounds()
		{
			return m_GameBounds;
		}

		/// Получить смещение по X для центра.
		float GetXOffset()
		{
			return m_XOffset;
		};

		/// Получить смещение по Y для центра.
		float GetYOffset()
		{
			return m_YOffset;
		};

        void ShowCursor(const bool show){};
		bool IsMobileSystem();

        
        void Sleep(const int miilseconds);


	private:
		

		/// Системное разрешение экрана.
		Vector m_SystemResolution;
		/// Коэффициент масштабирования игры.
		Vector m_ResolutionScale;
		/// Игровое разрешение экрана.
		Vector m_GameResolution; 
		/// Игровые границы экрана.
		Vector m_GameBounds;
		/// Получить смещение по X для центра.
		float m_XOffset;
		/// Получить смещение по Y для центра.
		float m_YOffset;


		std::string m_AppPath;
		std::string m_Locale;
	};
}
#endif
#endif // NBG_FRAMEWORK_GLOBAL_SYSTEM_WIN32
