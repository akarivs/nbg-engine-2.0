#ifdef NBG_IOS


#include "System.h" 
#include <Framework.h>

#include <UIKit/UIDevice.h>
#include <UIKit/UIScreen.h>

namespace NBG
{
	//==============================================================================
	CSystem::CSystem()
	{
		m_Locale = "";
        m_IsPortrait = false;
		m_AppPath = "";
        
       

      
        
        static char path[1024];
        CFBundleRef mainBundle = CFBundleGetMainBundle();
        assert(mainBundle);
        
        CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
        assert(mainBundleURL);
        
        CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
        assert(cfStringRef);
        
        CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
        CFRelease(mainBundleURL);
        CFRelease(cfStringRef);
        path[1023] = 0;
        
        strcat( path, "/" );
        
        m_AppPath = path;
        m_AppPath += "";
        
        NSFileManager *filemgr;
        NSArray *dirPaths;
        NSString *docsDir;
        
        filemgr =[NSFileManager defaultManager];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        docsDir = [dirPaths objectAtIndex:0];
        
        NSString *pathS = [NSString stringWithCString:m_AppPath.c_str()
                                                    encoding:[NSString defaultCStringEncoding]];
        
        if ([filemgr changeCurrentDirectoryPath: pathS] == NO)
        {
            CONSOLE("FAILED TO CHANGE DIRECTORY!");
        }
             
        float   scale = [[UIScreen mainScreen] scale];
        CGRect rect = [[UIScreen mainScreen] bounds];
        int width, height;
        if (rect.size.height * scale > rect.size.width * scale)
        {
            width = rect.size.height * scale;
        }
        else
        {
            width = rect.size.width * scale;
        }
        
        if (rect.size.height * scale > rect.size.width * scale)
        {
            height = rect.size.width * scale;
        }
        else
        {
            height = rect.size.height * scale;
        }
        m_SystemResolution.x = width;
        m_SystemResolution.y = height;

    }

	//==============================================================================
	CSystem::~CSystem()
	{

	}	

	//==============================================================================
	std::string &CSystem::GetLocale()
	{

			if (m_Locale == "")
			{				
				g_Config->GetValue("locale",m_Locale);			
			}
			return m_Locale;
	}

	//==============================================================================
	bool CSystem::IsFileExists(const std::string &file)
	{
		FILE * fileOp = fopen(file.c_str(),"r");
		if (fileOp)
		{
			fclose(fileOp);
			return true;
		}
		return false;
	}

	//==============================================================================
	bool CSystem::IsFolderExists(const std::string &folder)
	{
		std::string path = folder;
		/*DWORD ftyp = GetFileAttributesA(path.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!*/
        return true;
	}

	//==============================================================================
	void CSystem::SysDeleteFile(const std::string &file)
	{
		//unlink(file.c_str());
	}
    
    //==============================================================================
    bool CSystem::SysCreateFolder(const std::string &path)
    {
        NSError* ptrError = nil;
        BOOL returnValue = [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithUTF8String:path.c_str()] withIntermediateDirectories:YES attributes:nil error:&ptrError];
        return returnValue;
    }

	//==============================================================================
	STRING_VECTOR CSystem::GetFolderContents(const std::string &folder)
	{
        std::vector<std::string> dirContents;
        std::string dirPathS = folder;
        if (folder.substr(0,1)=="/")dirPathS=folder;
        NSString *dirPath = [NSString stringWithCString:dirPathS.c_str()
                                               encoding:[NSString defaultCStringEncoding]];
        
        NSError * error;
        NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dirPath error:&error];
        for(NSString * myStr in directoryContents)
        {
            dirContents.push_back(myStr.cString);
        }
        return dirContents;

	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		//m_SystemResolution = windowSize;

		m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;		

		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

		m_GameBounds = m_SystemResolution / m_ResolutionScale;						
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}

		if (m_GameBounds.x < 1024)
		{
			m_SystemResolution.x = 1024;
			m_SystemResolution.y = 768;	

			m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
			m_ResolutionScale.x = m_ResolutionScale.y;		

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;		

			m_GameBounds = m_SystemResolution / m_ResolutionScale;				
		}
		m_XOffset = (m_GameResolution.x - m_SystemResolution.x)/2.0f;			
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y)/2.0f;
	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
        vec *= 2.0f;
        
        vec.x -= m_GameResolution.x/2.0f;
        vec.y -= m_GameResolution.y/2.0f;
        
        float _x = g_GameApplication->GetWindowSize().x / m_GameResolution.x;
        float _y = g_GameApplication->GetWindowSize().y / m_GameResolution.y;
        vec.x += m_XOffset;
        vec.x *= _y;
        vec.y *= _y;
	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{		
		return true;
	}
    
    //==============================================================================
    std::string CSystem::GetSavePath(const std::string & company,const std::string & product)
    {
        static std::string buffer;
        NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        buffer = [ns_path UTF8String];
        
        
        std::string res = buffer;
        res += "/"+company;
		g_System->SysCreateFolder(res);
        res += "/"+product;
        g_System->SysCreateFolder(res);
		res += "/";
        return res;
    }
    
    ///
    void CSystem::Sleep(const int miilseconds)
    {
        //sleep(miliseconds);
    }
}
#endif
