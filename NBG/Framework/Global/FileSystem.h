#ifndef NBG_CORE_FILESYSTEM
#define NBG_CORE_FILESYSTEM

#include "FileSystem/BaseFileSystem.h"
#ifdef NBG_WIN32
    #include "FileSystem/win32/FileSystem.h"
#elif defined (NBG_IOS)
    #include "FileSystem/ios/FileSystem.h"
#elif defined (NBG_OSX)
    #include "FileSystem/osx/FileSystem.h"
#elif defined (NBG_ANDROID)
    #include "FileSystem/android/FileSystem.h"
#elif defined (NBG_LINUX)
    #include "FileSystem/linux/FileSystem.h"
#endif

#endif //NBG_CORE_FILESYSTEM
