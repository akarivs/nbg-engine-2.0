#ifndef NBG_FRAMEWORK_GLOBAL_INPUT
#define NBG_FRAMEWORK_GLOBAL_INPUT

#include <string>
#include <ostream>

#include <Framework/Datatypes/Vector.h>

#ifdef NBG_WIN32
	#include "Input/win32/KeyCodes.h"
#elif defined(NBG_OSX)
	#include "Input/osx/KeyCodes.h"
#elif defined(NBG_IOS)
	#include "Input/ios/KeyCodes.h"
#elif defined(NBG_ANDROID)
	#include "Input/android/KeyCodes.h"
#endif


namespace NBG
{
/** @brief Класс, реализующий доступ к глобальным переменным инпута.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class CInput
{
public:    
	CInput();
    ~CInput();

	void SetMousePos(int x, int y);
	Vector & GetMousePos();

	void SetLastInputChar(wchar_t ch)
	{
		m_LastInput = ch;
	}

	std::wstring &GetLastInput()
	{
		return m_LastInput;
	}

	static const int MOUSE_BUTTON_LEFT = 0;
	static const int MOUSE_BUTTON_MIDDLE = 1;
	static const int MOUSE_BUTTON_RIGHT = 2;

	void SetKeyDown(const int id, const bool isDown);
	bool IsKeyDown(const int id);
	bool IsSystemKey(const int id);
private:		
	std::wstring m_LastInput;
	Vector m_MousePos;
	bool m_Keys[256];
};
}
#endif // NBG_FRAMEWORK_GLOBAL_INPUT
