#ifdef NBG_LINUX

#include <NBG/NBG.h>
#include <core/Initializer.h>


#include "FileSystem.h"
#include <stdio.h>

namespace NBG
{
	CFileSystem::CFileSystem()
	{
	}

	CFileSystem::~CFileSystem()
	{
	}


	CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
	{


		FileContainer fc;
		fc.status = FileOk;
		fc.data = 0;
		fc.size = 0;
		fc.status = FileOk;

		return fc;
	}

	FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
	{
		FILE* file = fopen(filename.c_str(),mode.c_str());
		return file;
	}

	void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
	{
		FILE* file = fopen(filename.c_str(),"wb");
		if (!file)
		{
			return;
		}
		fwrite(data,size,1,file);
		fclose(file);
	}

	std::string CFileSystem::GetSaveDir()
	{
		return std::string("");
	}

	///Существует ли файл
	bool CFileSystem::IsFileExists(const std::string &filename)
	{
		return true;
	}

	///Существует ли директория
	bool CFileSystem::IsDirExists(const std::string &filename)
	{
		return false;    // this is not a directory!
	}

	bool CFileSystem::CreateDirectory(const std::string &path)
	{
		return true;
	}

	std::wstring CFileSystem::GetCurrentDirectory()
	{
		return std::wstring(L"");
	}

	///Установка текущей директории
	bool CFileSystem::SetCurrentDirectory(std::wstring dir)
	{
		return true;
	}


	///Получение директории процесса
	std::wstring CFileSystem::GetProcessDirectory()
	{
		return std::wstring(L"");
	}

	std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
	{
	}
}
#endif
