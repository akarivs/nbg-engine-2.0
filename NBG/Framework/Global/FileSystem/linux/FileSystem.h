#ifndef NBG_CORE_FILESYSTEM_LINUX
#define NBG_CORE_FILESYSTEM_LINUX

#ifndef NBG_LINUX
	#error This file must not be included in non-android projects! Or you forgot to add NBG_LINUX directive :)
#endif

#include <map>
#include <vector>
#include <core/Datatypes.h>

#include "../BaseFileSystem.h"

namespace NBG
{
/** @brief Класс, реализующий работу с файловой системой Android
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013 New Bridge Games
*
*/
class CFileSystem : public IFileSystem
{
public:
    CFileSystem();
    ~CFileSystem();

	/// @name Методы для работы с директориями
	///Создание директории
    virtual bool CreateDirectory(const std::string &path);
	///Получение текущей директории
    virtual std::wstring GetCurrentDirectory();
	///Установка текущей директории
    virtual bool SetCurrentDirectory(std::wstring dir);
	///Существует ли директория
	virtual bool IsDirExists(const std::string &filename);
	///Получение директории для сохранения
	virtual std::string GetSaveDir();
	///Получение директории процесса
	virtual std::wstring GetProcessDirectory();
    ///Получение списка файлов в директории
    virtual std::vector<std::string>GetDirectoryContents(const std::string &path);

	/// @name Методы для работы с файлами
    ///Чтение файла с диска
    virtual FileContainer ReadFile(const std::string &filename, const std::string &mode) ;
	///Чтение файла с диска
    virtual FILE * OpenFile(const std::string &filename, const std::string &mode);
	///Запись файла на диск
	virtual void SaveFile(const std::string &filename, const void * data, const int size);
	///Существует ли файл
	virtual bool IsFileExists(const std::string &filename);
private:
};
}
#endif //NBG_CORE_FILESYSTEM_LINUX
