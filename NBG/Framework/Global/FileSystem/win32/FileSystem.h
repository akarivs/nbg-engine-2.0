#ifndef NBG_CORE_FILESYSTEM_WIN
#define NBG_CORE_FILESYSTEM_WIN

#ifdef NBG_WIN32

#include <windows.h>
#include <map>
#include <vector>
#include <Framework/Datatypes.h>

#include "../BaseFileSystem.h"

namespace NBG
{
	class CGamePack;
	/** @brief Класс, реализующий работу с файловой системой Windows
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CFileSystem : public IFileSystem
	{
	public:
		CFileSystem();
		~CFileSystem();

		virtual void Init();

		/// @name Методы для работы с директориями	
		///Создание директории
		virtual bool CreateDirectory(const std::string &path);
		///Получение текущей директории
		virtual std::wstring GetCurrentDirectory();
		///Установка текущей директории
		virtual bool SetCurrentDirectory(std::wstring dir);
		///Существует ли директория
		virtual bool IsDirExists(const std::string &filename);
		///Получение директории для сохранения
		virtual std::string GetSaveDir();
		///Получение директории процесса
		virtual std::wstring GetProcessDirectory();
		///Получение списка файлов в директории
		virtual std::vector<std::string>GetDirectoryContents(const std::string &path);

		/// @name Методы для работы с файлами
		///Чтение файла с диска
		virtual FileContainer ReadFile(const std::string &filename, const std::string &mode) ;
		///Чтение файла с диска
		virtual FILE * OpenFile(const std::string &filename, const std::string &mode);
		///Запись файла на диск
		virtual void SaveFile(const std::string &filename, const void * data, const int size);
		///Удалить файл
		virtual bool DeleteFile(const std::string &filename);
		///Существует ли файл
		virtual bool IsFileExists(const std::string &filename);
	private:		
		CGamePack * m_GamePack;
	};
}
#endif
#endif //NBG_CORE_FILESYSTEM_WIN
