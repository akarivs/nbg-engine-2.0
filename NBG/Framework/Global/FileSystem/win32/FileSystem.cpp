#ifdef NBG_WIN32

#include <Framework.h>


#include "windows.h"
#include "FileSystem.h"
#include <stdio.h>
#include <Shlobj.h>
#include "../../GamePack.h"


namespace NBG
{
	//==============================================================================
	CFileSystem::CFileSystem()
	{
		m_GamePack = new CGamePack();		
	}

	//==============================================================================
	CFileSystem::~CFileSystem()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	void CFileSystem::Init()
	{
		m_GamePack->Init();
	}

	//==============================================================================
	CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
	{		
		FileContainer packResult = m_GamePack->ReadFile(filename);
		if (packResult.status == FileOk)return packResult;

		FileContainer fc;
		fc.status = FileOk;
		fc.data = NULL;
		fc.size = 0;

		FILE* file = fopen(filename.c_str(),mode.c_str());
		if (!file)
		{
			fc.status = FileNotFound;
			return fc;
		}
		fseek(file,0,SEEK_END);
		fc.size = ftell(file);
		fseek(file,0,SEEK_SET);
		char *buff = new char[fc.size];
		fread(buff,sizeof(char),fc.size,file);
		fclose(file);

		fc.data = buff;
		return fc;
	}

	//==============================================================================
	FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
	{
		std::string fileCorrect = g_EditionHelper->ConvertPath(filename);
		FILE* file = fopen(fileCorrect.c_str(),mode.c_str());
		return file;
	}

	//==============================================================================
	void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
	{
		FILE* file = fopen(filename.c_str(),"wb");
		if (!file)
		{
			return;
		}
		fwrite(data,size,1,file);
		fclose(file);
	}

	//==============================================================================
	bool CFileSystem::DeleteFile(const std::string &filename)
	{
		return ::DeleteFileA(filename.c_str());		
	}

	//==============================================================================
	std::string CFileSystem::GetSaveDir()
	{
		char data[256];
		SHGetSpecialFolderPathA(0,&data[0],CSIDL_APPDATA,false);				
		return std::string(data);
	}

	//==============================================================================	
	bool CFileSystem::IsFileExists(const std::string &filename)
	{
		std::string fileCorrect = g_EditionHelper->ConvertPath(filename);
		FILE* file = fopen(fileCorrect.c_str(),"r");
		if (!file)
		{
			return false;
		}
		fclose(file);
		return true;
	}

	//==============================================================================
	bool CFileSystem::IsDirExists(const std::string &filename)
	{		
		std::vector<std::string> dirContents;		
		dirContents = m_GamePack->GetDirectoryContents(filename);
		if (dirContents.size() > 0)return true;

		DWORD ftyp = GetFileAttributesA(filename.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!
		return false;    // this is not a directory!
	}

	//==============================================================================
	bool CFileSystem::CreateDirectory(const std::string &path)
	{
		int res = ::CreateDirectoryA(path.c_str(), 0);
		if (res == 0)
		{			
			return false;
		}
		return true;
	}

	//==============================================================================
	std::wstring CFileSystem::GetCurrentDirectory()
	{
		wchar_t str[255];
		::GetCurrentDirectoryW(255,&str[0]);		
		return std::wstring(str);
	}

	//==============================================================================
	bool CFileSystem::SetCurrentDirectory(std::wstring dir)
	{
		return ::SetCurrentDirectoryW(dir.c_str());
	}

	//==============================================================================
	std::wstring CFileSystem::GetProcessDirectory()
	{
		wchar_t str[255];
		GetModuleFileNameW(NULL,&str[0],255);
		return std::wstring(str);
	}

	//==============================================================================
	std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
	{
		std::vector<std::string> dirContents;		
		dirContents = m_GamePack->GetDirectoryContents(path);
		if (dirContents.size() > 0)return dirContents;

		WIN32_FIND_DATAA FindFileData;
		HANDLE hFind;
		std::wstring curDir = GetCurrentDirectory();
		if(!::SetCurrentDirectoryA(path.c_str()))
		{
			CONSOLE("Error: Not a directory - %s", path.c_str());
			return dirContents;
		}
		hFind = FindFirstFileA("*", &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE) 
		{
			///Invalid file handle
			SetCurrentDirectory(curDir);
			return dirContents;
		} 
		else 
		{
			while(FindNextFileA(hFind, &FindFileData))
			{
				std::string fileName = FindFileData.cFileName;
				if (fileName == "." || fileName == "..")continue;
				///File found
				dirContents.push_back(FindFileData.cFileName); 
			}
			FindClose(hFind);

			if (GetLastError() == ERROR_NO_MORE_FILES)
			{
				///Search finished
			}
			else
			{
				///Unknown error
				SetCurrentDirectory(curDir);
				return dirContents;
			}
		}
		SetCurrentDirectory(curDir);
		return dirContents;
	}
}
#endif
