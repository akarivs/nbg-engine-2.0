#ifndef NBG_CORE_FILESYSTEM_BASE
#define NBG_CORE_FILESYSTEM_BASE

#include <map>
#include <vector>
#include <Framework/Datatypes.h>

namespace NBG
{
	/** @brief Класс, реализующий работу с файловой системой
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class IFileSystem
	{
	public:
        /// @name Перечисления и структуры данных
		enum FileStatus
		{
			FileOk,
			FileNotFound,
			FileIsBusy,
		};
		///Контейнер хранящий файл и информацию о нем
		struct FileContainer
		{
			char* data; //данные файла
			int size; //размер файла в байтах
			FileStatus status; //ошибка, возникшая при чтении файла
		};

		virtual void Init(){};
        

		/// @name Методы для работы с директориями	
		///Создание директории
		virtual bool CreateDirectory(const std::string &path) = 0;
		///Получение текущей директории
		virtual std::wstring GetCurrentDirectory() = 0;
		///Установка текущей директории
		virtual bool SetCurrentDirectory(std::wstring dir) = 0;
		///Существует ли директория
		virtual bool IsDirExists(const std::string &filename) = 0;
		///Получение директории для сохранения
		virtual std::string GetSaveDir() = 0;
		///Получение директории процесса
		virtual std::wstring GetProcessDirectory() = 0;
		///Получение списка файлов в директории
		virtual std::vector<std::string>GetDirectoryContents(const std::string &path) = 0;

		/// @name Методы для работы с файлами
		///Чтение файла с диска
		virtual FileContainer ReadFile(const std::string &filename, const std::string &mode) = 0;
		virtual FILE * OpenFile(const std::string &filename, const std::string &mode) = 0;
		///Запись файла на диск
		virtual void SaveFile(const std::string &filename, const void * data, const int size) = 0;
		///Удалить файл
		virtual bool DeleteFile(const std::string &filename) = 0;
		///Существует ли файл
		virtual bool IsFileExists(const std::string &filename) = 0;
	};
}
#endif //NBG_CORE_FILESYSTEM_WIN
