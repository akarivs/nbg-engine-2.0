#ifdef NBG_ANDROID

#include "FileSystem.h"
#include <Framework.h>
#include <stdio.h>

#include <unistd.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>


namespace NBG
{
	CFileSystem::CFileSystem()
	{
	}

	CFileSystem::~CFileSystem()
	{
	}


	CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
	{
		FileContainer fc;

		JNIEnv * env = COperatingSystem::AndroidEnv;
		AAssetManager* mgr = COperatingSystem::AssetManager;
		AAsset* asset = AAssetManager_open(mgr, filename.c_str(), AASSET_MODE_UNKNOWN);
		if (NULL == asset) {
			fc.status = FileNotFound;
			return fc;
		}
		fc.size = AAsset_getLength(asset);
		fc.data = (char*) malloc (sizeof(char)*fc.size);		
		AAsset_read (asset,fc.data,fc.size);		
		AAsset_close(asset);		
		fc.status = FileOk;
		fc.status = FileOk;
		return fc;		
	}

	FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
	{
		FILE* file = fopen(filename.c_str(),mode.c_str());
		return file;
	}

	void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
	{
		FILE* file = fopen(filename.c_str(),"wb");
		if (!file)
		{
			return;
		}
		fwrite(data,size,1,file);
		fclose(file);
	}

	bool CFileSystem::DeleteFile(const std::string &filename)
	{
		return true;
	}

	std::string CFileSystem::GetSaveDir()
	{
		return std::string("");
	}

	///Существует ли файл
	bool CFileSystem::IsFileExists(const std::string &filename)
	{
		return true;
	}

	///Существует ли директория
	bool CFileSystem::IsDirExists(const std::string &filename)
	{
		return false;    // this is not a directory!
	}

	bool CFileSystem::CreateDirectory(const std::string &path)
	{
		return true;
	}

	std::wstring CFileSystem::GetCurrentDirectory()
	{
		return std::wstring(L"");
	}

	///Установка текущей директории
	bool CFileSystem::SetCurrentDirectory(std::wstring dir)
	{
		return true;
	}


	///Получение директории процесса
	std::wstring CFileSystem::GetProcessDirectory()
	{
		return std::wstring(L"");
	}

	std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
	{
		STRING_VECTOR vec;

		AAssetDir* assetDir = AAssetManager_openDir(COperatingSystem::AssetManager, path.c_str());
		const char* filename;
		while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
		{
			vec.push_back(filename);
			CONSOLE("%s",filename);
		}
		AAssetDir_close(assetDir);

		return vec;
	}
}
#endif
