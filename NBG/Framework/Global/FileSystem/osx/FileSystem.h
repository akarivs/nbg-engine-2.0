#ifndef NBG_CORE_FILESYSTEM_OSX
#define NBG_CORE_FILESYSTEM_OSX

#ifdef NBG_OSX

#include <map>
#include <vector>
#include <Framework/Datatypes.h>

#include "../BaseFileSystem.h"

namespace NBG
{
/** @brief Класс, реализующий работу с файловой системой Mac
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013 New Bridge Games
*
*/

class CFileSystem : public IFileSystem
{
public:
    CFileSystem();
    ~CFileSystem();

 
	/// @name Методы для работы с директориями	
	///Создание директории
    virtual bool CreateDirectory(const std::string &path);
	///Получение текущей директории
    virtual std::wstring GetCurrentDirectory();
	///Установка текущей директории
    virtual bool SetCurrentDirectory(std::wstring dir);
	///Существует ли директория
	virtual bool IsDirExists(const std::string &filename);
	///Получение директории для сохранения
	virtual std::string GetSaveDir();
	///Получение директории процесса
	virtual std::wstring GetProcessDirectory();
    ///Получение списка файлов в директории
    virtual std::vector<std::string>GetDirectoryContents(const std::string &path);
	
	/// @name Методы для работы с файлами
    ///Чтение файла с диска
    virtual FileContainer ReadFile(const std::string &filename, const std::string &mode) ;
    virtual FILE * OpenFile(const std::string &filename, const std::string &mode);
	///Запись файла на диск
	virtual void SaveFile(const std::string &filename, const void * data, const int size);
	///Существует ли файл
	virtual bool IsFileExists(const std::string &filename);    
    virtual bool DeleteFile(const std::string &filename){return true;}
private:
    std::string m_AppDirectory;
};
}
#endif
#endif //NBG_CORE_FILESYSTEM_OSX
