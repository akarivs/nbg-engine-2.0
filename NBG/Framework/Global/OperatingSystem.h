#ifndef NBG_CORE_OPERATING_SYSTEM
#define NBG_CORE_OPERATING_SYSTEM

#include "OperatingSystem/BaseOperatingSystem.h"
#ifdef NBG_WIN32
    #include "OperatingSystem/win32/OperatingSystem.h"
#elif defined (NBG_IOS)
    #include "OperatingSystem/ios/OperatingSystem.h"
#elif defined (NBG_OSX)
    #include "OperatingSystem/osx/OperatingSystem.h"
#elif defined (NBG_ANDROID)
    #include "OperatingSystem/android/OperatingSystem.h"
#elif defined (NBG_LINUX)
    #include "OperatingSystem/linux/OperatingSystem.h"
#endif

#endif //NBG_CORE_OPERATING_SYSTEM
