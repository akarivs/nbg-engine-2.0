#ifndef NBG_FRAMEWORK_GLOBAL_RENDER_IOS
#define NBG_FRAMEWORK_GLOBAL_RENDER_IOS

#ifdef NBG_IOS

#include <string>
#include <ostream>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
	/** @brief Класс, реализующий доступ к рендеру.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CRender
	{
	public:
        CRender();
		~CRender();

        void ShareContext();
        void UnShareContext();

		void SetMousePos(int x, int y);
		Vector & GetMousePos();

		void Init(void * layer);

		void CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture);
		void CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture);
		char** GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle);

		void ReleaseTexture(Texture * texture);
        
        bool CreateFrameBuffer();

		void SetBlendMode(BlendMode mode);

		void Reset();
		
		void Begin();
		void DrawMesh(Texture * texture, Mesh * mesh);
        void DrawLine(Vector ul, Vector dr, const float width, Color color);
		void DrawBatch();
		void End();

		Vector GetMatrixOffset()
		{
			return m_MatrixOffset;		
		};
        static const int MAX_OBJECTS=1000;
        
        ///STENCIL
		void StencilEnable();
		void StencilDisable();
		void StencilClear(char bt = 0);
		void StencilLevelSet(char bt);
		void StencilSetCmpFunc(CompareType type);
		void StencilSetPasOpFunc(StencilOperation op);
		void StencilSetFailOpFunc(StencilOperation op);
        
        
        int GetDrawCalls(){return m_DrawCalls;}
		int GetPolyCount(){return m_PolyCount;}
	private:
		
		Vector m_MatrixOffset;
		Mesh m_LineMesh;
		Vector m_lineVector;
        
        char stencilLevel;
        StencilOperation stencilFail;
        StencilOperation stencilPass;
		
		
		
		//pointer to vertex data (NEW)
		char* m_VertexDataPointer;
		unsigned long* m_IndexDataPointer;
	
		///Служебные переменные
		int vs;
		int vc;

		///Состояния рендеринга батча
		enum RenderActions
		{
			RA_None = -1,
			RA_Batch = 0,
			RA_Line = 1,
			RA_3D
		};

		BlendMode m_BlendMode;

		///Сменить состояние рендера
		virtual void ChangeState(RenderActions renderAction);

		///Сбросить текущие батч на рендер
		virtual void FlushVertexBuffer(bool needToLockBufferAgain = true);
		///Предыдущее состояние рендера
		int m_PrevAction;
		///Смещение по Y для центра экрана
		float m_MiddleY;
		///Текущий объект для отрисовки
		int m_CurrentObject; 
		///Текущий объект для отрисовки
		int m_CurrentDrawPoly; 
		///Смещение вершин в батче
		int m_VertexOffset;
		///Смещение индексов в батче
		int m_IndexOffset;
		///Количество Draw Calls
		int m_DrawCalls;
		///Количество отрисованных полигонов за предыдущий рендер
		int m_PolyCount;        
        
        char * m_vboBuffer;
        unsigned short * m_IndexBuffer;
        int m_IndexCount;
        
        Matrix m_ProjectionM;
        
        
        void convertX(float &x);
        void convertY(float &y);

	};
}
#endif
#endif // NBG_FRAMEWORK_GLOBAL_RENDER_IOS
