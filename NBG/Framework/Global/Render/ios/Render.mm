#ifdef NBG_IOS
#include "Render.h"


#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import <OpenGLES/EAGLDrawable.h>
#include <OpenGLES/ES2/gl.h>

#import <GLKit/GLKit.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include <Framework.h>


namespace NBG
{
    namespace IOSRender
    {
        GLuint vertexBuffer;
        GLuint indexBuffer;
        Vertex vertexes[CRender::MAX_OBJECTS*4];
        GLushort indices[CRender::MAX_OBJECTS*6];
        GLuint prevTextureId;
        char * vboBuffer;
        GLuint viewRenderbuffer, viewFramebuffer;
        GLuint depthRenderbuffer;
        EAGLContext * context;
        Matrix projectionM;
        
        GLuint _positionSlot;
        GLuint  _colorSlot;
        GLuint  _coordSlot;
        GLuint  _samplerSlot;
        GLuint  _projectionSlot;
        
        typedef struct {
            float Position[3];
            float Color[4];
            float Texture[2];
        } Vertex;
        
        bool CreateFrameBuffer(CALayer * layer)
        {
            glGenFramebuffers(1, &viewFramebuffer);
            glGenRenderbuffers(1, &viewRenderbuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
            [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)layer];
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, viewRenderbuffer);
            
            
            unsigned int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            projectionM.Identity();
            projectionM.Scale(2.0f/(float)g_System->GetSystemResolution().x,
                                2.0f/(float)g_System->GetSystemResolution().y);
            
            projectionM.m[3] = 0;
            projectionM.m[7] = -2;
            projectionM.m[10] = -1;
            
            NBG::Matrix matrix;
            matrix.Identity();
            matrix.Scale(g_System->GetResolutionScale().x,g_System->GetResolutionScale().y);
            projectionM.Multiply(&matrix);
            
            glViewport(0, 0, g_System->GetSystemResolution().x, g_System->GetSystemResolution().y);


            
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
                NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
                return NO;
            }
            return YES;
        }
        
        GLuint compileShader(NSString* shaderName,GLenum shaderType)
        {
            std::string pathShader = "shaders/";
            pathShader += shaderName.cString;
            pathShader += ".glsl";
            NSString * shaderPath = [NSString stringWithCString:pathShader.c_str()
                                                       encoding:[NSString defaultCStringEncoding]];;
            
            NSError* error;
            NSString* shaderString = [NSString stringWithContentsOfFile:shaderPath
                                                               encoding:NSUTF8StringEncoding error:&error];
            if (!shaderString) {
                NSLog(@"Error loading shader: %@", error.localizedDescription);
                exit(1);
            }
            
            // 2
            GLuint shaderHandle = glCreateShader(shaderType);
            
            // 3
            const char * shaderStringUTF8 = [shaderString UTF8String];
            int shaderStringLength = [shaderString length];
            glShaderSource(shaderHandle, 1, &shaderStringUTF8, &shaderStringLength);
            
            // 4
            glCompileShader(shaderHandle);
            
            // 5
            GLint compileSuccess;
            glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
            if (compileSuccess == GL_FALSE) {
                GLchar messages[256];
                glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
                NSString *messageString = [NSString stringWithUTF8String:messages];
                NSLog(@"%@", messageString);
                exit(1);
            }
            
            return shaderHandle;
        }
        
        void compileShaders()
        {
            // 1
            GLuint vertexShader = compileShader(@"SimpleVertex",GL_VERTEX_SHADER);
            GLuint fragmentShader = compileShader(@"SimpleFragment",GL_FRAGMENT_SHADER);
            
            
            // 2
            GLuint programHandle = glCreateProgram();
            glAttachShader(programHandle, vertexShader);
            glAttachShader(programHandle, fragmentShader);
            glLinkProgram(programHandle);
            
            // 3
            GLint linkSuccess;
            glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
            if (linkSuccess == GL_FALSE) {
                GLchar messages[256];
                glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
                NSString *messageString = [NSString stringWithUTF8String:messages];
                NSLog(@"%@", messageString);
                exit(1);
            }
            
            // 4
            glUseProgram(programHandle);
            
            // 5
            _positionSlot = glGetAttribLocation(programHandle, "Position");
            _colorSlot = glGetAttribLocation(programHandle, "SourceColor");
            _coordSlot = glGetAttribLocation(programHandle, "SourceUV");
            _samplerSlot = glGetAttribLocation(programHandle, "s_texture");
            _projectionSlot = glGetUniformLocation(programHandle, "projectionMatrix");
            
            
            GLuint centerOffset = glGetUniformLocation(programHandle,"centerOffset");
            
            glEnableVertexAttribArray(_positionSlot);
            glEnableVertexAttribArray(_colorSlot);
            glEnableVertexAttribArray(_coordSlot);
            
            
            
            //Set the offset
            glUniform2f(centerOffset,0,(float)g_System->GetSystemResolution().y*g_System->GetResolutionScale().y/2.0f);
            
            // Set the projection
            glUniformMatrix4fv(_projectionSlot,1,0,&projectionM.m[0]);
            
            
            // 2
            glVertexAttribPointer(_positionSlot, 3, GL_FLOAT, GL_FALSE,
                                  sizeof(Vertex), 0);
            glVertexAttribPointer(_colorSlot, 4, GL_FLOAT, GL_FALSE,
                                  sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
            glVertexAttribPointer(_coordSlot, 2, GL_FLOAT, GL_FALSE,
                                  sizeof(Vertex), (GLvoid*) (sizeof(float) * 7));
            
            glEnableVertexAttribArray(_positionSlot);
            glEnableVertexAttribArray(_colorSlot);
            glEnableVertexAttribArray(_coordSlot);
            
        }


    }
    
	//==============================================================================
	CRender::CRender()
	{
		m_PrevAction = RA_None;
		m_BlendMode = BLENDMODE_BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
        
        m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;
	}

	CRender::~CRender()
	{

	}
    
    void CRender::ShareContext()
    {
       /* EAGLContext *aContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:IOSRender::context.sharegroup];
        
        if([EAGLContext setCurrentContext:(EAGLContext*)aContext] == NO) {
            NSLog(@"Failed to share context!");
            return;
        }*/

    }
    void CRender::UnShareContext()
    {
        
    }

	void CRender::Init(void * layer)
	{
        CALayer * _layer = (__bridge CALayer*)layer;
        
       
        IOSRender::context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        if([EAGLContext setCurrentContext:(EAGLContext*)IOSRender::context] == NO) {
            NSLog(@"Failed to set current context!");
            return;
        }
        bool res = IOSRender::CreateFrameBuffer(_layer);
        if (!res)
        {
            NSLog(@"Could'nt create frame buffer!");
        }
        
        for (int i=0, j=0; i<MAX_OBJECTS; i+=6,j+=4)
        {
            IOSRender::indices[i] = j;
            IOSRender::indices[i+1] = j+1;
            IOSRender::indices[i+2] = j+2;
            IOSRender::indices[i+3] = j+3;
            IOSRender::indices[i+4] = j+2;
            IOSRender::indices[i+5] = j+1;
        }
        
        glGenBuffers(1, &IOSRender::vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, IOSRender::vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(IOSRender::vertexes), IOSRender::vertexes, GL_STREAM_DRAW);
        
        
        glGenBuffers(1, &IOSRender::indexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IOSRender::indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IOSRender::indices), IOSRender::indices, GL_STREAM_DRAW);
        
        IOSRender::compileShaders();
		Reset();
	}
    
    bool CRender::CreateFrameBuffer()
    {
        return true;
    }

	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		Vector resolution = g_System->GetSystemResolution();

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
	}



	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x,size.y,0, GL_RGB, GL_UNSIGNED_BYTE,data);
            
            texture->SetTextureData(new GLuint(text));
            return;
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x,size.y,GL_RGB, GL_UNSIGNED_BYTE,data);
        
        IOSRender::prevTextureId = -1;
	}

    void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        unsigned char * textureData = NULL;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            textureData = new unsigned char[(int)(size.x/2.0f*size.y*4)];
            memset(textureData,0,size.x/2.0f*size.y*4);
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x/2.0f,size.y,0, GL_RGBA, GL_UNSIGNED_BYTE,textureData);
            
            texture->SetTextureData(new GLuint(text));
            texture->SetUserData(textureData);
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
            textureData = (unsigned char*)texture->GetUserData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        
        unsigned char*  src     = data;
        unsigned char*  dst     = textureData;
        
        int i,j,k,x,y,w=size.x;
        int yPremult = 0;
        int sizeXDiv2 = size.x/2.0f;
        for (y=0;y<size.y-2;y++)
        {
            yPremult = (y+2)*w;
            for (x=0;x<sizeXDiv2;x++)
            {
                i=(y*sizeXDiv2+x)*4;
                j=(yPremult+x+4)*3;
                k=(yPremult+x+(sizeXDiv2))*3;
                memcpy(&dst[i],&src[j],3);
                dst[i+3]=src[k];
            }
        }
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x/2.0f,size.y,GL_RGBA, GL_UNSIGNED_BYTE,dst);
        
        //IOSRender::prevTextureId = -1;
        
        
	}

	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		char ** ret = NULL;
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1 ;

		ret = new char*[w];
		for (int i=0; i<w; i++)
		{
			ret[i] = new char[h];
			for (int f=0; f<h; f++)
			{
				ret[i][f] = 0;
			}
		}

		w =  texture->GetSize().x;
		h =  texture->GetSize().y;

		return ret;
	}


	void CRender::ReleaseTexture(Texture * texture)
	{
        texture->SetTextureData(NULL);
	}


	void CRender::Begin()
	{
        if (!IOSRender::context)return;
        
        [EAGLContext setCurrentContext:IOSRender::context];
        glBindFramebuffer(GL_FRAMEBUFFER, IOSRender::viewFramebuffer);
        glViewport(0, 0, g_System->GetSystemResolution().x, g_System->GetSystemResolution().y);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        
        m_CurrentObject = 0;
        IOSRender::prevTextureId = 0;
        m_DrawCalls = 0;
        m_PolyCount = 0;
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(IOSRender::vertexes), NULL, GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IOSRender::indices), NULL, GL_STREAM_DRAW);
        m_vboBuffer = (char*)glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
        m_IndexBuffer = (unsigned short*)glMapBufferOES(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
        
        m_VertexOffset = 0;
        m_IndexOffset = 0;
        m_IndexCount = 0;
	}

	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{
        if (m_CurrentObject == 0)return;
        glUnmapBufferOES(GL_ARRAY_BUFFER);
        glUnmapBufferOES(GL_ELEMENT_ARRAY_BUFFER);
        
        
        m_DrawCalls++;
        glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_SHORT, NULL);
        m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
        m_IndexCount = 0;
        
        if (needToLockBufferAgain)
        {
            glBufferData(GL_ARRAY_BUFFER, sizeof(IOSRender::vertexes), NULL, GL_STREAM_DRAW);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IOSRender::indices), NULL, GL_STREAM_DRAW);
            m_vboBuffer = (char*)glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
            m_IndexBuffer = (unsigned short*)glMapBufferOES(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
        }
	}

    IOSRender::Vertex Vertices[4];
	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
        GLuint textureId = -1;
        if (texture && texture->GetTextureData())
        {
            textureId = *(int*)texture->GetTextureData();
        }
        
        
		if (IOSRender::prevTextureId != 0)
		{
            
			if(IOSRender::prevTextureId != textureId)
			{
				FlushVertexBuffer();
				glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D,textureId);
                glUniform1i(IOSRender::_samplerSlot, 0);
                
			}
		}
		else
		{
            if (IOSRender::prevTextureId != textureId)
			{
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D,textureId);
                glUniform1i(IOSRender::_samplerSlot, 0);
            }
            
		}
		IOSRender::prevTextureId = textureId;
        
        vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount()/3.0f;
		m_PolyCount += vc;
		memcpy(&m_vboBuffer[m_VertexOffset],mesh->GetVertexes(),vs);
        m_VertexOffset      +=vs;
		m_CurrentObject     +=vc/2;
        
        int is = mesh->GetIndexesSize();
		int ic = mesh->GetIndexesCount();
        
		memcpy(&m_IndexBuffer[m_IndexCount],mesh->GetIndexes(),is);
        
		for (int i=0; i<ic; i++)
		{
			m_IndexBuffer[m_IndexCount+i] += m_IndexOffset;
		}
        m_IndexCount += ic;
		//m_IndexBuffer += ic;
		m_IndexOffset += mesh->GetVertexCount();
	}
    
    void CRender::DrawLine(Vector ul, Vector dr, const float width, Color color)
	{
		ChangeState(RA_Batch);
		ul.y = m_MatrixOffset.y - ul.y;
		dr.y = m_MatrixOffset.y - dr.y;
		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if(DirX == 0)
		{
			if(DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if(DirY == 0)
		{
			if(DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if(DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;
        
        
		Vertex * vertexes = m_LineMesh.GetVertexes();
        
		vertexes[0].x = ul.x + m_lineVector.x;
		vertexes[0].y = ul.y + m_lineVector.y;
		vertexes[1].x = ul.x - m_lineVector.x;
		vertexes[1].y = ul.y - m_lineVector.y;
		vertexes[2].x = dr.x + m_lineVector.x;
		vertexes[2].y = dr.y + m_lineVector.y;
		vertexes[3].x = dr.x - m_lineVector.x;
		vertexes[3].y = dr.y - m_lineVector.y;
        
		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 1; vertexes[2].v = 1;
		vertexes[3].u = 0; vertexes[3].v = 1;
        
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0; indexes[1] = 1; indexes[2] = 2;
		indexes[3] = 3; indexes[4] = 2; indexes[5] = 1;
        
        
		unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color =clr;
		DrawMesh(NULL, &m_LineMesh);
	}


	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
						break;
		case RA_Batch:
						break;
		case RA_3D:			
				
			break;
		}
	}

	void CRender::DrawBatch()
	{
		FlushVertexBuffer(false);
    }

	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BLENDMODE_BLEND)
		{
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (mode == BLENDMODE_ADD)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		}
		else if (mode == BLENDMODE_MOD)
		{
            glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_ONE);
		}
	}

	void CRender::End()
	{
        DrawBatch();
        glBindRenderbuffer(GL_RENDERBUFFER, IOSRender::viewRenderbuffer);
        [IOSRender::context presentRenderbuffer:GL_RENDERBUFFER];
	}
    
    /* TODO: implement stencil */
    void CRender::StencilEnable()
    {
        glEnable(GL_STENCIL_TEST);
        stencilFail = (StencilOperation)-1;
        stencilPass = (StencilOperation)-1;
        
    }
    void CRender::StencilDisable()
    {
        glDisable(GL_STENCIL_TEST);
    }
    void CRender::StencilClear(char bt)
    {
        glClear(GL_STENCIL_BUFFER_BIT);
        
    }
    void CRender::StencilLevelSet(char bt)
    {
        stencilLevel = bt;
    }
    void CRender::StencilSetCmpFunc(CompareType type)
    {
        glStencilFunc(type, (int)stencilLevel,0);
    }
    void CRender::StencilSetPasOpFunc(StencilOperation op)
    {
        stencilPass = op;
        if (stencilFail != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
        
    }
    void CRender::StencilSetFailOpFunc(StencilOperation op)
    {
        stencilFail = op;
        if (stencilPass != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
    }
}
#endif