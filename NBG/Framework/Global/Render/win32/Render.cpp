#ifdef NBG_WIN32
#include "Render.h"

#include <Framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CRender::CRender()
	{
		m_PrevAction = RA_None;
		m_BlendMode = BLENDMODE_BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
		m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;

		m_VertexBuffer = NULL;
		m_IndexBuffer = NULL;
		m_IsTextureRepeat = false;
		m_Device = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	CRender::~CRender()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Init()
	{		
		m_D3D = Direct3DCreate9(D3D_SDK_VERSION);
		if( m_D3D == NULL )
		{
			CONSOLE("Could not create Direct3D Object");
			SDL_assert(0);
			return;
		}
		m_PrevTexture = NULL;
		m_Device = NULL;
		Reset();
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		if (m_VertexBuffer)
		{
			m_VertexBuffer->Release();
			m_VertexBuffer = 0;
		}

		if (m_IndexBuffer)
		{
			m_IndexBuffer->Release();
			m_IndexBuffer = 0;
		}
		Vector resolution = g_System->GetSystemResolution();

		D3DDISPLAYMODE d3ddm;
		m_D3D->GetAdapterDisplayMode (D3DADAPTER_DEFAULT, &d3ddm);


		D3DPRESENT_PARAMETERS   present_parameters;
		ZeroMemory(&present_parameters, sizeof(present_parameters));
		present_parameters.Windowed = !g_GameApplication->IsFullscreen();
		if (present_parameters.Windowed)
		{
			present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
			present_parameters.SwapEffect				= D3DSWAPEFFECT_DISCARD;			
		}
		else
		{
			//present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_ONE;
			//present_parameters.SwapEffect				= D3DSWAPEFFECT_COPY;			
			present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
			present_parameters.SwapEffect				= D3DSWAPEFFECT_DISCARD;
		}

		present_parameters.EnableAutoDepthStencil = true;
		present_parameters.AutoDepthStencilFormat = D3DFMT_D24S8;

		COperatingSystem * op = CAST(COperatingSystem*,g_GameApplication->GetOperatingSystem());


		present_parameters.hDeviceWindow = op->GetWindow();
		present_parameters.BackBufferWidth = resolution.x;
		present_parameters.BackBufferHeight = resolution.y;
		present_parameters.BackBufferFormat = d3ddm.Format;
		present_parameters.MultiSampleType = D3DMULTISAMPLE_NONE;

		if (m_Device)
		{
			m_Device->Reset(&present_parameters);
		}
		else
		{
			if ( FAILED(m_D3D->CreateDevice (D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, GetActiveWindow(), D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED, &present_parameters, &m_Device)))
			{
				return;
			}
		}

		m_Device->GetDeviceCaps(&m_Caps);


		HRESULT hr = m_Device->CreateVertexBuffer(MAX_OBJECTS*sizeof(Vertex)*4,
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
			D3DFVF_CUSTOMVERTEX,
			D3DPOOL_DEFAULT,
			&m_VertexBuffer,
			NULL);
		hr = m_Device->CreateIndexBuffer(MAX_OBJECTS*sizeof(WORD)*6,D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,D3DFMT_INDEX16,D3DPOOL_DEFAULT,&m_IndexBuffer,NULL);		

		m_MatrixView		= new D3DXMATRIX();
		m_MatrixProjection	= new D3DXMATRIX();
		m_MatrixWorld		= new D3DXMATRIX();
		D3DXMatrixIdentity(m_MatrixView);
		D3DXMatrixIdentity(m_MatrixWorld);		

		Vector gameRes = g_System->GetGameResolution();
		Vector resScale = g_System->GetResolutionScale();
		D3DXMatrixOrthoLH(m_MatrixProjection, resolution.x,resolution.y, 0.0f, 1.0f);
		D3DXMatrixTranslation(m_MatrixWorld, 0.0f, -(resolution.y)/resScale.y, 0.0f);
		m_MatrixOffset.y = (gameRes.y)/resScale.y;
		D3DXMatrixScaling(m_MatrixView,resScale.x,resScale.y,1.0f);

		m_Device->SetTransform(D3DTS_VIEW, m_MatrixView);
		m_Device->SetTransform(D3DTS_PROJECTION, m_MatrixProjection);
		m_Device->SetTransform(D3DTS_WORLD, m_MatrixWorld);

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CheckDevice()
	{		
		if( m_Device == NULL)return;
		HRESULT hr = m_Device->TestCooperativeLevel();
		if(hr == D3DERR_DEVICELOST)
		{			
			m_VertexBuffer->Release();
			m_IndexBuffer->Release();
			Sleep(100);
			return;
		}
		else if(hr == D3DERR_DEVICENOTRESET)
		{		
			Reset();			
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
		IDirect3DTexture9* pTex = NULL;
		HRESULT hr   = S_OK;
		if (texture->GetTextureData() == NULL)
		{			
			hr = m_Device->CreateTexture(
				size.x,
				size.y,
				1,
				0,			
				D3DFMT_A8R8G8B8,
				D3DPOOL_MANAGED,
				&pTex,
				NULL);
		}
		else
		{
			pTex = CAST(IDirect3DTexture9*,texture->GetTextureData());
		}

		D3DLOCKED_RECT lockedRect;
		hr = pTex->LockRect(0, &lockedRect, NULL, 0);

		if(SUCCEEDED(hr))
		{	
			unsigned char*  src     = data;
			char*  dst     = (char*) lockedRect.pBits;
			int count = size.x * size.y * bpp;

			do
			{
				dst[0] = src[2];
				dst[1] = src[1];
				dst[2] = src[0];
				if (bpp == 4)
					dst[3] = src[3];
				else
					dst[3] = 255;

				dst+=4;
				src+=bpp;
			}while(count-=bpp);			
			hr = pTex->UnlockRect(0);
		}
		texture->SetTextureData(pTex);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
		IDirect3DTexture9* pTex = NULL;
		HRESULT hr   = S_OK;
		if (texture->GetTextureData() == NULL)
		{			
			hr = m_Device->CreateTexture(
				size.x/2.0f,
				size.y,
				1,
				0,			
				D3DFMT_A8R8G8B8,
				D3DPOOL_MANAGED,
				&pTex,
				NULL);
		}
		else
		{
			pTex = CAST(IDirect3DTexture9*,texture->GetTextureData());
		}

		D3DLOCKED_RECT lockedRect;
		hr = pTex->LockRect(0, &lockedRect, NULL, 0);

		if(SUCCEEDED(hr))
		{	
			unsigned char*  src     = data;
			char*  dst     = (char*) lockedRect.pBits;
			int count = size.x * size.y * bpp;

			int i,j,k,x,y,w=size.x;
			int yPremult = 0;
			int sizeXDiv2 = size.x/2.0f;
			for (y=0;y<size.y;y++)
			{
				yPremult = (y)*w;
				for (x=0;x<sizeXDiv2;x++)
				{
					i=(y*sizeXDiv2+x)*4;
					j=(yPremult+x)*3;
					k=(yPremult+x+(sizeXDiv2))*3;
					dst[i  ]=src[j+2];
					dst[i+1]=src[j+1];
					dst[i+2]=src[j];
					dst[i+3]=src[k];
				}
			}
			hr = pTex->UnlockRect(0);
		}
		texture->SetTextureData(pTex);
	}

	//////////////////////////////////////////////////////////////////////////
	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		char ** ret = NULL;
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1 ;

		ret = new char*[w];
		for (int i=0; i<w; i++)
		{
			ret[i] = new char[h];
			for (int f=0; f<h; f++)
			{
				ret[i][f] = 0;
			}
		}

		w =  texture->GetSize().x;
		h =  texture->GetSize().y;

		BYTE* PixelMap = NULL;
		RECT r;
		r.left = 0;
		r.top = 0;
		r.right = w;
		r.bottom = h;

		HRESULT hRes;
		IDirect3DTexture9 * directTexture = CAST(IDirect3DTexture9 *,texture->GetTextureData());

		D3DLOCKED_RECT RectLock;        		
		directTexture->LockRect(0,&RectLock,&r,D3DLOCK_DISCARD);

		PixelMap = (BYTE *)RectLock.pBits;

		char * pixel = NULL;

		int pitch = w*4;
		int bpp	= 4;

		int x1 = rectangle.left;
		int x2 = rectangle.right;
		int y1 = rectangle.top;
		int y2 = rectangle.bottom;
		int atlasPitch = w*h*4;
		int p = 0;

		int key = 0;
		int pitchKey = 0;
		int pitchMultiply = 0;
		for (int y=y1; y<y2; y++)
		{
			pitchMultiply = pitch*(y);
			for (int x=x1; x<x2; x++)
			{
				if (x*4+(pitchMultiply) < atlasPitch)
				{
					key = pitch * (y-y1) + 4 * (x-x1);
					pitchKey = x*4+(pitchMultiply);					
					if (PixelMap[pitchKey+3] > 0)
					{
						ret[x-(int)rectangle.left][y-(int)rectangle.top] = 1;
					}
					else
					{
						ret[x-(int)rectangle.left][y-(int)rectangle.top] = 0;
					}
				}
			}
		}
		directTexture->UnlockRect(0);
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::ReleaseTexture(Texture * texture)
	{
		IDirect3DTexture9* _texture = (IDirect3DTexture9*)texture->GetTextureData();
		if (_texture)
		{
			_texture->Release();
		}
		texture->SetTextureData(NULL);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Begin()
	{
		m_Device->Clear (0, NULL, D3DCLEAR_TARGET | D3DCLEAR_STENCIL | D3DCLEAR_ZBUFFER, 0xFF000000, 0.0f, 0);
		m_Device->BeginScene ();
		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_PolyCount = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;

		m_VertexBuffer->Lock(0,sizeof(m_VertexDataPointer),(void**)&m_VertexDataPointer,D3DLOCK_DISCARD);
		m_IndexBuffer->Lock(0,0,(void**)&m_IndexDataPointer,D3DLOCK_DISCARD);
		m_DrawCalls = 0;		
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{	
		if (m_CurrentDrawPoly == 0)
		{
			if (needToLockBufferAgain == false)
			{
				m_VertexBuffer->Unlock();	
				m_IndexBuffer->Unlock();
			}
			return;				
		}
		m_DrawCalls++;		
		ChangeState(RA_Batch);

		m_VertexBuffer->Unlock();	
		m_IndexBuffer->Unlock();
		m_Device->SetStreamSource(0,m_VertexBuffer,0,sizeof(D3DVERTEX));		

		/*Indexed*/		
		m_Device->SetIndices(m_IndexBuffer);		
		m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,0,0,m_CurrentDrawPoly*2,0,m_CurrentDrawPoly);		

		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		if (needToLockBufferAgain)
		{
			m_VertexBuffer->Lock(0,sizeof(m_VertexDataPointer),(void**)&m_VertexDataPointer,D3DLOCK_DISCARD);
			m_IndexBuffer->Lock(0,0,(void**)&m_IndexDataPointer,D3DLOCK_DISCARD);
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{		
		IDirect3DTexture9* _texture = NULL;
		HRESULT hr;
		if (texture)
		{
			_texture = (IDirect3DTexture9*)texture->GetTextureData();
		}

		if (m_CurrentObject == 0)
		{
			if (_texture)
			{
				hr = m_Device->SetTexture(0,_texture);
			}
		}
		if (m_PrevTexture != NULL)
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0,_texture);
			}
		}
		else
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0,_texture);
			}
		}
		m_PrevTexture = _texture;

		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount()/3;
		m_PolyCount += vc;
		memcpy((void*)(m_VertexDataPointer+(m_VertexOffset)),mesh->GetVertexes(),vs);
		m_CurrentObject     +=vc/2;	

		if (m_CurrentObject >= MAX_OBJECTS)
		{
			FlushVertexBuffer();
			m_PolyCount += vc;
			m_CurrentObject     +=vc/2;				
		}

		m_CurrentDrawPoly   +=vc;
		m_VertexOffset      +=vs;

		int is = mesh->GetIndexesSize();		
		int ic = mesh->GetIndexesCount();		
		memcpy(m_IndexDataPointer,mesh->GetIndexes(),is);		
		for (int i=0; i<ic; i++)
		{
			m_IndexDataPointer[i] += m_IndexOffset;			
		}		
		m_IndexDataPointer += ic;
		m_IndexOffset += mesh->GetVertexCount();
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawLine(Vector ul, Vector dr, const float width, Color &color)
	{		
		ChangeState(RA_Batch);
		ul.y = m_MatrixOffset.y - ul.y;
		dr.y = m_MatrixOffset.y - dr.y;
		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if(DirX == 0)
		{
			if(DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if(DirY == 0)
		{
			if(DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if(DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;


		Vertex * vertexes = m_LineMesh.GetVertexes(); 		

		vertexes[0].x = ul.x + m_lineVector.x;
		vertexes[0].y = ul.y + m_lineVector.y;
		vertexes[1].x = ul.x - m_lineVector.x;
		vertexes[1].y = ul.y - m_lineVector.y;
		vertexes[2].x = dr.x + m_lineVector.x;
		vertexes[2].y = dr.y + m_lineVector.y;
		vertexes[3].x = dr.x - m_lineVector.x;
		vertexes[3].y = dr.y - m_lineVector.y;			

		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 1; vertexes[2].v = 1;
		vertexes[3].u = 0; vertexes[3].v = 1;

		unsigned short * indexes = m_LineMesh.GetIndexes(); 		
		indexes[0] = 0; indexes[1] = 1; indexes[2] = 2;
		indexes[3] = 3; indexes[4] = 2; indexes[5] = 1;		


		unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color =clr;			
		DrawMesh(NULL, &m_LineMesh);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
			m_Device->SetLight(0, m_LineLight);
			m_Device->LightEnable(0, true);
			m_Device->SetTexture(0,0);

			m_Device->SetFVF(D3DFVF_XYZ); //set vertex format (NEW)

			m_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
			m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			m_Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);


			m_Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC);
			break;
		case RA_Batch:
			m_Device->SetTransform(D3DTS_VIEW, m_MatrixView);
			m_Device->SetTransform(D3DTS_PROJECTION, m_MatrixProjection);
			m_Device->SetTransform(D3DTS_WORLD, m_MatrixWorld);

			m_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
			m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
			m_Device->SetRenderState(D3DRS_ALPHAREF, 0x00);
			m_Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_Device->SetRenderState(D3DRS_CLIPPING, true);
			m_Device->SetRenderState(D3DRS_CLIPPLANEENABLE, false);
			m_Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_ALPHA | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED);
			m_Device->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
			m_Device->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, false);			
			m_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
			m_Device->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, true);
			m_Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
			m_Device->SetRenderState(D3DRS_WRAP0, 0);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

			m_Device->SetVertexShader(NULL);
			m_Device->SetFVF(D3DFVF_CUSTOMVERTEX); //set vertex format (NEW)
			m_Device->LightEnable(0, false);

			m_Device->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
			m_Device->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
			m_Device->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );

			m_Device->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );
			m_Device->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
			m_Device->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

			m_Device->SetTextureStageState( 1, D3DTSS_ALPHAOP, D3DTOP_DISABLE );
			m_Device->SetTextureStageState( 1, D3DTSS_COLOROP, D3DTOP_DISABLE);


			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSV,D3DTADDRESS_CLAMP);

			m_Device->SetSamplerState(0,D3DSAMP_MAXMIPLEVEL,0);
			m_Device->SetSamplerState(0,D3DSAMP_MAXANISOTROPY,m_Caps.MaxAnisotropy);
			m_Device->SetSamplerState(0,D3DSAMP_MINFILTER,D3DTEXF_LINEAR);
			m_Device->SetSamplerState(0,D3DSAMP_MIPFILTER,D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0,D3DSAMP_MAGFILTER,D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0,D3DSAMP_MIPMAPLODBIAS,0);
			m_Device->SetSamplerState(0,D3DSAMP_SRGBTEXTURE,0);		
			break;
		case RA_3D:			
			m_LineMaterial->Diffuse.a = 1.0f;
			m_LineMaterial->Diffuse.r = m_LineMaterial->Diffuse.g = m_LineMaterial->Diffuse.b = 1.0f;

			m_LineMaterial->Ambient.r = 1.0f;
			m_LineMaterial->Ambient.g = 1.0f;
			m_LineMaterial->Ambient.b = 1.0f;
			m_Device->SetMaterial(m_LineMaterial);
			m_Device->SetFVF(D3DFVF_CUSTOMVERTEX); //set vertex format (NEW)			
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawBatch()
	{
		FlushVertexBuffer(true);
		m_PrevTexture = NULL;
		_texture = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BLENDMODE_BLEND)
		{
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		}
		else if (mode == BLENDMODE_ADD)
		{
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);	
		}
		else if (mode == BLENDMODE_MOD)
		{
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_INVDESTCOLOR);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::SetTextureRepeat(const bool isRepeat)
	{
		if (isRepeat == m_IsTextureRepeat)return;
		m_IsTextureRepeat = isRepeat;
		FlushVertexBuffer();
		if (isRepeat)
		{
			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSU,D3DTADDRESS_WRAP);
			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSV,D3DTADDRESS_WRAP);			
		}
		else
		{
			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
			m_Device->SetSamplerState(0,D3DSAMP_ADDRESSV,D3DTADDRESS_CLAMP);			
		}	
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::End()
	{
		FlushVertexBuffer(false);
		m_PrevTexture = NULL;
		_texture = NULL;
		m_Device->EndScene ();
		m_Device->Present (NULL, NULL, NULL, NULL);		
	}
}
#endif