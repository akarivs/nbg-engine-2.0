#ifndef NBG_FRAMEWORK_GLOBAL_RENDER_WIN32
#define NBG_FRAMEWORK_GLOBAL_RENDER_WIN32

#ifdef NBG_WIN32
#include <Framework/Datatypes/Color.h>
#include <d3d9.h>
#include <d3dx9.h>
//constants
//x, y, z and color
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
//structures
struct D3DVERTEX
{
	float fX;
	float fY;
	float fZ;
	NBG::Color clr;
	float fU; //texture coordinates
	float fV;
};


#include <string>
#include <ostream>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Vector.h>


namespace NBG
{
	/** @brief Класс, реализующий доступ к рендеру.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CRender
	{
	public:    
		CRender();
		~CRender();

		void SetMousePos(int x, int y);
		Vector & GetMousePos();

		void ShareContext(){};
		void UnShareContext(){};

		void Init();		

		void CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture);
		void CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture);
		char** GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle);

		void ReleaseTexture(Texture * texture);

		void SetBlendMode(BlendMode mode);
		BlendMode GetBlendMode(){ return m_BlendMode; };
		void CheckDevice();
		void Reset();		
		
		void Begin();
		void DrawMesh(Texture * texture, Mesh * mesh);
		void DrawLine(Vector ul, Vector dr, const float width, Color &color);
		void DrawBatch();
		void End();

		Vector GetMatrixOffset()
		{
			return m_MatrixOffset;		
		};

		LPDIRECT3DDEVICE9 GetDevice()
		{
			return m_Device;
		}

		///STENCIL
		void StencilEnable()							{m_Device->SetRenderState(D3DRS_STENCILENABLE, true);}
		void StencilDisable()							{m_Device->SetRenderState(D3DRS_STENCILENABLE, false);}
		void StencilClear(char bt = 0)					{m_Device->Clear(0, NULL, D3DCLEAR_STENCIL, 0xffffffff,1.0f,0);}
		void StencilLevelSet(char bt)					{m_Device->SetRenderState(D3DRS_STENCILREF, bt);}
		void StencilSetCmpFunc(CompareType type)		{m_Device->SetRenderState(D3DRS_STENCILFUNC, type);}
		void StencilSetPasOpFunc(StencilOperation op)	{m_Device->SetRenderState(D3DRS_STENCILPASS, op);}
		void StencilSetFailOpFunc(StencilOperation op)	{m_Device->SetRenderState(D3DRS_STENCILFAIL, op);}

		int GetDrawCalls(){return m_DrawCalls;}
		int GetPolyCount(){return m_PolyCount;}

		void SetTextureRepeat(const bool isRepeat);


	private:
		static const int MAX_OBJECTS=3000;	       
		bool m_IsTextureRepeat;

		Vector m_MatrixOffset;
		LPDIRECT3D9 m_D3D;	// наш главный интерфейс d3d9		
		LPDIRECT3DDEVICE9 m_Device;
		LPDIRECT3DVERTEXBUFFER9 m_VertexBuffer;
		LPDIRECT3DINDEXBUFFER9 m_IndexBuffer;
		Mesh m_LineMesh;
		Vector m_lineVector;
		D3DLIGHT9 *m_LineLight;

		
		
		//pointer to vertex data (NEW)
		BYTE* m_VertexDataPointer;
		WORD* m_IndexDataPointer;
		D3DMATERIAL9 *m_LineMaterial;
		IDirect3DTexture9* _texture; 
		HWND m_Hwnd;                 
		D3DXMATRIX *m_MatrixView;		// Матрицы
		D3DXMATRIX *m_MatrixProjection;	// Матрицы
		D3DXMATRIX *m_MatrixWorld;		// Матрицы
		D3DCAPS9 m_Caps;	

		IDirect3DTexture9* m_PrevTexture;
		D3DVERTEX m_Polygon[6];			

		///Служебные переменные 
		int vs;
		int vc;

		///Состояния рендеринга батча
		enum RenderActions
		{
			RA_None = -1,
			RA_Batch = 0,
			RA_Line = 1,
			RA_3D
		};

		BlendMode m_BlendMode;

		///Сменить состояние рендера
		virtual void ChangeState(RenderActions renderAction);

		///Сбросить текущие батч на рендер
		virtual void FlushVertexBuffer(bool needToLockBufferAgain = true);
		///Предыдущее состояние рендера
		int m_PrevAction;
		///Смещение по Y для центра экрана
		float m_MiddleY;
		///Текущий объект для отрисовки
		int m_CurrentObject; 
		///Текущий объект для отрисовки
		int m_CurrentDrawPoly; 
		///Смещение вершин в батче
		int m_VertexOffset;
		///Смещение индексов в батче
		int m_IndexOffset;
		///Количество Draw Calls
		int m_DrawCalls;
		///Количество отрисованных полигонов за предыдущий рендер
		int m_PolyCount;

	};
}
#endif
#endif // NBG_FRAMEWORK_GLOBAL_RENDER_WIN32
