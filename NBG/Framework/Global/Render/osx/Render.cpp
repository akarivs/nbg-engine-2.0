#ifdef NBG_OSX
#include "Render.h"

#import <QuartzCore/QuartzCore.h>
#import <GLKit/GLKit.h>

#include <Framework.h>


namespace NBG
{
	//==============================================================================
	CRender::CRender()
	{
		
		m_PrevAction = RA_None;
		m_BlendMode = BLENDMODE_BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
        
        m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;
	}

	CRender::~CRender()
	{

	}


	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		Vector resolution = g_System->GetSystemResolution();

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
	}

	

	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE0);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x,size.y,0, GL_RGB, GL_UNSIGNED_BYTE,data);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            texture->SetTextureData(new GLuint(text));
            return;
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, text);
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x,size.y,GL_RGB, GL_UNSIGNED_BYTE,data);
	}

    void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        unsigned char * textureData = NULL;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            textureData = new unsigned char[(int)(size.x/2.0f*size.y*4)];
            memset(textureData,0,size.x/2.0f*size.y*4);
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x/2.0f,size.y,0, GL_RGBA, GL_UNSIGNED_BYTE,textureData);
            
            texture->SetTextureData(new GLuint(text));
            texture->SetUserData(textureData);
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
            textureData = (unsigned char*)texture->GetUserData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        
        unsigned char*  src     = data;
        unsigned char*  dst     = textureData;
        
        int i,j,k,x,y,w=size.x;
        int yPremult = 0;
        int sizeXDiv2 = size.x/2.0f;
        for (y=0;y<size.y-2;y++)
        {
            yPremult = (y+2)*w;
            for (x=0;x<sizeXDiv2;x++)
            {
                i=(y*sizeXDiv2+x)*4;
                j=(yPremult+x+4)*3;
                k=(yPremult+x+(sizeXDiv2))*3;
                memcpy(&dst[i],&src[j],3);
                dst[i+3]=src[k];
            }
        }
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x/2.0f,size.y,GL_RGBA, GL_UNSIGNED_BYTE,dst);
	}

	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		char ** ret = NULL;
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1 ;

		ret = new char*[w];
		for (int i=0; i<w; i++)
		{
			ret[i] = new char[h];
			for (int f=0; f<h; f++)
			{
				ret[i][f] = 0;
			}
		}

		w =  texture->GetSize().x;
		h =  texture->GetSize().y;

		return ret;
	}


	void CRender::ReleaseTexture(Texture * texture)
	{
        texture->SetTextureData(NULL);
	}


	void CRender::Begin()
	{
        m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_PolyCount = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		m_DrawCalls = 0;
		
        glMatrixMode(GL_PROJECTION|GL_MODELVIEW);
        glLoadIdentity();
        
        Vector size = g_System->GetSystemResolution();
        Vector halfSize = size/2.0f;
        Vector scale = g_System->GetResolutionScale();
        
        glOrtho(-halfSize.x,halfSize.x,-halfSize.y,halfSize.y, 0, 1);
        glScalef(scale.x,scale.y,1.0f);
        
        glViewport(0, 0, size.x, size.y);
        
        glEnable(GL_TEXTURE_2D);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        
        m_CurrentObject = 0;
        m_DrawCalls = 0;
        m_PolyCount = 0;
	}

	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{
		/*if (m_CurrentDrawPoly == 0)return;
		m_DrawCalls++;		
		ChangeState(RA_Batch);
		

		
		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		if (needToLockBufferAgain)
		{
        }*/
	}

	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
        GLuint textureId = -1;
        if (texture && texture->GetTextureData())
        {
            textureId = *(int*)texture->GetTextureData();
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,textureId);
        
        NBG::Vertex * vertexes = mesh->GetVertexes();
        NBG::Color clr;
        clr = Color(mesh->GetVertexes()[0].color);  //clr.Normalize();
        
        
        glBegin(GL_TRIANGLES);
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[0].u, vertexes[0].v);
        glVertex2f(vertexes[0].x, vertexes[0].y);
        
        clr = Color(mesh->GetVertexes()[1].color);  //clr.Normalize();
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[1].u, vertexes[1].v);
        glVertex2f(vertexes[1].x, vertexes[1].y);
        
        clr = Color(mesh->GetVertexes()[2].color);  //clr.Normalize();
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[2].u, vertexes[2].v);
        glVertex2f(vertexes[2].x, vertexes[2].y);
        glEnd();
        
        glBegin(GL_TRIANGLES);
        clr = Color(mesh->GetVertexes()[3].color);  //clr.Normalize();
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[3].u, vertexes[3].v);
        glVertex2f(vertexes[3].x, vertexes[3].y);
        
        clr = Color(mesh->GetVertexes()[2].color);  //clr.Normalize();
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[2].u, vertexes[2].v);
        glVertex2f(vertexes[2].x, vertexes[2].y);
        
        clr = Color(mesh->GetVertexes()[1].color); // clr.Normalize();
        glColor4f(clr.r,clr.g,clr.b,clr.a);
        glTexCoord2f(vertexes[1].u, vertexes[1].v);
        glVertex2f(vertexes[1].x, vertexes[1].y);
        
        
        glEnd();
        
        m_DrawCalls++;
        m_PolyCount += 2;

        return;

		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount()/3.0f;
		m_PolyCount += vc;
		memcpy((void*)(m_VertexDataPointer+(m_VertexOffset)),mesh->GetVertexes(),vs);
		m_CurrentObject     +=vc/2;	

		if (m_CurrentObject >= MAX_OBJECTS)
		{
			FlushVertexBuffer();
			m_PolyCount += vc;
			m_CurrentObject     +=vc/2;				
		}

		m_CurrentDrawPoly   +=vc;
		m_VertexOffset      +=vs;

		int is = mesh->GetIndexesSize();		
		int ic = mesh->GetIndexesCount();		
		memcpy(m_IndexDataPointer,mesh->GetIndexes(),is);		
		for (int i=0; i<ic; i++)
		{
			m_IndexDataPointer[i] += m_IndexOffset;			
		}		
		m_IndexDataPointer += ic;
		m_IndexOffset += mesh->GetVertexCount();
	}

    
    
    void CRender::DrawLine(Vector ul, Vector dr, const float width, Color color)
	{
		ChangeState(RA_Batch);
		ul.y = m_MatrixOffset.y - ul.y;
		dr.y = m_MatrixOffset.y - dr.y;
		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if(DirX == 0)
		{
			if(DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if(DirY == 0)
		{
			if(DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if(DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;
        
        
		Vertex * vertexes = m_LineMesh.GetVertexes();
        
		vertexes[0].x = ul.x + m_lineVector.x;
		vertexes[0].y = ul.y + m_lineVector.y;
		vertexes[1].x = ul.x - m_lineVector.x;
		vertexes[1].y = ul.y - m_lineVector.y;
		vertexes[2].x = dr.x + m_lineVector.x;
		vertexes[2].y = dr.y + m_lineVector.y;
		vertexes[3].x = dr.x - m_lineVector.x;
		vertexes[3].y = dr.y - m_lineVector.y;
        
		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 1; vertexes[2].v = 1;
		vertexes[3].u = 0; vertexes[3].v = 1;
        
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0; indexes[1] = 1; indexes[2] = 2;
		indexes[3] = 3; indexes[4] = 2; indexes[5] = 1;
        
        
		unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color =clr;
		DrawMesh(NULL, &m_LineMesh);
	}

	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
						break;
		case RA_Batch:
						break;
		case RA_3D:			
				
			break;
		}
	}

	void CRender::DrawBatch()
	{
		FlushVertexBuffer(false);
		//m_PrevTexture = NULL;
    }

	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BLENDMODE_BLEND)
		{
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (mode == BLENDMODE_ADD)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		}
		else if (mode == BLENDMODE_MOD)
		{
			
		}
	}

	void CRender::End()
	{
		
	}
    
    /* TODO: implement stencil */
    void CRender::StencilEnable()
    {
        glEnable(GL_STENCIL_TEST);
        stencilFail = (StencilOperation)-1;
        stencilPass = (StencilOperation)-1;
        
    }
    void CRender::StencilDisable()
    {
        glDisable(GL_STENCIL_TEST);
    }
    void CRender::StencilClear(char bt)
    {
        glClear(GL_STENCIL_BUFFER_BIT);
        
    }
    void CRender::StencilLevelSet(char bt)
    {
        stencilLevel = bt;
    }
    void CRender::StencilSetCmpFunc(CompareType type)
    {
        glStencilFunc(type, (int)stencilLevel,0);
    }
    void CRender::StencilSetPasOpFunc(StencilOperation op)
    {
        stencilPass = op;
        if (stencilFail != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
        
    }
    void CRender::StencilSetFailOpFunc(StencilOperation op)
    {
        stencilFail = op;
        if (stencilPass != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
    }
}
#endif