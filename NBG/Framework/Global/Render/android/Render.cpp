#ifdef NBG_ANDROID
#include "Render.h"

#include <Framework.h>

#include <android/native_window.h> // requires ndk r5 or newer
#include <EGL/egl.h> // requires ndk r5 or newer
#include <GLES/gl.h>

namespace NBG
{
	const int size = 0x50;
	static GLint vertices[][3] = {
		{ -size, -size, 0 },
		{  size, -size, 0 },
		{  size,  size, 0 },
		{ -size,  size, 0 },
		{ -size, -size,  0 },
		{  size, -size,  0 },
		{  size,  size,  0 },
		{ -size,  size,  0 }
	};

	static GLint colors[][4] = {
		{ 0x00000, 0x00000, 0x00000, 0x10000 },
		{ 0x10000, 0x00000, 0x00000, 0x10000 },
		{ 0x10000, 0x10000, 0x00000, 0x10000 },
		{ 0x00000, 0x10000, 0x00000, 0x10000 },
		{ 0x00000, 0x00000, 0x10000, 0x10000 },
		{ 0x10000, 0x00000, 0x10000, 0x10000 },
		{ 0x10000, 0x10000, 0x10000, 0x10000 },
		{ 0x00000, 0x10000, 0x10000, 0x10000 }
	};

	GLubyte __indices[] = {
		0, 4, 5,    0, 5, 1,
		1, 5, 6,    1, 6, 2,
		2, 6, 7,    2, 7, 3,
		3, 7, 4,    3, 4, 0,
		4, 7, 6,    4, 6, 5,
		3, 0, 1,    3, 1, 2
	};

	namespace AndroidRender
	{
		typedef struct {
			float Position[3];
			float Color[4];
			float Texture[2];
		} Vertex;

		GLuint vertexBuffer;
		GLuint indexBuffer;
		Vertex vertexes[CRender::MAX_OBJECTS*4];
		GLushort indices[CRender::MAX_OBJECTS*6];
		GLuint prevTextureId;
		char * vboBuffer;
		GLuint viewRenderbuffer, viewFramebuffer;
		GLuint depthRenderbuffer;		
		Matrix projectionM;
		
		GLuint _positionSlot;
		GLuint  _colorSlot;
		GLuint  _coordSlot;
		GLuint  _samplerSlot;
		GLuint  _projectionSlot;

		

		bool CreateFrameBuffer()
		{
			/*CONSOLE("Initializing context");



		

			projectionM.Identity();
			projectionM.Scale(2.0f/(float)g_System->GetSystemResolution().x,
				2.0f/(float)g_System->GetSystemResolution().y);

			projectionM.m[3] = 0;
			projectionM.m[7] = -2;
			projectionM.m[10] = -1;

			NBG::Matrix matrix;
			matrix.Identity();
			matrix.Scale(g_System->GetResolutionScale().x,g_System->GetResolutionScale().y);
			projectionM.Multiply(&matrix);

			glViewport(0, 0, g_System->GetSystemResolution().x, g_System->GetSystemResolution().y);
			CONSOLE("Viewport: %f %f",g_System->GetSystemResolution().x,g_System->GetSystemResolution().y);*/
			return true;
		}

		GLuint compileShader(std::string shader,GLenum shaderType)
		{		
			/*GLuint shaderHandle = glCreateShader(shaderType);
			int size = shader.size();
			const char* l_text = shader.c_str();
			glShaderSource(shaderHandle, 1, &l_text, NULL);
			glCompileShader(shaderHandle);
			GLint compileSuccess;
			glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
			if (compileSuccess == GL_FALSE) 
			{
				GLchar messages[256];
				glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
				CONSOLE("%s",&messages[0]);
				exit(1);
			}
			return shaderHandle;*/
			return 0;
		}

		void compileShaders()
		{
			/*std::string simpleFragment = "varying lowp vec4 DestinationColor; // 1 \n" \
										 "varying lowp vec2 texCoord; \n" \
										 "uniform sampler2D s_texture; \n"\
										 "void main(void) { // 2 \n"\
										 "	gl_FragColor = DestinationColor*texture2D( s_texture, texCoord ); \n"\
										 "}";

			std::string simpleVertex = "attribute vec4 Position; // 1 \n"\
										"attribute vec4 SourceColor; // 2 \n"\
										"attribute vec2 SourceUV; \n"\
										"uniform mat4 projectionMatrix; \n"\
										"uniform vec2 centerOffset; \n"\
										" \n"\
										" \n"\
										"varying vec4 DestinationColor; // 3 \n"\
										"varying vec2 texCoord; \n"\
										" \n"\
										"void main(void) { // 4 \n"\
										"	DestinationColor = SourceColor; // 5 \n"\
										"	texCoord = SourceUV; \n"\
										"	gl_Position = Position; // 6 \n"\
										"	gl_Position *= projectionMatrix; \n"\
										"}";


			// 1
			GLuint vertexShader = compileShader(simpleVertex,GL_VERTEX_SHADER);
			GLuint fragmentShader = compileShader(simpleFragment,GL_FRAGMENT_SHADER);


			// 2
			GLuint programHandle = glCreateProgram();
			glAttachShader(programHandle, vertexShader);
			glAttachShader(programHandle, fragmentShader);
			glLinkProgram(programHandle);

			// 3
			GLint linkSuccess;
			glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
			if (linkSuccess == GL_FALSE) {
				GLchar messages[256];
				glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
				CONSOLE("%s",&messages[0]);
				exit(1);
			}

			// 4
			glUseProgram(programHandle);

			// 5
			_positionSlot = glGetAttribLocation(programHandle, "Position");
			_colorSlot = glGetAttribLocation(programHandle, "SourceColor");
			_coordSlot = glGetAttribLocation(programHandle, "SourceUV");
			_samplerSlot = glGetAttribLocation(programHandle, "s_texture");
			_projectionSlot = glGetUniformLocation(programHandle, "projectionMatrix");


			GLuint centerOffset = glGetUniformLocation(programHandle,"centerOffset");

			glEnableVertexAttribArray(_positionSlot);
			glEnableVertexAttribArray(_colorSlot);
			glEnableVertexAttribArray(_coordSlot);



			//Set the offset
			glUniform2f(centerOffset,0,(float)g_System->GetSystemResolution().y*g_System->GetResolutionScale().y/2.0f);

			// Set the projection
			glUniformMatrix4fv(_projectionSlot,1,0,&projectionM.m[0]);


			// 2
			glVertexAttribPointer(_positionSlot, 3, GL_FLOAT, GL_FALSE,
				sizeof(Vertex), 0);
			glVertexAttribPointer(_colorSlot, 4, GL_FLOAT, GL_FALSE,
				sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
			glVertexAttribPointer(_coordSlot, 2, GL_FLOAT, GL_FALSE,
				sizeof(Vertex), (GLvoid*) (sizeof(float) * 7));

			glEnableVertexAttribArray(_positionSlot);
			glEnableVertexAttribArray(_colorSlot);
			glEnableVertexAttribArray(_coordSlot);*/

		}
	}

	//==============================================================================
	CRender::CRender()
	{
		m_PrevAction = RA_None;
		m_BlendMode = BLENDMODE_BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;

		m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;
	}

	CRender::~CRender()
	{

	}

	void CRender::SetWindow(ANativeWindow* window)
	{
		_window = window;

		const EGLint attribs[] = {
			EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
			EGL_BLUE_SIZE, 8,
			EGL_GREEN_SIZE, 8,
			EGL_RED_SIZE, 8,
			EGL_NONE
		};
		EGLDisplay display;
		EGLConfig config;    
		EGLint numConfigs;
		EGLint format;
		EGLSurface surface;
		EGLContext context;
		EGLint width;
		EGLint height;
		GLfloat ratio;

		CONSOLE("Initializing context");

		if ((display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY) {
			CONSOLE("eglGetDisplay() returned error %d", eglGetError());
			return;
		}
		if (!eglInitialize(display, 0, 0)) {
			CONSOLE("eglInitialize() returned error %d", eglGetError());
			return;
		}

		if (!eglChooseConfig(display, attribs, &config, 1, &numConfigs)) {
			CONSOLE("eglChooseConfig() returned error %d", eglGetError());
			exit(1);
			return;
		}

		if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format)) {
			CONSOLE("eglGetConfigAttrib() returned error %d", eglGetError());
			exit(1);
			return;
		}

		ANativeWindow_setBuffersGeometry(_window, 0, 0, format);

		if (!(surface = eglCreateWindowSurface(display, config, _window, 0))) {
			CONSOLE("eglCreateWindowSurface() returned error %d", eglGetError());
			exit(1);
			return;
		}

		if (!(context = eglCreateContext(display, config, 0, 0))) {
			CONSOLE("eglCreateContext() returned error %d", eglGetError());
			exit(1);
			return;
		}

		if (!eglMakeCurrent(display, surface, surface, context)) {
			CONSOLE("eglMakeCurrent() returned error %d", eglGetError());
			exit(1);
			return;
		}

		if (!eglQuerySurface(display, surface, EGL_WIDTH, &width) ||
			!eglQuerySurface(display, surface, EGL_HEIGHT, &height)) {
				CONSOLE("eglQuerySurface() returned error %d", eglGetError());
				exit(1);
				return;
		}
		

		_display = display;
		_surface = surface;
		_context = context;	

		Vector winSize = Vector(width,height);
		Vector logical = g_GameApplication->GetLogicalSize();
		g_System->UpdateSize(winSize,logical,true);

		Vector gameRes = g_System->GetGameResolution();
		Vector resScale = g_System->GetResolutionScale();
		//m_MatrixOffset.y = (gameRes.y)/resScale.y;	


		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrthof(0.0f,width,height,0.0f, 0, 1.0f);
		

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.5f, -(height)/resScale.y-0.5f, 0.0f);
		m_MatrixOffset.y = 0.0f;
		glScalef(resScale.x, resScale.y, 1.0f);		
		

		CONSOLE("Window size: %d %d",width,height);
		CONSOLE("Game size: %f %f",gameRes.x, gameRes.y);
		CONSOLE("Logical size: %f %f",logical.x, logical.y);
	}

	void CRender::Init()
	{	
		for (int i=0, j=0; i<MAX_OBJECTS; i+=6,j+=4)
		{
			AndroidRender::indices[i] = j;
			AndroidRender::indices[i+1] = j+1;
			AndroidRender::indices[i+2] = j+2;
			AndroidRender::indices[i+3] = j+3;
			AndroidRender::indices[i+4] = j+2;
			AndroidRender::indices[i+5] = j+1;
		}

		/*bool res = AndroidRender::CreateFrameBuffer();
		if (!res)
		{
			CONSOLE("Could'nt create frame buffer!");
		}
		else
		{
			CONSOLE("Frame buffer created");
		}



		glGenBuffers(1, &AndroidRender::vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, AndroidRender::vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(AndroidRender::vertexes), AndroidRender::vertexes, GL_STREAM_DRAW);


		glGenBuffers(1, &AndroidRender::indexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, AndroidRender::indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(AndroidRender::indices), AndroidRender::indices, GL_STREAM_DRAW);

		AndroidRender::compileShaders();		

		Reset();*/


	}

	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();




		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
	}
	
	void CRender::CheckDevice()
	{		
	}

	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
	}

	void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
	}

	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		return NULL;
	}


	void CRender::ReleaseTexture(Texture * texture)
	{
	}
	
	void CRender::Begin()
	{		
		glClearColor(0,0,0,1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		

		AndroidRender::prevTextureId = 0;

		Vector gameRes = g_System->GetGameResolution();
		Vector sysRes = g_System->GetSystemResolution() /2.0f;
		Vector resScale = g_System->GetResolutionScale();		

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.5f + sysRes.x, -0.5f + sysRes.y, 0.0f);		
		glScalef(resScale.x, resScale.y, 1.0f);		

		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_PolyCount = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;	
		m_DrawCalls = 0;	

		
	}
	#define BUFFER_OFFSET(i) ((char*)NULL + (i))
	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{			
		if (m_CurrentObject == 0)return;		
		m_DrawCalls++;

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Position[0]);
		glColorPointer(4, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Color[0]);		
		glTexCoordPointer(2, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Texture[0]);	
		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_SHORT, &AndroidRender::indices[0]);

		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		m_IndexCount = 0;
	}

	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{		
		GLuint textureId = -1;
		if (texture && texture->GetTextureData())
		{
			textureId = *(int*)texture->GetTextureData();
		}


		if (AndroidRender::prevTextureId != 0)
		{

			if(AndroidRender::prevTextureId != textureId)
			{
				FlushVertexBuffer();
				glBindTexture(GL_TEXTURE_2D,textureId);				
			}
		}
		else
		{
			if (AndroidRender::prevTextureId != textureId)
			{
				glBindTexture(GL_TEXTURE_2D,textureId);						
			}

		}
		AndroidRender::prevTextureId = textureId;
		
		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount()/3.0f;
		m_PolyCount += vc;
		memcpy(&AndroidRender::vertexes[m_VertexOffset],mesh->GetVertexes(),vs);		
		
		m_VertexOffset      +=mesh->GetVertexCount();
		m_CurrentObject     +=vc/2;

		int is = mesh->GetIndexesSize();
		int ic = mesh->GetIndexesCount();

		memcpy(&AndroidRender::indices[m_IndexCount],mesh->GetIndexes(),is);

		for (int i=0; i<ic; i++)
		{
			AndroidRender::indices[m_IndexCount+i] += m_IndexOffset;
		}
		m_IndexCount += ic;		
		m_IndexOffset += mesh->GetVertexCount();		
	}

	void CRender::DrawLine(Vector ul, Vector dr, const float width, Color color)
	{		
		/*ChangeState(RA_Batch);
		ul.y = m_MatrixOffset.y - ul.y;
		dr.y = m_MatrixOffset.y - dr.y;
		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if(DirX == 0)
		{
			if(DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if(DirY == 0)
		{
			if(DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if(DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;


		Vertex * vertexes = m_LineMesh.GetVertexes(); 		

		vertexes[0].x = ul.x + m_lineVector.x;
		vertexes[0].y = ul.y + m_lineVector.y;
		vertexes[1].x = ul.x - m_lineVector.x;
		vertexes[1].y = ul.y - m_lineVector.y;
		vertexes[2].x = dr.x + m_lineVector.x;
		vertexes[2].y = dr.y + m_lineVector.y;
		vertexes[3].x = dr.x - m_lineVector.x;
		vertexes[3].y = dr.y - m_lineVector.y;			

		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 1; vertexes[2].v = 1;
		vertexes[3].u = 0; vertexes[3].v = 1;

		unsigned short * indexes = m_LineMesh.GetIndexes(); 		
		indexes[0] = 0; indexes[1] = 1; indexes[2] = 2;
		indexes[3] = 3; indexes[4] = 2; indexes[5] = 1;		


		unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color =clr;			
		DrawMesh(NULL, &m_LineMesh);*/
	}


	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
			break;
		case RA_Batch:
			break;
		case RA_3D:			
			break;
		}
	}

	void CRender::DrawBatch()
	{
	}

	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BLENDMODE_BLEND)
		{
		}
		else if (mode == BLENDMODE_ADD)
		{
		}
		else if (mode == BLENDMODE_MOD)
		{
		}
	}


	void CRender::End()
	{
		FlushVertexBuffer(false);		
		
		if (!eglSwapBuffers(_display, _surface)) {
			//CONSOLE("eglSwapBuffers() returned error %d", eglGetError());
		}
	}
}
#endif