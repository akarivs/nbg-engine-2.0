#ifndef NBG_CORE_TEXTURE_MANAGER
#define NBG_CORE_TEXTURE_MANAGER

#include "TextureManager/BaseTextureManager.h"
#ifdef NBG_WIN32
    #include "TextureManager/win32/TextureManager.h"
#elif defined (NBG_IOS)
    #include "TextureManager/ios/TextureManager.h"
#elif defined (NBG_OSX)
    #include "TextureManager/osx/TextureManager.h"
#elif defined (NBG_ANDROID)
    #include "TextureManager/android/TextureManager.h"
#elif defined (NBG_LINUX)
    #include "TextureManager/linux/TextureManager.h"
#endif

#endif //NBG_CORE_TEXTURE_MANAGER
