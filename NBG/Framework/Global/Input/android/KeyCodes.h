#ifndef NBG_CORE_INPUT_KEY_CODES_IOS
#define NBG_CORE_INPUT_KEY_CODES_IOS

#define KEY_BACK	        8
#define KEY_TAB	        9
#define KEY_CLEAR	        12
#define KEY_RETURN	    13
#define KEY_ENTER	        13
#define KEY_SHIFT	        16
#define KEY_CONTROL	    17
#define KEY_MENU	        18
#define KEY_ALT 	        18
#define KEY_PAUSE	        19
#define KEY_CAPITAL	    20
#define KEY_KANA	        0x15
#define KEY_HANGEUL	    0x15
#define KEY_HANGUL	    0x15
#define KEY_JUNJA	        0x17
#define KEY_FINAL	        0x18
#define KEY_HANJA	        0x19
#define KEY_KANJI	        0x19
#define KEY_ESCAPE	    0x1B
#define KEY_CONVERT	    0x1C
#define KEY_NONCONVERT	0x1D
#define KEY_ACCEPT	    0x1E
#define KEY_MODECHANGE	0x1F
#define KEY_SPACE	        32
#define KEY_PRIOR	        33
#define KEY_NEXT	        34
#define KEY_END	        35
#define KEY_HOME	        36
#define KEY_LEFT	        37
#define KEY_UP	        38
#define KEY_RIGHT	        39
#define KEY_DOWN	        40
#define KEY_SELECT	    41
#define KEY_PRINT	        42
#define KEY_EXECUTE	    43
#define KEY_SNAPSHOT	    44
#define KEY_INSERT	    45
#define KEY_DELETE	    46
#define KEY_HELP	        47
#define KEY_LWIN	        0x5B
#define KEY_RWIN	        0x5C
#define KEY_APPS	        0x5D
#define KEY_SLEEP	        0x5F
#define KEY_NUMPAD0	    0x60
#define KEY_NUMPAD1	    0x61
#define KEY_NUMPAD2	    0x62
#define KEY_NUMPAD3	    0x63
#define KEY_NUMPAD4	    0x64
#define KEY_NUMPAD5	    0x65
#define KEY_NUMPAD6	    0x66
#define KEY_NUMPAD7	    0x67
#define KEY_NUMPAD8	    0x68
#define KEY_NUMPAD9	    0x69
#define KEY_MULTIPLY	    0x6A
#define KEY_ADD	        0x6B
#define KEY_SEPARATOR	    0x6C
#define KEY_SUBTRACT	    0x6D
#define KEY_DECIMAL	    0x6E
#define KEY_DIVIDE	    0x6F
#define KEY_F1	        0x70
#define KEY_F2	        0x71
#define KEY_F3	        0x72
#define KEY_F4	        0x73
#define KEY_F5	        0x74
#define KEY_F6	        0x75
#define KEY_F7	        0x76
#define KEY_F8	        0x77
#define KEY_F9	        0x78
#define KEY_F10	        0x79
#define KEY_F11	        0x7A
#define KEY_F12	        0x7B
#define KEY_F13	        0x7C
#define KEY_F14	        0x7D
#define KEY_F15	        0x7E
#define KEY_F16	        0x7F
#define KEY_F17	        0x80
#define KEY_F18	        0x81
#define KEY_F19	        0x82
#define KEY_F20	        0x83
#define KEY_F21	        0x84
#define KEY_F22	        0x85
#define KEY_F23	        0x86
#define KEY_F24	        0x87
#define KEY_NUMLOCK              0x90
#define KEY_SCROLL               0x91
#define KEY_LSHIFT               0xA0
#define KEY_RSHIFT               0xA1
#define KEY_LCONTROL             0xA2
#define KEY_RCONTROL             0xA3
#define KEY_LCOMMAND         0x0
#define KEY_LMENU	        0xA4
#define KEY_RMENU	        0xA5
#define KEY_BROWSER_BACK         0xA6
#define KEY_BROWSER_FORWARD	0xA7
#define KEY_BROWSER_REFRESH	0xA8
#define KEY_BROWSER_STOP         0xA9
#define KEY_BROWSER_SEARCH	0xAA
#define KEY_BROWSER_FAVORITES	0xAB
#define KEY_BROWSER_HOME         0xAC
#define KEY_VOLUME_MUTE          0xAD
#define KEY_VOLUME_DOWN          0xAE
#define KEY_VOLUME_UP            0xAF
#define KEY_MEDIA_NEXT_TRACK	0xB0
#define KEY_MEDIA_PREV_TRACK	0xB1
#define KEY_MEDIA_STOP	0xB2
#define KEY_MEDIA_PLAY_PAUSE	0xB3
#define KEY_LAUNCH_MAIL	0xB4
#define KEY_LAUNCH_MEDIA_SELECT	0xB5
#define KEY_LAUNCH_APP1	0xB6
#define KEY_LAUNCH_APP2	0xB7
#define VKEY_OEM_1	0xBA
#define KEY_OEM_PLUS	0xBB
#define KEY_OEM_COMMA	0xBC
#define KEY_OEM_MINUS	0xBD
#define KEY_OEM_PERIOD	0xBE
#define KEY_OEM_2	0xBF
#define KEY_OEM_3	0xC0
#define KEY_OEM_4	0xDB
#define KEY_OEM_5	0xDC
#define KEY_OEM_6	0xDD
#define KEY_OEM_7	0xDE
#define KEY_OEM_8	0xDF
#define KEY_OEM_102	0xE2
#define KEY_PROCESSKEY	0xE5
#define KEY_PACKET	0xE7
#define KEY_ATTN         0xF6
#define KEY_CRSEL	0xF7
#define KEY_EXSEL	0xF8
#define KEY_EREOF	0xF9
#define KEY_PLAY 	0xFA
#define KEY_ZOOM         0xFB
#define KEY_NONAME	0xFC
#define KEY_PA1          0xFD
#define KEY_OEM_CLEAR	0xFE
#define KEY_CLOSE_WINDOW 1001
#define KEY_A    0x41
#define KEY_B    0x42
#define KEY_C    0x43
#define KEY_D    0x44
#define KEY_E    0x45
#define KEY_F    0x46
#define KEY_G    0x47
#define KEY_H    0x48
#define KEY_I    0x49
#define KEY_J    0x4A
#define KEY_K    0x4B
#define KEY_L    0x4C
#define KEY_M    0x4D
#define KEY_N    0x4E
#define KEY_O    0x4F
#define KEY_P    0x50
#define KEY_Q    0x51
#define KEY_R    0x52
#define KEY_S    0x53
#define KEY_T    0x54
#define KEY_U    0x55
#define KEY_V    0x56
#define KEY_W    0x57
#define KEY_X    0x58
#define KEY_Y    0x59
#define KEY_Z    0x5A
#endif // NBG_CORE_INPUT_KEY_CODES_IOS
