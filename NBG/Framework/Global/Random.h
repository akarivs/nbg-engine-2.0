#ifndef NBG_CORE_RANDOM
#define NBG_CORE_RANDOM

namespace NBG
{
	/** @brief Класс, реализующий работу со случайными значениями.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>	
	* @copyright 2013-2014 New Bridge Games
	*
	*/	
	class CRandom
	{
	public:
		/// @name Конструкторы
			
		CRandom();	
		~CRandom();
		
		///Инициализация класса.
		void	Init();

		/// Получить значения в требуемом float диапазоне.
		float	RandF(float start, float end);
		/// Получить значения в требуемом int диапазоне.
		int		RandI(int start, int end);
		/// Получить true или false
		bool	RandB();
	private:				
	};
}
#endif ///NBG_CORE_RANDOM
