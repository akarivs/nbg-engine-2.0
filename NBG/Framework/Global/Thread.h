#ifndef NBG_CORE_THREAD
#define NBG_CORE_THREAD

#ifdef NBG_WIN32
    #include "Thread/win32/Thread.h"
#elif defined NBG_IOS
    #include "Thread/ios/Thread.h"
#elif defined NBG_OSX
#include "Thread/osx/Thread.h"
#elif defined NBG_ANDROID
    #include "Thread/android/Thread.h"
#elif defined NBG_LINUX
    #include "Thread/linux/Thread.h"
#endif

#endif //NBG_CORE_THREAD
