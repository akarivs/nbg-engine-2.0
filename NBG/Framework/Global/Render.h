#ifndef NBG_FRAMEWORK_GLOBAL_RENDER
#define NBG_FRAMEWORK_GLOBAL_RENDER

#ifdef NBG_WIN32
	#include "Render/win32/Render.h"
#elif defined(NBG_OSX)
	#include "Render/osx/Render.h"
#elif defined(NBG_IOS)
    #include "Render/ios/Render.h"
#elif defined(NBG_ANDROID)
    #include "Render/android/Render.h"
#endif

#endif // NBG_FRAMEWORK_GLOBAL_RENDER
