#ifdef NBG_WIN32
#include "TextureManager.h"
#include <Framework.h>

namespace NBG
{
	//==========================================================================
	CTextureManager::CTextureManager()
	{
		m_DefaultTexture = NULL;
	}

	//==========================================================================
	void CTextureManager::InitDefaultTexture()
	{
		m_DefaultTexture = new Texture();
		int w = 32;
		int h = 32;

		IDirect3DTexture9 * texture;

		BYTE* PixelMap = NULL;
		RECT r;
		r.left = 0;
		r.top = 0;
		r.right = w;
		r.bottom = h;
		HRESULT hRes;

		D3DLOCKED_RECT RectLock;        
		hRes = D3DXCreateTexture(g_Render->GetDevice(),w,h,1,0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED,&texture);
		texture->LockRect(0,&RectLock,&r,D3DLOCK_DISCARD);

		PixelMap = (BYTE *)RectLock.pBits;

		char * pixel = NULL;

		int pitch = w*4;
		int bpp	= 4;

		int x1 = 0;
		int x2 = 0 + (int)w;
		int y1 = 0;
		int y2 = 0 + (int)h;
		int atlasPitch = w*h*4;
		int p = 0;

		int key = 0;
		int pitchKey = 0;
		int pitchMultiply = 0;
		for (int y=y1; y<y2; y++)
		{
			pitchMultiply = pitch*(y);
			for (int x=x1; x<x2; x++)
			{
				if (x*4+(pitchMultiply) < atlasPitch)
				{
					key = pitch * (y-y1) + 4 * (x-x1);
					pitchKey = x*4+(pitchMultiply);
					if (abs(x-(x2/2)) < 5)
					{
						PixelMap[pitchKey]	    = 0;
						PixelMap[pitchKey+1]	= 0;
						PixelMap[pitchKey+2]	= 255;
					}
					else if (y<y2/2)
					{
						PixelMap[pitchKey]	    = 0;
						PixelMap[pitchKey+1]	= 255;
						PixelMap[pitchKey+2]	= 255;
					}
					else
					{
						PixelMap[pitchKey]	    = 255;
						PixelMap[pitchKey+1]	= 0;
						PixelMap[pitchKey+2]	= 0;
					}

					PixelMap[pitchKey+3]	= 255;
				}
			}
		}
		m_DefaultTexture->SetSize(w,h);
		m_DefaultTexture->SetTextureData(texture);
		texture->UnlockRect(0);
		return;
	}	

	//==========================================================================
	bool CTextureManager::LoadTexture(CTextureResource * res)
	{		
		IDirect3DTexture9 * texture;
		std::string extension = NBG::StringUtils::GetExtension(res->GetPath());
		unsigned int w = 0;
		unsigned int h = 0;				
		PNG_HELPER::Image image;
		image.data = NULL;		
		if (extension == "png")
		{	
			image = PNG_HELPER::PNGDecode((png_bytep)(res->GetData()));			
			if( image.colorType == PNG_COLOR_TYPE_RGB ) // Если без альфы то выпрыгиваем.  (пока не получается загрузить)
			{
				delete[] image.data;
				return false;
			}
			w = image.width;
			h = image.height;						
		}
		else 
		{
			D3DXIMAGE_INFO		info;
			D3DXGetImageInfoFromFileInMemory((void*)res->GetData(),res->GetSize(), &info);
			if (D3DXCreateTextureFromFileInMemoryEx(g_Render->GetDevice(),res->GetData(),res->GetSize(),info.Width, info.Height,info.MipLevels, 0,info.Format,D3DPOOL_MANAGED,D3DX_DEFAULT, D3DX_DEFAULT, NULL,
				&info, NULL,&texture) != D3D_OK)
			{
				CONSOLE("Failed to load texture from memory!");
				return false;
			}
			w = info.Width;
			h = info.Height;
			Texture * _texture = new Texture();
			_texture->SetSize(w,h);
			_texture->SetTextureData(texture);
			res->SetTexture(_texture);
			res->SetTextureWidth(w);
			res->SetTextureHeight(h);				
			return true;
		}
		res->SetTextureWidth(w);
		res->SetTextureHeight(h);

		BYTE* PixelMap = NULL;
		RECT r;
		r.left = 0;
		r.top = 0;
		r.right = w;
		r.bottom = h;
		HRESULT hRes;

		D3DLOCKED_RECT RectLock;       
		hRes = D3DXCreateTexture(g_Render->GetDevice(),w,h,1,0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED,&texture);
		texture->LockRect(0,&RectLock,&r,D3DLOCK_DISCARD);
		PixelMap = (BYTE *)RectLock.pBits;
		memcpy(PixelMap,image.data,w*h*4);
		texture->UnlockRect(0);

		Texture * _texture = new Texture();
		_texture->SetSize(w,h);
		_texture->SetTextureData(texture);
		res->SetTexture(_texture);				
		if (image.data)
		{
			delete[] image.data;
		}		
		return true;
	}

	//==========================================================================
	void CTextureManager::ReleaseTexture(Texture * texture)
	{		
		IDirect3DTexture9* _texture = (IDirect3DTexture9*)texture->GetTextureData();
		if (_texture)
		{
			_texture->Release();
		}
	}


	char** CTextureManager::GetAlphaFromRegion(Texture * texture, FRect rectangle)
	{
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1 ;

		char ** ret = new char*[w];
		for (int i=0; i<w; i++)
		{
			ret[i] = new char[h];
			for (int f=0; f<h; f++)
			{
				ret[i][f] = 0;
			}
		}


		w =  texture->GetSize().x;
		h =  texture->GetSize().y;

		BYTE* PixelMap = NULL;
		RECT r;
		r.left = 0;
		r.top = 0;
		r.right = w;
		r.bottom = h;
		
		HRESULT hRes;
		IDirect3DTexture9 * directTexture = CAST(IDirect3DTexture9 *,texture->GetTextureData());
		
		D3DLOCKED_RECT RectLock;        		
		directTexture->LockRect(0,&RectLock,&r,D3DLOCK_DISCARD);

		PixelMap = (BYTE *)RectLock.pBits;

		char * pixel = NULL;

		int pitch = w*4;
		int bpp	= 4;

		int x1 = rectangle.left;
		int x2 = rectangle.right;
		int y1 = rectangle.top;
		int y2 = rectangle.bottom;
		int atlasPitch = w*h*4;
		int p = 0;

		int key = 0;
		int pitchKey = 0;
		int pitchMultiply = 0;
		for (int y=y1; y<y2; y++)
		{
			pitchMultiply = pitch*(y);
			for (int x=x1; x<x2; x++)
			{
				if (x*4+(pitchMultiply) < atlasPitch)
				{
					key = pitch * (y-y1) + 4 * (x-x1);
					pitchKey = x*4+(pitchMultiply);					
					if (PixelMap[pitchKey+3] > 0)
					{
						ret[x-(int)rectangle.left][y-(int)rectangle.top] = 1;
					}
					else
					{
						ret[x-(int)rectangle.left][y-(int)rectangle.top] = 0;
					}
				}
			}
		}
		directTexture->UnlockRect(0);
		return ret;
	}
}
#endif //NBG_WIN32
