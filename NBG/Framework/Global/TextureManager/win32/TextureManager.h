#ifndef NBG_CORE_TEXTURE_MANAGER_WIN32
#define NBG_CORE_TEXTURE_MANAGER_WIN32

#ifdef NBG_WIN32

#include <string>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Global/Resources/TextureResource.h>


#include "../BaseTextureManager.h"

namespace NBG
{
	/** @brief Текстурный менеджер для Windows.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CTextureManager : public ITextureManager
	{
	public:
		/// @name Конструкторы
		CTextureManager();
		~CTextureManager(){};

		/// Загрузка текстуры.
		virtual bool LoadTexture(CTextureResource * res);

		/// Инициализация дефолтной текстуры.
		virtual void InitDefaultTexture();

		/// Освободить текстуру и очистить память.
		virtual void ReleaseTexture(Texture * texture);

		/// Получить массив альфы с региона
		virtual char** GetAlphaFromRegion(Texture * texture, FRect rectangle);
	};
}
#endif
#endif
