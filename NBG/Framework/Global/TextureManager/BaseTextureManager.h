#ifndef NBG_CORE_TEXTURE_MANAGER_BASE
#define NBG_CORE_TEXTURE_MANAGER_BASE

#include <string>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Global/Resources/TextureResource.h>

#include <../external/png/libpng/png.h>

namespace NBG
{
	/** @brief Базовый текстурный менеджер. 
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class ITextureManager
	{
	public:
		/// Загрузить текстуру.
		virtual bool LoadTexture(CTextureResource * res) = 0;

		/// Инициализация текстуры по умолчанию. 
		virtual void InitDefaultTexture() = 0; 	

		/// Освободить текстуру и очистить память от неё.
		virtual void ReleaseTexture(Texture * texture) = 0;

		/// Получить текстуру по умолчанию.
		inline Texture * GetDefaultTexture(){return m_DefaultTexture;}

		/// Получить массив альфы с региона
		virtual char** GetAlphaFromRegion(Texture * texture, FRect rectangle){return NULL;};
        
        virtual void SetRepeatTexture(Texture * texture){};

	protected:
		/// Дефолтная текстура.
		Texture * m_DefaultTexture; 
	};
}
#endif //NBG_CORE_TEXTURE_MANAGER_BASE

#ifndef NBG_CORE_TEXTURE_MANAGER_PNG_HELPER
#define NBG_CORE_TEXTURE_MANAGER_PNG_HELPER

namespace NBG
{
	namespace PNG_HELPER
	{
		/** @brief Помощник для работы с PNG.
		*
		* @author Vadim Simonov <akari.vs@gmail.com>
		* @copyright 2013-2014 New Bridge Games
		*
		*/
		//==========================================================================
		struct Image
		{
			png_uint_32 width;
			png_uint_32 height;
			png_int_32 bitDepth;
			png_int_32 colorType;
			png_bytep data;
		};

		//==========================================================================
		struct PNGBuffer
		{
			png_bytep data;
			png_uint_32 position;
		};

		//==========================================================================
		/// Прочитать PNG.
		void PNGRead(png_structp pngStruct, png_bytep data, png_size_t length);
		//==========================================================================
		/// Декодировать PNG.
		Image PNGDecode(png_bytep data, const bool SwapBGRA = true);
	}
}
#endif