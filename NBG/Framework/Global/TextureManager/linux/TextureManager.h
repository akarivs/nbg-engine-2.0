#ifndef NBG_CORE_TEXTURE_MANAGER_LINUX
#define NBG_CORE_TEXTURE_MANAGER_LINUX


#ifndef NBG_LINUX
	#error This file must not be included in non-android projects! Or you forgot to add NBG_LINUX directive :)
#endif

#include <string>
#include <map>

#include <core/Datatypes.h>
#include <core/Datatypes/Texture.h>
#include <core/ResourcesManager/TextureResource.h>

#include <core/Device.h>


#include "../BaseTextureManager.h"

namespace NBG
{
	class CTextureManager : public CBaseTextureManager
	{
	public:
		CTextureManager();
		~CTextureManager(){};

		virtual bool LoadTexture(CTextureResource * res);
		virtual void InitDefaultTexture();
		virtual void ReleaseTexture(Texture * texture);
	};
}
#endif //NBG_CORE_TEXTURE_MANAGER_LINUX
