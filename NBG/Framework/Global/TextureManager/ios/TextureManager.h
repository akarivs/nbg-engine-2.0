#ifndef NBG_CORE_TEXTURE_MANAGER_IOS
#define NBG_CORE_TEXTURE_MANAGER_IOS

//#include <pugixml.hpp>
#ifndef NBG_IOS
	#error This file must not be included in non-ios projects! Or you forgot to add NBG_IOS directive :)
#endif

#include <string>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Global/Resources/TextureResource.h>

#include "../BaseTextureManager.h"

namespace NBG
{
	class CTextureManager : public ITextureManager
	{
	public:
		CTextureManager();
		~CTextureManager(){};

		virtual bool LoadTexture(CTextureResource * res);
        virtual void InitDefaultTexture();
		virtual void ReleaseTexture(NBG::Texture *texture);
        virtual void SetRepeatTexture(Texture * texture);
	};
}
#endif
