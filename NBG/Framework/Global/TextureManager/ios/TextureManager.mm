#ifdef NBG_IOS
#include "TextureManager.h"
#include <Framework.h>
#include <../external/png/libpng/png.h>
#include <stdlib.h>

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#include <OpenGLES/ES2/gl.h>

#import <GLKit/GLKit.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

namespace NBG
{
	CTextureManager::CTextureManager()
	{
        m_DefaultTexture = NULL;
	}
    
    void CTextureManager::InitDefaultTexture()
    {
        m_DefaultTexture = new Texture();
        std::vector<unsigned char> textureData;
        
        int w = 32;
        int h = 32;
		
		m_DefaultTexture->SetSize(w,h);
		      
        GLuint _textureId;
        // Bind the texture
        
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &_textureId);
        // Bind the texture object
        glBindTexture(GL_TEXTURE_2D, _textureId);
        
        char * data = new char[w*h*4];
        memset(data,w*h*4,255);
        // Load the texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, data);
        
        // Set the filtering mode
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
       
        m_DefaultTexture->SetTextureData(new GLuint(_textureId));
		textureData.clear();
        delete[] data;
        
    }

    void
    sTexture_SizeToPow2(int &width, int &height)
    {
        int w = 1, h = 1;
        
        while (w < width) {w = w << 1;}
        while (h < height) {h = h << 1;}
        
        width = w;
        height = h;
    }
	bool CTextureManager::LoadTexture(CTextureResource * res)
	{
        std::vector<unsigned char> textureData;
        char * data = NULL;
        unsigned int w = 0;
        unsigned int h = 0;
        
        NSData *imageD = NULL;
        UIImage* image = NULL;

        
        
		std::string extension = NBG::StringUtils::GetExtension(res->GetPath());
        if (extension == "png")
        {
            PNG_HELPER::Image image = PNG_HELPER::PNGDecode((png_bytep)(res->GetData()), false);
			if( image.colorType == PNG_COLOR_TYPE_RGB )
			{
                CONSOLE("This is PNG without alpha channel, fix it! %s",res->GetPath().c_str());
                res->SetTexture(m_DefaultTexture);
                res->SetTextureWidth(m_DefaultTexture->GetSize().x);
                res->SetTextureHeight(m_DefaultTexture->GetSize().y);
                delete[] image.data;
                return false;
			}
			w = image.width;
			h = image.height;
            res->SetTextureWidth(w);
            res->SetTextureHeight(h);
            data = (char*)image.data;
        }
        else if (extension == "jpg")
        {
            NSData *imageD = [NSData dataWithBytesNoCopy:res->GetData() length:res->GetSize() freeWhenDone:NO] ;
            UIImage* image = [UIImage imageWithData:imageD];
            

            CGImageRef spriteImage;
            CGContextRef spriteContext;
            
            int	width_npot, height_npot, width_pot, height_pot;
            
            spriteImage = image.CGImage;
            
            width_pot = width_npot = CGImageGetWidth(spriteImage);
            height_pot = height_npot = CGImageGetHeight(spriteImage);
            
         
            sTexture_SizeToPow2(width_pot, height_pot);
            
            res->SetTextureWidth(width_npot);
            res->SetTextureHeight(height_npot);
            
            w = width_pot;
            h = height_pot;
            data = (char *) malloc(width_pot * height_pot * 4);
            
            spriteContext = CGBitmapContextCreate(data, width_pot, height_pot, 8, width_pot * 4, CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast);
            CGContextDrawImage(spriteContext, CGRectMake(0.0, 0.0, (CGFloat)width_pot, (CGFloat)height_pot), spriteImage);
            CGContextRelease(spriteContext);
            
        }
        
		
		
        
        GLuint _textureId;
        // Bind the texture
        
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &_textureId);
        // Bind the texture object
        glBindTexture(GL_TEXTURE_2D, _textureId);
        
        
        // Load the texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, data);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );        
       
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        
        
                        
        Texture * _texture = new Texture();
        _texture->SetSize(w,h);
        _texture->SetTextureData(new GLuint(_textureId));
		res->SetTexture(_texture);
		textureData.clear();
        if (extension == "jpg")
        {
            //[image release];
            //[imageD release];
            free(data);
        }
        else
        {
            free(data);
        }
       	return true;
 
	}
    
    void CTextureManager::ReleaseTexture(NBG::Texture *texture)
    {
        glDeleteTextures(1,(GLuint*)texture->GetTextureData());
    }
    
    void CTextureManager::SetRepeatTexture(Texture * texture)
    {
        GLuint * textureData = (GLuint*)texture->GetTextureData();
        glBindTexture(GL_TEXTURE_2D, *textureData);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    
    
}
#endif //NBG_WINDOWS
