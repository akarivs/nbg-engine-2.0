#include "BaseTextureManager.h"
#include <cstring>

namespace NBG
{
	namespace PNG_HELPER
	{
	//==========================================================================
	void PNGRead(png_structp pngStruct, png_bytep data, png_size_t length)
	{
		PNGBuffer* pngBuffer = (PNGBuffer*)(png_get_io_ptr(pngStruct));
		memcpy(data, pngBuffer->data + pngBuffer->position, length);
		pngBuffer->position += length;
	}

	//==========================================================================
	Image PNGDecode(png_bytep data, const bool SwapBGRA)
	{
		png_structp pngStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
		png_infop   pngInfo = png_create_info_struct(pngStruct);
		PNGBuffer   pngBuffer = {data, 8};

		png_set_read_fn(pngStruct, (void*)&pngBuffer, PNGRead);
		png_set_sig_bytes(pngStruct, 8);
		png_read_info(pngStruct, pngInfo);

		Image image;
		png_get_IHDR(pngStruct, pngInfo, &image.width, &image.height, &image.bitDepth, &image.colorType, 0, 0, 0);

		// преобразование цвета
		if(image.bitDepth == 16) png_set_strip_16(pngStruct);
		switch(image.colorType)
		{
			case PNG_COLOR_TYPE_GRAY:
				if(image.bitDepth < 8) png_set_expand_gray_1_2_4_to_8(pngStruct);
				png_set_gray_to_rgb(pngStruct);
				break;

			case PNG_COLOR_TYPE_GRAY_ALPHA:
				png_set_gray_to_rgb(pngStruct);
				break;

			case PNG_COLOR_TYPE_PALETTE:
				png_set_palette_to_rgb(pngStruct);
				break;
		}
		if(png_get_valid(pngStruct, pngInfo, PNG_INFO_tRNS) != 0) png_set_tRNS_to_alpha(pngStruct);		

		png_read_update_info(pngStruct, pngInfo);
        if (SwapBGRA)
        {
            png_set_bgr(pngStruct);
        }
		png_get_IHDR(pngStruct, pngInfo, &image.width, &image.height, &image.bitDepth, &image.colorType, 0, 0, 0);

		// чтение изображения
		png_uint_32 bytesInRow = png_get_rowbytes(pngStruct, pngInfo);
		png_bytepp rowPointer = new png_bytep[image.height];
		image.data = new png_byte[bytesInRow * image.height];

		for(png_uint_32 i = 0; i < image.height; ++i) rowPointer[i] = image.data + i * bytesInRow;
		png_read_image(pngStruct, rowPointer);

		png_destroy_read_struct(&pngStruct, &pngInfo, 0);
		delete[] rowPointer;
		return image;
	}
}
}
