#ifdef NBG_ANDROID
#include "TextureManager.h"
#include <Framework.h>

namespace NBG
{
	CTextureManager::CTextureManager()
	{
	    m_DefaultTexture = NULL;
	}

	void CTextureManager::InitDefaultTexture()
	{
		m_DefaultTexture = new Texture();
		int w = 32;
		int h = 32;
		m_DefaultTexture->SetSize(w,h);
		
		GLuint _textureId;
		// Bind the texture

		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &_textureId);
		// Bind the texture object
		glBindTexture(GL_TEXTURE_2D, _textureId);

		char * data = new char[w*h*4];
		memset(data,w*h*4,255);
		// Load the texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, data);

		// Set the filtering mode
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		m_DefaultTexture->SetTextureData(new GLuint(_textureId));		
		delete[] data;
	}	

	bool CTextureManager::LoadTexture(CTextureResource * res)
	{
		glEnable(GL_TEXTURE_2D);
		std::vector<unsigned char> textureData;
		char * data = NULL;
		unsigned int w = 0;
		unsigned int h = 0;

		std::string extension = NBG::StringUtils::GetExtension(res->GetPath());
		if (extension == "png")
		{
			PNG_HELPER::Image image = PNG_HELPER::PNGDecode((png_bytep)(res->GetData()), false);
			if( image.colorType == PNG_COLOR_TYPE_RGB )
			{
				CONSOLE("This is PNG without alpha channel, fix it! %s",res->GetPath().c_str());
				res->SetTexture(m_DefaultTexture);
				res->SetTextureWidth(m_DefaultTexture->GetSize().x);
				res->SetTextureHeight(m_DefaultTexture->GetSize().y);
				delete[] image.data;
				return false;
			}
			w = image.width;
			h = image.height;
			res->SetTextureWidth(w);
			res->SetTextureHeight(h);
			data = (char*)image.data;
		}
		else if (extension == "jpg")
		{
			
		}



		GLuint * _textureId = new GLuint();
		glGenTextures(1, _textureId);		
		glBindTexture(GL_TEXTURE_2D, *_textureId);	


	

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// Load the texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
			GL_UNSIGNED_BYTE, data);


		Texture * _texture = new Texture();
		_texture->SetSize(w,h);
		_texture->SetTextureData(_textureId);
		res->SetTexture(_texture);
		textureData.clear();
		if (extension == "jpg")
		{
			free(data);
		}
		else
		{
			free(data);
		}		
		return true;
	}

	void CTextureManager::ReleaseTexture(Texture * texture)
	{		

	}
}
#endif
