#ifndef NBG_CORE_TEXTURE_MANAGER_ANDROID
#define NBG_CORE_TEXTURE_MANAGER_ANDROID


#ifdef NBG_ANDROID


#include <string>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Global/Resources/TextureResource.h>

#include "../BaseTextureManager.h"

namespace NBG
{
	class CTextureManager : public ITextureManager
	{
	public:
		CTextureManager();
		~CTextureManager(){};

		virtual bool LoadTexture(CTextureResource * res);
		virtual void InitDefaultTexture();
		virtual void ReleaseTexture(Texture * texture);
	};
}
#endif
#endif //NBG_CORE_TEXTURE_MANAGER_ANDROID
