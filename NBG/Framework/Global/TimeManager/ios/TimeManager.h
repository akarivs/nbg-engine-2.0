#ifndef NBG_CORE_TIME_MANAGER_IOS
#define NBG_CORE_TIME_MANAGER_IOS

#ifdef NBG_IOS


#include "../BaseTimeManager.h"

namespace NBG
{
	class CTimeManager : public ITimeManager
	{
	public:
		CTimeManager();
		~CTimeManager();
		virtual void Init();
		virtual double Reset();
		virtual double ReadTimer();
		virtual double GetTimeInterval();
	};
}



#endif
#endif //NBG_CORE_TIME_MANAGER_IOS
