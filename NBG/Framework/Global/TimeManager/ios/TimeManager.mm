#ifdef NBG_IOS
#include "TimeManager.h"

#import <QuartzCore/QuartzCore.h>
#include <Framework.h>

namespace NBG
{
	CTimeManager::CTimeManager()
	{

	}
	CTimeManager::~CTimeManager()
	{
	}

	void CTimeManager::Init()
	{
	    m_StartTime =  ReadTimer();
	}

	double CTimeManager::Reset()
	{
        double val = GetTimeInterval();
        m_StartTime = ReadTimer();
        return val;
	}

	double CTimeManager::GetTimeInterval()
	{
        double frameDuration = ReadTimer() - m_StartTime;
		return frameDuration;
	}

	double CTimeManager::ReadTimer()
	{
		return CACurrentMediaTime();
	}
}
#endif
