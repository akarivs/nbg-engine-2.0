#ifndef NBG_CORE_TIME_MANAGER_LINUX
#define NBG_CORE_TIME_MANAGER_LINUX

#ifndef NBG_LINUX
	#error This file must not be included in non-android projects! Or you forgot to add NBG_LINUX directive :)
#endif

#include "../BaseTimeManager.h"

namespace NBG
{
	class CTimeManager : public CBaseTimeManager
	{
	public:
		CTimeManager();
		~CTimeManager();
		virtual void Init();
		virtual double Reset();
		virtual double ReadTimer();
		virtual double GetTimeInterval();
	};
}



#endif //NBG_CORE_TIME_MANAGER_LINUX
