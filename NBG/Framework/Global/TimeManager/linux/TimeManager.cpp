#ifdef NBG_LINUX

#include "TimeManager.h"

namespace NBG
{
	CTimeManager::CTimeManager()
	{

	}
	CTimeManager::~CTimeManager()
	{

	}

	void CTimeManager::Init()
	{
		m_StartTime = 0.0f;
	}

	double CTimeManager::Reset()
	{
		return 0.033f;
	}

	double CTimeManager::GetTimeInterval()
	{
	        return ReadTimer() - m_StartTime;
	}

	double CTimeManager::ReadTimer()
	{
		return 0.33f;
	}
}
#endif
