#ifndef NBG_CORE_TIME_MANAGER_WIN_32
#define NBG_CORE_TIME_MANAGER_WIN_32

#ifdef NBG_WIN32

#include "../BaseTimeManager.h"

namespace NBG
{
	/** @brief Менеджер работы со временем в Windows.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CTimeManager : public ITimeManager
	{
	public:
		/// @name Конструкторы.
		CTimeManager();
		~CTimeManager();

		/// Инициализация.
		virtual void Init();

		/// Сбросить таймер и получить интервал.
		virtual double Reset();

		/// Прочитать значение таймера.
		virtual double ReadTimer();

		/// Получить интервал.
		virtual double GetTimeInterval();
	};
}
#endif
#endif //NBG_CORE_TIME_MANAGER_WIN_32
