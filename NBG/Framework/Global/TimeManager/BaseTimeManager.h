#ifndef NBG_CORE_TIME_MANAGER_BASE
#define NBG_CORE_TIME_MANAGER_BASE

namespace NBG
{
	/** @brief Базовый менеджер работы со временем. 
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class ITimeManager
	{
	public:
		/// Инициализация
		virtual void Init() = 0;
		
		/// Сбросить и получить предыдущее значение.
		virtual double Reset() = 0;

		/// Прочитать значение таймера.
		virtual double ReadTimer() = 0;

		/// Получить предыдущий временной интервал.
		virtual double GetTimeInterval() = 0;
	protected:
		/// Предыдущее время, от которого считать прошедшее время.
		double m_StartTime;
	};
}
#endif //#ifndef NBG_CORE_TIME_MANAGER_BASE
