#ifndef NBG_CORE_TIME_MANAGER_ANDROID
#define NBG_CORE_TIME_MANAGER_ANDROID

#ifdef NBG_ANDROID

#include "../BaseTimeManager.h"

namespace NBG
{
	class CTimeManager : public ITimeManager
	{
	public:
		CTimeManager();
		~CTimeManager();
		virtual void Init();
		virtual double Reset();
		virtual double ReadTimer();
		virtual double GetTimeInterval();
	};
}     
#endif
#endif //NBG_CORE_TIME_MANAGER_ANDROID
