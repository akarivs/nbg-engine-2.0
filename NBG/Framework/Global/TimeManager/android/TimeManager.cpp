#ifdef NBG_ANDROID

#include "TimeManager.h"
#include <time.h>

namespace NBG
{
	CTimeManager::CTimeManager()
	{

	}
	CTimeManager::~CTimeManager()
	{
		
	}

	void CTimeManager::Init()
	{
		m_StartTime = ReadTimer();
	}

	double CTimeManager::Reset()
	{
		double val = GetTimeInterval();
		m_StartTime = ReadTimer();
		return val * 0.001f;
	}

	double CTimeManager::GetTimeInterval()
	{
	        return ReadTimer() - m_StartTime;
	}

	double CTimeManager::ReadTimer()
	{
		struct timespec res;
		clock_gettime(CLOCK_REALTIME, &res);
		return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
	}
}
#endif
