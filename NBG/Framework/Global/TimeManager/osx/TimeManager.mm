#ifdef NBG_OSX
#include "TimeManager.h"
#include <Framework.h>

namespace NBG
{
	CTimeManager::CTimeManager()
	{

	}
	CTimeManager::~CTimeManager()
	{
	}

	void CTimeManager::Init()
	{
	    m_StartTime =  ReadTimer();
	}

	double CTimeManager::Reset()
	{
        double val = GetTimeInterval();
        m_StartTime = ReadTimer();
		return val*0.001f;
	}

	double CTimeManager::GetTimeInterval()
	{
        double frameDuration = ReadTimer() - m_StartTime;
		return frameDuration;
	}

	double CTimeManager::ReadTimer()
	{       
		return SDL_GetTicks();
	}
}
#endif
