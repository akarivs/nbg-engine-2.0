#ifndef NBG_CORE_XML_RESOURCE
#define NBG_CORE_XML_RESOURCE

#include "IResource.h"
#include <../xml/pugixml.hpp>

namespace NBG
{
	/** @brief Текстурный ресурс.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CXMLResource: public IResource
	{
	public:
		/// @name Конструкторы.
		CXMLResource(void);
		virtual ~CXMLResource(void);

		/// Получение указателя на pugi::xml документ.
		pugi::xml_document * GetXML(){return m_XMLDoc;};

		/// Загрузка XML
		virtual bool Load(const std::string filePath);
	private:
		/// pugi::xml документ.
		pugi::xml_document * m_XMLDoc;
	};
}
#endif //NBG_CORE_XML_RESOURCE
