#ifndef NBG_CORE_I_RESOURCE
#define NBG_CORE_I_RESOURCE

#include <Framework/Datatypes.h>


namespace NBG
{
	/** @brief Базовый ресурс.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class IResource
	{
	public:
		/// @name Конструкторы
		IResource(void);
		virtual ~IResource(void);

		/// Получить количество использований ресурса.
		int GetRefsCount();
		/// Увеличеть количество использований ресурса.
		void IncreaseRefsCount();
		/// Уменьшить количество использований ресурса.
		void DecreaseRefsCount();

		/// Получить данные ресурса.
		char * GetData() {return m_Data;};
		/// Получение размера ресурса.
		int GetSize() {return m_Size;};

		/// Установить данные ресурса.
		void SetData(char * data) {m_Data = data;};
		/// Получение размера ресурса.
		void  SetSize(const int size) {m_Size = size;};



		/// DEPRECATED: Получить ID ресурса. 
		std::string GetId(){return m_Id;};
		/// Получить путь к ресурсу.
		std::string GetPath(){return m_FilePath;};


		FILE * GetFilePointer()
		{
			return m_File;
		};

		/// Загрузка ресурса.
		virtual bool Load(const std::string filePath);
	protected:
		/// Количество ссылок на объект.
		int m_RefsCount; 

		/// Данные ресурса
		char* m_Data; 

		/// Размер ресурса в байтах
		int m_Size; 

		///DEPRECATED: ID ресурса
		std::string m_Id; 
		
		//Путь к ресурсу
		std::string m_FilePath; 

		FILE * m_File;
	};
}
#endif // NBG_CORE_I_RESOURCE
