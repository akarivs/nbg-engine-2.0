#include "XMLResource.h"

namespace NBG
{
	//==============================================================================
	CXMLResource::CXMLResource(void):IResource()
	{
		m_XMLDoc = NULL;
	}

	//==============================================================================
	CXMLResource::~CXMLResource(void)
	{
		if (m_XMLDoc)
		{
			delete m_XMLDoc;
			m_XMLDoc = NULL;
		}
	}

	//==============================================================================s
	bool CXMLResource::Load(const std::string filePath)
	{
		m_XMLDoc = new pugi::xml_document();
		if (IResource::Load(filePath))
		{
			if (m_Data == NULL)
			{
				m_Data = new char[m_Size];
				fread(m_Data,m_Size,1,m_File);			
				fclose(m_File);
			}
			bool res = m_XMLDoc->load_buffer(GetData(),GetSize());			
			return res;
		}
		return false;

	}
}
