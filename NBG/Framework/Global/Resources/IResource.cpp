#include "IResource.h"

#include <Framework.h>
#include <Framework/Datatypes.h>
#include <Framework/Utils/StringUtils.h>

#ifdef NBG_ANDROID
#include <jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <Framework/Global/OperatingSystem/android/OperatingSystem.h>
#endif


namespace NBG
{
	//==============================================================================
	IResource::IResource(void)
	{
		m_RefsCount = 0;
		m_Data = NULL;
		m_Size = 0;
	}

	//==============================================================================
	IResource::~IResource(void)
	{
		if (m_Data)
		{
			delete m_Data;
		}
	}

	//==============================================================================
	int IResource::GetRefsCount()
	{
		return m_RefsCount;
	}

	//==============================================================================
	void IResource::IncreaseRefsCount()
	{
		m_RefsCount++;
	}

	//==============================================================================
	void IResource::DecreaseRefsCount()
	{
		m_RefsCount--;
		if (m_RefsCount < 0)
		{
			m_RefsCount = 0;
			CONSOLE("%s - something try to unsubscribe unused resource!", m_Id.c_str());
		}
	}

	//==============================================================================
	bool IResource::Load(const std::string filePath)
	{
		m_FilePath = filePath;
		m_Id = filePath;

		IFileSystem::FileContainer fc = g_FileSystem->ReadFile(filePath,"rb");
		if (fc.status != IFileSystem::FileOk)
		{
			return false;
		}
		m_Data = fc.data;
		m_Size = fc.size;		
		return true;
	}
}
