#include "TextureResource.h"

#include <Framework.h>
#include <Framework/Datatypes.h>
#include <Framework/Global/Render.h>

#include <Framework/Utils/StringUtils.h>

namespace NBG
{
	CTextureResource * CTextureResource::m_DefaultTexture = NULL;
	//==============================================================================
	CTextureResource * CTextureResource::GetDefaultTexture()
	{
		if (!CTextureResource::m_DefaultTexture)
		{
			Texture * texture = g_GameApplication->GetTextureManager()->GetDefaultTexture();					

			CTextureResource::m_DefaultTexture = new CTextureResource();
			CTextureResource::m_DefaultTexture->SetTexture(texture);
			CTextureResource::m_DefaultTexture->SetTextureWidth(32);
			CTextureResource::m_DefaultTexture->SetTextureHeight(32);
		}
		return CTextureResource::m_DefaultTexture;
	}

	//==============================================================================
	CTextureResource::CTextureResource(void):IResource()
	{
		m_Texture = NULL;
	}

	//==============================================================================
	CTextureResource::~CTextureResource(void)
	{    
		///Если пытаемся удалить ресурс, который является ресурсом по умолчанию - шлём в сад.
		if (CTextureResource::GetDefaultTexture()->GetTexture() == m_Texture)return;
		g_Render->ReleaseTexture(m_Texture);
	}

	//==============================================================================
	bool CTextureResource::Load(std::string filePath)
	{
		if (!IResource::Load(filePath))
		{	
			Texture * texture = GetDefaultTexture()->GetTexture();		
			SetTexture(texture);
			SetTextureWidth(texture->GetSize().x);
			SetTextureHeight(texture->GetSize().y);		
		}
		else
		{	
			if (m_Data == NULL)
			{
				m_Data = new char[m_Size];
				fread(m_Data,m_Size,1,m_File);
				fclose(m_File);
			}			
			return g_GameApplication->GetTextureManager()->LoadTexture(this);
		}
		return false;
	}

	//==============================================================================
	void CTextureResource::SetTexture(Texture * texture)
	{		
		m_Texture = texture;
		///После того, как подгрузили текстуру в видео-память, больше нет нужды держать её в памяти.	 
		if (m_Data)
		{
			delete[] m_Data;
			m_Data = NULL;
		}
	}
}
