#ifndef NBG_CORE_TEXTURE_RESOURCE
#define NBG_CORE_TEXTURE_RESOURCE

#include "IResource.h"
#include <Framework/Datatypes/Texture.h>

namespace NBG
{
	/** @brief Текстурный ресурс.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CTextureResource: public IResource
	{
	public:
		/// @name Конструкторы.
		CTextureResource(void);
		virtual ~CTextureResource(void);

		/// Установка ширины текстуры.
		void SetTextureWidth(const int width) { m_TextureWidth = width; };
		
		/// Установка высоты текстуры.
		void SetTextureHeight(const int height) { m_TextureHeight = height; };

		/// Установка текстуры.
		void SetTexture(Texture * texture);

		/// Получение текстуры по умолчании. 
		static CTextureResource * GetDefaultTexture();

		/// Получение ширины текстуры.
		int GetTextureWidth() {return m_TextureWidth;};

		/// Получение высоты текстуры.
		int GetTextureHeight() {return m_TextureHeight;};

		/// Получение текстуры.
		Texture * GetTexture(){return m_Texture;};

		/// Загрузка текстуры.
		virtual bool Load(const std::string filePath);
	private:
		/// Текстура по умолчанию.
		static CTextureResource * m_DefaultTexture;

		/// Текстура.
		Texture * m_Texture;

		/// Ширина текстуры.
		int m_TextureWidth;

		/// Высота текстуры.
		int m_TextureHeight;
	};
}

#endif //NBG_CORE_TEXTURE_RESOURCE
