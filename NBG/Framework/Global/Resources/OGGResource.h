#ifndef NBG_CORE_OGG_RESOURCE
#define NBG_CORE_OGG_RESOURCE

#include "IResource.h"

namespace NBG
{
	/** @brief OGG Ресурс.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class COGGResource: public IResource
	{
	public:
		/// @name Конструкторы
		COGGResource(void);
		virtual ~COGGResource(void);	

		///Загрузка ресурса
		virtual bool Load(const std::string filePath);
	private:

	};
}

#endif //NBG_CORE_OGG_RESOURCE
