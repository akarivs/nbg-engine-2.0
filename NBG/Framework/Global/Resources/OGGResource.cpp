#include "OGGResource.h"

#include <Framework.h>

namespace NBG
{
	//==============================================================================
	COGGResource::COGGResource(void):IResource()
	{

	}

	//==============================================================================
	COGGResource::~COGGResource(void)
	{
	}

	//==============================================================================
	bool COGGResource::Load(const std::string filePath)
	{  
		if (IResource::Load(filePath))
		{
			if (m_Data == NULL)
			{
				m_Data = new char[m_Size];
				fread(m_Data,m_Size,1,m_File);
				fclose(m_File);
			}			
			return true;
		}
		return false;

	}
}
