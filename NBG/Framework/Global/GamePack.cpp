#include "GamePack.h"


#include <Framework.h>

namespace NBG
{
	//==================================================================================
	CGamePack::CGamePack(void)
	{
		m_PackExists = false;
		m_FilePointer = NULL;
	}

	//==================================================================================
	CGamePack::~CGamePack(void)
	{
		if (m_FilePointer)
		{
			fclose(m_FilePointer);
		}
	}

	//==================================================================================
	void CGamePack::Init()
	{
		///��������� ������������� ���� ����.
		m_FilePointer = fopen(g_EditionHelper->ConvertPath("game.pack").c_str(), "rb");
		if (!m_FilePointer)
		{        
			m_FilePointer = fopen("game.pack", "rb");
			if (!m_FilePointer)
			{
				return;
			}
		}
		m_PackExists = true;    
		char header[8];
		fread(&header[0],8,1,m_FilePointer);
		if (std::string(header).find("NBGPACK") == std::string::npos)
		{
			fclose(m_FilePointer);
			CONSOLE("This is dont NBG Game Pack!");
			return;
		}
		int version;
		fread(&version,sizeof(int),1,m_FilePointer);
		if (version != 1)
		{
			fclose(m_FilePointer);
			CONSOLE("Pack version isn't 1!");
			return;
		}
		int headerSize;
		fread(&headerSize,sizeof(int),1,m_FilePointer);	
		while (headerSize>0)
		{
			char fileName[256];
			fread(&fileName[0],256,1,m_FilePointer);			
			PackedInfo info;	
			fread(&info.size,sizeof(int),1,m_FilePointer);		
			fread(&info.offset,sizeof(int),1,m_FilePointer);
			std::string fn = fileName;
			fn = fn.substr(0,fn.length()-1);
			fn = StringUtils::StringReplace(fn,std::string("\\"),std::string("/"),true);			
			m_FileData[fn] = info;
			headerSize -= 256+sizeof(int)+sizeof(int);
		}	

		int directoryXMLSize = 0;
		fread(&directoryXMLSize,sizeof(int),1,m_FilePointer);	
		char * xml = new char[directoryXMLSize];
		fread(xml,directoryXMLSize,1,m_FilePointer);
		pugi::xml_document doc;
		doc.load_buffer(xml,directoryXMLSize);	
		for (pugi::xml_node dir = doc.first_child().child("directory"); dir; dir = dir.next_sibling("directory"))
		{
			std::vector<std::string> * dirVector = &m_Directories[dir.attribute("name").value()];		
			for (pugi::xml_node file = dir.first_child().child("file"); file; file = file.next_sibling("file"))
			{	
				std::string fn = file.attribute("name").value();
				fn = StringUtils::StringReplace(fn,std::string("\\"),std::string("/"),true);			
				dirVector->push_back(fn);
			}
		}
	}

	//==================================================================================
	CFileSystem::FileContainer CGamePack::ReadFile(const std::string &filename)
	{
		CFileSystem::FileContainer fc;
		fc.status = CFileSystem::FileNotFound;
		fc.data = NULL;
		fc.size = 0;

		if (!m_PackExists)return fc;

		if (m_FileData.find(filename) == m_FileData.end())
		{
			return fc;
		}
		PackedInfo pi = m_FileData[filename];
		fc.status = CFileSystem::FileOk;
		fc.size = pi.size;


		fseek(m_FilePointer,pi.offset,SEEK_SET);
		char *buff = new char[fc.size];
		fread(buff,fc.size,1,m_FilePointer);
		fc.data = buff;
		return fc;
	}

	//==================================================================================
	std::vector<std::string> CGamePack::GetDirectoryContents(const std::string &path)
	{	
		std::vector<std::string>res;
		if (!m_PackExists)return res;
		std::string correctPath = path;
		//if (correctPath.substr(correctPath.length()-1,1) != "/")correctPath+="/";
		if (m_Directories.find(correctPath) == m_Directories.end())
		{
			return res;
		}
		return m_Directories[correctPath];
	}
}