#ifndef NBG_CORE_RESOURCES_MANAGER
#define NBG_CORE_RESOURCES_MANAGER

#include <map>

#include <Framework/Global/Resources/IResource.h>

namespace NBG
{
	/// Типы ресурсов.
	enum ResourceType
	{
		RS_TEXTURE,
		RS_SOUND,
		RS_VIDEO,
		RS_XML,
		RS_UNDEFINED
	};


	/** @brief Класс, реализующий работу с ресурсами.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>	
	* @copyright 2013-2014 New Bridge Games
	*
	*/	
	class CResourcesManager
	{
	public:
		/// @name Конструкторы		
		CResourcesManager();
		~CResourcesManager();

		///Инициализация
		void Init();

		/// Получить ресурс по пути.
		IResource* GetResource(const std::string file_name);

		/// Добавить статически созданный ресурс
		void AddResource(const std::string &name, IResource * resource);

		/// Освободить ресурс по ID.
		void ReleaseResource(const std::string id);

		/// Освободить ресурс по указателю на ресурс.
		void ReleaseResource(IResource * resource);

		/// Освободить все неиспользуемые ресурсы.
		void ReleaseUnusedResources();	

		/// Дамп всех используемых ресурсы
		void DumpResources();

		/// Перезагрузить текстуры (нужно на некоторых платформах)
		void ReloadTextures();
	private:				

		/// Карта ресурсов.
		std::map<std::string, IResource* >m_Resources;

		/// Итератор для карты ресурсов.
		std::map<std::string, IResource* >::iterator m_ResourcesIter;		

		/// Получить тип файла по его полному пути.
		ResourceType GetTypeByExtension(const std::string file_name);
	};
}
#endif //NBG_CORE_RESOURCES_MANAGER
