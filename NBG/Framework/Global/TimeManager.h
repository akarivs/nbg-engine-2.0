#ifndef NBG_CORE_TIME_MANAGER
#define NBG_CORE_TIME_MANAGER

#include "TimeManager/BaseTimeManager.h"
#ifdef NBG_WIN32
    #include "TimeManager/win32/TimeManager.h"
#elif defined(NBG_IOS)
    #include "TimeManager/ios/TimeManager.h"
#elif defined(NBG_OSX)
    #include "TimeManager/osx/TimeManager.h"
#elif defined(NBG_ANDROID)
    #include "TimeManager/android/TimeManager.h"
#elif defined(NBG_LINUX)
    #include "TimeManager/linux/TimeManager.h"
#endif

#endif //NBG_CORE_TIME_MANAGER
