#ifdef NBG_WIN32

#include "OperatingSystem.h"
#include <Framework.h>
#include <windowsx.h>


#include "../../../Project.Win32/resource1.h"


#include "DbgHelp.h"
#include <eh.h>
#include <Psapi.h>
#include <string>
#include <sstream>
#include <fstream>



namespace NBG
{
	//==============================================================================
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{	
		wchar_t buffer[1];
		BYTE lpKeyState[256];

		switch (message)
		{
		case WM_KEYDOWN:
			GetKeyboardState(lpKeyState);
			if (ToUnicode(wParam, HIWORD(lParam)&0xFF, lpKeyState, buffer, 1, 0) > 0)
			{			
				g_Input->SetLastInputChar(buffer[0]);
			}
			g_GameApplication->OnKeyDown((int)wParam);
			g_Input->SetKeyDown(wParam, true);			
			break;
		case WM_CHAR:

			break;
		case WM_MOUSEMOVE:			
			g_Input->SetMousePos((float)GET_X_LPARAM(lParam), (float)GET_Y_LPARAM(lParam));			
			g_GameApplication->OnMouseMove();
			break;
		case WM_LBUTTONDOWN:
			g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);			
			break;
		case WM_LBUTTONUP:
			g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);
			break;
		case WM_RBUTTONDOWN:
			g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_RIGHT);
			break;
		case WM_RBUTTONUP:
			g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_RIGHT);
			break;
		case WM_KEYUP:
			g_GameApplication->OnKeyUp((int)wParam);
			g_Input->SetKeyDown(wParam, false);		
			break;
		case WM_NCLBUTTONDBLCLK:
			g_GameApplication->SetFullscreen(true);
			break;
		case WM_ACTIVATEAPP:
			if(wParam == false)
			{		
				g_GameApplication->Pause();
				g_SoundManager->OnLostFocus();				
				g_Render->CheckDevice();	
			}
			else if(wParam == TRUE)
			{	
				g_GameApplication->Resume();
				g_SoundManager->OnGetFocus();				
				g_Render->CheckDevice();								
				g_GameApplication->OnAfterRenderChange();
				g_GameApplication->GetTimeManager()->Reset();
			}
			break;	
		case WM_ENTERSIZEMOVE:			
			g_GameApplication->Pause();
			g_SoundManager->OnLostFocus();
			g_Render->CheckDevice();			
			break;
		case WM_SIZE:
			if (wParam == SIZE_MAXIMIZED)
			{
				g_GameApplication->SetFullscreen(true);
			}			
			break;
		case WM_EXITSIZEMOVE:			
			g_GameApplication->Resume();
			g_SoundManager->OnGetFocus();
			g_Render->CheckDevice();									
			g_GameApplication->GetTimeManager()->Reset();
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
		case WM_CLOSE:			
			g_GameApplication->StopApp();
			break;
		case WM_DESTROY:
			g_GameApplication->StopApp();
			break;
		default:		
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return false;
	}
	
	//==============================================================================
	COperatingSystem::COperatingSystem()
	{
	
	}

	//==============================================================================
	COperatingSystem::~COperatingSystem()
	{
		
	}	

	//==============================================================================
	bool COperatingSystem::CreateAppWindow()
	{
		// Register the window class.
		const wchar_t *CLASS_NAME  = new wchar_t();
		CLASS_NAME = L"NewBridgeGameEngineClass";

		RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values

		WindowRect.left=(long)0;									// Set Left Value To 0
		WindowRect.right=(long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
		WindowRect.top=(long)0;										// Set Top Value To 0
		WindowRect.bottom=(long)(int)g_System->GetSystemResolution().y;								// Set Bottom Value To Requested Height

		bool fullScreen = g_GameApplication->IsFullscreen();		

		WNDCLASSEXW wc = { };

		wc.cbSize = sizeof(WNDCLASSEX);
		wc.lpfnWndProc   = WndProc;
		wc.style		 = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.hInstance     = m_hInstance;
		wc.lpszClassName = (LPCWSTR)CLASS_NAME;
		wc.cbClsExtra		= 0;
		wc.cbWndExtra		= 0;
		wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground	= NULL;
		wc.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON1));
		wc.hIconSm = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON2));
		

		DWORD exS = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		DWORD exWS = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS |	WS_CLIPCHILDREN;
		exWS ^= WS_THICKFRAME;

		if (fullScreen)
		{
			exS = 0;
			exWS = WS_EX_TOPMOST | WS_POPUP;
		}

		AdjustWindowRectEx(&WindowRect, exWS, false, exS);	// Adjust Window To True Requested Size

		RegisterClassEx(&wc);
		HWND hwnd = CreateWindowExW(
			exS,                              // Optional window styles.
			(LPCWSTR)CLASS_NAME,             // Window class
			L"NBG: Game Engine",     // Window text
			exWS,            // Window style
			// Size and position
			0, 0, WindowRect.right-WindowRect.left, WindowRect.bottom-WindowRect.top,
			NULL,       // Parent window
			NULL,       // Menu
			m_hInstance,  // Instance handle
			NULL        // Additional application data
			);

		if (hwnd == NULL)
		{
			return false;
		}
		m_HWND = hwnd;


		SetWindowPos( m_HWND, HWND_NOTOPMOST
			,(GetSystemMetrics(SM_CXSCREEN)/2.0f - WindowRect.right/2.0f) ,(GetSystemMetrics(SM_CYSCREEN)/2.0f - WindowRect.bottom/2.0f),
			WindowRect.right-WindowRect.left,
			WindowRect.bottom-WindowRect.top,
			SWP_SHOWWINDOW );

		ShowWindow(hwnd,SW_NORMAL);
		UpdateWindow(hwnd);

		ShowCursor(false);
		ShowCursor(true);

		return true;
	}

	//==============================================================================
	void COperatingSystem::SetTitle(const std::wstring title)
	{
		SetWindowTextW(m_HWND,(LPCWSTR)&title.c_str()[0]);
	}

	//==============================================================================
	void COperatingSystem::SetFullscreenMode(bool isFullscreen)
	{		
		Vector windowSize;
		if (isFullscreen)
		{
			windowSize.x = GetSystemMetrics(SM_CXSCREEN);
			windowSize.y = GetSystemMetrics(SM_CYSCREEN);
		}
		else
		{
			windowSize = g_GameApplication->GetWindowSize();
		}
		g_System->UpdateSize(windowSize, g_GameApplication->GetWindowSize(),isFullscreen);
		
		if (isFullscreen)
		{
			SetWindowLong(m_HWND, GWL_EXSTYLE, 0);
			SetWindowLong(m_HWND, GWL_STYLE,  WS_EX_TOPMOST | WS_POPUP);

			RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values
			WindowRect.left=(long)0;									// Set Left Value To 0
			WindowRect.right=(long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
			WindowRect.top=(long)0;										// Set Top Value To 0
			WindowRect.bottom=(long)(int)g_System->GetSystemResolution().y;					// Set Bottom Value To Requested Height			
			DWORD exS = 0;
			DWORD exWS = WS_EX_TOPMOST | WS_POPUP;
			AdjustWindowRectEx(&WindowRect, exWS, FALSE, exS);	// Adjust Window To True Requested Size			
			SetWindowPos( m_HWND, HWND_TOPMOST
				,(GetSystemMetrics(SM_CXSCREEN)/2.0f - WindowRect.right/2.0f) ,(GetSystemMetrics(SM_CYSCREEN)/2.0f - WindowRect.bottom/2.0f),
				WindowRect.right-WindowRect.left,
				WindowRect.bottom-WindowRect.top, SWP_SHOWWINDOW );
		}
		else
		{
			DWORD exS = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
			DWORD exWS = (WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS |	WS_CLIPCHILDREN) &~(WS_THICKFRAME) ;

			SetWindowLong(m_HWND, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_WINDOWEDGE);
			SetWindowLong(m_HWND, GWL_STYLE,exWS);
			RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values
			WindowRect.left=(long)0;									// Set Left Value To 0
			WindowRect.right=(long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
			WindowRect.top=(long)0;										// Set Top Value To 0
			WindowRect.bottom=(long)(int)g_System->GetSystemResolution().y;					// Set Bottom Value To Requested Height

			AdjustWindowRectEx(&WindowRect, exWS, FALSE, exS);	// Adjust Window To True Requested Size

			SetWindowPos( m_HWND, HWND_NOTOPMOST
				,(GetSystemMetrics(SM_CXSCREEN)/2.0f - (WindowRect.right-WindowRect.left)/2.0f) ,(GetSystemMetrics(SM_CYSCREEN)/2.0f - (WindowRect.bottom-WindowRect.top)/2.0f),
				WindowRect.right-WindowRect.left,
				WindowRect.bottom-WindowRect.top, SWP_SHOWWINDOW );

			RECT clienRect;
			GetClientRect(m_HWND,&clienRect);

			///Some magic to adjust window size
			Vector diff = g_System->GetSystemResolution()-Vector(clienRect.right - clienRect.left,clienRect.bottom - clienRect.top	);		
			SetWindowPos( m_HWND, HWND_NOTOPMOST
				,(GetSystemMetrics(SM_CXSCREEN)/2.0f - (WindowRect.right-WindowRect.left)/2.0f) ,(GetSystemMetrics(SM_CYSCREEN)/2.0f - (WindowRect.bottom-WindowRect.top)/2.0f),
				WindowRect.right-WindowRect.left+diff.x,
				WindowRect.bottom-WindowRect.top+diff.y, SWP_SHOWWINDOW );
			GetClientRect(m_HWND,&WindowRect);
		}
		SetFocus(m_HWND);
		UpdateWindow( m_HWND );
		SetForegroundWindow(m_HWND);
		SetActiveWindow(m_HWND);
		g_Render->Reset();      
	}

	//==============================================================================
	void COperatingSystem::MainLoop()
	{	
		MSG msg;
		ZeroMemory(&msg, sizeof(msg));
		while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				break;
			}
			if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)	break;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				Sleep(1);
			}
			if (g_GameApplication->IsPaused())
			{
				Sleep(100);
				continue;
			}

			if (g_GameApplication->IsFullscreen())
			{
				POINT p;
				GetCursorPos(&p);
				g_Input->SetMousePos(p.x, p.y);			
				g_GameApplication->OnMouseMove();
			}			

			g_GameApplication->Update();
			g_GameApplication->Draw();
		}
	}
}
#endif
