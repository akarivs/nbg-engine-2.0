#ifndef NBG_CORE_OPERAING_SYSTEM_LINUX
#define NBG_CORE_OPERAING_SYSTEM_LINUX


#ifndef NBG_LINUX
	#error This file must not be included in non-android projects! Or you forgot to add NBG_LINUX directive :)
#endif

#include <NBG/NBG.h>
#include <core/Device.h>

#include "../BaseOperatingSystem.h"

namespace NBG
{
	class COperatingSystem : public IOperatingSystem
	{
	public:
		COperatingSystem();
		~COperatingSystem();

		virtual bool CreateAppWindow();
		virtual bool CreateDevice();
		virtual IDevice* GetDevice();
		virtual void MainLoop();
	private:
	};
}

#endif //NBG_CORE_OPERAING_SYSTEM_LINUX
