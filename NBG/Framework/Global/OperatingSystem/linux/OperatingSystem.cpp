#ifdef NBG_LINUX

#include "OperatingSystem.h"
#include <core/Input.h>
#include <core/Initializer.h>
#include <core/Device.h>
#include <utils/StringU.h>

#include <string>
#include <sstream>
#include <fstream>

namespace NBG
{
	COperatingSystem::COperatingSystem()
	{
		m_Device = new CDevice();
	}

	COperatingSystem::~COperatingSystem()
	{

	}

	IDevice* COperatingSystem::GetDevice()
	{
		return m_Device;
	}

	bool COperatingSystem::CreateAppWindow()
	{
		return true;
	}


	bool COperatingSystem::CreateDevice()
	{
		return true;
		//return CAST(CDevice*,m_Device)->Init(m_HWND);
	}

	void COperatingSystem::MainLoop()
	{

	}
}
#endif
