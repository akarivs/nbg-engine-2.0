#ifdef NBG_ANDROID

#include "OperatingSystem.h"
#include <Framework.h>

#include <string>
#include <sstream>
#include <fstream>

#include <jni.h>
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer

#include <GLES/gl.h>
#include <EGL/egl.h>

		

JNIEnv * COperatingSystem::AndroidEnv;
JavaVM * COperatingSystem::AndroidVM;
AAssetManager *COperatingSystem::AssetManager;

enum EEventType
{
	E_MOUSE_MOVE,
	E_MOUSE_UP,
	E_MOUSE_DOWN,
	E_SET_WINDOW
};

struct SEvent
{
	EEventType type;
	float var1;
	float var2;	
};

ANativeWindow* window;

std::queue<SEvent>m_Events;

extern "C"
{
	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Initialize(JNIEnv* env, jobject pointer,jobject assetManager)	
	{		
		env->GetJavaVM(&COperatingSystem::AndroidVM);
		COperatingSystem::AndroidVM->AttachCurrentThread(&COperatingSystem::AndroidEnv, 0);
		COperatingSystem::AssetManager = AAssetManager_fromJava(env, assetManager);
		CONSOLE("JAVAVM inited");
		NBG_Init();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchDown(JNIEnv* env, jobject pointer,float x, float y)	
	{	
		SEvent evt;
		evt.type = E_MOUSE_DOWN;
		evt.var1 = x;
		evt.var2 = y;
		m_Events.push(evt);

		//g_Input->SetMousePos(x,y);		
		//g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_SetSurface(JNIEnv* env, jobject pointer,jobject surface)	
	{		
		if (surface != 0) {
			SEvent evt;
			evt.type = E_SET_WINDOW;
			window = ANativeWindow_fromSurface(env, surface);
			m_Events.push(evt);
			CONSOLE("Got window %p", window);			
		} else {
			CONSOLE("Releasing window");
			ANativeWindow_release(window);
		}
		
		

		//g_Input->SetMousePos(x,y);			
		//g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchUp(JNIEnv* env, jobject pointer,float x, float y)	
	{	
		SEvent evt;
		evt.type = E_MOUSE_UP;
		evt.var1 = x;
		evt.var2 = y;
		m_Events.push(evt);
		//g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);		
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchMove(JNIEnv* env, jobject pointer,float x, float y)	
	{	
		SEvent evt;
		evt.type = E_MOUSE_MOVE;
		evt.var1 = x;
		evt.var2 = y;
		m_Events.push(evt);
		//g_Input->SetMousePos(x,y);			
		//g_GameApplication->OnMouseMove();				
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_ChangeResolution(JNIEnv* env, jobject pointer,int w, int h)	
	{		
		g_System->SetSystemResolution(w,h);
		Vector size = g_System->GetSystemResolution();
		Vector logicalSize = g_GameApplication->GetLogicalSize();
		g_System->UpdateSize(size,logicalSize,true);
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Pause(JNIEnv* env, jobject pointer)	
	{
		g_GameApplication->Pause();
		g_SoundManager->OnLostFocus();				
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Resume(JNIEnv* env, jobject pointer)	
	{		
		g_GameApplication->Resume();
		g_SoundManager->OnGetFocus();						
		g_GameApplication->GetTimeManager()->Reset();		
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Loop(JNIEnv* env, jobject pointer)	
	{

	}
};


namespace NBG
{
	COperatingSystem::COperatingSystem()
	{

	}

	COperatingSystem::~COperatingSystem()
	{

	}

	bool COperatingSystem::CreateAppWindow()
	{
		return true;
	}

	void COperatingSystem::MainLoop()
	{
		CThread::CreateThread(COperatingSystem::Loop);
	}

	//////////////////////////////////////////////////////////////////////////
	int COperatingSystem::Loop(void * data)
	{				
		bool isInited = false;
		while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				break;
			}
			while (m_Events.empty() == false)
			{
				auto event = m_Events.front();
				m_Events.pop();
				if (event.type == E_MOUSE_DOWN)
				{
					g_Input->SetMousePos(event.var1,event.var2);		
					g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
				}
				else if (event.type == E_MOUSE_MOVE)
				{
					g_Input->SetMousePos(event.var1,event.var2);			
					g_GameApplication->OnMouseMove();		
				}
				else if (event.type == E_MOUSE_UP)
				{
					g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);					
				}
				else if (event.type == E_SET_WINDOW)
				{
					g_Render->SetWindow(window);
					g_Render->Init();		
					if (isInited == false)
					{
						g_GameApplication->AfterRun();
						isInited = true;
					}					
					g_ResManager->ReloadTextures();
				}
			}
			if (isInited && g_GameApplication->IsPaused() == false)
			{
				g_GameApplication->Update();
				g_GameApplication->Draw();
			}			
		}
	}
}
#endif
