#ifndef NBG_CORE_OPERAING_SYSTEM_ANDROID
#define NBG_CORE_OPERAING_SYSTEM_ANDROID

#ifdef NBG_ANDROID

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include <jni.h>                                    
extern "C" {
JNIEXPORT jint JNICALL Java_com_nbg_common_GameModule_main(JNIEnv *, jobject);
}

#include "../BaseOperatingSystem.h"
namespace NBG
{
	class COperatingSystem : public IOperatingSystem
	{
	public:
		COperatingSystem();
		~COperatingSystem();

		virtual bool CreateAppWindow();
		virtual void MainLoop();

		static JNIEnv * AndroidEnv;
		static JavaVM * AndroidVM;
		static AAssetManager *AssetManager;
		static int Loop(void * data);
	private:		
		
		
	};
}
#endif
#endif //NBG_CORE_OPERAING_SYSTEM_ANDROID
