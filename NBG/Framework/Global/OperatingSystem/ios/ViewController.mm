//
//  ViewController.m
//  test
//
//  Created by SVETLANA BONDARENKO on 10/1/13.
//  Copyright (c) 2013 New Bridge Games. All rights reserved.
//

#include <Framework.h>
#import "ViewController.h"


@implementation ViewController

- (void)dealloc
{
   [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    CONSOLE("Received memory warning, trying to release unused resources.");
    g_ResManager->ReleaseUnusedResources();
}


@end
