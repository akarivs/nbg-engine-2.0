#ifndef NBG_CORE_OPERAING_SYSTEM_IOS
#define NBG_CORE_OPERAING_SYSTEM_IOS


#ifdef NBG_IOS


#include <framework/Global/Render.h>

#include "../BaseOperatingSystem.h"

namespace NBG
{
	class COperatingSystem : public IOperatingSystem
	{
	public:
		COperatingSystem();
		~COperatingSystem(){};

		virtual bool CreateAppWindow();
		virtual void MainLoop();
	};
}
#endif
#endif
