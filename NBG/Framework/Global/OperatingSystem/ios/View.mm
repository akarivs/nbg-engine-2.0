#import "View.h"

#include <Framework.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#include <OpenGLES/ES2/gl.h>


// A class extension to declare private methods
@interface View ()
{
    CADisplayLink *displayLink;
}

@property (nonatomic, assign) NSTimer *animationTimer;


@end


@implementation View
@synthesize animationTimer;


// You must implement this method
+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)initView {
    NSLog(@"initView");
   
}

- (void)startLoop {
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(onDisplayLink:)];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [displayLink setFrameInterval:2];
    else
        [displayLink setFrameInterval:1];
}

- (void)stopLoop {
    if( displayLink == nil ) return;
    [displayLink invalidate];
    displayLink = nil;    
}


-(id) initWithFrame:(CGRect)frame
{
    if( !(self = [super initWithFrame:frame]) ) return nil;
    g_Render->Init((__bridge void*)self.layer);
    g_GameApplication->AfterRun();
   
    
    //[self startLoop];
    return self;
}

-(void) onDisplayLink:(id)sender
{
    g_GameApplication->Update();
    g_GameApplication->Draw();
}

- (void)setEnableSetNeedsDisplay:(bool)test
{
    
    
}

- (void)drawView{
   // NBG::g_App->Loop();
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch;
    CGPoint location;
    CGPoint oldLocation;
    
    NSEnumerator * enumerator = [touches objectEnumerator];
    
    while ( (touch = (UITouch*)[enumerator nextObject])) {
        
        location = [touch locationInView:self];
        
        g_Input->SetMousePos(location.x, location.y);
        g_GameApplication->OnMouseMove();
        g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
    }
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch;
    CGPoint location;
    CGPoint oldLocation;
    
    NSEnumerator * enumerator = [touches objectEnumerator];
    
    while ( (touch = (UITouch*)[enumerator nextObject])) {
        
        location = [touch locationInView:self];
        
        g_Input->SetMousePos(location.x, location.y);
        g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);
    }
    
}


-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch;
    CGPoint location;
    CGPoint oldLocation;
    
    NSEnumerator * enumerator = [touches objectEnumerator];
    
    while ( (touch = (UITouch*)[enumerator nextObject])) {
        
        location = [touch locationInView:self];
      
        g_Input->SetMousePos(location.x, location.y);
        g_GameApplication->OnMouseMove();
    }
}


- (void)dealloc {
    [super dealloc];
}

@end
