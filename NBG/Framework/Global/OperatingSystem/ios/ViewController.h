//
//  ViewController.h
//  test
//
//  Created by SVETLANA BONDARENKO on 10/1/13.
//  Copyright (c) 2013 New Bridge Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController

@end
