//
//  ShaderAppDelegate.h
//  Shader
//
//  Created by Noel on 8/21/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "View.h"
#import "ViewController.h"

@interface Delegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    View *view;
    ViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet View *view;
@property (nonatomic, retain) IBOutlet ViewController *viewController;


@end

