//
//  ShaderAppDelegate.m
//  Shader
//
//  Created by Noel on 8/21/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//


#import "Delegate.h"
#import "View.h"
#import "ViewController.h"
#include <Framework.h>

@implementation Delegate

@synthesize window;
@synthesize view;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {
    float   scale = [[UIScreen mainScreen] scale];
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    int width, height;
    if (rect.size.height * scale > rect.size.width * scale)
    {
        width = rect.size.height * scale;
    }
    else
    {
        width = rect.size.width * scale;
    }
    
    if (rect.size.height * scale > rect.size.width * scale)
    {
        height = rect.size.width * scale;
    }
    else
    {
        height = rect.size.height * scale;
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor blackColor];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    } else {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    }
    self.view = [[View alloc] initWithFrame: CGRectMake(0, 0, width, height)];  //  Создаем вид под опенгл
    self.view.backgroundColor = [UIColor blackColor];
    self.viewController.view = self.view;
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
  }



- (void)applicationWillResignActive:(UIApplication *)application {
    g_GameApplication->Pause();
    g_SoundManager->OnLostFocus();
    [self.view stopLoop];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    g_GameApplication->Resume();
    g_SoundManager->OnGetFocus();
    g_GameApplication->GetTimeManager()->Reset();
    [self.view startLoop];
}


- (void)dealloc {
	[window release];
	[view release];
	[super dealloc];
}

@end
