#ifdef NBG_IOS

#include "OperatingSystem.h"
#include "Framework.h"
#include <mach/mach.h>

#include "Delegate.h"

namespace NBG
{
    
    
	COperatingSystem::COperatingSystem()
	{

       
	}
	bool COperatingSystem::CreateAppWindow()
	{
	        return true;
	}
	
	void COperatingSystem::MainLoop()
	{
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        int retVal = UIApplicationMain(0, 0, nil, NSStringFromClass([Delegate class]));
        [pool release];
        
		return;
	}
                
}


#endif
