#ifndef NBG_CORE_OPERAING_SYSTEM_OSX
#define NBG_CORE_OPERAING_SYSTEM_OSX


#ifdef NBG_OSX


#include "../BaseOperatingSystem.h"

#include <SDL2/SDL.h>



namespace NBG
{
   
	class COperatingSystem : public IOperatingSystem
	{
	public:
        /// @name Конструкторы
		COperatingSystem();
		~COperatingSystem();
        
		/// Создание окна.
		virtual bool CreateAppWindow();
		/// Установка заголовка окна.
		virtual void SetTitle(const std::wstring title);
		/// Главный цикл приложения.
		virtual void MainLoop();
		/// Установка полноэкранного режима.
		virtual void SetFullscreenMode(bool isFullscreen);
    private:
        SDL_Window * m_Window;
        SDL_GLContext m_Context;
        SDL_GLContext m_ThreadContext;
	};
}
#endif
#endif
