#ifdef NBG_OSX

#include "OperatingSystem.h"

#include "Framework.h"
#include <mach/mach.h>


#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

#include <SDL2/SDL.h>


namespace NBG
{
    
    
	COperatingSystem::COperatingSystem()
	{
       
	}


	bool COperatingSystem::CreateAppWindow()
	{
               
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
            std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
            return 1;
        }
        
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
        
        
        SDL_GL_SetSwapInterval(1);
        
        m_Window = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);
        if (m_Window == NULL){
            std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return 1;
        }
        
        m_Context = SDL_GL_CreateContext(m_Window);
        SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
        m_ThreadContext = SDL_GL_CreateContext(m_Window);
        
        /*SDL_Renderer *ren = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (ren == NULL){
            SDL_DestroyWindow(m_Window);
            std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return 1;
        }     */
        return true;
	}

	void COperatingSystem::SetTitle(const std::wstring title)
    {
        
    }
    
    void COperatingSystem::SetFullscreenMode(bool isFullscreen)
	{
        if (isFullscreen)
		{
            SDL_SetWindowSize(m_Window, g_System->GetSystemResolution().x, g_System->GetSystemResolution().y);
            SDL_SetWindowFullscreen(m_Window, true);            
		}
		else
		{
            SDL_SetWindowFullscreen(m_Window, false);
            SDL_SetWindowSize(m_Window, g_System->GetSystemResolution().x, g_System->GetSystemResolution().y);
            
        }
        g_Render->Reset();
	}
    
	void COperatingSystem::MainLoop()
	{
        SDL_Event e;
        while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				break;
			}
            while (SDL_PollEvent(&e))
            {
                //If user closes the window
                if (e.type == SDL_QUIT)
                {
                    g_GameApplication->StopApp();
                }
                //If user presses any key
                else if (e.type == SDL_KEYDOWN)
                {
                    
                }
                else if (e.type == SDL_MOUSEBUTTONDOWN)
                {
                    g_GameApplication->OnMouseDown(e.button.type);
                }
                else if (e.type == SDL_MOUSEBUTTONUP)
                {
                    g_GameApplication->OnMouseUp(e.button.type);
                }
                else if (e.type == SDL_MOUSEMOTION)
                {
                    g_Input->SetMousePos(e.motion.x, e.motion.y);
                    g_GameApplication->OnMouseMove();
                }
            }
            
            
			if (g_GameApplication->IsPaused())
			{
				g_System->Sleep(100);
				continue;
			}
			g_GameApplication->Update();
			g_GameApplication->Draw();
            
            SDL_GL_SwapWindow(m_Window);
		}
	}
}


#endif
