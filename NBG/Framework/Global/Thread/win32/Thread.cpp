#ifdef NBG_WIN32
#include "Thread.h"
#include <Framework/Datatypes.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CThread::CThread()
	{

	}
	//////////////////////////////////////////////////////////////////////////
	CThread* CThread::CreateThread( EventFunc ptr_, void* params_ )
	{
		CThread* newThread = new CThread();	

		newThread->m_Function = ptr_;
		newThread->m_Param = params_;
		newThread->m_Handle = ::CreateThread( NULL,				//	Дескриптор защиты
			0,													//	Начальный размер стека
			(LPTHREAD_START_ROUTINE)CThread::Callback,			//	Указатель на функцию
			(void*)newThread,									//	Параметр потока
			0,													//	Опции создания
			&newThread->m_ThreadId );							//	Идентификатор потока

		SetThreadPriority( newThread->m_Handle, THREAD_PRIORITY_NORMAL );
		return newThread;
	}

	//////////////////////////////////////////////////////////////////////////

	DWORD CThread::GetReturnValue()
	{
		DWORD value = 0;
		BOOL result = GetExitCodeThread( m_Handle, &value );
		if( result )
			return value;
		else
			return -1;
	}

	//////////////////////////////////////////////////////////////////////////

	bool CThread::Stop()
	{
		bool result = TerminateThread( m_Handle, NBG::RC_DESTROY);	
		return result;
	}

	//////////////////////////////////////////////////////////////////////////

	DWORD CThread::Callback(void* param)
	{
		CThread* thread = (CThread*)param;
		DWORD result = thread->m_Function( thread->m_Param );
		return result;
	}
}
//////////////////////////////////////////////////////////////////////////
#endif //NBG_WINDOWS
