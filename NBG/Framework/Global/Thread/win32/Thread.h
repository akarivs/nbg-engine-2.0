#ifndef NBG_CORE_THREAD_WIN
#define NBG_CORE_THREAD_WIN


#include <Framework/Datatypes.h>

#ifdef NBG_WIN32
#include <windows.h>
#include <vector>

namespace NBG
{
	/** @brief Класс для работы с Windows потоками.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CThread
	{
	public:    
		/// Дефайн для функции потока.
		typedef int (*EventFunc)( void* );

		/// Создаёт новый поток и можно передать специфичные параметры либо NULL.
		static 	CThread* CreateThread( EventFunc ptr_, void* params_ = NULL);	

		/// Получить значение от потока.
		virtual DWORD	GetReturnValue();	

		/// Остановление потока.
		virtual bool	Stop();
	private:
		/// @name Конструкторы
		CThread();
		~CThread(){};

		/// Функция потока.
		EventFunc m_Function;

		/// Параметр потока.
		void * m_Param;

		/// Идентификатор потока.
		HANDLE m_Handle;

		/// ID потока.
		DWORD m_ThreadId;

		/// Callback, который вызывает функцию потока.
		static DWORD Callback( void* param );	
	};
}
#endif
#endif //NBG_CORE_THREAD_WIN
