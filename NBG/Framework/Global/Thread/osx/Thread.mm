#ifdef NBG_OSX
#include "Thread.h"

namespace NBG
{
    
	struct ThreadInfo
	{
		CThread::EventFunc f;
		void * params;
	};
    
	void * ThreadFunc(void * data)
	{
		ThreadInfo * info = CAST(ThreadInfo*,data);
		info->f(info->params);
		return NULL;
	}
    
    //==================================================================================
    CThread::CThread()
    {
        
    }
    //==================================================================================
    CThread* CThread::CreateThread( EventFunc ptr_, void* params_ )
    {
        CThread* newThread = new CThread();
        
        ThreadInfo * info = new ThreadInfo();
        info->f = ptr_;
        info->params = params_;
        
        pthread_t thread;
        int result=pthread_create(&thread, NULL, ThreadFunc, info);
        
        
        
        newThread->m_Function = ptr_;
        newThread->m_Param = params_;
        newThread->m_Thread = thread;
        
        return newThread;
    }
    
    //==================================================================================
    
    int CThread::GetReturnValue()
    {
        return -1;
    }
    
    //==================================================================================
    
    bool CThread::Stop()
    {
        pthread_detach(m_Thread);
        return true;
    }
    
    //==================================================================================
    
    int CThread::Callback(void* param)
    {
        return 0;
    }
}

#endif
