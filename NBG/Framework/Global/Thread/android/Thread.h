#ifndef NBG_CORE_THREAD_ANDROID
#define NBG_CORE_THREAD_ANDROID

#include <Framework/Datatypes.h>

#ifdef NBG_ANDROID
#include <vector>

namespace NBG
{

class CThread
{
public:    
	typedef int (*EventFunc)( void* );
	static 	CThread* CreateThread( EventFunc ptr_, void* params_ = NULL);	///Создаёт новый поток и можно передать специфичные параметры либо NULL
	virtual int	GetReturnValue();	
	virtual bool	Stop();
private:
	CThread();
    	~CThread(){};

	EventFunc m_Function;
	void * m_Param;
	pthread_t m_Thread;
	static int Callback( void* param );	
};
}
#endif
#endif //NBG_CORE_THREAD_WIN
