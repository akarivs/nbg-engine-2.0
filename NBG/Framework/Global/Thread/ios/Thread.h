#ifndef JC_THREAD_H_INCLUDED
#define JC_THREAD_H_INCLUDED

#include <Framework/Datatypes.h>

#ifdef NBG_IOS

#include <vector>

using namespace std;

namespace NBG
{

class CThread
{

public:
    typedef int (*EventFunc)( void* );
	typedef	vector< CThread* > Vector;
	typedef	vector< CThread* >::iterator	Iterator;

	static CThread* CreateThread( EventFunc ptr_, void* params_ );	/// Создает новый поток, принимает указатель на функцию, и параметры для нее

public:
	int						GetReturnValue();
	/// @name Принудительное завершение потока
	bool						Stop();

private:

	CThread();
    ~CThread(){};

	int						_idThread;											///	Идентификатор потока получаемы от ОС
	
	EventFunc					_function;											///	Указатель на функцию которую надо вызвать в потоке
	void*						_param;												/// Параметры для вызываемой функции



	static Vector				_threadList;										///	Список всех созданных потоков
	static int				threadFunction( void* param );
	static void					deleteThread( CThread* thread );
};

}
#endif
#endif
