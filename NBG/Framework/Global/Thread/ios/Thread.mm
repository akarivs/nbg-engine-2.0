#ifdef NBG_IOS
#include "Thread.h"

#include <Framework/Datatypes.h>
#import <Foundation/Foundation.h> 


//The C++ base
class ThreadBase
{
public:
    void Run()
    {
        m_Func(m_Data);
    };
    NBG::CThread::EventFunc m_Func;
    void * m_Data;
};

typedef ThreadBase * (*ThreadCreator)();

//The ObjC wrapper

@interface ThreadStarter:NSObject
{
    ThreadBase * base;
}

-(void)Run;
-(id)init:(ThreadBase*)tc;
@end

@implementation ThreadStarter
-(id)init:(ThreadBase*)tc
{
    base = tc;
    return [super init];
}

-(void)Run
{
    base->Run();
}
@end


ThreadBase *MyThreadCreator()
{
    return new ThreadBase();
}


namespace NBG
{
    
    
    
    
CThread::Vector	CThread::_threadList;
//==================================================================================

CThread*	CThread::CreateThread( EventFunc ptr_, void* params_ )
{
	CThread* newThread = new CThread();
	_threadList.push_back( newThread );
    
    newThread->_function = ptr_;
	newThread->_param = params_;
    
    ThreadBase *Thread = new ThreadBase();
    Thread->m_Func = ptr_;
    Thread->m_Data = params_;
    ThreadStarter *tc = [[ThreadStarter alloc]init:Thread];    
    NSThread* myThread = [[NSThread alloc] initWithTarget:tc selector:@selector(Run) object:nil]; [myThread start];
    
	return newThread;
}

//==================================================================================

int CThread::GetReturnValue()
{
	return -1;
}

//==================================================================================

bool CThread::Stop()
{
	return -1;
}

//==================================================================================

int CThread::threadFunction(void* param)
{
	CThread* thread = (CThread*)param;
	int result = thread->_function( thread->_param );
	CThread::deleteThread( thread );
	return result;
}

//==================================================================================

void CThread::deleteThread( CThread* thread )
{
	Iterator iter = _threadList.begin();

	while( iter != _threadList.end() )
	{
		if( (*iter)->_idThread == thread->_idThread ){
			//delete *iter;
			_threadList.erase( iter );
			return;
		}
	}
}

//==================================================================================

CThread::CThread()
{

}
}
//==================================================================================
#endif //#ifdef NBG_IOS
