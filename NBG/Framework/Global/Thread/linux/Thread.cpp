#ifdef NBG_LINUX
#include "Thread.h"
#include <core/Datatypes.h>

namespace NBG
{
//==================================================================================
CThread::CThread()
{

}
//==================================================================================
CThread* CThread::CreateThread( EventFunc ptr_, void* params_ )
{
	CThread* newThread = new CThread();
	return newThread;
}

//==================================================================================

DWORD CThread::GetReturnValue()
{
	return -1;
}

//==================================================================================

bool CThread::Stop()
{
	return true;
}

//==================================================================================

DWORD CThread::Callback(void* param)
{
	return 0;
}
}

#endif
