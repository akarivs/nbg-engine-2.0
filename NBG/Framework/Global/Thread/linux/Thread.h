#ifndef NBG_CORE_THREAD_LINUX
#define NBG_CORE_THREAD_LINUX

#include <core/Datatypes.h>

#ifndef NBG_LINUX
	#error This file must not be included in non-android projects! Or you forgot to add NBG_LINUX directive :)
#endif
#include <vector>

namespace NBG
{

class CThread
{
public:
	typedef DWORD (*EventFunc)( void* );
	static 	CThread* CreateThread( EventFunc ptr_, void* params_ = NULL);	///Создаёт новый поток и можно передать специфичные параметры либо NULL
	virtual DWORD	GetReturnValue();
	virtual bool	Stop();
private:
	CThread();
    	~CThread(){};

	EventFunc m_Function;
	void * m_Param;
	static DWORD Callback( void* param );
};
}

#endif //NBG_CORE_THREAD_LINUX
