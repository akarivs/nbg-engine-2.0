#include "Input.h"

#include <Framework.h>

namespace NBG
{	
	//==============================================================================
	CInput::CInput()
	{
		m_MousePos = Vector(0,0);
		memset(&m_Keys[0],0,256);
	}

	CInput::~CInput()
	{
		
	}

	void CInput::SetMousePos(int x, int y)
	{		
		m_MousePos.x = x;
		m_MousePos.y = y;        
        
		g_System->ConvertMousePos(m_MousePos);
	}

	Vector & CInput::GetMousePos()
	{
		return m_MousePos;
	}

	void CInput::SetKeyDown(const int id, const bool isDown)
	{
		if (id < 0 || id >= 256)return;
		if (isDown)
			m_Keys[id] = 1;
		else
			m_Keys[id] = 0;
	}

	bool CInput::IsKeyDown(const int id)
	{
		if (id < 0 || id >= 256)return false;
		return m_Keys[id] == 1;
	}

	bool CInput::IsSystemKey(const int id)
	{
		return false;
	}
}
