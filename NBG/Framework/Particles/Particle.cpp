#include "Particle.h"
#include <Framework.h>

using namespace NBG;
namespace NBG
{
    //==============================================================================
    CParticle::CParticle()
    {        
        m_IsCreated = false;	
		m_IsKilled = false;
		m_TextureResource = NULL;

		m_Mesh.Init(4);
		m_UV = NBG::FRect(0,0,1,1);
		m_Size = Vector(8,8);
		m_Mesh.SetRectData(&m_UV,&m_Transform,m_Size,Color(),NULL,false);
		m_Vertexes = m_Mesh.GetVertexes();	
    }

    //==============================================================================
    CParticle::~CParticle()
    {

    }

	//================================================================================
	bool CParticle::Create(const float lifeTime, const FloatColor &startColor, const FloatColor &midColor, const FloatColor &endColor, const float angle, const float speed, const float velocity, const Vector &size, const float angular)
	{		
		if (m_IsCreated)return false;
		m_CurrentTime	= 0.0f;
		m_LifeTime		= lifeTime;
		m_StartColor	= startColor;
		m_MidColor		= midColor;
		m_EndColor		= endColor;
		m_Angle			= angle;
		m_Dir			= Vector(cos(angle),-sin(angle));
		m_Speed			= speed;
		m_Velocity		= velocity;
		m_Angular		= angular;
		m_Size			= size;
		m_IsCreated = true;		
		Update();
		return true;
	}

	Vertex __g_PartVertexes[4];
	Vector __g_PartResVec;

    //==============================================================================
    void CParticle::Update()
    {		
		if (!m_IsCreated)return;	

		m_CurrentTime += g_FrameTime;
		float percent = m_CurrentTime/m_LifeTime;		
		if (percent >= 1.0f)
		{
			percent = 1.0f;
			m_IsCreated = false;		
			m_IsKilled = true;					
		}
		m_Speed += m_Velocity*g_FrameTime;

		if (m_Angular != 0.0f)
		{
			m_Angle += m_Angular*g_FrameTime;
			m_Dir	= Vector(cos(m_Angle),-sin(m_Angle));			
			m_Transform.rz = m_Angle;
		}

		float sp = m_Speed*g_FrameTime;
		m_Transform.x += m_Dir.x*sp;  
		m_Transform.y += m_Dir.y*sp;  

		float size;
		FloatColor floatClr;
		Color clr;
		/* Обновление цвета*/
        if( percent > 0.5f ){
			floatClr.a = (float)m_MidColor.a + ((float)(m_EndColor.a - m_MidColor.a) * ((percent-0.5f)/0.5f)); 			
			floatClr.r = (float)m_MidColor.r + ((float)(m_EndColor.r - m_MidColor.r) * ((percent-0.5f)/0.5f)); 			
			floatClr.g = (float)m_MidColor.g + ((float)(m_EndColor.g - m_MidColor.g) * ((percent-0.5f)/0.5f)); 			
			floatClr.b = (float)m_MidColor.b + ((float)(m_EndColor.b - m_MidColor.b) * ((percent-0.5f)/0.5f)); 			

			size = m_Size.y + ((m_Size.z - m_Size.y) * ((percent-0.5f)/0.5f)); 			
        }else{
			floatClr.a = (float)m_StartColor.a + ((float)(m_MidColor.a - m_StartColor.a) *  (percent/0.5f));      
			floatClr.r = (float)m_StartColor.r + ((float)(m_MidColor.r - m_StartColor.r) *  (percent/0.5f));      
			floatClr.g = (float)m_StartColor.g + ((float)(m_MidColor.g - m_StartColor.g) *  (percent/0.5f));      
			floatClr.b = (float)m_StartColor.b + ((float)(m_MidColor.b - m_StartColor.b) *  (percent/0.5f));      		
			
			size = m_Size.x + ((m_Size.y - m_Size.x) * (1.0f - (percent*2.0f))); 			
        }
		
		clr = floatClr;
		clr.Normalize();
		//int color = clr.GetPackedColor();		
		float middleY = g_Render->GetMatrixOffset().y;		


		m_Transform.hx = size/2.0f;
		m_Transform.hy = m_Transform.hx;


		m_Vertexes[0].x = 0;
		m_Vertexes[0].y = 0;
		m_Vertexes[0].color = clr;

		m_Vertexes[1].x = size;
		m_Vertexes[1].y = m_Vertexes[0].y;
		m_Vertexes[1].color = clr;

		m_Vertexes[2].x = m_Vertexes[0].x;
		m_Vertexes[2].y = size;
		m_Vertexes[2].color = clr;

		m_Vertexes[3].x = m_Vertexes[1].x;
		m_Vertexes[3].y = m_Vertexes[2].y;

		__g_PartVertexes[0] = m_Vertexes[0];
		__g_PartVertexes[1] = m_Vertexes[1];
		__g_PartVertexes[2] = m_Vertexes[2];
		__g_PartVertexes[3] = m_Vertexes[3];

		Matrix matrix;
		Transform global = m_Transform.GetAbsoluteTransform();
		matrix.CreateFromTransform(global);

		bool flipU = false;
		bool flipV = false;
		if (global.scalex < 0.0f)flipU = true;
		if (global.scaley < 0.0f)flipV = true;

		for (int i=0; i<4; i++)
		{
			int index = i;
			if (flipU && !flipV)
			{
				if (i==0)index=1;
				else if(i==1)index=0;
				else if (i==2)index=3;
				else if(i==3)index=2;
			}
			else if (flipV && !flipU)
			{
				if (i==0)index=2;
				else if(i==1)index=3;
				else if (i==2)index=0;
				else if(i==3)index=1;
			}
			else if (flipV && flipU)
			{
				if (i==0)index=3;
				else if(i==1)index=2;
				else if (i==2)index=1;
				else if(i==3)index=0;			
			}

			Vector res = matrix.TransformPoint(Vector(__g_PartVertexes[i].x,__g_PartVertexes[i].y));				
			m_Vertexes[index].x = res.x;
#ifdef NBG_ANDROID
			m_Vertexes[index].y = res.y;
#else
			m_Vertexes[index].y = middleY - res.y;
#endif
			m_Vertexes[index].color = clr;
		}

    }

    //==============================================================================
    void CParticle::Draw()
    {		
		if (!m_IsCreated)return;
		if (m_TextureResource)
		{	
			g_Render->DrawMesh(m_TextureResource->GetTexture(),&m_Mesh);
		}
		else
		{
			g_Render->DrawMesh(NULL,&m_Mesh);		
		}		
    }
}
