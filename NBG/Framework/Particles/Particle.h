#ifndef FRAMEWORK_PARTICLE
#define FRAMEWORK_PARTICLE



#include <Framework/GUI/BaseObject.h>
#include <Framework/GUI/GraphicsObject.h>

#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Vertex.h>
#include <Framework/Datatypes/Transform.h>

#include <Framework/Global/Resources/TextureResource.h>

using namespace NBG;

namespace NBG
{
    /** @brief Класс, реализующий работу cо спрайтовыми частицами.
    *
	* По идее это будет медленный класс, работа с частицами будет переведена на отдельный VBO
    * @author Vadim Simonov <akari.vs@gmail.com>
    * @copyright 2013 New Bridge Games
    *
    */
    class CParticle
    {
    public:
        /// @name Конструктор/деструктор
        CParticle();
        virtual ~CParticle();

		///Возвращает, создана ли частица.
		bool IsCreated()
		{
			return m_IsCreated;
		}
		bool Create(const float lifeTime, const FloatColor &startColor, const FloatColor &midColor, const FloatColor &endColor, const float angle, const float speed, const float velocity, const Vector &size, const float angular);

		Vector GetPosition()
		{
			return Vector(m_Transform.x,m_Transform.y,m_Transform.z);
		}
		void  SetPosition(const Vector &position)
		{
			m_Transform.x = position.x;
			m_Transform.y = position.y;
			m_Transform.z = position.z;
		}

		void SetTexture(CTextureResource * texRes)
		{
			m_TextureResource = texRes;
		};

		Transform * GetTransform()
		{
			return &m_Transform;
		}
		
        
        void Update();        
		void Draw();
    protected:
		float m_CurrentTime; ///текущее время жизни		
		
		float m_LifeTime;	///общее время жизни
		FloatColor m_StartColor;
		FloatColor m_MidColor;
		FloatColor m_EndColor;
		Vector m_Dir;
		Vector m_Size;
		float m_Angle;
		float m_Speed;		
		float m_Velocity;
		float m_Angular;
		NBG::Vertex * m_Vertexes;

		bool m_IsCreated;
		bool m_IsKilled;
		Transform m_Transform;

		CTextureResource * m_TextureResource;
		Mesh m_Mesh;
        NBG::FRect m_UV;
    };
}
#endif
