#ifndef FRAMEWORK_EMMITER
#define FRAMEWORK_EMMITER



#include <Framework/GUI/BaseObject.h>
#include <Framework/GUI/GraphicsObject.h>

#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Vertex.h>
#include <Framework/Datatypes/Transform.h>

#include "Particle.h"

using namespace NBG;

namespace NBG
{
    /** @brief Класс, реализующий работу эммитера
    *	
    * @author Vadim Simonov <akari.vs@gmail.com>
    * @copyright 2013 New Bridge Games
    *
    */
	class CEmmiter: public CBaseObject
    {
    public:
        /// @name Конструктор/деструктор
        CEmmiter(const std::string &id);
        virtual ~CEmmiter();

		enum CreateZoneType
		{
			ZONE_Point,		///создание из точки
			ZONE_Circle,	///создание в круге
			ZONE_Rectangle, ///создание в прямоугольнике
			ZONE_Mask,		///создание по маске точек
			ZONE_AlphaMask,	///создание по альфа-каналу
		};

		void Init(const int maxParticles, const std::string &texture);

		virtual void OnPositionChanged();
		virtual int OnMouseMove(){return EventSkip;};

		void SetSpeed(const float speed, const float maxspeed);
		void SetAngle(const float angle, const float maxangle);
		void SetVelocity(const float velocity, float maxvelocity);
		void SetAngularVelocity(const float velocity, float maxvelocity);
		void SetLifeTime(const float time, const float maxtime);
		void SetSize(const Vector &size, const Vector &maxsize);
		void SetEmmisionRate(const int rate);
		void SetEmmiterColor(Color start, Color mid, Color end);
		void SetEmmiterColor(const FloatColor &start, const FloatColor &mid, const FloatColor &end);
		void SetAdditive(const bool additive);
		void SetParticleOffset(const Vector &offset);
		Vector & GetParticleOffset(){return m_ParticlesOffset;};

		///Установка зоны создания частиц
		void SetCircleZone(const float minRadius,const float maxRadius);
		void SetRectangleZone(const float width,const float height);
		void SetMaskZone(const std::vector<Vector> &mask);
		void SetMaskZone(const std::string &maskPath);
		void SetAlphaMaskZone(char ** alphaMask, const Vector size);

		void Play(const float time = 0.0f);
		void Stop();
		///Выстрелить указанным количеством частиц
		void Shoot(const int count); 
        
        virtual int Update();        
		virtual int Draw();        
    protected:		
		Vector GetAlphaMaskPos();
		CParticle ** m_Particles;
		int m_ParticlesCount;
		bool m_IsStoped;
		
		float m_TimerStop;

		void Emit(const int count = -1);

		///Скорость
		float m_Speed;
		float m_SpeedMax;

		///Ускорение
		float m_Velocity;
		float m_VelocityMax;

		///Угловое ускорение
		float m_AngularVelocity;
		float m_AngularVelocityMax;

		///Время жизни
		float m_LifeTime;
		float m_LifeTimeMax;

		//Цвет
		FloatColor m_StartColor;
		FloatColor m_MidColor;
		FloatColor m_EndColor;

		///Частота эммисии
		int m_EmmisionRate;

		///Размер
		Vector m_Size; ///используются x,y,z: x - стартовый, y - в средине жизни, z - в конце
		Vector m_SizeMax; ///используются x,y,z: x - стартовый, y - в средине жизни, z - в конце

		///Угол
		float m_Angle;
		float m_AngleMax;

		///Тип блендинга
		bool m_AdditiveMode;

		///Создание зон
		CreateZoneType m_ZoneType;
		float m_RadiusMin;
		float m_RadiusMax;		
		float m_ZoneWidth;
		float m_ZoneHeight;

		Vector m_ParticlesOffset;
		std::vector<Vector>m_Mask;
		int m_CurrentMaskPoint;

		char ** m_AlphaMask;
		Vector m_AlphaMaskSize;
    };
}
#endif
