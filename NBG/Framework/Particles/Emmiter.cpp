#include "Emmiter.h"


#include <Framework.h>

using namespace NBG;
namespace NBG
{
    //==============================================================================
    CEmmiter::CEmmiter(const std::string &id):CBaseObject(id)
    {        
		m_Particles = NULL;	
		m_ZoneType = ZONE_Point;
		m_IsStoped = true;
		m_AdditiveMode = true;
		m_AlphaMask = NULL;

		m_Speed = m_SpeedMax = 0.0f;
		m_AngularVelocity = m_AngularVelocityMax = 0.0f;
		m_Velocity = m_VelocityMax = 0.0f;
		m_Angle = m_AngleMax = 0.0f;
		m_LifeTime = m_LifeTimeMax = 0.0f;		
		m_TimerStop = 0.0f;
    }

    //==============================================================================
    CEmmiter::~CEmmiter()
    {
		if (m_Particles)
		{			
			for (int i=0; i<m_ParticlesCount; i++)
			{
                delete m_Particles[i];
			}
			delete[] m_Particles;			
			m_Particles = NULL;
		}
		if (m_AlphaMask)
		{
			for (int x=0; x<m_AlphaMaskSize.x; x++)
			{
				delete[] m_AlphaMask[x];
			}
			delete[] m_AlphaMask;
			m_AlphaMask = NULL;
		}
    }

	//==============================================================================
	void CEmmiter::Init(const int maxParticles, const std::string &texture)
	{
		CTextureResource * res = CAST(CTextureResource*,g_ResManager->GetResource(texture));
		m_ParticlesCount = maxParticles;
		m_Particles = new CParticle*[m_ParticlesCount];
		for (int i=0; i<m_ParticlesCount; i++)
		{
			m_Particles[i] = new CParticle();
			CParticle * particle = m_Particles[i];
			particle->SetTexture(res);
			particle->SetPosition(Vector(0.0f,0.0f));	
			particle->GetTransform()->SetParent(GetTransform());
		}		
	}

	//==============================================================================
	void CEmmiter::SetSpeed(const float speed, const float maxspeed)
	{
		m_Speed = speed;
		m_SpeedMax = maxspeed;
	}

	//==============================================================================
	void CEmmiter::SetVelocity(const float velocity, float maxvelocity)
	{
		m_Velocity = velocity;
		m_VelocityMax = maxvelocity;
	}

	//==============================================================================
	void CEmmiter::SetAngularVelocity(const float velocity, float maxvelocity)
	{
		m_AngularVelocity = velocity;
		m_AngularVelocityMax = maxvelocity;
	}

	//==============================================================================
	void CEmmiter::SetAngle(const float angle, float maxangle)
	{
		m_Angle		= MathUtils::ToRadian(angle);
		m_AngleMax	= MathUtils::ToRadian(maxangle);
	}	

	//==============================================================================
	void CEmmiter::SetLifeTime(const float time, const float maxtime)
	{
		m_LifeTime = time;
		m_LifeTimeMax = maxtime;
	}

	//==============================================================================
	void CEmmiter::SetSize(const Vector &size, const Vector &maxsize)
	{
		m_Size = size;
		m_SizeMax = maxsize;
	}

	//==============================================================================
	void CEmmiter::SetCircleZone(const float minRadius,const float maxRadius)
	{
		m_RadiusMin = minRadius;
		m_RadiusMax = maxRadius;		
		m_ZoneType = ZONE_Circle;
	}

	//==============================================================================
	void CEmmiter::SetRectangleZone(const float width,const float height)
	{
		m_ZoneWidth = width/2.0f;
		m_ZoneHeight = height/2.0f;
		m_ZoneType = ZONE_Rectangle;
	}

	//==============================================================================
	void CEmmiter::SetMaskZone(const std::vector<Vector> &mask)
	{		
		m_Mask = mask;
		m_CurrentMaskPoint = 0;
		m_ZoneType = ZONE_Mask;
	}

	//==============================================================================
	void CEmmiter::SetMaskZone(const std::string &maskPath)
	{		
		std::vector<Vector>mask;


		FILE * op = fopen(maskPath.c_str(),"rb");


		fseek(op,0,SEEK_END);
		int size  = ftell(op);
		fseek(op,0,SEEK_SET);	
		
		char * buffer =  new char[size];
		fread(buffer,size,1,op);
		fclose(op);

		int offset = 0;

		bool isOptimizedVersion = false;
		char identifier[10];
		memcpy(&identifier,&buffer[0],10);
		std::string identifierString = identifier;
		if (identifierString == "OPTIMIZED") 
		{
	        offset += 10;
			isOptimizedVersion = true;
		}
	    else
	    {
			offset += sizeof(int);
			offset += sizeof(int);
		}
		while (offset < size)
		{
			Vector  pos;
			if (isOptimizedVersion)
			{
				int point;
				memcpy(&point,&buffer[offset],sizeof(int));offset += sizeof(int);
				pos.x = point / 1024;
				pos.y = point % 1024;
			}
			else
			{
				int x, y;
				memcpy(&x,&buffer[offset],sizeof(int));
				offset += sizeof(int);
				memcpy(&y,&buffer[offset],sizeof(int));
				offset += sizeof(int);
				pos.x = x;
				pos.y = y;
			}
			mask.push_back(pos);
		}
		SetMaskZone(mask);		
		delete[] buffer;
	}

	//==============================================================================
	void CEmmiter::SetAlphaMaskZone(char ** alphaMask, const Vector size)
	{		
		std::vector<Vector>mask;
		m_AlphaMask = alphaMask;
		m_AlphaMaskSize = size;
		m_ZoneType = ZONE_AlphaMask;
	}

	//==============================================================================
	void CEmmiter::SetEmmisionRate(const int rate)
	{
		m_EmmisionRate = rate;
	}

	//==============================================================================
	void CEmmiter::SetEmmiterColor(Color start, Color mid, Color end)
	{
		m_StartColor.SetColor(start.GetPackedColor());
		m_MidColor.SetColor(mid.GetPackedColor());
		m_EndColor .SetColor(end.GetPackedColor());		
	}

	//==============================================================================
	void CEmmiter::SetEmmiterColor(const FloatColor &start, const FloatColor &mid, const FloatColor &end)
	{
		m_StartColor = start;
		m_MidColor = mid;
		m_EndColor = end;
	}

	//==============================================================================
	void CEmmiter::SetAdditive(const bool additive)
	{
		m_AdditiveMode = additive;
	}

	//==============================================================================
	void CEmmiter::SetParticleOffset(const Vector &offset)
	{
		m_ParticlesOffset = offset;
	}

	//==============================================================================
	void CEmmiter::OnPositionChanged()
	{
		
	}

	//==============================================================================
	void CEmmiter::Play(const float timer)
	{
		CONSOLE("%f",timer);
		m_TimerStop = timer;
		m_IsStoped = false;
	}

	//==============================================================================
	void CEmmiter::Stop()
	{
		m_IsStoped = true;
	}

	//==============================================================================
	void CEmmiter::Shoot(const int count)
	{
		Emit(count);
	}

	//==============================================================================
	Vector CEmmiter::GetAlphaMaskPos()
	{
		Vector res = m_ParticlesOffset;		
		while (true)
		{
			int xR = g_Random->RandI(0,m_AlphaMaskSize.x-1);
			int yR = g_Random->RandI(0,m_AlphaMaskSize.y-1);
			
			int val = m_AlphaMask[xR][yR];			
			 
			if (val == 1)
			{
				res.x += xR;
				res.y += yR;
				break;
			}
		}
		return res;
	}

	//==============================================================================
	void CEmmiter::Emit(const int count)
	{
		if (m_IsStoped && count == -1)return;		
		float radius;
		Vector size;		
		int particlesPerSecond = m_EmmisionRate;
		if (count != -1)
		{
			particlesPerSecond = count;
		}
		
		for (int i=0; i<m_ParticlesCount; i++)
		{
			CParticle * part = m_Particles[i];			
			if (!part->IsCreated())
			{
				float angle = g_Random->RandF(m_Angle,m_AngleMax);				
				switch (m_ZoneType)
				{
				case ZONE_Point:
					part->SetPosition(m_ParticlesOffset);
					break;
				case ZONE_Circle:					
					radius = g_Random->RandF(m_RadiusMin,m_RadiusMax);
					part->SetPosition(Vector(cos(angle)*radius +m_ParticlesOffset.x, -sin(angle)*radius+m_ParticlesOffset.y));
					break;
				case ZONE_Rectangle:
					part->SetPosition(Vector(g_Random->RandF(-m_ZoneWidth,m_ZoneWidth)+m_ParticlesOffset.x, g_Random->RandF(-m_ZoneHeight,m_ZoneHeight))+m_ParticlesOffset.y);
					break;
				case ZONE_Mask:
					if (m_CurrentMaskPoint >= m_Mask.size())
					{
						Stop();
						return;
					}
					Vector *point;
					point = &m_Mask[m_CurrentMaskPoint];
					part->SetPosition(*point+m_ParticlesOffset);
					m_CurrentMaskPoint++;
					break;
				case ZONE_AlphaMask:					
					part->SetPosition(GetAlphaMaskPos()+m_ParticlesOffset);					
					break;
				}
				size.x = g_Random->RandF(m_Size.x,m_SizeMax.x);
				size.y = g_Random->RandF(m_Size.y,m_SizeMax.y);
				size.z = g_Random->RandF(m_Size.z,m_SizeMax.z);

				part->Create(g_Random->RandF(m_LifeTime,m_LifeTimeMax),m_StartColor,m_MidColor,m_EndColor,angle,g_Random->RandF(m_Speed,m_SpeedMax), g_Random->RandF(m_Velocity,m_VelocityMax),size,g_Random->RandF(m_AngularVelocity,m_AngularVelocityMax));				
				particlesPerSecond--;
			}						
			if (particlesPerSecond<=0)break;
		}	
	}

    //==============================================================================
    int CEmmiter::Update()
    {			
		if (m_TimerStop > 0.0f)
		{
			m_TimerStop -= g_FrameTime;
			if (m_TimerStop <= 0.0f)
			{
				m_TimerStop = 0.0f;
				Stop();
			};

		}
		for (int i=0; i<m_ParticlesCount; i++)
		{
			m_Particles[i]->Update();						
		}		
		Emit();		
        return EventSkip;
    }

    //==============================================================================
    int CEmmiter::Draw()
    {
        bool active = false;
        for (int i=0; i<m_ParticlesCount; i++)
		{
            if (m_Particles[i]->IsCreated())
            {
                active = true;
                break;
            }
		}
        if (active == false)return EventSkip;
        
		if (m_AdditiveMode)
		{
			g_Render->SetBlendMode(BLENDMODE_ADD);
		}
		else
		{
			g_Render->SetBlendMode(BLENDMODE_BLEND);
		}
		for (int i=0; i<m_ParticlesCount; i++)
		{
			m_Particles[i]->Draw();						
		}					
        return EventSkip;
    }
}
