#include <string>
#include <vector>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/FloatColor.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Transform.h>

#include <Framework/Global/Input.h>
#include <Framework/Global/Render.h>
#include <Framework/Global/System.h>
#include <Framework/Global/Random.h>
#include <Framework/Global/TimeManager.h>

#include <Framework/Global/ResourcesManager.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Global/Resources/XMLResource.h>

#include <Framework/Animation/Clip.h>



#include <Framework/Helpers/Actions/ActionsFactory.h>
#include <Framework/Helpers/Actions/AlphaToAction.h>
#include <Framework/Helpers/Actions/HideAction.h>
#include <Framework/Helpers/Actions/ShowAction.h>
#include <Framework/Helpers/Actions/SleepAction.h>
#include <Framework/Helpers/Actions/MoveToAction.h>
#include <Framework/Helpers/Actions/MoveToBezierAction.h>
#include <Framework/Helpers/Actions/MoveByAction.h>
#include <Framework/Helpers/Actions/PlaySoundAction.h>
#include <Framework/Helpers/Actions/PlayVideoAction.h>
#include <Framework/Helpers/Actions/ScaleToAction.h>
#include <Framework/Helpers/Actions/ScaleByAction.h>
#include <Framework/Helpers/Actions/CallbackAction.h>
#include <Framework/Helpers/Actions/CallbackLUAAction.h>

#include <Framework/Helpers/ActionHelper.h>
#include <Framework/Helpers/AtlasHelper.h>
#include <Framework/Helpers/BinaryHelper.h>
#include <Framework/Helpers/EditionHelper.h>
#include <Framework/Helpers/StencilHelper.h>

#include <Framework/Utils/StringUtils.h>
#include <Framework/Utils/MathUtils.h>
#include <Framework/Utils/Config.h>
#include <Framework/Utils/Tweener.h>

#include <Framework/Particles/Emmiter.h>
#include <Framework/Particles/Particle.h>


#include <Framework/GUI/AnimatedObject.h>
#include <Framework/GUI/AssembledObject.h>
#include <Framework/GUI/BaseObject.h>
#include <Framework/GUI/CheckBoxObject.h>
#include <Framework/GUI/ButtonObject.h>
#include <Framework/GUI/GraphicsObject.h>
#include <Framework/GUI/LabelObject.h>
#include <Framework/GUI/ListObject.h>
#include <Framework/GUI/ParticleObject.h>
#include <Framework/GUI/VideoObject.h>
#include <Framework/GUI/WindowObject.h>
#include <Framework/GUI/SliderObject.h>
#include <Framework/GUI/InputTextObject.h>
#include <Framework/GUI/GuiFactory.h>
#include <Framework/GUI/CursorObject.h>
#include <Framework/GameApplication.h>


namespace NBG
{
	extern CGameApplication * g_GameApplication;
}


#define g_Input g_GameApplication->GetInput()
#define g_MousePos g_Input->GetMousePos()
#define g_System g_GameApplication->GetSystem()
#define g_Render g_GameApplication->GetRender()
#define g_Random g_GameApplication->GetRandom()

#define g_Config			g_GameApplication->GetConfig()
#define g_FrameTime			g_GameApplication->GetFrameTime()
#define g_ScenesManager		g_GameApplication->GetScenesManager()
#define g_FontsManager		g_GameApplication->GetFontsManager()
#define g_LocaleManager		g_GameApplication->GetLocalizationManager()
#define g_PlayersManager	g_GameApplication->GetPlayersManager()
#define g_SoundManager		g_GameApplication->GetSoundManager()
#define g_Cursor			g_GameApplication->GetCursor()
#define g_FileSystem		g_GameApplication->GetFileSystem()
#define g_ResManager		g_GameApplication->GetResourcesManager()
#define g_Tweener			g_GameApplication->GetTweener() 
#define g_GuiFactory		g_GameApplication->GetGuiFactory()
#define g_TimeManager		g_GameApplication->GetTimeManager()

#define g_ActionsFactory	g_GameApplication->GetActionFactory()
#define g_ActionHelper		g_GameApplication->GetActionHelper()
#define g_AtlasHelper		g_GameApplication->GetAtlasHelper()
#define g_EditionHelper		g_GameApplication->GetEditionHelper()
#define g_LuaHelper			g_GameApplication->GetLuaHelper()
#define g_BinaryHelper		g_GameApplication->GetBinaryHelper()
#define g_StencilHelper		g_GameApplication->GetStencilHelper()

using namespace NBG;
using namespace NBG;

#ifndef __forceinline
	#define __forceinline inline
#endif