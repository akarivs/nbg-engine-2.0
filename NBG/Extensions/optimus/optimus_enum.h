#ifndef OPTIMUS_ENUM_H
#define OPTIMUS_ENUM_H
namespace NBG
{
namespace optimus
{
namespace ui
{
	enum class ReturnCodes
	{
		Skip,
		SkipDontProcess,
		Block,
		BlockDontDispatch
	};

	enum class SortModes
	{
		Manual,
		ByLayer
	};

}
}
}
#endif