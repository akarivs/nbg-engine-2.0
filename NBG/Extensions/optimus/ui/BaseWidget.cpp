#include "BaseWidget.h"

#include <Framework.h>

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CBaseWidget::CBaseWidget()
			{
				m_ChildsLock = false;
				m_Parent = nullptr;

				m_Name				= "Base Object";
				m_IsCatchTouch		= false;
				m_IsCatchKeyboard	= false;
				m_IsUpdatable		= false;
				m_IsDrawable		= true;
				m_IsVisible			= true;
				m_IsDisabled		= false;
				m_IsDestroyed		= false;
				m_HotSpot			= HS_MID;
				m_SortMode			= SortModes::ByLayer;
				m_Tag = -1;
				m_Layer = 0;
				m_TopLayer = 0;
				m_Anchor = HS_MID;

				m_Angle				= 0.0f;

				m_IsCascadeOpacityChange	= true;
				m_IsCascadeColorChange		= false;

				m_BoundPoints.resize(4);

				m_BlendMode = -1;
			}

			//////////////////////////////////////////////////////////////////////////
			CBaseWidget::~CBaseWidget()
			{
				auto m_ChildsIter = m_Childs.begin();
				while (m_ChildsIter != m_Childs.end())
				{
					auto obj = (*m_ChildsIter);
					if (obj)
					{
						delete obj;
						obj = NULL;
						m_ChildsIter++;
					}
				}

			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::LoadFromXML(const std::string path)
			{
				CXMLResource * xml = CAST(CXMLResource*, g_ResManager->GetResource(g_EditionHelper->ConvertPath(path)));
				pugi::xml_document * doc = xml->GetXML();
				LoadFromXMLNode(doc->first_child());
				g_ResManager->ReleaseResource(xml);

			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::LoadFromXMLNode(pugi::xml_node node, CBaseWidget * parent)
			{
				if (node.empty())return;
				CBaseWidget * obj = this;				
				if (parent)
				{
					while (!node.empty())
					{
						if (std::string(node.name()) == "object")
						{
							/*
							obj = g_GuiFactory->create(node.attribute("type").value());
							obj->LoadFromXMLNode(node);
							parent->AddChild(obj);
							*/
						}
						node = node.next_sibling();
					}
				}
				else ///иначе ставим параметры текущему объекту
				{
					if (!node.attribute("name").empty())
					{
						SetName(node.attribute("name").value());
					}
					else if (!node.attribute("tag").empty())
					{
						SetTag(node.attribute("tag").as_int());
					}

					float x = node.attribute("x").as_float();
					float y = node.attribute("y").as_float();
					float z = node.attribute("z").as_float();
					float width = node.attribute("width").as_float();
					float height = node.attribute("height").as_float();
					int layer = node.attribute("layer").as_int();
					float angle = node.attribute("angle").as_float();

					float sx = 1.0f;
					float sy = 1.0f;
					float sz = 1.0f;

					if (!node.attribute("sx").empty())
						sx = node.attribute("sx").as_float();
					if (!node.attribute("sy").empty())
						sy = node.attribute("sy").as_float();
					if (!node.attribute("sz").empty())
						sz = node.attribute("sz").as_float();

					Color clr;
					if (!node.attribute("color").empty())
					{
						clr.SetColor(node.attribute("color").value());
					}

					if (!node.attribute("hide").empty())
					{
						SetVisible(!node.attribute("hide").as_bool());
					}
					

					SetPosition(x, y, z);
					SetSize(width, height);
					SetScale(sx, sy, sz);
					SetRotation(angle);
					SetColor(clr);
					//SetLayer(layer);

					SetHotSpot((HotSpot)StringUtils::StringToHotSpot(node.attribute("hot_spot").value()));
					SetAnchor((HotSpot)StringUtils::StringToHotSpot(node.attribute("anchor").value()));
					OnLoadFromXMLNode(node);

					if (!node.attribute("disable").empty())
					{
						SetDisabled(node.attribute("disable").as_bool());
					}

					LoadFromXMLNode(node.first_child(), this);
					AfterLoadFromXMLNode();
					if (m_SortMode == SortModes::ByLayer)
					{						
						std::stable_sort(m_Childs.begin(), m_Childs.end(), CBaseWidget::SortByLayer);
					}
				}
				UpdateAnchors();
			}

			//==============================================================================
			void CBaseWidget::BringForward()
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringForwardSelf, NULL);
					return;
				}
				m_Parent->BringForward(this);
			}

			//==============================================================================
			void CBaseWidget::BringBackward()
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringBackwardSelf, NULL);
					return;
				}
				m_Parent->BringBackward(this);
			}

			//==============================================================================
			void CBaseWidget::BringBefore(CBaseWidget * place)
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringBeforeSelf, place);
					return;
				}
				m_Parent->BringBefore(this, place);
			}

			//==============================================================================
			void CBaseWidget::BringAfter(CBaseWidget * place)
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringAfterSelf, place);
					return;
				}
				m_Parent->BringAfter(this, place);
			}

			//==============================================================================
			void CBaseWidget::BringForward(CBaseWidget * child)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringForward, child);
					return;
				}

				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					m_Childs.push_back(child);
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(m_TopLayer + 1);
				}


			}

			//==============================================================================
			void CBaseWidget::BringBackward(CBaseWidget * child)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringBackward, child);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					m_Childs.insert(m_Childs.begin(), child);
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(0);
				}
			}

			//==============================================================================
			void CBaseWidget::BringBefore(CBaseWidget * child, CBaseWidget * place)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringBefore, child, place);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == place)
						{
							m_ChildsIter++;
							m_Childs.insert(m_ChildsIter, child);
							break;
						}
					}
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(place->GetLayer() - 1);
				}


			}

			//==============================================================================
			void CBaseWidget::BringAfter(CBaseWidget * child, CBaseWidget * place)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringAfter, child, place);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == place)
						{
							m_Childs.insert(m_ChildsIter, child);
							break;
						}
					}
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(place->GetLayer() + 1);
				}
			}

			//==============================================================================
			int CBaseWidget::GetTopLayer()
			{
				return m_TopLayer;
			}

			//==============================================================================
			void CBaseWidget::SetLayer(const int layer)
			{
				m_Layer = layer;

				if (m_Parent && m_Parent->GetSortMode() == SortModes::ByLayer)
				{
					m_Parent->OnChildLayerChange(this);
				}
			}

			//==============================================================================
			bool CBaseWidget::SortByLayer(CBaseWidget *a, CBaseWidget *b)
			{
				return (a->GetLayer() < b->GetLayer());
			}

			//==============================================================================
			void CBaseWidget::OnChildLayerChange(CBaseWidget * child)
			{
				if (child->GetLayer() > m_TopLayer)
				{
					m_TopLayer = child->GetLayer();
				}
				std::stable_sort(m_Childs.begin(), m_Childs.end(), CBaseWidget::SortByLayer);
			}




			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetContentSize()
			{
				//TODO
				return Vector();
			}


			///*POSITION*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPosition(const Vector &position)
			{
				m_Transform.x = position.x;
				m_Transform.y = position.y;
				m_Transform.z = position.z;

				m_SavedTransform = m_Transform;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionX(const float x)
			{
				m_Transform.x = x;
				m_SavedTransform = m_Transform;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionY(const float y)
			{
				m_Transform.y = y;
				m_SavedTransform = m_Transform;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionZ(const float z)
			{
				m_Transform.z = z;
				m_SavedTransform = m_Transform;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetPosition()
			{
				return Vector(m_SavedTransform.x, m_SavedTransform.y, m_SavedTransform.z);
			}

			///*SCALE*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScale(const Vector &scale)
			{
				m_Transform.scalex = scale.x;
				m_Transform.scaley = scale.y;
				m_Transform.scalez = scale.z;

				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleX(const float x)
			{
				m_Transform.scalex = x;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleY(const float y)
			{
				m_Transform.scalex = y;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleZ(const float z)
			{
				m_Transform.scalex = z;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetScale()
			{
				return Vector(m_Transform.scalex, m_Transform.scaley, m_Transform.scalez);
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleX()
			{
				return m_Transform.scalex;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleY()
			{
				return m_Transform.scaley;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleZ()
			{
				return m_Transform.scalez;
			}

			///*HOT SPOT*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetHotSpot(const HotSpot type, const Vector offset /* = Vector(0 , 0, 0)*/)
			{
				m_HotSpot = type;
				m_HotSpotOffset = offset;
				UpdateHotSpot();
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetHotSpotOffset(const int &hotSpot)
			{
				Vector ret;
				switch (hotSpot)
				{
				case HS_UL:
					ret.x = 0.0f;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HS_MU:
					ret.x = GetSize().x / 2.0f;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HS_UR:
					ret.x = GetSize().x;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HS_ML:
					ret.x = 0.0f;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HS_MID:
					ret.x = GetSize().x / 2.0f;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HS_MR:
					ret.x = GetSize().x;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HS_DL:
					ret.x = 0.0f;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HS_MD:
					ret.x = GetSize().x / 2.0f;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HS_DR:
					ret.x = GetSize().x;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HS_CUSTOM:
					return m_HotSpotOffset;
					break;
				}
				return ret;
			}



			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::UpdateHotSpot()
			{
				Vector offset = GetHotSpotOffset(m_HotSpot);
				m_Transform.hx = offset.x;
				m_Transform.hy = offset.y;
				m_Transform.hz = offset.z;
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SendOnChangeParentTransform(CBaseWidget * object)
			{
				auto childs = &object->GetChilds();
				auto m_ChildsIter = childs->begin();
				while (m_ChildsIter != childs->end())
				{
					CBaseWidget * obj = (*m_ChildsIter);
					obj->OnParentTransformChanged();
					obj->SendOnChangeParentTransform(obj);
					m_ChildsIter++;
				}
			}

			///*ANCHORS*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetAnchor(const HotSpot anchor)
			{
				m_Anchor = anchor;
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::UpdateAnchors()
			{
				SendOnChangeParentTransform(this);
				if (!m_Parent)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						CBaseWidget * obj = (*m_ChildsIter);
						obj->UpdateAnchors();
					}
					return;
				}
				Vector parentSize = m_Parent->GetSize();
				if (parentSize.x == 0.0f && parentSize.y == 0.0f)
				{
					if (g_GameApplication)
						parentSize = g_GameApplication->GetWindowSize();
				}

				//if (m_Anchor != HS_MID)

				Vector parentOffset = m_Parent->GetHotSpotOffset(m_Parent->GetHotSpot());
				switch (m_Anchor)
				{
				case HS_UL:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HS_MU:
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HS_UR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HS_ML:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					break;
				case HS_MR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					break;
				case HS_DL:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HS_MD:
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HS_DR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HS_MID:
					m_Transform.x = m_SavedTransform.x;
					m_Transform.y = m_SavedTransform.y;
					break;
				}

				switch (m_Parent->GetHotSpot())
				{
				case HS_UL:
					m_Transform.x += parentSize.x / 2.0f;
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HS_MU:
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HS_UR:
					m_Transform.x -= parentSize.x / 2.0f;
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HS_ML:
					m_Transform.x += parentSize.x / 2.0f;
					break;
				case HS_MR:
					m_Transform.x -= parentSize.x / 2.0f;
					break;
				case HS_DL:
					m_Transform.x += parentSize.x / 2.0f;
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				case HS_MD:
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				case HS_DR:
					m_Transform.x -= parentSize.x / 2.0f;
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				}

				OnPositionChanged();
				for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
				{
					CBaseWidget * obj = (*m_ChildsIter);
					obj->UpdateAnchors();
				}
			}

			///*COLOR*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetColor(Color clr, const bool changeBaseColor)
			{
				if (changeBaseColor)
				{
					if (m_Parent)
					{
						auto pColor = m_Parent->GetColor();
						if (m_Parent->IsCascadeColorChange())
						{							
							clr.r *= pColor.r / 255.0f;
							clr.g *= pColor.g / 255.0f;
							clr.b *= pColor.b / 255.0f;
						}
						if (m_Parent->IsCascadeOpacityChange())
						{
							clr.a *= pColor.a / 255.0f;						
						}
					}
					m_BaseColor = clr;
				}
				m_Color = clr;

				OnColorChanged();
				for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
				{
					CBaseWidget * obj = (*m_ChildsIter);
					Color baseColor = obj->GetBaseColor();
					if (m_IsCascadeOpacityChange)
					{
						baseColor.a *= m_Color.a / 255.0f;
					}
					if (m_IsCascadeColorChange)
					{
						baseColor.r *= m_Color.r / 255.0f;
						baseColor.g *= m_Color.g / 255.0f;
						baseColor.b *= m_Color.b / 255.0f;
					}					
					obj->SetColor(baseColor, false);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetOpacity(float alpha)
			{
				m_Color.a = alpha;
				SetColor(m_Color);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CBaseWidget::IsContains(const Vector &pos)
			{				
				Matrix m;
				m.CreateFromTransform(m_Transform.GetAbsoluteTransform());

				Vector pUL(0.0f, 0.0f);
				Vector pUR(GetSize().x, 0.0f);
				Vector pDL(0.0f, GetSize().y);
				Vector pDR(GetSize().x, GetSize().y);

				pUL = m.TransformPoint(pUL);
				pUR = m.TransformPoint(pUR);
				pDL = m.TransformPoint(pDL);
				pDR = m.TransformPoint(pDR);

				m_BoundPoints[0] = pUL;
				m_BoundPoints[1] = pUR;
				m_BoundPoints[2] = pDR;
				m_BoundPoints[3] = pDL;
				return MathUtils::IsPointInsidePoly(pos, m_BoundPoints);
			}



			///*CHILD SYSTEM*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::AddChild(CBaseWidget * child)
			{
				if (!child)
				{
					CONSOLE("ERROR: Child is NULL!");
					return;
				}
				if (child == this)
				{
					CONSOLE("ERROR: You try to add object as his child! %s");
					return;
				}

				///���� ������ ������� �� ������, ����� ��������� ������ � ������
				if (m_ChildsLock)
				{
					m_ChildsQueue.push(child);
					return;
				}

				if (child->GetParent())
				{
					CONSOLE("ERROR: Object - %s was already added to parent %s!'", child->GetName().c_str(), child->GetParent()->GetName().c_str());
					return;
				}
				m_Childs.push_back(child);
				child->SetParent(this);
				OnChildAdded(child);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::RemoveChild(CBaseWidget * child)
			{
				for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
				{
					if ((*m_ChildsIter) == child)
					{
						(*m_ChildsIter)->SetParent(NULL);
						m_Childs.erase(m_ChildsIter);
						return;
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetParent(CBaseWidget * parent)
			{
				m_Parent = parent;
				if (parent)
				{
					m_Transform.SetParent(parent->GetTransform());
				}
				SetColor(GetColor());
				OnAddedToParent();
			}

			//////////////////////////////////////////////////////////////////////////			
			void CBaseWidget::ProceedDelayedSort()
			{
				for (int i = 0; i < m_DelayedSortItems.size(); i++)
				{
					auto &item = m_DelayedSortItems[i];
					switch (item.type)
					{
					case Delayed_BringForward:
						BringForward(item.obj1);
						break;
					case Delayed_BringBackward:
						BringBackward(item.obj1);
						break;
					case Delayed_BringBefore:
						BringBefore(item.obj1, item.obj2);
						break;
					case Delayed_BringAfter:
						BringAfter(item.obj1, item.obj2);
						break;
					case Delayed_BringForwardSelf:
						if (m_Parent)BringForward();
						break;
					case Delayed_BringBackwardSelf:
						if (m_Parent)BringBackward();
						break;
					case Delayed_BringBeforeSelf:
						if (m_Parent)BringBefore(item.obj1);
						break;
					case Delayed_BringAfterSelf:
						if (m_Parent)BringAfter(item.obj1);
						break;
					}
				}
				m_DelayedSortItems.clear();
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::Update(const float dt)
			{
				if (m_ChildsQueue.size() > 0)
				{
					while (m_ChildsQueue.empty() == false)
					{
						AddChild(m_ChildsQueue.front());
						m_ChildsQueue.pop();
					}
				}
				if (!m_IsUpdatable)return ReturnCodes::SkipDontProcess;
				if (m_Childs.size() == 0)return ReturnCodes::Skip;
				if (m_DelayedSortItems.size() > 0)ProceedDelayedSort();


				auto code = ReturnCodes::Skip;
				bool isNeedToDestroy = false;

				LOCK_CHILDS_VECTOR
					for (auto m_ChildsRIter = m_Childs.rbegin(); m_ChildsRIter != m_Childs.rend(); ++m_ChildsRIter)
					{
						CBaseWidget * obj = (*m_ChildsRIter);
						if (obj->IsDestroyed())
						{
							isNeedToDestroy = true;
							continue;
						}
						if (obj->IsUpdatable() == false)continue;
						if (obj->IsDisabled() == true)continue;						
						code = obj->Update(dt);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR

					if (isNeedToDestroy)
					{
						auto m_ChildsIter = m_Childs.begin();
						while (m_ChildsIter != m_Childs.end())
						{
							CBaseWidget * obj = (*m_ChildsIter);
							if (obj->IsDestroyed())
							{
								delete obj;
								m_ChildsIter = m_Childs.erase(m_ChildsIter);
								continue;
							}
							m_ChildsIter++;
						}

					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::Draw()
			{
				if (!m_IsDrawable)return ReturnCodes::SkipDontProcess;
				if (!m_IsVisible)
				{
					return ReturnCodes::SkipDontProcess;
				}								
				LOCK_CHILDS_VECTOR
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						CBaseWidget * obj = (*m_ChildsIter);
						if (obj->IsDrawable() == false)continue;
						if (obj->IsVisible() == false)continue;
						auto code = obj->Draw();
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							m_ChildsLock = false;
							return code;
						}
					}
				UNLOCK_CHILDS_VECTOR
					return ReturnCodes::Skip;
			}

		}
	}
}