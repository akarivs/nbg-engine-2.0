#ifndef OPTIMUS_UI_BASE_WIDGET
#define OPTIMUS_UI_BASE_WIDGET

#include <Framework/Utils/MathUtils.h>

#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Transform.h>

#include <../xml/pugixml.hpp>
#include <queue>

#include <Framework/Datatypes.h>
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
#define LOCK_CHILDS_VECTOR m_ChildsLock = true;
#define UNLOCK_CHILDS_VECTOR m_ChildsLock = false;
			class CBaseWidget
			{
			public:		
				OPTIMUS_CREATE(CBaseWidget);				
				virtual ~CBaseWidget();

				Transform * GetTransform()
				{
					return &m_Transform;
				}

				void LoadFromXML(const std::string path);				
				void LoadFromXMLNode(pugi::xml_node node, CBaseWidget * parent = NULL);

				

				///*POSITION*///
				void SetPosition(const Vector &position);
				void SetPositionX(const float x);
				void SetPositionY(const float y);
				void SetPositionZ(const float z);
				inline void SetPosition(const float x, const float y, const float z = 0.0f)
				{
					SetPosition(Vector(x, y, z));
				};				
				Vector GetPosition();
				float GetPositionX();
				float GetPositionY();
				float GetPositionZ();

				///*SCALE*///
				void SetScale(const Vector &scale);
				void SetScaleX(const float x);
				void SetScaleY(const float y);
				void SetScaleZ(const float z);
				inline void SetScale(const float scale)
				{
					SetScale(Vector(scale, scale, scale));
				}
				inline void SetScale(const float x, const float y, const float z = 0.0f)
				{
					SetScale(Vector(x, y, z));
				};
				Vector GetScale();
				float GetScaleX();
				float GetScaleY();
				float GetScaleZ();



				///*Size*///
				void SetSize(const Vector& size)
				{
					m_Size = size;
					UpdateHotSpot();
					OnSizeChanged();
					UpdateAnchors();

				}
				void SetSize(const float x, const float y, const float z = 0.0f)
				{
					m_Size = Vector(x,y,z);
					SetSize(m_Size);
				}
				void SetSizeX(const float x)
				{
					m_Size.x = x;
					SetSize(m_Size);
				}
				void SetSizeY(const float y)
				{
					m_Size.y = y;
					SetSize(m_Size);
				}
				void SetSizeZ(const float z)
				{
					m_Size.z = z;
					SetSize(m_Size);
				}
				Vector GetSize()
				{
					return m_Size;
				}
				float GetSizeX()
				{
					return m_Size.x;
				}
				float GetSizeY()
				{
					return m_Size.y;
				}
				float GetSizeZ()
				{
					return m_Size.z;
				}

				Vector GetContentSize();

				///*COLOR*///
				///��������� �����
				void SetColor(Color clr, const bool changeBaseColor = true);
				void SetOpacity(float alpha);
				///��������� �����
				inline Color GetColor()
				{
					return m_Color;
				};
				inline float GetOpacity()
				{
					return m_Color.a;
				};
				inline Color GetBaseColor()
				{
					return m_BaseColor;
				};

				///*ROTATION*///
				///��������� �������� � ��������
				inline void SetRotation(const float &angle)
				{
					m_Angle = angle;
					if (m_Angle >= 360.0f)
					{
						m_Angle -= 360.0f;
					}
					else if (m_Angle < 0.0f)
					{
						m_Angle += 360.0f;
					}
					m_Transform.rz = MathUtils::ToRadian(m_Angle);
					OnRotationChanged();
					SendOnChangeParentTransform(this);
				};
				///��������� �������� � ��������
				inline float GetRotation()
				{
					return m_Angle;
				};

				///*HOT SPOT*///
				//Set object hotspot
				void SetHotSpot(const HotSpot type, const Vector offset = Vector(0, 0, 0));
				///Get hotspot offset
				Vector GetHotSpotOffset(const int &hotSpot);
				inline int GetHotSpot()
				{ 
					return m_HotSpot; 
				};
				void UpdateHotSpot();

				///*ANCHOR *///
				void SetAnchor(const HotSpot anchor);
				void UpdateAnchors();

				///*SORT*///
				///������� ������ ����������
				///������ ������ �� ����� ����
				void BringForward();
				///������ ������ �� ����� ���
				void BringBackward();
				///������ ������ ����� �������� place
				void BringBefore(CBaseWidget * place);
				///������ ������ ����� ������� place
				void BringAfter(CBaseWidget * place);

				///������ ������ �� ����� ����
				void BringForward(CBaseWidget * child);
				///������ ������ �� ����� ���
				void BringBackward(CBaseWidget * child);
				///������ ������ ����� �������� place
				void BringBefore(CBaseWidget * child, CBaseWidget * place);
				///������ ������ ����� ������� place
				void BringAfter(CBaseWidget * child, CBaseWidget * place);

				///������� ���������� �� �����
				///�������� ������� ������������ ����
				int GetTopLayer();
				///���������� ���� �������
				void SetLayer(const int layer);
				///�������� ���� �������
				inline int GetLayer()
				{
					return m_Layer;
				};
				///�����������, ��� ���������� �� �����
				static bool SortByLayer(CBaseWidget *a, CBaseWidget *b);
				///������� � ������, ����� � ������ �������� ����.
				void OnChildLayerChange(CBaseWidget * child);


				virtual bool IsContains(const Vector &pos);


				///CHILD SYSTEM///
				void AddChild(CBaseWidget * child);
				void RemoveChild(CBaseWidget * child);
				void SetParent(CBaseWidget * parent);
				CBaseWidget * GetParent()
				{
					return m_Parent;
				}
				std::vector<CBaseWidget *>& GetChilds(){ return m_Childs; };

				virtual void OnPositionChanged(){};
				virtual void OnRotationChanged(){};
				virtual void OnScaleChanged(){};
				virtual void OnSizeChanged(){};
				virtual void OnColorChanged(){};
				virtual void OnHotSpotChanged(){};
				virtual void OnLoadFromXMLNode(pugi::xml_node node){};
				virtual void AfterLoadFromXMLNode(){};
				virtual void OnParentTransformChanged(){};
				virtual void OnSetDisabled(){};
				virtual void OnChildAdded(CBaseWidget * child){};
				virtual void OnAddedToParent(){};


				///���������� ���������� ���������� �������.
				enum EDelayedSortType
				{
					Delayed_BringForward,
					Delayed_BringBackward,
					Delayed_BringBefore,
					Delayed_BringAfter,
					Delayed_BringForwardSelf,
					Delayed_BringBackwardSelf,
					Delayed_BringBeforeSelf,
					Delayed_BringAfterSelf,
				};
				void AddDelayedSort(EDelayedSortType sortType, CBaseWidget * obj1, CBaseWidget* obj2 = NULL)
				{
					SDelayedSortItem item;
					item.type = sortType;
					item.obj1 = obj1;
					item.obj2 = obj2;
					m_DelayedSortItems.push_back(item);
				}
				struct SDelayedSortItem
				{
					EDelayedSortType type;
					CBaseWidget * obj1;
					CBaseWidget * obj2;
				};
				void ProceedDelayedSort();
				std::vector<SDelayedSortItem>m_DelayedSortItems;
				
				
				virtual ReturnCodes Update(const float dt);
				virtual ReturnCodes Draw();
			protected:
				CBaseWidget();
				void SendOnChangeParentTransform(CBaseWidget * obj);				

				///BASE SETTINGS///
				GETTER_SETTER(std::string, m_Name, Name);		
				GETTER_SETTER(BlendMode, m_BlendMode,BlendMode);
				GETTER_SETTER(int, m_Tag, Tag);				
				GETTER_SETTER_BOOL(CatchTouch);
				GETTER_SETTER_BOOL(CatchKeyboard);
				GETTER_SETTER_BOOL(Updatable);
				GETTER_SETTER_BOOL(Drawable);
				GETTER_SETTER_BOOL(Disabled);
				GETTER_SETTER_BOOL(Visible);
				GETTER_SETTER_BOOL(Destroyed);
				GETTER_SETTER_BOOL(CascadeOpacityChange);
				GETTER_SETTER_BOOL(CascadeColorChange);
				GETTER_SETTER(SortModes, m_SortMode, SortMode);
				///Size of an object
				Vector m_Size;
				///Size of objects container
				Vector m_ContentSize;
				///Hot spot of an object
				HotSpot m_HotSpot;
				Vector m_HotSpotOffset;
				Transform m_Transform;
				Transform m_SavedTransform;
				Color m_Color;
				Color m_BaseColor;
				float m_Angle;
				int m_Layer;
				int m_TopLayer;

				HotSpot m_Anchor;

				std::vector<Vector> m_BoundPoints;
				
				

				///CHILD SYSTEM///
				bool m_ChildsLock;
				std::vector<CBaseWidget*> m_Childs;
				std::queue<CBaseWidget*>  m_ChildsQueue;
				CBaseWidget * m_Parent;
			};
		}
	}
}
#endif //OPTIMUS_UI_BASE_WIDGET