#include "ImageWidget.h"
#include "BaseWidget.h"

#include <Framework.h>

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CImageWidget::CImageWidget():CBaseWidget()
			{
				m_IsUpdatable = true;
				m_IsNeedToUpdateMesh = false;

				m_Mesh.Init(4);
				m_MeshType = MeshType::Mesh_Rectangle;

				m_Resource = nullptr;				
				m_IsTileTexture = false;
			}

			//////////////////////////////////////////////////////////////////////////
			CImageWidget::~CImageWidget()
			{
				if (m_Resource)
				{
					g_ResManager->ReleaseResource(m_Resource);
					m_Resource = nullptr;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetTexture(const std::string &texturePath)
			{
				//����������� ���������� ��������.
				if (m_Resource != NULL)
				{
					g_ResManager->ReleaseResource(m_Resource);
					m_Resource = nullptr;
				}

				std::string path = g_EditionHelper->ConvertPath(texturePath);
				path = StringUtils::StringReplace(path, std::string("%LOCALE%"), g_LocaleManager->GetLocale());

				m_Desc = g_AtlasHelper->GetTextureDescription(path);

				///���� ����� �������� � ������
				if (m_Desc)
				{
					path = g_EditionHelper->ConvertPath(m_Desc->atlasID);
					m_Resource = CAST(CTextureResource*, g_ResManager->GetResource(path));
					SetSize(m_Desc->size.x, m_Desc->size.y);
					m_UV = m_Desc->uv;
					m_UseAtlas = true;
				}
				///����� ������� ��������� �������� � �����
				else
				{
					m_Resource = CAST(CTextureResource*, g_ResManager->GetResource(path.c_str()));
					if (m_Resource)
					{
						SetSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
						m_UseAtlas = false;
						m_UV.left = 0.0f;
						m_UV.top = 0.0f;
						m_UV.right = 1.0f;
						m_UV.bottom = 1.0f;
					}
					else
					{
						SetTexture(NBG::CTextureResource::GetDefaultTexture());
					}
				}
				m_IsNeedToUpdateMesh = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetTexture(NBG::CTextureResource * textureRes)
			{
				if (textureRes)
				{
					m_Resource = textureRes;
					textureRes->IncreaseRefsCount();
					SetSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
					m_UseAtlas = false;
					m_IsNeedToUpdateMesh = true;
				}
				else
				{
					m_Resource = NULL;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::UpdateMesh()
			{
				Texture * texture = NULL;
				if (m_Resource)
				{
					texture = m_Resource->GetTexture();
				}
				if (m_MeshType == MeshType::Mesh_Rectangle )
				{
					if ((m_Mesh.GetIndexesCount() == 6 && m_Mesh.GetVertexCount() == 4) == false)return;
					Vector offset;
					if (m_Desc)
					{
						offset = m_Desc->offset;
					}
					m_Mesh.SetRectData(&m_UV, GetTransform(), GetSize(), GetColor(), texture, m_IsTileTexture, offset);
				}
				else if (m_MeshType == MeshType::Mesh_Custom)
				{
					m_Mesh.UpdateVertexes(GetTransform(), texture, m_IsTileTexture);
				}
			}


			
			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CImageWidget::Update(const float dt)
			{
				auto code = CBaseWidget::Update(dt);
				if (m_IsNeedToUpdateMesh)
				{
					UpdateMesh();
					m_IsNeedToUpdateMesh = false;
				}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CImageWidget::Draw()
			{				
				int prevBlendMode = g_Render->GetBlendMode();
				if (m_BlendMode != -1)
				{
					g_Render->SetBlendMode(m_BlendMode);
				}
				
				if (m_Resource)
				{
					g_Render->DrawMesh(m_Resource->GetTexture(), &m_Mesh);
				}
				else
				{
					g_Render->DrawMesh(NULL, &m_Mesh);
				}
				auto code = CBaseWidget::Draw();

				if (m_BlendMode != -1)
				{
					g_Render->SetBlendMode(prevBlendMode);
				}

				return code;
			}

		}
	}
}