#ifndef OPTIMUS_UI_IMAGE_WIDGET
#define OPTIMUS_UI_IMAGE_WIDGET

#include <Framework/Utils/MathUtils.h>

#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Transform.h>

#include <Framework/Datatypes/Transform.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../xml/pugixml.hpp>
#include <queue>


#include "BaseWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{			
			class CImageWidget : public CBaseWidget
			{
			public:		
				OPTIMUS_CREATE(CImageWidget);				
				virtual ~CImageWidget();

				enum class MeshType
				{
					Mesh_Rectangle,
					Mesh_Custom
				};

				void SetTexture(const std::string &texturePath);
				void SetTexture(NBG::CTextureResource * textureRes);

				/// @name ��������������� ������� �������� �������.
				inline virtual void OnPositionChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnScaleChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnRotationChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnColorChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnSizeChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnHotSpotChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnParentTransformChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};				
				virtual ReturnCodes Update(const float dt);
				virtual ReturnCodes Draw();
			protected:				
				CImageWidget();
				GETTER_SETTER_BOOL(NeedToUpdateMesh);
				void UpdateMesh();

				std::string m_TexturePath;
				CTextureResource * m_Resource;
				AtlasTextureDescription * m_Desc;				
				NBG::FRect m_UV;
				bool m_UseAtlas;
				bool m_IsTileTexture;

				Mesh m_Mesh;
				MeshType m_MeshType;				
			};
		}
	}
}
#endif //OPTIMUS_UI_IMAGE_WIDGET