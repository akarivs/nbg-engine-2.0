#ifndef OPTIMUS_HELPERS_LUA
#define OPTIMUS_HELPERS_LUA

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Mesh.h>


#include <lua.hpp>


#include "../ui/BaseWidget.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, реализующий помощь в работе со скриптами.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CLuaHelper
			{
			public:
				/// @name Конструктор деструктор
				static CLuaHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CLuaHelper();						
					}
					return self;
				}
				~CLuaHelper();

				void Init();				
				void InitUI();
				
				void ReportErrors(lua_State *l, const int status);
				lua_State * GetLuaState(){return m_LuaState;}

				void RunScript(ui::CBaseWidget * obj, const std::string &script);

				template <class Type>
				void PushObject(Type * obj, const std::string &name)
				{
					//SLB::push(m_LuaState, obj);
					//lua_setglobal(m_LuaState, name.c_str());
				}

			private:			
				CLuaHelper();
				static CLuaHelper * self;				
				lua_State *m_LuaState;
			};
		}
	}	
}

#endif ///OPTIMUS_HELPERS_LUA
