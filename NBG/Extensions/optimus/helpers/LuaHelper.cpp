#include "LuaHelper.h"

#include <Framework.h>
#include "../optimus.h"
#include <lualib.h>


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{

			CLuaHelper * CLuaHelper::self = nullptr;
			
			//////////////////////////////////////////////////////////////////////////
			CLuaHelper::CLuaHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CLuaHelper::~CLuaHelper()
			{
				lua_close(m_LuaState);
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::Init()
			{
				m_LuaState = luaL_newstate();
				luaL_Reg lualibs[] =
				{
					{ "math", luaopen_math },
					{ "base", luaopen_base },
					{ NULL, NULL }
				};

				const luaL_Reg *lib = lualibs;
				for (; lib->func != NULL; lib++)
				{
					lib->func(m_LuaState);
					lua_settop(m_LuaState, 0);
				}
				luaL_openlibs(m_LuaState);
				InitUI();
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::InitUI()
			{
				
			}
			

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::RunScript(ui::CBaseWidget * obj, const std::string &script)
			{				
				//lua_setglobal(m_LuaState, "this");			
				int status = luaL_dostring(m_LuaState, script.c_str());
				ReportErrors(m_LuaState, status);
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::ReportErrors(lua_State *l, const int status)
			{
				if (status != 0) {
					std::cerr << "-- " << lua_tostring(l, -1) << std::endl;
					lua_pop(l, 1); // remove error message		
				}
			}
		}
	}
}